<!-- JS MAIN-->
<script type="text/javascript" src="assets/js/jquery-2.2.2.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>


<script src="assets/js/nyroModal/js/jquery.nyroModal.custom.min.js"></script>

<!--DATA TABLES JQUERY-->
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
<!--END OF DATA TABLES JQUERY-->

<script type="text/javascript">
    $(document).ready(function() {
        $('#table-peserta').DataTable({
            "bFilter": true,
            "bLengthChange": false,
            "bPaginate": false,
            "showNEntries": false,
            "bInfo": true,
            "bSort": true
        });
        $('a[href*=\\#]:not([href=\\#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
        $('.navbar').scrollToFixed();
        $('.berita-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            dots: true,
            autoplaySpeed: 2000,
            responsive: [{
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }]
        });
        $('.nyroModal').nm();
    });
</script>

<!-- FULLCALENDAR-->
<link href='assets/fullcalendar-3.1.0/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/fullcalendar-3.1.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='assets/fullcalendar-3.1.0/lib/moment.min.js'></script>
<!--<script src='assets/fullcalendar-3.1.0/lib/jquery.min.js'></script>-->
<script src='assets/fullcalendar-3.1.0/fullcalendar.min.js'></script>
<script src="assets/js/jquery-scrolltofixed-min.js"></script>
<script src="assets/js/slick/slick.min.js"></script>