<nav id="navbar" class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#banner" class="navbar-brand"><h3><img src="assets/img/logo.png" alt="" width="180">&nbsp;&nbsp;Wajib Kerja dokter Spesialis</h3></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#beranda">Beranda</a></li>
                <li><a href="#pengumuman">Pengumuman</a></li>
                <li><a href="#alur-pendaftaran">Alur Pendaftaran</a></li>
                <li><a href="#berita">Berita</a></li>
                <li><a href="#peserta">Peserta</a></li>
                <li><a href="#kontak">Kontak</a></li>
                <li><a rev="modal" class="btn menu-btn nyroModal" href="login.php">Login</a></li>
                <li><a rev="modal" class="btn menu-btn nyroModal" href="register.php">Register</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>