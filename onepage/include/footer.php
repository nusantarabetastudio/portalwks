<footer id="page-footer">
    <div class="footer-wrapper">
        <div id="kontak" class="block">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="col-sm-4">
                     
                    </div>
                    <div class="col-sm-2">
                        <h5>LINK TERKAIT</h5>
                        <ul>
                            <li><a href="pusrengun.inf">Aplikasi Renbut SDMK</a></li>
                           <!-- <li><a href="#">E-Planning</a></li> -->
                            <li><a href="http://www.bppsdmk.kemkes.go.id/web/">BPPSDMK</a></li>
                           <!-- <li><a href="#">Aplikasi Pemetaan SDMK</a></li>
                            <li><a href="#">Dupak Online</a></li>  -->
                        </ul>
                    </div>
                    <div class="col-sm-2">
                        <h5>HUBUNGI KAMI</h5>
                        <p>Badan PPSDM Kesehatan RI
                            <br>Alamat: Jl. Hang Jebat III Blok F3, Kebayoran Baru Jakarta Selatan, 12120
                            <br>Kotak Pos No. 6015/JKS/GN
                            <br>Telefon: 021-7245517, 72797302
                            <br>Fax: 021-72797302</p>
                    </div>
                    <div class="col-sm-4">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.19153040676!2d106.79241261424139!3d-6.238468395484193!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f1407503ef21%3A0xe414727750a5ea56!2sBadan+PPSDM+Kesehatan+RI!5e0!3m2!1sen!2sid!4v1485093127368" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                        <ul class="socmed">
                            <li><a class="fb" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="tw" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="gp" href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="background-wrapper">
                    <div class="bg-transfer opacity-50">
                        <img src="assets/img/footer-bg.png" alt="">
                    </div>
                </div>
                <!--end background-wrapper-->
            </div>
        </div>
        <div class="footer-navigation">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                    <!-- <div class="element width-50 text-align-right">
                        <a href="index.html">Home</a>
                        <a href="listing-grid-right-sidebar.html">Listings</a>
                        <a href="submit.html">Submit Item</a>
                        <a href="contact.html">Contact</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</footer>