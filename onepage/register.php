<a href="" class="close-btn nyroModalClose"></a>
<div>
    <section class="page-title">
        <h1>Registrasi Ulang</h1>
    </section <!--end page-title-->
    <section>
        <form class="form inputs-underline" method="post">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="first_name">Nama Lengkap</label>
                        <input type="text" class="form-control" name="fullname" id="first_name" placeholder="Nama Lengkap">
                    </div>
                    <!--end form-group-->
                </div>
                <!--end col-md-6-->
                <div class="col-md-6 col-sm-6">
                    <div class="form-group">
                        <label for="last_name">NIM</label>
                        <input type="text" class="form-control" name="email" id="last_name" placeholder="NIM">
                    </div>
                    <!--end form-group-->
                </div>
                <!--end col-md-6-->
            </div>
            <!--enr row-->
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" name="kode_user" id="email" placeholder="Email">
            </div>
            <!--end form-group-->
            <div class="form-group center">
                <button type="submit" class="btn btn-primary width-100">Daftarkan sekarang</button>
            </div>
            <!--end form-group-->
        </form>

        <hr>

        <p class="center">Dengan mengkkllik tombol "Daftarkan sekarang" anda telah menerima <a href="terms-conditions.html">Terms & Conditions</a></p>
    </section>
</div>