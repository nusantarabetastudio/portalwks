<!DOCTYPE html>
<html lang="en-US">
<?php include('include/head.php');

if(isset($_POST['fullname'])) {
    $date = date("Y-m-d H:i:s");
    $sql = "INSERT INTO `m_user` (`kode_user`, `email`, `password`, `fullname`, `id_role`, `active`, `created_date`) VALUES ('{$_POST['kode_user']}', '{$_POST['email']}', 'j2W9Sckcj2VGkUo9jUgfj2s6k2gejKxx', '{$_POST['fullname']}', 8, 'Y', '{$date}')";

$ress = mysql_query($sql);


    echo '<script>alert("Anda Berhasil Melakukan Proses Daftar Ulang, Harap segera Login untuk melengkapi Data");</script>';


}


?>

    <body data-spy="scroll" data-target="#navbar">
        <div class="page-wrapper">
            <!--page-header-->
            <?php include('section/view_banner.php');?>
                <?php include('include/menu.php') ?>
                    <!--end page header-->
                    <div class="container">
                        <section id="beranda" class="section clearfix">
                            <h3 class="section-title">Beranda</h3>
                            <div class="col-sm-6">
                                <img src="foto/51.jpg" width="100%">
                            </div>
                            <div class="col-sm-6" style="text-align:justify">
                                <h3 class="con-title">Selamat Datang</h3>
                                <p>Program Indonesia Sehat dilaksanakan dengan menegakkan tiga pilar utama, yaitu (1) penerapan paradigma sehat, (2) penguatan pelayanan kesehatan, dan (3) pelaksanaan jaminan kesehatan nasional (JKN). Penerapan paradigma sehat dilakukan dengan strategi pengarusutamaan kesehatan dalam pembangunan, penguatan upaya promotif dan preventif, serta pemberdayaan masyarakat. Penguatan pelayanan kesehatandilakukan dengan strategi peningkatan akses pelayanan kesehatan, optimalisasi sistem rujukan, dan peningkatan mutu menggunakan pendekatan continuum of care dan intervensi berbasis resiko kesehatan. Sedangkan pelaksanaan JKN dilakukan dengan strategi perluasan sasaran dan manfaat (benefit), serta kendali mutu dan biaya. Kesemuanya itu ditujukan kepada tercapainya keluarga-keluarga sehat.</p>
                                <p>Wajib Kerja Dokter Spesialis adalah penempatan dokter spesialis di rumah sakit milik pemerintah pusat dan pemerintah daerah. Peserta wajib kerja dokter spesialis adalah setiap dokter spesialis lulusan pendidikan profesi program dokter spesialis dari perguruan tinggi negeri di dalam negeri dan perguruan tinggi di luar negeri. Untuk tahap awal peserta wajib kerja dokter spesialis diprioritaskan bagi lulusan pendidikan profesi program dokter spesialis obstetri dan ginekologi, spesialis anak, spesialis bedah, spesialis penyakit dalam dan spesialis anestesi dan terapi intensif.</p>
                                <p>Jangka waktu pelaksanaan Wajib Kerja Dokter Spesialis bagi peserta wajib kerja dokter spesialis mandiri paling singkat selama 1 (satu) tahun. Sementara jangka waktu pelaksanaan Wajib Kerja Dokter Spesialis bagi peserta Wajib Kerja Dokter Spesialis penerima beasiswa dan/atau program bantuan biaya pendidikan dilaksanakan sesuai dengan ketentuan peraturan perundang-undangan.</p>
                                <p></p>
                            </div>
                        </section>
                        <section id="pengumuman" class="section clearfix">
                            <h3 class="section-title">Pengumuman</h3>
                            <div class="col-sm-12">
                                <?php include('section/view_list_information.php');?>
                            </div>
                        </section>
                        <section id="alur-pendaftaran" class="section clearfix">
                            <h3 class="section-title">Alur Pendaftaran</h3>
                            <section>
                                <div class="row">
                                    <div class="step col-sm-4">
                                        <div class="circle">
                                            <figure>1</figure>
                                            <i class="icon_id"></i>
                                            <div class="circle-bg opacity-20"></div>
                                        </div>
                                        <div class="box">
                                            <h2>Melihat Informasi Pengumuman</h2>
                                            <p>Melihat pengumuman pada portal WKS dan pastikan bahwa nama anda tercantum dalam peserta WKS .
                                            </p>
                                            <a href="information.php" class="btn btn-default btn-rounded shadow btn-xs">Lihat Pengumuman</a>
                                        </div>
                                    </div>
                                    <!--end step-->
                                    <div class="step col-sm-4">
                                        <div class="circle">
                                            <figure>2</figure>
                                            <i class="icon_adjust-vert "></i>
                                            <div class="circle-bg opacity-30"></div>
                                        </div>
                                        <div class="box">
                                            <h2>Mengisi Form Registrasi Ulang</h2>
                                            <p>Setelah anda memastikan bahwa nama anda tercantum dalam peserta WKS, maka anda harus melakukan Registrasi Ulang Pada Portal WKS untuk mendapatkan akun.

                                            </p>
                                            <a href="register.php" class="btn btn-default btn-rounded shadow btn-xs">Registrasi Ulang</a>
                                        </div>
                                    </div>
                                    <!--end step-->
                                    <div class="step col-sm-4">
                                        <div class="circle">
                                            <figure>3</figure>
                                            <i class="icon_lock"></i>
                                            <div class="circle-bg opacity-60"></div>
                                        </div>
                                        <div class="box">
                                            <h2>Login ke Aplikasi</h2>
                                            <p>Setelah anda mendapatkan akun, maka anda dipersilahkan untuk masuk ke dalam aplikasi dengan username dan password yang telah anda daftarkan</p>
                                            <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Login</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="step col-sm-4">
                                        <div class="circle">
                                            <figure>4</figure>
                                            <i class="icon_pencil"></i>
                                            <div class="circle-bg opacity-60"></div>
                                        </div>
                                        <div class="box">
                                            <h2>Mengisi Formulir Lapor Peserta WKS</h2>
                                            <p>Setelah anda berhasil masuk ke aplikasi, silahkan lengkapi data anda dan mengisi Formulir Lapor Peserta WKS</p>
                                            <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Isi Formulir</a>
                                        </div>
                                    </div>
                                    <div class="step col-sm-4">
                                        <div class="circle">
                                            <figure>5</figure>
                                            <i class="icon_key"></i>
                                            <div class="circle-bg opacity-60"></div>
                                        </div>
                                        <div class="box">
                                            <h2>Mengisi Surat Perjalanan Dinas</h2>
                                            <p>Setelah mengisi Formulir anda harus mengisi surat perjalan dinas</p>
                                            <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Isi SPPD</a>
                                        </div>
                                    </div>
                                    <div class="step col-sm-4">
                                        <div class="circle">
                                            <figure>6</figure>
                                            <i class="icon_box-checked"></i>
                                            <div class="circle-bg opacity-90"></div>
                                        </div>
                                        <div class="box">
                                            <h2>Review data anda kembali</h2>
                                            <p>Setelah anda melakukan proses 1-5 silahkan review data anda terlebih dahulu sebelum anda submit</p>
                                            <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Review</a>
                                        </div>
                                    </div>
                                </div>
                                <!--end row-->
                            </section>
                        </section>
                        <section id="berita" class="section clearfix">
                            <h3 class="section-title">Berita</h3>
                            <div class="row">
                            <div class="col-sm-6">
                                <img src="foto/24.jpg" width="100%">
                                <img src="foto/36.jpg" width="100%">
                            </div>
                            <div class="col-sm-6" style="text-align:justify">
                                <h3 class="con-title">Visitasi</h3>
                                <section class="page-title">

    <!-- <h2>Seputar Program Wajib Kerja Dokter Spesialis</h2> -->
    <p style="text-align:justify;">Dalam rangka persiapan penempatan Wajib Kerja Dokter Spesialis (WKDS) maka dilaksanakan visitasi. Tujuan dari visitasi adalah : 
    </p><ul>
    <li>(1) memberikan advokasi dan sosialisasi kepada Kadinkes Provinsi, Kadinkes Kab/Kota dan Direktur RS tentang Program Wajib Kerja Spesialis, </li>
    <li>(2) melakukan verifikasi usulan kebutuhan dokter spesialis kepada Pemda dan rumah sakit  yang telah mengusulkan kepada Kemenkes, </li>
    <li>(3) melihat kesiapan rumah sakit dari sisi ketenagaan, sarana prasarana serta sumber daya pendukung lainnya, </li>
    <li>(4) mendapatkan data yang akurat terkait data tenaga kesehatan rumah sakit, sarana prasarana dan kondisi rumah sakit dan sosek, </li>
    <li>  (5) mendapatkan informasi terkait permasalah kesehatan utamanya AKI dan AKB diwilayah kerja rumah sakit, dan </li>
    <li>(6) memberikan pendampingan bila dibutuhkan sesuai dengan bidang keahlian. </li>
    </ul>
   <div style="text-align:justify; padding-top:10px;"> Visitasi dilakukan di 124 rumah sakit yang telah mengusulkan dengan rincian 2 rumah sakit perbatasan, 35 rumah sakit rujukan regional, 81 rumah sakit kelas C, dan 6 rumah sakit rujukan provinsi. Pelaksanaan visitasi dilakukan pada tanggal 28 November sd 10 Desember 2016, yang dibagi menjadi 2 tahap yaitu : tahap I : 29 November sd 3 Desember 2016 di Provinsi Aceh, Sumatera Utara, Sumatera Barat, Riau, Jambi, Sumatera Selatan, D.I Yogyakarta, Jawa Tengah, Kalimantan Selatan, Jawa Timur, Kalimantan Timur, Kalimantan Utara, Sulawesi Utara, Sulawesi Tengah, Sulawesi Selatan, Sulawesi Tenggara, Gorontalo, Sulawesi Barat dan Maluku.
   </div>
   <div style="text-align:justify; padding-top:10px;"> 
   Visitasi tahap II dilaksanakan pada tanggal 5 – 10 Desember 2016 di provinsi Kepulauan Bangka Belitung, Bengkulu, Kepulauan Riau, .Lampung, Jawa Barat, Kalimantan Barat, Nusa Tenggara Barat dan Nusa Tenggara Timur, Maluku Utara dan Papua Barat.
Komposisi tim visitasi terdiri dari unsur Komite Penemoatan Dokter Spesialis (KPDS), Organisasi Profesi, Kolegium, Kemenkes, OP Cabang dan Dinas Kesehatan Provinsi. Total jumlah tim yang turun visitasi di 124 rumah sakit adalah sebanyak 53 tim. 
Adapun penilaian rumah sakit dapat ditetapkan sebagai lokus WKDS dengan mempertimbangkan : 

   </div>

<ul>
    <li> (1)	Ketersediaan SDM (kosong/kurang), sarana prasarana (ada, kondisi baik serta layak pakai), validasi rumah sakit dan sosial ekonomi (resistensi dari tenaga kesehatan lainnya, faktor keamanan, dan lain-lain)
 </li>
    <li>  (2)	Dukungan dari Pemerintah Daerah seperti penyediaan sarana tempat tinggal, kendaraan dinas, insentif daerah, kemudahan pengurusan Surat Ijin Praktik (SIP), dan lain-lain.
 </li>
    <li> (3)	Kebutuhan pelayanan seperti jumlah pasien, beban kerja , akses, sistem rujukan, SDM pendukung, dan lain
 </li>
</ul>

<p></p>
</section>
                            </div>
                            </div>
                        </section>
                        <section id="peserta" class="section clearfix">
                            <h3 class="section-title">Daftar Peserta WKS</h3>
                            <table id="table-peserta" class="display" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>NIM</th>
                                        <th>Asal Fakultas Kedokteran</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $query_peserta = mysql_query('select * from m_user where id_role = 8 order by id_user asc LIMIT 0,1000');
                                        
                                        while($b = mysql_fetch_array($query_peserta)) {
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $b['fullname']; ?>
                                            </td>
                                            <td>
                                                <?php echo $b['kode_user'] ?>
                                            </td>
                                            <td>
                                                Fakultas Kedokteran Universitas Indonesia
                                            </td>
                                          
                                        </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                        </section>
                    </div>
                    <!--end container-->
                    <!--end page-content-->
                    <?php include('include/footer.php');
?>
                        <!--end page-footer-->
        </div>
        <!--end page-wrapper-->
        <?php include('include/js.php');
?>
    </body>