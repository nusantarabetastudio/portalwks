<section>
    <h2>Berita Lainnya</h2>
    <div class="item" data-id="1">
        <a href="post_article.php?id=1&modul=article">
            <div class="description">
                <figure>Admin 06/04/2016</figure>
                <h4>Kuliah Gratis, Lulusan Dokter Wajib Mengabdi</h4>
            </div>
            <!--end description-->
            <div class="image bg-transfer">
                <img src="assets/img/items/1.jpg" alt="">
            </div>
            <!--end image-->
        </a>
        <div class="controls-more">
            <ul>
                <li><a href="blog.html#">Add to favorites</a></li>
                <li><a href="blog.html#">Add to watchlist</a></li>
                <li><a href="blog.html#" class="quick-detail">Quick detail</a></li>
            </ul>
        </div>
    </div>

    <div class="item" data-id="1">
        <a href="post_article.php?id=2&modul=article">
            <div class="description">
                <figure>Admin 06/04/2016</figure>
                <h4>Para Pakar Kanker Berkumpul di Singapura Berbagi Ilmu</h4>
            </div>
            <!--end description-->
            <div class="image bg-transfer">
                <img src="assets/img/items/pakar.jpg" alt="">
            </div>
            <!--end image-->
        </a>
        <div class="controls-more">
            <ul>
                <li><a href="blog.html#">Add to favorites</a></li>
                <li><a href="blog.html#">Add to watchlist</a></li>
                <li><a href="blog.html#" class="quick-detail">Quick detail</a></li>
            </ul>
        </div>
    </div>

    <div class="item" data-id="1">
        <a href="post_article.php?id=3&modul=article">
            <div class="description">
                <figure>Admin 06/04/2016</figure>
                <h4>Tangani Korban Asap, Kemenkes Kirim 15 Dokter Spesialis</h4>
            </div>
            <!--end description-->
            <div class="image bg-transfer">
                <img src="assets/img/items/korban_asap.jpg" alt="">
            </div>
            <!--end image-->
        </a>
        <div class="controls-more">
            <ul>
                <li><a href="blog.html#">Add to favorites</a></li>
                <li><a href="blog.html#">Add to watchlist</a></li>
                <li><a href="blog.html#" class="quick-detail">Quick detail</a></li>
            </ul>
        </div>
    </div>

</section>
