<section>
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <form class="form inputs-underline">
                <h3>Pencarian informasi</h3>
                <div class="form-group">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Masukan kata kunci">
                </div>
                <!--end form-group-->
            </form>
        </div>
        <!--end col-md-8-->
        <div class="col-md-4 col-sm-4">
            <div class="featured-contact">
                <i class="icon_comment_alt"></i>
                <h4>Ada pertanyaan?<br>Hubungi operator</h4>
                <h3>228-341-8068</h3>
            </div>
        </div>
    </div>
    <!--end row-->
</section>
<section>
    <div class="answer">
        <div class="box">
            <h3>
            <a href="agenda.php">
            Jadwal Mengisi Form Rekapitulasi Keadaan & Kebutuhan SDM Kesehatan untuk Rumah Sakit
            </a>
            </h3>
            <p>
                Informasi Jadwal Pengisian Form Rekapitulasi Keadaan & Kebutuhan Oleh Rumah Sakit
            </p>
        </div>
    </div>
    <!--end answer-->
    <div class="answer">
        <div class="box">
            <h3>
            <a href="#">Informasi Regionalisasi Fakultas Kedokteran
            </a>
            </h3>
            <p>Informasi Lengkap Regionalisasi Fakultas Kedokteran dan Rumah Sakit Penempatannya
            </p>
        </div>
    </div>
    <!--end answer-->
    <div class="answer">
        <div class="box">
            <h3>
            <a href="information_detil.php">
                Rekapitulasi Kebutuhan Kelulusan Dokter Spesialis</a>
            </h3>
        </div>
    </div>

</section   >