<a href="" class="close-btn nyroModalClose"></a>
<section class="page-title">
    <h1>Login</h1>
</section>
<!--end page-title-->
<section>
    <form class="form inputs-underline" action="../login/process" method="post">
        <div class="form-group">
            <label for="email">Username</label>
            <input type="text" class="form-control" name="username" id="username" placeholder="Masukan username anda">
        </div>
        <!--end form-group-->
        <div class="form-group">
            <label for="password">Password</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="Masukan password anda">
        </div>
        <div class="form-group center">
           )* Untuk Peserta WKDS Harap Login Dengan NIM anda.  <br/>
           )** Jika Anda belum melakukan pergantian password, password anda adalah 123456 </br> setelah itu harap lakukan pergantian password
        </div>
        <div class="form-group center">
            <button type="submit" class="btn btn-primary width-100">Submit</button>
        </div>
        <!--end form-group-->
    </form>

    <hr>

    <a href="reset_password.php">Lupa password?</a>
</section>