$(document).ready(function() {

  /*var id_perencanaan = $('#dynamic-table').attr('rel');
  table = $('#dynamic-table').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5a/ajax_list_skm",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });*/

  /*table2 = $('#data_mahasiswa').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": false, //Feature control DataTables' server-side processing mode.
    "paging":false,
    "info":false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5a/ajax_list_history",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });*/

   /*table = $('#data_prediksi').DataTable(
    {
      data:[],
        "columns": [
                    {"data": "nama_provinsi"},
                    {"data": "dmp_nama"},
                    {"data": "dmp_nim"},
                    {"data": "prod_name"},
                    {"data": "fk_name"},
                    {"data": "dmp_tanggal_lulus"}
        ] ,
        "paging":false, 
        "info":false, 
        "filter":false, 
    }
  );

  table2 = $('#data_usulan').DataTable(
    {
      data:[],
        "columns": [
                    {"data": "nama_provinsi"   },
                    {"data": "nama_rs"   },
                    {"data": "nama_jenis_sdmk"   }
        ] ,
        "paging":false, 
        "info":false, 
        "filter":false, 
    }
  );*/

  $('#submit_form').click( function() {
        var data = table2.$('input, select').serialize();
        alert(
            "The following data would have been submitted to the server: \n\n"+
            data.substr( 0, 120 )+'...'
        );
        return false;
  } );

  var counter = 1;
  $('#addRow').click( function () {

        $.getJSON("master_data/m_prodi/get_data_prodi", '', function(data) {
            $.each(data, function(i, o) {
                $('<option value="'+o.prod_id+'">'+o.prod_name+'</option>').appendTo($('.prod_box'));
            });

        });

        table2.row.add( [
            '<div class="center">'+counter+'<div>',
            '<div class="center"><select name="prodi[]" class="form-control prod_box"><option value="0">-Silahkan Pilih Prodi-</option></select></div>',
            '<div class="center"><input type="text" style="margin-left:-5px;margin-right:-5px;" name=""></div>',
            '<div class="center"><input type="text" style="margin-left:-5px;margin-right:-5px;" name=""></div>',
            '<div class="center"><select name="prodi[]" class="form-control prod_box"><option value="TUBEL">TUBEL</option><option value="MANDIRI">MANDIRI</option></select></div>',
            '<div class="center"><input type="text" style="margin-left:-5px;margin-right:-5px;" name=""></div>',
            '<div class="center"><input type="text" style="margin-left:-5px;margin-right:-5px;" name=""></div>'
        ] ).draw( false );

        $("html, body").animate({ scrollTop: "400px" });
 
        counter++;
    } );

  $('#data_mahasiswa tbody').on( 'click', 'tr', function () {
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table2.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    } );
 
    $('#button_delete_row').click( function () {
        table2.row('.selected').remove().draw( false );
        $("html, body").animate({ scrollTop: "400px" });
    } );

  // Automatically add a first row of data
    //$('#addRow').click();



  // search form
  $('#btn_search').click(function () {
        $.ajax({
        url: 'form_wks/form_5a/searchDataForm',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          show_result(data);
        }
      });
  });


  // view history
  $('#btn_view_history').click(function () {
        $.ajax({
        url: 'form_wks/form_5a/searchHistory',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          view_history(data);
        }
      });
  });

  // btn proses usulan 
  $('#btn_save_usulan').click(function () {
      url = "form_wks/form_5a/proses_usulan_kebutuhan";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_proses_penempatan').click(function () {
      url = "form_wks/form_5a/prosesUpdateUsulanKebutuhan";
      var formData = new FormData($('#form_penempatan_lokus')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

});

function show_result(data){

  // hide table history
  $('#div_history').hide();

  if(data.status != 1){
    $('#search_status').html('<div class="alert alert-danger"><b><i class="fa fa-times"></i> Peringatan ! </b>'+data.message+'</div>');
    $('#data_skm').hide();
    $('#search_status').show();
    $('#div_table').hide();
    $('#page_title').hide();

  }else{
    
    var result = data.data_perencanaan; // data response

    // hide show div
    $('#search_status').hide();
    $('#page_title ').show();
    $('#data_skm').show();
    $('#div_table').show();
    $('#page_title').html('<h1>Rekapitulasi Keadaan dan Usulan Kebutuhan SDM Kesehatan Rumah Sakit<br> ( '+result.kode_rsu+' ) '+result.nama_rs+'<br>Tahun '+result.tahun+'</h1>');
    table.ajax.url('form_wks/form_5a/ajax_list_skm/'+result.id_perencanaan).load();
    $("html, body").animate({ scrollTop: "400px" });
  }

}

function getGrade(params){

    if(params == 0){
      format = '<span style="color:blue"><strong> '+params+' ( Sesuai ) </strong></span>';
    }else if (params > 0) {
      format = '<span style="color:green"><strong> '+params+' ( Lebih ) </strong></span>';
    }else{
      format = '<span style="color:red"><strong> '+params+' ( Kurang ) </strong></span>';
    }

    return format;

}

function view_history(data){

    var params = data;
    $('#data_prediksi tbody').remove();
    $('#data_usulan tbody').remove();
    // page title //
    var html = '';
        html += 'Analisa kebutuhan SDM Kesehatan dan Prediksi Kelulusan FK';
        if(params.prov != 0){ html += '<br>Provinsi '+params.nama_provinsi+''; }
        if(params.kab != 0){ html += ', '+params.nama_kabupaten+''; }
        /*if(params.kode != 0){ html += '<br>'+params.fk_name+''; }*/
        if(params.smt != 0){ html += '<br>Semester '+params.semester_text+''; }
        if(params.m != 0){ html += ', Periode '+params.bulan_text+''; }
        if(params.y != 0){ html += ', Tahun '+params.y+''; }
        
    $('#page_title').show();
    $('#page_title').html('<h1>'+html+'</h1>');

    // hide table usulan 
    $('#div_table').hide();

    // show table history
    $('#div_history').show();

    //table2.ajax.url('form_wks/form_5a/ajax_list_history?y='+params.tahun+'&m='+params.bulan+'&smt='+params.semester+'&prov='+params.id_provinsi+'&kab='+params.id_kabupaten+'&kode='+params.fk_id+'&f=4a').load();
    
    //table.rows.add(params.prediksi).draw();
    //table2.rows.add(params.usulan).draw();

    // DATA USULAN PREDIKSI //
    $('#data_usulan_prediksi tbody').remove()
    no = 1;
    $.each(params.usulan, function(i, o) {
        //$('<tr><td>'+no+'</td><td>'+o.nama_provinsi+'</td><td colspan="2">'+o.nama_rs+'</td></tr>').appendTo($('#data_usulan_prediksi'));
        $.each(o.detail_sdmk, function(k, v) {
            $('<tr><td align="center">'+no+'</td><td>'+o.nama_provinsi+'</td><td>'+o.nama_rs+'</td><td>'+v.nama_jenis_sdmk+'</td><td align="center">'+v.usulan_kebutuhan+'</td></tr>').appendTo($('#data_usulan_prediksi'));
          no++;
        });
    });

    noa = 1;
    $.each(params.prediksi, function(i, o) {
        $('<tr><td align="center">'+noa+'</td><td>'+o.nama_provinsi+'</td><td>'+o.dmp_nama+'</td><td>'+o.prod_name+'</td><td>'+o.fk_name+'</td><td><a href="#" onclick="penempatan('+o.dmp_id+')" class="btn btn-xs btn-success">Penempatan</a></td></tr>').appendTo($('#data_prediksi'));
    noa++;
    });

    // DATA PREDIKSI //
    /*no = 0;
    $.each(params.prediksi, function(i, o) {
      no++;
        $('<tr><td>'+no+'</td><td>'+o.nama_provinsi+'</td><td>'+o.dmp_nama+'</td><td>'+o.dmp_nim+'</td><td>'+o.prod_name+'</td><td>'+o.fk_name+'</td><td>'+o.dmp_tanggal_lulus+'</td></tr>').appendTo($('#data_prediksi'));
    });*/

    // ==== DATA USULAN KEBUTUHAN SDMK ==== //
    /*noa = 0;
    html = '';
    var temp = JSON.parse(JSON.stringify(params.usulan)); 
    $.each(temp, function(i, o) {
      noa++;
        html += '<tr>';
        html += '<td>'+noa+'</td>';
        html += '<td>'+o.nama_provinsi+'</td>';
        html += '<td>'+o.nama_rs+'</td>';
        html += '<td>';
        var temp2 = JSON.parse(JSON.stringify(o)); 
          $.each(temp2.detail_sdmk, function(i, v) {
            html += '<li>'+v.nama_jenis_sdmk+'</li>';
          });
        html += '</td>';
        html += '</tr>';
        $(html).appendTo($('#data_usulan'));
    });*/

    $("html, body").animate({ scrollTop: "400px" });

}

function detail(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a/detail_usulan_kebutuhan/' + id + '?_=' + (new Date()).getTime());

}

function penempatan(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a/penempatan/' + id + '?_=' + (new Date()).getTime());

}

function delete_uk_sdmk(id){

  url = "form_wks/form_5a/prosesDeleteUkSdmk/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
           $(this).closest('tr').remove();
        }
    });

}

function sendToVerifikator(id){

  url = "form_wks/form_5a/prosesSendToDinkesKab/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
          greatComplete(data);
          backlist();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
          backlist();
        }
    });

}

function backlist(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a?_=' + (new Date()).getTime());
 }

// jquery choosen //
jQuery(function($) {
      
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }


});