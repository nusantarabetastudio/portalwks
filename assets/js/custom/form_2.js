$(document).ready(function() {

  table = $('#dynamic-table').DataTable({ 
          
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "searching":false,
      "bLengthChange": false,
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "form_wks/form_2/getDataUsulan",
          "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
  
  // search form
  $('#btn_search').click(function () {
        $.ajax({
        url: 'form_wks/form_2/searchDataUsulanRs',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          show_result(data);
        }
      });
  });

  $('#btn_search_uk').click(function () {
      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var prov = $('#id_provinsi').val();
      var kab = $('#kab-box').val();
      var rs = $('#rsu-box').val();
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_2/getDataUsulan?y='+y+'&m='+m+'&prov='+prov+'&kab='+kab+'&rs='+rs+'').load();
  });

  $('#btn_reset').click(function () {
      $("html, body").animate({ scrollTop: "400px" });
      table.ajax.url('form_wks/form_2/getDataUsulan').load();
  });

  // btn proses usulan 
  $('#btn_save_verifikasi').click(function () {
      url = "form_wks/form_2/prosesVerifikasiDinkesKab";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              backlist();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_proses_upload_and_send_to_verifikator').click(function () {
      url = "form_wks/form_2/prosesUploadAndSendToVerifkator";
      var formData = new FormData($('#form_upload_and_send')[0]);
          formData.append('file', $('input[type=file]')[0].files[0]);

        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              backlist();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });


});

function show_result(data){

    $('#data_skm tbody').remove();
    if(data.status != 1){
      $('#search_status').html('<div class="alert alert-danger"><b><i class="fa fa-times"></i> Peringatan ! </b>'+data.message+'</div>');
      $('#data_skm').hide();
      $('#search_status').show();
      $('#div_table').hide();
      $('#page_title').hide();

    }else{

      // hide show div
      $('#search_status').hide();
      $('#page_title ').show();
      $('#data_skm').show();
      $('#div_table').show();

      var result = data.result;
      $('#page_title').html('<h1>Verifikasi Usulan Kebutuhan SDM Kesehatan Rumah Sakit <br> oleh Dinkes Kab/Kota</h1>');

      var no = 0;
      $.each(result, function(i, v) {
          no++;
          var temp = JSON.parse(JSON.stringify(v)); 
          $('<tr style="background-color:#84c3f4"><td class="center">'+no+'</td><td class="left" colspan="12"><b>'+temp.nama_rs+'</b> <input type="hidden" name="uk_id[]" value="'+temp.uk_id+'"> </td></tr>').appendTo($('#data_skm'));
          var no2 = 0;
          $.each(temp.detail, function(i2, v2){
            no2++;
            var temp2 = JSON.parse(JSON.stringify(v2));
            if(temp2.app_dinkes_kab=='Y'){
              var checked = 'checked';
            }else{
              var checked = '';
            }
            $('<tr><td class="center"></td><td class="left">&nbsp;&nbsp;&nbsp;'+no2+'. '+temp2.nama_jenis_sdmk+'</td><td class="center">'+temp2.jml_pns+'</td><td class="center">'+temp2.jml_pppk+'</td><td class="center">'+temp2.jml_ptt+'</td><td class="center">'+temp2.jml_honorer+'</td><td class="center">'+temp2.jml_blud+'</td><td class="center">'+temp2.jml_tks+'</td><td class="center">'+temp2.total_jml+'</td><td class="center">'+temp2.total_standar+'</td><td class="center">'+getGrade(temp2.total_kesenjangan)+'</td><td class="center">'+temp2.usulan_kebutuhan+'</td><td class="center"><div class="checkbox" style="margin:-3px"><label class="block"><input name="checked['+temp2.duk_id+']" type="checkbox" class="ace input-lg" value="Y" '+checked+' /><span class="lbl bigger-70">&nbsp;</span></label></div></td></tr>').appendTo($('#data_skm'));
          }); 
      });

      $("html, body").animate({ scrollTop: "400px" });
    }

}
  
 function backlist(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_2?_=' + (new Date()).getTime());
 }

function verifikasi(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_2/verifikasi/' + id + '?_=' + (new Date()).getTime());

}

function sendToVerifikator(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_2/sendToVerifikator/' + id + '?_=' + (new Date()).getTime());

}

function getGrade(params){

    if(params == 0){
      format = '<span style="color:blue"><strong> '+params+' ( Sesuai ) </strong></span>';
    }else if (params > 0) {
      format = '<span style="color:green"><strong> '+params+' ( Lebih ) </strong></span>';
    }else{
      format = '<span style="color:red"><strong> '+params+' ( Kurang ) </strong></span>';
    }

    return format;

}

/*function sendToVerifikator(id){

  url = "form_wks/form_2/prosesSendToDinkesProv/"+id;
    if(confirm('Apakah anda yakin akan mengirim ke dinkes provinsi?'))
    {
      $.ajax({
          url : url,
          type: "POST",
          dataType: "JSON",
          processData: false,
          success: function(data, textStatus, jqXHR, responseText)
          { 
            greatComplete(data);
            backlist();
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            backlist();
          }
      });
    }
}*/


// jquery choosen //
jQuery(function($) {
      
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }


});