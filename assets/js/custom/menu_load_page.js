var loading = "<center><img src='"+window.location.origin+"/assets/images/loading.gif' style='width:150px;padding-top:200px;cursor:auto;z-index:15;'/></center>";

function getMenu(link)
{
	$('#nav nav-list li a').click(function(e) {
        //e.preventDefault();
        $('#nav nav-list li a').removeClass('active open');
        $(this).addClass('active open');
    });

    $('#page-area-content').html(loading);
    $('#page-area-content').load(link +'?_=' + (new Date()).getTime());

}

function getMainMenu(menuId)
{
	$('#nav nav-list li a').click(function(e) {
        //e.preventDefault();
        $('#nav nav-list li a').removeClass('active open');
        $(this).addClass('active open');
    });

    $('#page-area-content').html(loading);
    $('#page-area-content').load('pengaturan/menu/load_sub_menu/' + menuId +'?_=' + (new Date()).getTime());

}

function delete_file_lampiran(id){
    url = "form_wks/form_1/processDeleteFile/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
           $(this).addClass('deleted_file');
           greatComplete(data);
        }
    });
}

