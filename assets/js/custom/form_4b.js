$(document).ready(function() {

  table = $('#dynamic-table').DataTable({ 
          
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "paging":false,
      "info":false,
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "form_wks/form_4b/getPeriodeKelulusanMhs",
          "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });

    table2 = $('#data-mhs').DataTable({ 
          
      "processing": false, //Feature control the processing indicator.
      "serverSide": false, //Feature control DataTables' server-side processing mode.
      "info": false, //Feature control DataTables' server-side processing mode.
      "paging": false, //Feature control DataTables' server-side processing mode.
      
      // Load data for the table's content from an Ajax source
      /*"ajax": {
          "url": "form_wks/form_4b/getPeriodeKelulusanMhs",
          "type": "POST"
      },*/

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });
  
  // search form
  $('#btn_search').click(function () {
        $.ajax({
        url: 'form_wks/form_4b/searchDataUsulanRs',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          show_result(data);
        }
      });
  });

   $('#btn_search_uk').click(function () {
      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var prov = $('#id_provinsi').val();
      var fk = $('#fk-box').val();
      var smt = $('input[type=radio][name=semester]').val();
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_4b/getPeriodeKelulusanMhs?y='+y+'&m='+m+'&prov='+prov+'&kode='+fk+'&smt='+smt+'').load();
  });

  $('#btn_reset').click(function () {
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_4b/getPeriodeKelulusanMhs').load();
  });

  // btn proses usulan 
  $('#btn_save_verifikasi').click(function () {
      url = "form_wks/form_4b/prosesVerifikasiKolegium";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              backlist();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

});

  
 function backlist(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_4b?_=' + (new Date()).getTime());
 }

function verifikasi(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_4b/verifikasi/' + id + '?_=' + (new Date()).getTime());

}

// jquery choosen //
jQuery(function($) {
      
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }


});