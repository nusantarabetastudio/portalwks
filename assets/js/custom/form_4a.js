$(document).ready(function() {

  table = $('#dynamic-table').DataTable({ 
          
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      "searching":false,
      "bLengthChange": false,
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "form_wks/form_4a/getPeriodeKelulusanMhs",
          "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });

  var plm_id = $('#data_mahasiswa').attr('rel');
  table2 = $('#data_mahasiswa').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":false,
    "info":false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_4a/ajax_list_data_mhs",
        "type": "POST",
        "data": {ID:plm_id}
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  // search form
  $('#btn_search').click(function () {
        $.ajax({
        url: 'form_wks/form_4a/searchDataForm',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          show_result(data);
        }
      });
  });

  $('#btn_search_uk').click(function () {
      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var prov = $('#id_provinsi').val();
      var fk = $('#fk-box').val();
      var smt = $('input[type=radio][name=semester]').val();
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_4a/getPeriodeKelulusanMhs?y='+y+'&m='+m+'&prov='+prov+'&kode='+fk+'&smt='+smt+'').load();
  });

  $('#btn_reset').click(function () {
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_4a/getPeriodeKelulusanMhs').load();
  });


  // view history
  $('#btn_view_history').click(function () {
        $.ajax({
        url: 'form_wks/form_4a/searchHistory',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          view_history(data);
        }
      });
  });

  $('#add_periode_kelulusan').click(function () {
        $('#page-area-content').html(loading);
        $('#page-area-content').load('form_wks/form_4a/add_periode_kelulusan?_=' + (new Date()).getTime());
  });

  // btn proses usulan 
  $('#btn_save_usulan').click(function () {
      url = "form_wks/form_4a/proses_usulan_kebutuhan";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_update_usulan_kebutuhan').click(function () {
      url = "form_wks/form_4a/prosesUpdateUsulanKebutuhan";
      var formData = new FormData($('#form_detail_usulan_kebutuhan')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#addRow').click(function () {

      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var smt = $('input[name="semester"]:checked').val();
      var prov = $('#id_provinsi').val();
      var kode = $('#fk-box').val();
      

      url = "form_wks/form_4a/prosesTambahMhs";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              $('#form_usulan_sdmk')[0].reset();
              $("html, body").animate({ scrollTop: "500px" });
              $('#btn_name').html(' <i class="ace-icon fa fa-toggle-down icon-on-right bigger-110"></i>  Tambahkan');
              //table2.ajax.reload(null,false);
              table2.ajax.url('form_wks/form_4a/ajax_list_data_mhs?y='+y+'&m='+m+'&smt='+smt+'&prov='+prov+'&kode='+kode+'&f=4a').load();
    
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_update_periode').click(function () {

      url = "form_wks/form_4a/prosesUpdatePeriodeKelulusan";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              var result = data.result;
              greatComplete(data);
              $('#tahun').val(result.plm_tahun);
              $('#bulan').val(result.plm_bulan);
              $("html, body").animate({ scrollTop: 0 });
              
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });


});

function detailMhs(id){

  url = "form_wks/form_4a/detailMhs/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
          $("html, body").animate({ scrollTop: 280 }, 'slow');
          $('#nama_mahasiswa').val(data.dmp_nama);
          $('#nim').val(data.dmp_nim);
          $('#prodi').val(data.prod_id);
          //var status_psrt = data.dmp_status_peserta == 'TUBEL' ? 1 : 2;
          $("input[name='status_peserta'][value='"+data.dmp_status_peserta+"']").prop("checked",true);
          $('#instansi').val(data.dmp_instansi);
          $('#tgl_lulus').val(data.dmp_tanggal_lulus);
          $('#dmp_id').val(data.dmp_id);
          $('#dmp_pemberi_beasiswa').val(data.dmp_pemberi_beasiswa);
          $('#dmp_no_ijasah').val(data.dmp_no_ijasah);
          $('#btn_name').html(' <i class="ace-icon fa fa-toggle-down icon-on-right bigger-110"></i>  Ubah data ');
        }
    });

}

function show_result(data){

  // hide table history
  $('#div_history').hide();

  if(data.status != 1){
    $('#search_status').html('<div class="alert alert-danger"><b><i class="fa fa-times"></i> Peringatan ! </b>'+data.message+'</div>');
    $('#data_skm').hide();
    $('#search_status').show();
    $('#div_table').hide();
    $('#page_title').hide();

  }else{
    
    var result = data.data_perencanaan; // data response

    // hide show div
    $('#search_status').hide();
    $('#page_title ').show();
    $('#data_skm').show();
    $('#div_table').show();
    $('#page_title').html('<h1>Rekapitulasi Keadaan dan Usulan Kebutuhan SDM Kesehatan Rumah Sakit<br> ( '+result.kode_rsu+' ) '+result.nama_rs+'<br>Tahun '+result.tahun+'</h1>');
    table.ajax.url('form_wks/form_4a/ajax_list_skm/'+result.id_perencanaan).load();
    $("html, body").animate({ scrollTop: "400px" });
  }

}

function getGrade(params){

    if(params == 0){
      format = '<span style="color:blue"><strong> '+params+' ( Sesuai ) </strong></span>';
    }else if (params > 0) {
      format = '<span style="color:green"><strong> '+params+' ( Lebih ) </strong></span>';
    }else{
      format = '<span style="color:red"><strong> '+params+' ( Kurang ) </strong></span>';
    }

    return format;

}

function view_history(data){

    var params = data;

    // page title //
    var html = '';
        html += 'Prediksi Kelulusan Mahasiswa Fakultas Kedokteran';
        if(params.prov != 0){ html += '<br>Provinsi '+params.nama_provinsi+''; }
        if(params.kab != 0){ html += ', '+params.nama_kabupaten+''; }
        if(params.fk_id != 0){ html += '<br>'+params.fk_name+''; }
        if(params.smt != 0){ html += '<br>Semester '+params.semester_text+''; }
        if(params.m != 0){ html += ', Periode '+params.bulan_text+''; }
        if(params.y != 0){ html += ', Tahun '+params.y+''; }
        
    $('#page_title').show();
    $('#page_title').html('<h1>'+html+'</h1>');

    // hide table usulan 
    $('#div_table').hide();

    // show table history
    $('#div_history').show();
    table2.ajax.url('form_wks/form_4a/ajax_list_data_mhs?y='+params.y+'&m='+params.m+'&smt='+params.smt+'&prov='+params.prov+'&kab='+params.kab+'&kode='+params.fk_id+'&f=4a').load();
    
    //table2.rows.add(params.result).draw();

    /*no = 1;
    $.each(params.result, function(i, o) {
      no++;
        $('<tr><td>'+no+'</td><td>'+o.prod_name+'</td><td></td><td></td><td></td><td></td><td></td></tr>').appendTo($('#data_mahasiswa'));
    });*/

    $("html, body").animate({ scrollTop: "400px" }, "slow");

}

function detail(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_4a/detail_usulan_kebutuhan/' + id + '?_=' + (new Date()).getTime());

}

function add_mhs(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_4a/add_periode_kelulusan/' + id + '?_=' + (new Date()).getTime());

}

function detailPeriodeKelulusan(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_4a/add_periode_kelulusan/' + id + '/detail?_=' + (new Date()).getTime());

}

function delete_mhs(id){

  url = "form_wks/form_4a/prosesDeleteMhs/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
           table2.ajax.reload();
        }
    });

}

function delete_periode(id){

  url = "form_wks/form_4a/prosesDeletePeriode/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
           table.ajax.reload();
        }
    });

}

function sendToVerifikator(id){

  url = "form_wks/form_4a/prosesSendToDinkesKab/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
          greatComplete(data);
          backlist();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
          backlist();
        }
    });

}

function backlist(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_4a?_=' + (new Date()).getTime());
 }

// jquery choosen //
jQuery(function($) {
  
  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
  
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }


});