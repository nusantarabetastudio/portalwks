$(document).ready(function() {

  table = $('#dynamic-table').DataTable({ 
          
      "processing": true, //Feature control the processing indicator.
      "serverSide": true, //Feature control DataTables' server-side processing mode.
      
      // Load data for the table's content from an Ajax source
      "ajax": {
          "url": "form_wks/form_5b/getPeriodeKelulusanMhs",
          "type": "POST"
      },

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });

    table2 = $('#data-mhs').DataTable({ 
          
      "processing": false, //Feature control the processing indicator.
      "serverSide": false, //Feature control DataTables' server-side processing mode.
      "info": false, //Feature control DataTables' server-side processing mode.
      "paging": false, //Feature control DataTables' server-side processing mode.
      
      // Load data for the table's content from an Ajax source
      /*"ajax": {
          "url": "form_wks/form_5b/getPeriodeKelulusanMhs",
          "type": "POST"
      },*/

      //Set column definition initialisation properties.
      "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
      ],

    });

    table3 = $('#data_history').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5b/ajax_list_peserta_wks",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });
  
  // search form
  $('#btn_search').click(function () {
        $.ajax({
        url: 'form_wks/form_5b/searchDataUsulanRs',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          show_result(data);
        }
      });
  });

  // btn proses usulan 
  $('#btn_save_verifikasi').click(function () {
      url = "form_wks/form_5b/prosesVerifikasiKolegium";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              backlist();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  // view history
  $('#btn_view_history').click(function () {
        $.ajax({
        url: 'form_wks/form_5b/searchPesertaWks',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          $('#search_status').hide();
          view_history(data);
        }
      });
  });

});
  
  function view_history(data){

    var params = data;

    // page title //
    var html = '';
        html += 'Daftar Peserta Wajib Kerja Spesialis<br>';
        if(params.tahun != 0){ html += 'Tahun '+params.tahun+' '; }
        if(params.bulan != 0){ html += 'Bulan '+params.bulan+' '; }
        if(params.sampai_bulan != 0){ html += ' s/d '+params.sampai_bulan+' <br>'; }
        if(params.id_provinsi != 0){ html += '<br>Provinsi '+params.nama_provinsi+''; }
        if(params.id_kabupaten != 0){ html += ', '+params.nama_kabupaten+''; }

    $('#page_title').show();
    $('#page_title').html('<h1>'+html+'</h1>');

    // hide table usulan 
    $('#div_table').hide();

    // show table history
    $('#div_history').show();
    table3.ajax.url('form_wks/form_5b/ajax_list_peserta_wks?y='+params.tahun+'&prov='+params.id_provinsi+'&kab='+params.id_kabupaten+'&m='+params.m+'&to='+params.to+'&smt='+params.semester+'').load();
    $("html, body").animate({ scrollTop: "400px" });

}

  
 function backlist(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5b?_=' + (new Date()).getTime());
 }

 function search_form(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5b/search_form?_=' + (new Date()).getTime());
 }

function verifikasi(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5b/verifikasi/' + id + '?_=' + (new Date()).getTime());

}

// jquery choosen //
jQuery(function($) {
      
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }


});