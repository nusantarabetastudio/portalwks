$(document).ready(function() {

  table = $('#data_usulan_kebutuhan_rs').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_4c/ajax_list_history",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  table2 = $('#data_prediksi').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_4c/ajax_list_data_mhs",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  $('#btn_search_and_anlyze').click(function () {

      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var to = $('#to').val();
      var prov = $('#id_provinsi').val();
      var kab = $('#kab-box').val();
      var rs = $('#rsu-box').val();
      var kode = $('#fk-box').val();
      var smt = $('input[type=radio][name=semester]').val();
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_4c/ajax_list_history?y='+y+'&m='+m+'&prov='+prov+'&kab='+kab+'&rs='+rs+'').load();
      table2.ajax.url('form_wks/form_4c/ajax_list_data_mhs?y='+y+'&m='+m+'&smt='+smt+'&prov='+prov+'&kode='+kode+'&f=4c').load();
    
    });


});

function update_status(id){
  $.ajax({
      url : "form_wks/form_4c/updateRowStatusUk/"+id,
      type: "POST",
      dataType: "JSON",
      contentType: false,
      processData: false,
      success: function(data, textStatus, jqXHR, responseText)
      { 
        greatComplete(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
      }
  });

}

function update_status_dmp(id){
  $.ajax({
      url : "form_wks/form_4c/updateRowStatusDmp/"+id,
      type: "POST",
      dataType: "JSON",
      contentType: false,
      processData: false,
      success: function(data, textStatus, jqXHR, responseText)
      { 
        greatComplete(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
      }
  });

}
