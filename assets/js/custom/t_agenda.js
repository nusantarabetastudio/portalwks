    var save_method; //for save method string
    var table;
      $(document).ready(function() {
        wu_id = $('#data-agenda').attr('rel');
        table = $('#data-agenda').DataTable({ 
          
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          
          // Load data for the table's content from an Ajax source
          "ajax": {
              "url": "conf_web/t_agenda/ajax_list/" + wu_id,
              "type": "POST"
          },

          //Set column definition initialisation properties.
          "columnDefs": [
          { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
          },
          ],

        });
      });

      // greating //
      function greatSuccess() {

        $.gritter.add({
               title: 'SUKSES!',
               text: 'Data berhasil diproses.',
               sticky: false,
               time: '1500',
               class_name: 'gritter-success'
        })

      }

    function greatError() {
        $.gritter.add({
             title: 'PERINGATAN!',
             text: 'Anda tidak memiliki hak akses atau terjadi kesalahan dalam proses.',
             sticky: false,
             time: '1500',
             class_name: 'gritter-error'
        })

    }
    function edit(id)
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('conf_web/t_agenda/form/' + id + '?_=' + (new Date()).getTime());

    }

    function view_table(id)
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('conf_web/t_agenda/view_table/'+id+'?_=' + (new Date()).getTime());

    }

    function backlist()
    {
    
      $('#page-area-content').empty();
      $('#page-area-content').load('t_agenda'+ '?_=' + (new Date()).getTime());

    }

    function add()
    {
      $('#page-area-content').empty();
      $('#page-area-content').load('conf_web/t_agenda/form/'+ '?_=' + (new Date()).getTime());

    }

    function reload_table()
    {
      table.ajax.reload(null,false); //reload datatable ajax 
    }

    function delete_t_agenda(id)
    {

      if(confirm('Apakah anda yakin akan menghapus data ini?'))
      {
        // ajax delete data to database
          $.ajax({
            url : "conf_web/t_agenda/ajax_delete/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
               //if success reload ajax table
               greatSuccess();
               reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                greatError();
            }
        });
         
      }

    }


$.validator.setDefaults({
  submitHandler: function() { 
    
     url = "conf_web/t_agenda/ajax_add";

       // ajax adding data to database
          $.ajax({
            url : url,
            type: "POST",
            data: $('#form_t_agenda').serialize(),
            dataType: "JSON",
            success: function(data)
            { 

              greatSuccess();
              $('#btnSave').hide();
              $('#btnReset').hide();
              $('#btnAdd').show();
              //$('input[type="text"],texatrea, select', this).val('');
              //$('#form_t_agenda').resetForm();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {

              greatError();

            }
        });

  }

});

function disabledBtn()
  {
    $('#btnSave').disabled = true;
    return true;
  }


// jquery validation //
$().ready(function() {

  // validate signup form on keyup and submit
  $("#form_t_agenda").validate({
    rules: {
      
      t_agenda_name: "required",
      active: "required"
    },

    messages: {
      t_agenda_name: "Masukan Nama Ex Master!"
    }
  });


});

$('select[name="wu_id"]').change(function() {
    if ($(this).val()) {
        $('#page-area-content').empty();
        $('#page-area-content').load('conf_web/t_agenda/view_table/'+ $(this).val() +'?_=' + (new Date()).getTime());
    }else{
       $('#page-area-content').empty();
        $('#page-area-content').load('conf_web/t_agenda/view_table/0?_=' + (new Date()).getTime());
    }
});

jQuery(function($) {

  $(".select2").css('width','340px').select2({allowClear:true})
  .on('change', function(){
    $(this).closest('form').validate().element($(this));
  }); 

  $('#path_thumbnail').ace_file_input({
            no_file:'No File ...',
            btn_choose:'Choose',
            btn_change:'Change',
            droppable:false,
            onchange:null,
            thumbnail:false //| true | large
            //whitelist:'gif|png|jpg|jpeg'
            //blacklist:'exe|php'
            //onchange:''
            //
  });
});
