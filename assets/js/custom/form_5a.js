$(document).ready(function() {

  table = $('#data_usulan_kebutuhan_rs').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5a/ajax_list_history",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  table2 = $('#data_prediksi').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5a/ajax_list_data_mhs",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  table3 = $('#data_periode_penempatan').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5a/ajax_list_penempatan_periode",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  var perpp_id = $('#peserta_wkds').attr('rel');
  table4 = $('#peserta_wkds').DataTable({ 
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_5a/ajax_list_daftar_peserta/"+perpp_id,
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });


  $('#add_periode_penempatan').click(function () {
        $('#page-area-content').html(loading);
        $('#page-area-content').load('form_wks/form_5a/add_periode_penempatan?_=' + (new Date()).getTime());
  });

  $('#btn_search_and_anlyze').click(function () {

      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var to = $('#to').val();
      var prov = $('#id_provinsi').val();
      var kab = $('#kab-box').val();
      var rs = $('#rsu-box').val();
      var kode = $('#fk-box').val();
      var smt = $('input[type=radio][name=semester]').val();
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table.ajax.url('form_wks/form_5a/ajax_list_history?y='+y+'&m='+m+'&prov='+prov+'&kab='+kab+'&rs='+rs+'').load();
      table2.ajax.url('form_wks/form_5a/ajax_list_data_mhs?y='+y+'&m='+m+'&smt='+smt+'&prov='+prov+'&kode='+kode+'&f=4c').load();
    
  });

  $('#btn_penempatan_manual').click(function () {
    $("html, body").animate({ scrollTop: "400px" }, "slow");
    $('#penempatan_manual_form').show();
  });

  $('#btn_proses_penempatan').click(function () {
      url = "form_wks/form_5a/prosesPenempatan";
      var formData = new FormData($('#form_penempatan_lokus')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_submit_periode_penempatan').click(function () {
      url = "form_wks/form_5a/prosesPeriodePenempatan";
      var formData = new FormData($('#form_periode_penempatan')[0]);
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              backlist();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });
  

});

function update_status(id){
  $.ajax({
      url : "form_wks/form_5a/updateRowStatusUk/"+id,
      type: "POST",
      dataType: "JSON",
      contentType: false,
      processData: false,
      success: function(data, textStatus, jqXHR, responseText)
      { 
        greatComplete(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
      }
  });

}

function update_status_dmp(id){
  $.ajax({
      url : "form_wks/form_5a/updateRowStatusDmp/"+id,
      type: "POST",
      dataType: "JSON",
      contentType: false,
      processData: false,
      success: function(data, textStatus, jqXHR, responseText)
      { 
        greatComplete(data);
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
      }
  });

}

function hapus_peserta(id)
{

  if(confirm('Apakah anda yakin akan menghapus data ini?'))
  {
    // ajax delete data to database
      $.ajax({
        url : "form_wks/form_5a/delete_peserta/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
           //if success reload ajax table
           greatComplete(data);
           reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
        }
    });
     
  }

}

function backlist(){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a?_=' + (new Date()).getTime());

}

function penempatan_form(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a/penempatan/' + id + '?_=' + (new Date()).getTime());

}

function penempatan_periode(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a/penempatan_periode/' + id + '?_=' + (new Date()).getTime());

}

function penempatan_form_rs(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_5a/penempatan_by_rs/' + id + '?_=' + (new Date()).getTime());

}

function dynInput(id, cbox) {

  $("html, body").animate({ scrollTop: "400px" }, "slow");
  var vis = (cbox.checked) ? "block" : "none";
  document.getElementById(id).style.display = vis

 
 }


// jquery choosen //
jQuery(function($) {
  
  $('.date-picker').datepicker({
    autoclose: true,
    todayHighlight: true
  })
  //show datepicker when clicking on the icon
  .next().on(ace.click_event, function(){
    $(this).prev().focus();
  });
  
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }


});
