$(document).ready(function() {

  var id_perencanaan = $('#dynamic-table').attr('rel');
  
  table = $('#dynamic-table').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_1/ajax_list_skm",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });

  table2 = $('#data_history').DataTable({ 
                
    "processing": true, //Feature control the processing indicator.
    "serverSide": true, //Feature control DataTables' server-side processing mode.
    "paging":true,
    "searching":false,
    "bLengthChange": false,
    // Load data for the table's content from an Ajax source
    "ajax": {
        "url": "form_wks/form_1/ajax_list_history",
        "type": "POST"
    },

    //Set column definition initialisation properties.
    "columnDefs": [
      { 
        "targets": [ -1 ], //last column
        "orderable": false, //set not orderable
      },
    ],

  });


  // search form
  $('#btn_search').click(function () {
        $.ajax({
        url: 'form_wks/form_1/searchDataForm',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          show_result(data);
        }
      });
  });

  $('#btn_search_uk').click(function () {
      var y = $('#tahun').val();
      var m = $('#bulan').val();
      var prov = $('#id_provinsi').val();
      var kab = $('#kab-box').val();
      var rs = $('#rsu-box').val();
      $("html, body").animate({ scrollTop: "400px" }, "slow");
      table2.ajax.url('form_wks/form_1/ajax_list_history?y='+y+'&m='+m+'&prov='+prov+'&kab='+kab+'&rs='+rs+'').load();
  });

  $('#btn_reset').click(function () {
      $("html, body").animate({ scrollTop: "400px" });
      table2.ajax.url('form_wks/form_1/ajax_list_history').load();
  });

  // view history
  $('#printPreview').click(function () {
        var id = $('#uk_id').val();
        $('#page-area-content').html(loading);
        $('#page-area-content').load('form_wks/form_1/printPreview/' + id + '?_=' + (new Date()).getTime());
  });

  // view history
  $('#add_usulan').click(function () {
        $('#page-area-content').html(loading);
        $('#page-area-content').load('form_wks/form_1/add_usulan?_=' + (new Date()).getTime());
  });

  // view history
  $('#btn_view_history').click(function () {
        $.ajax({
        url: 'form_wks/form_1/searchHistory',
        type: "post",
        data: $('#form_usulan_sdmk').serialize(),
        dataType: "json",
        success: function(data) {
          $('#search_status').hide();
          view_history(data);
        }
      });
  });

  // btn proses usulan 
  $('#btn_save_usulan').click(function () {
      url = "form_wks/form_1/proses_usulan_kebutuhan";
      var formData = new FormData($('#form_usulan_sdmk')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_update_usulan_kebutuhan').click(function () {
      url = "form_wks/form_1/prosesUpdateUsulanKebutuhan";
      var formData = new FormData($('#form_detail_usulan_kebutuhan')[0]);
      /*var banner_description = $("#banner_description").val();*/
         /*formData.append('path_thumbnail', $('input[type=file]')[0].files[0]);
         formData.append('banner_description', banner_description);*/
        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });

  $('#btn_proses_upload_and_send_to_verifikator').click(function () {
      url = "form_wks/form_1/prosesUploadAndSendToVerifkator";
      var formData = new FormData($('#form_upload_and_send')[0]);
          formData.append('file', $('input[type=file]')[0].files[0]);

        $.ajax({
            url : url,
            type: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            processData: false,
            success: function(data, textStatus, jqXHR, responseText)
            { 
              greatComplete(data);
              backlist();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
              greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
            }
        });
  });


});

function show_result(data){

  // hide table history
  $('#div_history').hide();

  if(data.status != 1){
    $('#search_status').html('<div class="alert alert-danger"><b><i class="fa fa-times"></i> Peringatan ! </b>'+data.message+'</div>');
    $('#data_skm').hide();
    $('#search_status').show();
    $('#div_table').hide();
    $('#page_title').hide();

  }else{
    
    var result = data.data_perencanaan; // data response

    // hide show div
    $('#search_status').hide();
    $('#page_title ').show();
    $('#data_skm').show();
    $('#div_table').show();
    $('#page_title').html('<h1>Rekapitulasi Keadaan dan Usulan Kebutuhan SDM Kesehatan Rumah Sakit<br> ( '+result.kode_rsu+' ) '+result.nama_rs+'<br>Tahun '+result.tahun+'</h1>');
    table.ajax.url('form_wks/form_1/ajax_list_skm/'+result.id_perencanaan).load();
  }

  $("html, body").animate({ scrollTop: "400px" });

}

function getGrade(params){

    if(params == 0){
      format = '<span style="color:blue"><strong> '+params+' ( Sesuai ) </strong></span>';
    }else if (params > 0) {
      format = '<span style="color:green"><strong> '+params+' ( Lebih ) </strong></span>';
    }else{
      format = '<span style="color:red"><strong> '+params+' ( Kurang ) </strong></span>';
    }

    return format;

}

function view_history(data){

    var params = data;

    // page title //
    var html = '';
        html += 'Riwayat Usulan Kebutuhan SDM Kesehatan';
        if(params.tahun != 0){ html += 'Tahun '+params.tahun+''; }
        if(params.id_provinsi != 0){ html += '<br>Provinsi '+params.nama_provinsi+''; }
        if(params.id_kabupaten != 0){ html += ', '+params.nama_kabupaten+''; }
        if(params.kode_rsu != 0){ html += '<br>'+params.nama_rs+''; }
    $('#page_title').show();
    $('#page_title').html('<h1>'+html+'</h1>');

    // hide table usulan 
    $('#div_table').hide();

    // show table history
    $('#div_history').show();
    table2.ajax.url('form_wks/form_1/ajax_list_history?y='+params.tahun+'&prov='+params.id_provinsi+'&kab='+params.id_kabupaten+'&kode='+params.kode_rsu+'&f=1').load();
    $("html, body").animate({ scrollTop: "400px" });

}

function detail(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_1/detail_usulan_kebutuhan/' + id + '?_=' + (new Date()).getTime());

}

function sendToVerifikator(id){

    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_1/sendToVerifikator/' + id + '?_=' + (new Date()).getTime());

}

function delete_uk_sdmk(id){

  url = "form_wks/form_1/prosesDeleteUkSdmk/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
           $(this).closest('tr').remove();
        }
    });

}

/*function sendToVerifikator(id){

  url = "form_wks/form_1/prosesSendToDinkesKab/"+id;
    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        processData: false,
        success: function(data, textStatus, jqXHR, responseText)
        { 
          greatComplete(data);
          backlist();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          greatComplete({message:'Error code '+xhr.status+' : '+thrownError, gritter:'gritter-error'});
          backlist();
        }
    });

}*/

function backlist(){
    $('#page-area-content').html(loading);
    $('#page-area-content').load('form_wks/form_1?_=' + (new Date()).getTime());
 }

// jquery choosen //
jQuery(function($) {
      
  if(!ace.vars['touch']) {
    $('.chosen-select').chosen({allow_single_deselect:true}); 
    //resize the chosen on window resize

    $(window)
    .off('resize.chosen')
    .on('resize.chosen', function() {
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    }).trigger('resize.chosen');
    //resize chosen on sidebar collapse/expand
    $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
      if(event_name != 'sidebar_collapsed') return;
      $('.chosen-select').each(function() {
         var $this = $(this);
         $this.next().css({'width': $this.parent().width()});
      })
    });

  }

});

/*$(document).on('click', '#btnPopup', function () {
        //var id = $('#uk_id').val();
         var url = '/form_wks/form_1/printPreview/';
        $.ajax({
            type: 'GET',
            url: url,
            success: function (output) {
            $('#myModal').modal('show');// I tried to show this response as modal popup
            },
            error: function(output){
            alert("fail");
            }
        });
    });*/