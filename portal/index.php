<!DOCTYPE html>
<html lang="en-US">
    <head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="ThemeStarz">
    <link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
    <!-- <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'> -->
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jquery.nouislider.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <title>Portal Wajib Kerja Dokter Spesialis - Kementerian Kesehatan Republik Indonesia</title>
</head>
    <body>
        <div class="page-wrapper">
            <!--page-header-->
           <header id="page-header">
    <nav>
        <div class="left">
            <a href="#" class="brand"><h1><img src="assets/img/logo.png" alt="" width="180">  Portal WKS</h1></a>
        </div>
        <!--end left-->
        <div class="right">
            <div class="primary-nav has-mega-menu">
                <ul class="navigation">
                    <li class=""><a href="index.php">Beranda</a></li>
                    <li class=""><a href="article.php">Berita</a></li>
                    <li class=""><a href="information.php">Pengumuman</a></li>
                    <li class=""><a href="agenda.php">Kalendar Agenda</a></li>
                    <li class=""><a href="flow_register.php">Alur Pendaftaran</a></li>
                </ul>
                <!--end navigation-->
            </div>
            <!--end primary-nav-->
            <div class="secondary-nav">
                <a href="login.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-lock"></i><span>Login</span></a>
                <a href="register.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-user"></i><span>Registrasi Ulang</span></a>
            </div>
            <!--end secondary-nav-->
            <div class="nav-btn">
                <i></i>
                <i></i>
                <i></i>
            </div>
            <!--end nav-btn-->
        </div>
        <!--end right-->
    </nav>
    <!--end nav-->
</header>
            <!--end page header-->
            <div id="page-content">
                <div class="container" style="min-width: 1000px">
                    <!-- <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Publik</a></li>
                        <li class="active">Beranda</li>
                    </ol> -->
                    <hr class="sparator">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <section class="page-title">
    <h1>Berita Seputar Program WKS</h1>
    <!-- <h2>Seputar Program Wajib Kerja Dokter Spesialis</h2> -->
</section>
<section>
    <article class="blog-post">
    </article><!-- /.blog-post -->
</section>
<section>
    <div class="center">
        <nav aria-label="Page navigation">
            <ul class="pagination">
                <li class="disabled previous">
                    <a href="blog.html#" aria-label="Previous">
                        <i class="arrow_left"></i>
                    </a>
                </li>
                <li class="active"><a href="blog.html#">1</a></li>
                <li class="next">
                    <a href="blog.html#" aria-label="Next">
                        <i class="arrow_right"></i>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</section>
</div>
                        <!--end col-md-9-->
                       <section>
    <h2>Pengumuman</h2>
    <form class="form inputs-underline">
        <ul>
            <li> <a href="#">Jadwal Mengisi Form Rekapitulasi Keadaan & Kebutuhan SDM Kesehatan untuk Rumah Sakit</a></li>
            <li><a href="#">Regionalisasi Fakultas Kedokteran</a></li>
            <li><a href="#">Rekapitulasi Kebutuhan Kelulusan Dokter Spesialis</a></li>
            <li><a href="#">Download Surat Penugasan Penempatan Wajib Kerja Spesialis</a></li>
            <li><a href="#">Jadwal Mengisi Form Rekapitulasi Keadaan & Kebutuhan SDM Kesehatan untuk Rumah Sakit</a></li>
        </ul>
        <!--end form-group-->
    </form>
</section>
                             
                               
                             
<script>
    $(document).ready(function() {
        $('#calendar_view').fullCalendar({
            defaultView: 'listWeek',
            //defaultDate: '2016-12-12',
            navLinks: false, // can click day/week names to navigate views
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            events: [
                {
                    title: 'Mengisi usulan kebutuhan dr Spesialis oleh Rumah Sakit',
                    start: '2017-01-02'
                },
                {
                    title: 'Mengisi formulir peserta wajib kerja dr Spesialis',
                    start: '2017-01-02',
                    end: '2017-01-07'
                },
                {
                    id: 999,
                    title: 'Verifikasi Dinkes Kab/Kota',
                    start: '2017-01-08'
                },
                {
                    id: 999,
                    title: 'Verifikasi Dinkes Provinsi',
                    start: '2017-01-010'
                },
                {
                    title: 'Sosialisasi Portal WKS online',
                    start: '2017-01-03',
                    end: '2017-01-03'
                },
                {
                    title: 'Pendampingan Peserta WKS ke Daerah',
                    start: '2017-02-02',
                    end: '2017-02-09'
                },
                {
                    title: 'Pengembangan Aplikasi WKS Online oleh tim konsultan',
                    start: '2016-12-20'
                },
                {
                    title: 'Rapat koordinasi untuk mereview Aplikasi WKS online',
                    start: '2016-12-16'
                }
            ]
        });
    });
</script>
<style>
    #calendar_view {
        max-width: 900px;
        margin: 0 auto;
    }
</style>
                            </aside>
                            <!--end sidebar-->
                        </div>
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->
           <footer id="page-footer">
    <div class="footer-wrapper">
        <div class="block">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="first_name">Nama</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="Masukan Nama">
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="last_name">Email</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" value="Masukan Email">
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <!--end col-md-6-->
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="last_name">Pertanyaan</label>
                                    <textarea class="form-control"> </textarea>
                                </div>
                                <!--end form-group-->
                            </div>
                            <div class="col-md-12 col-sm-12 text-right">
                               <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-rounded">Proses</button>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                        </div>
                    </div>
                    <div class="element width-50 text-align-right">
                        <img src="assets/img/Screen Shot 2016-12-13 at 10.39.21 AM.png" width="520px" alt=""/>
                    </div>
                </div>
                <div class="background-wrapper">
                    <div class="bg-transfer opacity-50">
                        <img src="assets/img/footer-bg.png" alt="">
                    </div>
                </div>
                <!--end background-wrapper-->
            </div>
        </div>
        <div class="block">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">
                        <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                    </div>
                    <!-- <div class="element width-50 text-align-right">
                        <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                        <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                        <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                    </div> -->
                </div>
                <div class="background-wrapper">
                    <div class="bg-transfer opacity-50">
                        <img src="assets/img/footer-bg.png" alt="">
                    </div>
                </div>
                <!--end background-wrapper-->
            </div>
        </div>
        <div class="footer-navigation">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                    <!-- <div class="element width-50 text-align-right">
                        <a href="index.html">Home</a>
                        <a href="listing-grid-right-sidebar.html">Listings</a>
                        <a href="submit.html">Submit Item</a>
                        <a href="contact.html">Contact</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</footer>
            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->
        <!-- JS MAIN-->
<script type="text/javascript" src="assets/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&amp;libraries=places"></script> -->
<script type="text/javascript" src="assets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets/js/jQuery.MultiFile.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/maps.js"></script>
<!-- datatables-->
<link rel="stylesheet" type="text/css" href="assets/datatable/media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="assets/datatable/examples/resources/syntax/shCore.css">
<script type="text/javascript" language="javascript" src="assets/datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatable/examples/resources/syntax/shCore.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatable/examples/resources/demo.js"></script>
<script type="text/javascript" language="javascript" class="init">
$(document).ready(function() {
	$('#example').DataTable({
		"bFilter" : false,               
		"bLengthChange": false,
		"bPaginate": false,
		"showNEntries" : false,
		"bInfo" : false,
		 "bSort": false
	});
} );
</script>
<!-- FULLCALENDAR-->
<link href='assets/fullcalendar-3.1.0/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/fullcalendar-3.1.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='assets/fullcalendar-3.1.0/lib/moment.min.js'></script>
<script src='assets/fullcalendar-3.1.0/lib/jquery.min.js'></script>
<script src='assets/fullcalendar-3.1.0/fullcalendar.min.js'></script>
    </body>
