<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Alur Pendaftaran WKS</li>
                    </ol>

                    <section class="center page-title">
                        <h1>Alur Pendaftaran WKS</h1>
                    </section>

                    <section>
                        <div class="row">
                            <div class="col-md-8 cols-m-8 col-md-offset-2">
                                <div class="step">
                                    <div class="circle">
                                        <figure>1</figure>
                                        <i class="icon_id"></i>
                                        <div class="circle-bg opacity-20"></div>
                                    </div>
                                    <div class="box">
                                        <h2>Melihat Informasi Pengumuman</h2>
                                        <p>Melihat pengumuman pada portal WKS dan pastikan bahwa nama anda tercantum dalam peserta WKS
.
                                        </p>
                                        <a href="information.php" class="btn btn-default btn-rounded shadow btn-xs">Lihat Pengumuman</a>
                                    </div>
                                </div>
                                <!--end step-->
                                <div class="step">
                                    <div class="circle">
                                        <figure>2</figure>
                                        <i class="icon_adjust-vert "></i>
                                        <div class="circle-bg opacity-30"></div>
                                    </div>
                                    <div class="box">
                                        <h2>Mengisi Form Registrasi Ulang</h2>
                                        <p>Setelah anda memastikan bahwa nama anda tercantum dalam peserta WKS, maka anda harus melakukan Registrasi Ulang Pada Portal WKS untuk mendapatkan akun.

                                        </p>
                                        <a href="register.php" class="btn btn-default btn-rounded shadow btn-xs">Registrasi Ulang</a>
                                    </div>
                                </div>
                                <!--end step-->
                                <div class="step">
                                    <div class="circle">
                                        <figure>3</figure>
                                        <i class="icon_lock"></i>
                                        <div class="circle-bg opacity-60"></div>
                                    </div>
                                    <div class="box">
                                        <h2>Login ke Aplikasi</h2>
                                        <p>Setelah anda mendapatkan akun, maka anda dipersilahkan untuk masuk ke dalam aplikasi dengan username dan password yang telah anda daftarkan</p>
                                        <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Login</a>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="circle">
                                        <figure>4</figure>
                                        <i class="icon_pencil"></i>
                                        <div class="circle-bg opacity-60"></div>
                                    </div>
                                    <div class="box">
                                        <h2>Mengisi Formulir Lapor Peserta WKS</h2>
                                        <p>Setelah anda berhasil masuk ke aplikasi, silahkan lengkapi data anda dan mengisi Formulir Lapor Peserta WKS</p>
                                        <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Isi Formulir</a>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="circle">
                                        <figure>5</figure>
                                        <i class="icon_key"></i>
                                        <div class="circle-bg opacity-60"></div>
                                    </div>
                                    <div class="box">
                                        <h2>Mengisi Surat Perjalanan Dinas</h2>
                                        <p>Setelah mengisi Formulir anda harus mengisi surat perjalan dinas</p>
                                        <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Isi SPPD</a>
                                    </div>
                                </div>

                                <div class="step">
                                    <div class="circle">
                                        <figure>6</figure>
                                        <i class="icon_box-checked"></i>
                                        <div class="circle-bg opacity-90"></div>
                                    </div>
                                    <div class="box">
                                        <h2>Review data anda kembali</h2>
                                        <p>Setelah anda melakukan proses 1-5 silahkan review data anda terlebih dahulu sebelum anda submit</p>
                                        <a href="login.php" class="btn btn-default btn-rounded shadow btn-xs">Review</a>
                                    </div>
                                </div>

                                <!--end step-->
                            </div>
                            <!--end col-md-8-->
                        </div>
                        <!--end row-->
                    </section>
                    <!-- <section>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="section-title">
                                    <h2>What Is Included</h2>
                                    <h3 class="subtitle">Check what you’ll get after registration</h3>
                                </div>
                                <ul class="list-unstyled bullets">
                                    <li>Praesent id diam non est molestie elementum</li>
                                    <li>Curabitur cursus, eros vitae tempor eleifend, magna est venenatis lectus</li>
                                    <li>a volutpat lacus tortor a sem. Class aptent taciti sociosqu</li>
                                    <li>ad litora torquent per conubia nostra, per inceptos himenaeos.</li>
                                    <li>Proin efficitur at diam nec pharetra</li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="section-title">
                                    <h2>Our Satisfied Clients</h2>
                                    <h3 class="subtitle">All we do is for satisfaction of our clients</h3>
                                </div>
                                <div class="testimonials">
                                    <div class="owl-carousel" data-owl-items="1" data-owl-nav="0" data-owl-dots="1">
                                        <blockquote>
                                            <div class="image">
                                                <div class="bg-transfer">
                                                    <img src="assets/img/person-01.jpg" alt="">
                                                </div>
                                            </div>
                                            <h3>Jane Woodstock</h3>
                                            <h4>CEO at ArtBrands</h4>
                                            <p>Ut nec vulputate enim. Nulla faucibus convallis dui. Donec arcu enim, scelerisque gravida lacus vel.</p>
                                        </blockquote>
                                        <blockquote>
                                            <div class="image">
                                                <div class="bg-transfer">
                                                    <img src="assets/img/person-04.jpg" alt="">
                                                </div>
                                            </div>
                                            <h3>Peter Doe</h3>
                                            <h4>CEO at ArtBrands</h4>
                                            <p>Donec arcu enim, scelerisque gravida lacus vel, dignissim cursus lectus. Aliquam laoreet purus in iaculis sodales.</p>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> -->
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


