<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header_admin.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Profil</a></li>
                        <li class="active">Ubah Profil</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            
                            <form class="form inputs-underline">
                                <section>
                                    <div class="user-details box">
                                        <div class="user-image">
                                            <div class="image">
                                                <div class="bg-transfer"><img src="assets/img/person-01.jpg" alt=""></div>
                                                <!--end bg-transfer-->
                                                <div class="single-file-input">
                                                    <input type="file" id="user_image" name="user_image">
                                                    <div>Upload foto<i class="fa fa-upload"></i></div>
                                                </div>
                                            </div>
                                            <!--end image-->

                                        </div>
                                        <!--end user-image-->
                                        <div class="description clearfix">
                                            <h3>Nama</h3>
                                            <h2>dr. Muhammad Amin Lubis</h2>
                                            <a href="profile.php" class="btn btn-default btn-rounded btn-xs">Selengkapnya</a>
                                        <hr>
                                            <figure>
                                                <div class="pull-left"><strong>Terakhir diperbaharui:</strong></div>
                                                <div class="pull-right">24/12/2016</div>
                                            </figure>

                                        </div>
                                        <!--end description-->
                                    
                                    </div>
                                    <a href="edit_profile.php" class="btn btn-primary btn-rounded"><i class="fa fa-user"></i> Ubah Profil</a>
                                    <a href="edit_password.php" class="btn btn-primary btn-rounded"><i class="fa fa-key"></i> Ubah Password</a>
                                </section>
                                <!--end user-details-->
                                
                                <hr>
                                <section>
                                    <h3>Ubah Profil</h3>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">Nama Lengkap</label>
                                                <input type="text" class="form-control" name="first_name" id="first_name" value="dr. Muhammad Amin Lubis">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">No KTP</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" value="36031223090004">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label for="last_name">Tempat / Tanggal Lahir</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" value="Tangerang / 23 November 1990">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        
                                        <div class="col-md-12 col-sm-12">
                                            <label for="message">Alamat tempat tinggal</label>
                                            <textarea class="form-control" id="message" rows="1" name="message" placeholder="Alamat tempat tingal"></textarea>
                                        </div>

                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">NIM</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" value="108093000086">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label for="last_name">Prodi</label>
                                                <select class="form-control" name="prodi">
                                                    <option value="">--Pilih Prodi--</option>
                                                    <option value="1">Kesehatan Masyarakat</option>
                                                    <option value="1">Kebidanan</option>
                                                    <option value="1">Keperawatan</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label for="last_name">Jurusan</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Jurusan--</option>
                                                    <option value="1">Spesialis Gigi dan Mulut</option>
                                                    <option value="1">Spesialis Bedah Orthopedi</option>
                                                    <option value="1">Spesialis Mata</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>

                                        <!--end col-md-6-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" name="email" id="email" value="aminlubis23@example.com">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-6-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="phone">Phone</label>
                                                <input type="text" class="form-control" name="phone" id="phone" value="+1 260-478-7987">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-6-->
                                        
                                    </div>
                                    <!--end row-->
                                    <div class="form-group">
                                        <label for="message">Deskripsi Singkat Tentang Saya</label>
                                        <textarea class="form-control" id="message" rows="2" name="message" placeholder="Something about me"></textarea>
                                    </div>
                                    <!--end form-group-->
                                </section>
                                <section>
                                    <h3>Mohon lampirkan file berikut : </h3>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">Pas Foto</label><br><br>
                                            <input type="file" class="form-control" name="last_name">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">Surat Keterangan Lulus/Ijasah</label><br><br>
                                            <input type="file" class="form-control" name="last_name">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">KTP</label><br><br>
                                            <input type="file" class="form-control" name="last_name">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">NPWP</label><br><br>
                                            <input type="file" class="form-control" name="last_name">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">Surat Keterangan Sehat</label><br><br>
                                            <input type="file" class="form-control" name="last_name">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">Halaman Depan Buku Tabungan BNI</label><br><br>
                                            <input type="file" class="form-control" name="last_name">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end row-->
                                </section>
                                <section class="center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-rounded">Save Changes</button>
                                    </div>
                                    <!--end form-group-->
                                </section>
                            </form>
                            
                        </div>
                        <!--end col-md-9-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


