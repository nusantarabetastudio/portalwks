<!DOCTYPE html>

<html lang="en-US">
    <head>
        <meta charset="UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="ThemeStarz">

        <link href="assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="assets/fonts/elegant-fonts.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,400italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.css" type="text/css">
        <link rel="stylesheet" href="assets/css/jquery.nouislider.min.css" type="text/css">

        <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
        <link rel="stylesheet" href="assets/css/style.css" type="text/css">

        <title>Portal Wajib Kerja Dokter Spesialis - Kementerian Kesehatan Republik Indonesia</title>

    </head>

    <body>
        <div class="page-wrapper">
            <header id="page-header">
                <nav>
                    <div class="left">
                        <a href="#" class="brand"><h1><img src="assets/img/logo.png" alt="" width="180">  Portal WKS</h1></a>
                    </div>
                    <!--end left-->
                    <div class="right">
                        <div class="primary-nav has-mega-menu">
                            <ul class="navigation">
                                <li><a href="beranda.html">Beranda</a>
                                </li>
                                <li ><a href="berita.html">Berita</a>
                                </li>
                                <li class=""><a href="pengumuman.html">Pengumuman</a>
                                </li>
                                <li class="active"><a href="calendar.html">Kalendar Agenda</a>
                                </li>

                            </ul>
                            <!--end navigation-->
                        </div>
                        <!--end primary-nav-->
                        <div class="secondary-nav">
                            <a href="#" class="btn btn-primary btn-small btn-rounded icon shadow add-listing" ><i class="fa fa-lock"></i><span>Login</span></a>
                            <a href="#" class="btn btn-primary btn-small btn-rounded icon shadow add-listing" ><i class="fa fa-user"></i><span>Register</span></a>
                        </div>
                        <!--end secondary-nav-->
                        <div class="nav-btn">
                            <i></i>
                            <i></i>
                            <i></i>
                        </div>
                        <!--end nav-btn-->
                    </div>
                    <!--end right-->
                </nav>
                <!--end nav-->
            </header>
            <!--end page-header-->

            <div>
                <div class="container" style="min-width: 1000px">
                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Publik</a></li>
                        <li class="active">Beranda</li>
                    </ol>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <section class="page-title">
                                <h2>Kalendar Agenda</h2>
                            </section>
                            <!--end section-title-->
                            <section>
                                <article class="blog-post">
                                   
                                </article><!-- /.blog-post -->
                            </section>
                            
                        </div>
                        <!--end col-md-9-->
                        
                        
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
<div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="first_name">Nama</label>
                                            <input type="text" class="form-control" name="first_name" id="first_name" value="Masukan Nama">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label for="last_name">Email</label>
                                            <input type="text" class="form-control" name="last_name" id="last_name" value="Masukan Email">
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
                                    <!--end col-md-6-->
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="last_name">Pertanyaan</label>
                                            <textarea class="form-control"> </textarea>
                                        </div>
                                        <!--end form-group-->
                                    </div>
                                    <div class="col-md-12 col-sm-12 text-right">
                                       <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-rounded">Proses</button>
                                </div>
                                        <!--end form-group-->
                                    </div>
                                    <!--end col-md-6-->
                                  
                                </div>
                                </div>
                                <div class="element width-50 text-align-right">
                                    <img src="assets/img/Screen Shot 2016-12-13 at 10.39.21 AM.png" width="520px" alt=""/>
                                </div>
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>
                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2017 BPPSDM - PUSRENGUN Kementerian Kesehatan RI</div>
                                <div class="element width-50 text-align-right">
                                    <a href="http://pusrengun.info">pusrengun.info</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <script type="text/javascript" src="assets/js/jquery-2.2.1.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&amp;libraries=places"></script>
        <script type="text/javascript" src="assets/js/richmarker-compiled.js"></script>
        <script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="assets/js/jquery.nouislider.all.min.js"></script>
        <script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="assets/js/custom.js"></script>
        <script type="text/javascript" src="assets/js/maps.js"></script>

    </body>


