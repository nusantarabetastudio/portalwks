<?php include('include/data.php');?>

<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Berita</a></li>
                        <li class="active"><?php echo $modul_title?></li>
                    </ol>

                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <section class="page-title">
                                <h1><?php echo $modul_title?></h1>
                            </section>
                            <!--end section-title-->
                            <article class="blog-post">
                                <a href="blog-detail.html"><img src="assets/img/items/<?php echo $modul_image?>" alt=""></a>
                                <!-- <header><a href="blog-detail.html"><h2>Vivamus porta orci eu turpis vulputate ornare fusce hendrerit arcu risu</h2></a></header> -->
                                <figure class="meta">
                                    <a href="blog-detail.html#" class="link-icon"><i class="fa fa-user"></i> Admin</a>
                                    <a href="blog-detail.html#" class="link-icon"><i class="fa fa-calendar"></i> 06/04/2014</a>
                                    <div class="tags">
                                        <a href="blog-detail.html#" class="tag article">Architecture</a>
                                        <a href="blog-detail.html#" class="tag article">Design</a>
                                        <a href="blog-detail.html#" class="tag article">Trend</a>
                                    </div>
                                </figure>
                                <p align="justify"><?php echo $modul_content?></p>
                                
                            </article><!-- /.blog-post-listing -->

                            <section id="about-author">
                                <header><h3>Tentang penulis</h3></header>
                                <div class="post-author">
                                    <img src="assets/img/person-01.jpg">
                                    <div class="wrapper">
                                        <header>Administrator</header>
                                       
                                    </div>
                                </div>
                            </section>
                            
                        </div>
                        <!--end col-md-9-->

                        <div class="col-md-4 col-sm-4">
                            <aside class="sidebar">
                                
                                <?php include('section/view_search.php');?>

                                <?php include('section/view_recent_news.php');?>

                                <?php include('section/view_information.php');?>

                            </aside>
                            <!--end sidebar-->
                        </div>
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <?php include('include/footer.php');?>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


