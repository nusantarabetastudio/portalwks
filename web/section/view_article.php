

<div class="container" style="padding-top:50px">
<h1>Kegiatan Visitasi Program Wajib Kerja dokter Spesialis</h1>
	<div class="row">
		<div class="span12">
    	    <div class="well"> 
                <div id="myCarousel" class="carousel slide">
                 
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                 
                <!-- Carousel items -->
                <div class="carousel-inner">
                    
         <div class="item active">
                	<div class="row-fluid">
                	  <div class="span3"><a href="#x" class="thumbnail"><img src="photo/1.jpg" alt="Image" width="250px" height="250px" /></a></div>
                	  <div class="span3"><a href="#x" class="thumbnail"><img src="photo/2.jpg" alt="Image" width="250px" height="250px" /></a></div>
                	  <div class="span3"><a href="#x" class="thumbnail"><img src="photo/3.jpg" alt="Image" width="250px" height="250px" /></a></div>
                	  <div class="span3"><a href="#x" class="thumbnail"><img src="photo/4.jpg" alt="Image" width="250px" height="250px" /></a></div>
                	</div><!--/row-fluid--8-->
                </div><!--/item--8-->
           <?php
           $k=4;
           for($i=$k;$i<=12;$i++) { 
               $k++;
               ?>         
                <div class="item">
                	<div class="row-fluid">
                    <?php for($j=1;$j<=4;$j++) {
                          
                         ?> 
                	  <div class="span3"><a href="#x" class="thumbnail"><img src="photo/<?php echo $k ?>.jpg" alt="Image" width="250px" height="250px" /></a></div>
                    <?php   $k++; } ?> 
                    </div><!--/row-fluid-->
                </div><!--/item-->

           <?php  } ?>   
                 
               
                 
                </div><!--/carousel-inner-->
                 
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
                </div><!--/myCarousel-->
                 
            </div><!--/well-->   
		</div>
	</div>
</div>

<section class="page-title">

    <!-- <h2>Seputar Program Wajib Kerja Dokter Spesialis</h2> -->
    <p style="text-align:justify;">Dalam rangka persiapan penempatan Wajib Kerja Dokter Spesialis (WKDS) maka dilaksanakan visitasi. Tujuan dari visitasi adalah : 
    <ul>
    <li>(1) memberikan advokasi dan sosialisasi kepada Kadinkes Provinsi, Kadinkes Kab/Kota dan Direktur RS tentang Program Wajib Kerja Spesialis, </li>
    <li>(2) melakukan verifikasi usulan kebutuhan dokter spesialis kepada Pemda dan rumah sakit  yang telah mengusulkan kepada Kemenkes, </li>
    <li>(3) melihat kesiapan rumah sakit dari sisi ketenagaan, sarana prasarana serta sumber daya pendukung lainnya, </li>
    <li>(4) mendapatkan data yang akurat terkait data tenaga kesehatan rumah sakit, sarana prasarana dan kondisi rumah sakit dan sosek, </li>
    <li>  (5) mendapatkan informasi terkait permasalah kesehatan utamanya AKI dan AKB diwilayah kerja rumah sakit, dan </li>
    <li>(6) memberikan pendampingan bila dibutuhkan sesuai dengan bidang keahlian. </li>
    </ul>
   <div style="text-align:justify; padding-top:10px;"> Visitasi dilakukan di 124 rumah sakit yang telah mengusulkan dengan rincian 2 rumah sakit perbatasan, 35 rumah sakit rujukan regional, 81 rumah sakit kelas C, dan 6 rumah sakit rujukan provinsi. Pelaksanaan visitasi dilakukan pada tanggal 28 November sd 10 Desember 2016, yang dibagi menjadi 2 tahap yaitu : tahap I : 29 November sd 3 Desember 2016 di Provinsi Aceh, Sumatera Utara, Sumatera Barat, Riau, Jambi, Sumatera Selatan, D.I Yogyakarta, Jawa Tengah, Kalimantan Selatan, Jawa Timur, Kalimantan Timur, Kalimantan Utara, Sulawesi Utara, Sulawesi Tengah, Sulawesi Selatan, Sulawesi Tenggara, Gorontalo, Sulawesi Barat dan Maluku.
   </div>
   <div style="text-align:justify; padding-top:10px;"> 
   Visitasi tahap II dilaksanakan pada tanggal 5 – 10 Desember 2016 di provinsi Kepulauan Bangka Belitung, Bengkulu, Kepulauan Riau, .Lampung, Jawa Barat, Kalimantan Barat, Nusa Tenggara Barat dan Nusa Tenggara Timur, Maluku Utara dan Papua Barat.
Komposisi tim visitasi terdiri dari unsur Komite Penemoatan Dokter Spesialis (KPDS), Organisasi Profesi, Kolegium, Kemenkes, OP Cabang dan Dinas Kesehatan Provinsi. Total jumlah tim yang turun visitasi di 124 rumah sakit adalah sebanyak 53 tim. 
Adapun penilaian rumah sakit dapat ditetapkan sebagai lokus WKDS dengan mempertimbangkan : 

   </div>

<ul>
    <li> (1)	Ketersediaan SDM (kosong/kurang), sarana prasarana (ada, kondisi baik serta layak pakai), validasi rumah sakit dan sosial ekonomi (resistensi dari tenaga kesehatan lainnya, faktor keamanan, dan lain-lain)
 </li>
    <li>  (2)	Dukungan dari Pemerintah Daerah seperti penyediaan sarana tempat tinggal, kendaraan dinas, insentif daerah, kemudahan pengurusan Surat Ijin Praktik (SIP), dan lain-lain.
 </li>
    <li> (3)	Kebutuhan pelayanan seperti jumlah pasien, beban kerja , akses, sistem rujukan, SDM pendukung, dan lain
 </li>
</ul>

</p>
</section>




