    <style type="text/css">
        body { padding-top: 20px; }
#my2Carousel .nav a small {
    display:block;
}
#my2Carousel .nav {
	background:#eee;
}
#my2Carousel .nav a {
    border-radius:0px;
}
    </style>



<div class="container" style="padding-top:30px">
    <div id="my2Carousel" class="carousel slide" data-ride="carousel">
    
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
      
        <div class="item active">
          <img src="assets/slider/img/46.jpg">
           <div class="carousel-caption">
            <p>Wajib Kerja Dokter Spesialis adalah penempatan dokter spesialis di rumah sakit milik pemerintah pusat dan pemerintah daerah. Peserta wajib kerja dokter spesialis adalah setiap dokter spesialis lulusan pendidikan profesi program dokter spesialis dari perguruan tinggi negeri di dalam negeri dan perguruan tinggi di luar negeri. Untuk tahap awal peserta wajib kerja dokter spesialis diprioritaskan bagi lulusan pendidikan profesi program dokter spesialis obstetri dan ginekologi, spesialis anak, spesialis bedah, spesialis penyakit dalam dan spesialis anestesi dan terapi intensif. </p>
          </div>
        </div><!-- End Item -->
        <div class="item">
          <img src="assets/slider/img/1.jpg">
           <div class="carousel-caption">
            <p>Program Indonesia Sehat dilaksanakan dengan menegakkan tiga pilar utama, yaitu (1) penerapan paradigma sehat, (2) penguatan pelayanan kesehatan, dan (3) pelaksanaan jaminan kesehatan nasional (JKN). Penerapan paradigma sehat dilakukan dengan strategi pengarusutamaan kesehatan dalam pembangunan, penguatan upaya promotif dan preventif, serta pemberdayaan masyarakat. Penguatan pelayanan kesehatandilakukan dengan strategi peningkatan akses pelayanan kesehatan, optimalisasi sistem rujukan, dan peningkatan mutu menggunakan pendekatan continuum of care dan intervensi berbasis resiko kesehatan. Sedangkan pelaksanaan JKN dilakukan dengan strategi perluasan sasaran dan manfaat (benefit), serta kendali mutu dan biaya. Kesemuanya itu ditujukan kepada tercapainya keluarga-keluarga sehat.
 </p>
          </div>
        </div><!-- End Item -->
        <div class="item">
          <img src="assets/slider/img/3.jpg">
           <div class="carousel-caption">
            <p>
            Peserta wajib kerja dokter spesialis ditempatkan pada : (1) rumah sakit daerah terpencil, perbatasan dan kepulauan, (2) rumah sakit rujukan regional, (3) rumah sakit rujukan provinsi yang ada di seluruh Indonesia dan apabila sudah terpenuhi maka peserta wajib kerja ditempatkan pada rumah sakit milik Pemerintah Pusat atau rumah sakit milik Pemerintah Daerah sesuai perencanaan kebutuhan. 
</p>
          </div>
        </div><!-- End Item -->
         <div class="item">
          <img src="assets/slider/img/5.jpg">
           <div class="carousel-caption">
            <p>
            Jangka waktu pelaksanaan Wajib Kerja Dokter Spesialis bagi peserta wajib kerja dokter spesialis mandiri paling singkat selama 1 (satu) tahun. Sementara jangka waktu pelaksanaan Wajib Kerja Dokter Spesialis bagi peserta Wajib Kerja Dokter Spesialis penerima beasiswa dan/atau program bantuan biaya pendidikan dilaksanakan sesuai dengan ketentuan peraturan perundang-undangan.

</p>
          </div>
        </div><!-- End Item -->
 
        
      </div><!-- End Carousel Inner -->


    	<ul class="nav nav-pills nav-justified">
          <li data-target="#my2Carousel" data-slide-to="0" class="active"><a href="#">Tentang WKDS</a></li>
          <li data-target="#my2Carousel" data-slide-to="1"><a href="#">Latar Belakang</a></li>
          <li data-target="#my2Carousel" data-slide-to="2"><a href="#">Penempatan</a></li>
          <li data-target="#my2Carousel" data-slide-to="3"><a href="#">Jangka Waktu WKDS</a></li>
        </ul>


    </div><!-- End Carousel -->
</div>
<script type="text/javascript">
$(document).ready( function() {
    $('#my2Carousel').carousel({
		interval:   4000
	});
	
	var clickEvent = false;
	$('#my2Carousel').on('click', '.nav a', function() {
			clickEvent = true;
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
});
</script>