<section>
    <h2>Agenda minggu ini</h2>
    <div id='calendar_view'></div>
</section>
<script>

    $(document).ready(function() {
        
        $('#calendar_view').fullCalendar({
            
            defaultView: 'listWeek',
            //defaultDate: '2016-12-12',
            navLinks: false, // can click day/week names to navigate views
            editable: false,
            eventLimit: false, // allow "more" link when too many events
            events: [
                {
                    title: 'Mengisi usulan kebutuhan dr Spesialis oleh Rumah Sakit',
                    start: '2017-01-02'
                },
                {
                    title: 'Mengisi formulir peserta wajib kerja dr Spesialis',
                    start: '2017-01-02',
                    end: '2017-01-07'
                },
                {
                    id: 999,
                    title: 'Verifikasi Dinkes Kab/Kota',
                    start: '2017-01-08'
                },
                {
                    id: 999,
                    title: 'Verifikasi Dinkes Provinsi',
                    start: '2017-01-010'
                },
                {
                    title: 'Sosialisasi Portal WKS online',
                    start: '2017-01-03',
                    end: '2017-01-03'
                },
                {
                    title: 'Pendampingan Peserta WKS ke Daerah',
                    start: '2017-02-02',
                    end: '2017-02-09'
                },
                {
                    title: 'Pengembangan Aplikasi WKS Online oleh tim konsultan',
                    start: '2016-12-20'
                },
                {
                    title: 'Rapat koordinasi untuk mereview Aplikasi WKS online',
                    start: '2016-12-16'
                }
            ]
        });
        
    });

</script>
<style>

    #calendar_view {
        max-width: 900px;
        margin: 0 auto;
    }

</style>