<!DOCTYPE html>
<html lang="en-US">
    <?php include('include/head2.php');
?>

 <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
     <link href="assets/bootstrap-combined.min.css" rel="stylesheet">
    
    <style type="text/css">
        /* Removes the default 20px margin and creates some padding space for the indicators and controls */
.carousel {
    margin-bottom: 0;
	padding: 0 40px 30px 40px;
}
/* Reposition the controls slightly */
.carousel-control {
	left: -12px;
}
.carousel-control.right {
	right: -12px;
}
/* Changes the position of the indicators */
.carousel-indicators {
	right: 50%;
	top: auto;
	bottom: 0px;
	margin-right: -19px;
}
/* Changes the colour of the indicators */
.carousel-indicators li {
	background: #c0c0c0;
}
.carousel-indicators .active {
background: #333333;
}
    </style>
    <body>
        <div class="page-wrapper">
            <!--page-header-->
            <?php include('include/header.php');
?>
            <!--end page header-->
            <div id="page-content">
                <?php include('section/view_banner.php');
?>
                <div class="container" style="min-width: 1000px">
                    <!-- <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Publik</a></li>
                        <li class="active">Beranda</li>
                    </ol> -->
                    <hr class="sparator">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <?php include('section/view_article.php');
?>
                        </div>
                        <!--end col-md-9-->
                        
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->
            <?php include('include/footer.php');
?>
            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->
        <?php //include('include/js.php');
?>
<script type="text/javascript">
jQuery(document).ready(function() {
    jQuery('#myCarousel').carousel({
	    interval: 10000
	})
});
</script>

    </body>
