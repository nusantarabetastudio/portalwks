<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Kalendar Agenda</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <section class="page-title">
                                <h1>Kalendar Agenda</h1>
                            </section>
                            
                            <div id='calendar_event'></div>
                            
                        </div>
                        <!--end col-md-9-->

                      
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <?php include('include/footer.php');?>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    

<script>

    $(document).ready(function() {

        $('#calendar_event').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listMonth'
            },
            navLinks: true, // can click day/week names to navigate views
            businessHours: true, // display business hours
            editable: true,
            events: [
                {
                    title: 'Mengisi usulan kebutuhan dr Spesialis oleh Rumah Sakit',
                    start: '2017-01-04'
                },
                {
                    title: 'Mengisi formulir peserta wajib kerja dr Spesialis',
                    start: '2017-01-02',
                    end: '2017-01-06'
                },
                {
                    id: 999,
                    title: 'Verifikasi Dinkes Kab/Kota',
                    start: '2017-01-08'
                },
                {
                    id: 999,
                    title: 'Verifikasi Dinkes Provinsi',
                    start: '2017-01-010'
                },
            ]
        });
        
    });

</script>


</html>


