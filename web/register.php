<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Registrasi Ulang</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                            <section class="page-title">
                                <h1>Registrasi Ulang</h1>
                            </section
                            <!--end page-title-->
                            <section>
                                <form class="form inputs-underline">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="first_name">Nama Depan</label>
                                                <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First name">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-6-->
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Nama Belakang</label>
                                                <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last name">
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <!--end col-md-6-->
                                    </div>
                                    <!--enr row-->
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                                    </div>
                                    <!--end form-group-->
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                                    </div>
                                    <!--end form-group-->
                                    <div class="form-group">
                                        <label for="confirm_password">Konfirmasi Password</label>
                                        <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                                    </div>
                                    <!--end form-group-->
                                    <div class="form-group center">
                                        <button type="submit" class="btn btn-primary width-100">Daftarkan sekarang</button>
                                    </div>
                                    <!--end form-group-->
                                </form>

                                <hr>

                                <p class="center">Dengan mengkkllik tombol "Daftarkan sekarang" anda telah menerima <a href="terms-conditions.html">Terms & Conditions</a></p>
                            </section>
                        </div>
                        <!--col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


