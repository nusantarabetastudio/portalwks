<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Login</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                            <section class="page-title">
                                <h1>Login</h1>
                            </section>
                            <!--end page-title-->
                            <section>
                                <form class="form inputs-underline" action="http://localhost:8888/portalwks/login/process" method="post">
                                    <div class="form-group">
                                        <label for="email">Username</label>
                                        <input type="text" class="form-control" name="username" id="username" placeholder="Masukan username anda">
                                    </div>
                                    <!--end form-group-->
                                    <div class="form-group">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="password" placeholder="Masukan password anda">
                                    </div>
                                    <div class="form-group center">
                                        <button type="submit" class="btn btn-primary width-100">Submit</button>
                                    </div>
                                    <!--end form-group-->
                                </form>

                                <hr>

                                <a href="reset_password.php" >Lupa password?</a>
                            </section>
                        </div>
                        <!--col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


