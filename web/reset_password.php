<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Lupa password</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-md-offset-4 col-sm-offset-4">
                            <section class="page-title">
                                <h1>Lupa Password</h1>
                            </section>
                            <!--end page-title-->
                            <section>
                                <form class="form inputs-underline">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="email" class="form-control" name="email" id="email" placeholder="Masukan email anda">
                                    </div>
                                    <!--end form-group-->
                                    <div class="form-group center">
                                        <button type="submit" class="btn btn-primary width-100">Kirim email</button>
                                    </div>
                                    <!--end form-group-->
                                </form>
                                <!--end form-->
                            </section>
                        </div>
                        <!--col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque aliquam at neque sit amet vestibulum. <a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div>
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 Your Company, All right reserved</div>
                                <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


