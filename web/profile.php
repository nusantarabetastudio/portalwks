<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header_admin.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Profil</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            
                            <form class="form inputs-underline">
                                <section>
                                    <div class="user-details box">
                                        <div class="user-image">
                                            <div class="image">
                                                <div class="bg-transfer"><img src="assets/img/person-01.jpg" alt=""></div>
                                                <!--end bg-transfer-->
                                                <div class="single-file-input">
                                                    <input type="file" id="user_image" name="user_image">
                                                    <div>Upload foto<i class="fa fa-upload"></i></div>
                                                </div>
                                            </div>
                                            <!--end image-->

                                        </div>
                                        <!--end user-image-->
                                        <div class="description clearfix">
                                            <h3>Nama</h3>
                                            <h2>dr. Muhammad Amin Lubis</h2>
                                        <hr>
                                            <figure>
                                                <div class="pull-left"><strong>Terakhir diperbaharui:</strong></div>
                                                <div class="pull-right">24/12/2016</div>
                                            </figure>

                                        </div>
                                        <!--end description-->
                                        <br>
                                        <div class="box">
                                            <address>
                                                <strong>Alamat</strong>
                                                <figure>4758 Nancy Street</figure>
                                                <br>
                                                <strong>No Telp / HP</strong>
                                                <figure>+1 919-571-2528</figure>
                                                <br>
                                                <strong>Email</strong>
                                                <figure><a href="contact.html#">hello@example.com</a></figure>
                                                <br>
                                                <strong>Customer Care</strong>
                                                <figure><a href="contact.html#">support@example.com</a></figure>
                                            </address>
                                        </div>

                                    </div>
                                        

                                    <a href="edit_profile.php" class="btn btn-primary btn-rounded"><i class="fa fa-user"></i> Ubah Profil</a>
                                    <a href="edit_password.php" class="btn btn-primary btn-rounded"><i class="fa fa-key"></i> Ubah Password</a>
                                </section>
                                <!--end user-details-->
                                
                                <hr>
                            </form>

                            

                            
                        </div>
                        <!--end col-md-9-->

                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


