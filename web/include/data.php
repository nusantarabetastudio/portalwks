<?php

	if($_GET['modul'] == 'article'){
		if($_GET['id'] == 1){
			$modul_title = 'Kuliah Gratis, Lulusan Dokter Wajib Mengabdi';
			$modul_content = 'JAKARTA - Universitas Padjadjaran (Unpad) kembali membuat terobosan. Kali ini, Unpad berencana menggratiskan biaya pendidikan mahasiswa baru program studi Sarjana Pendidikan Dokter dan Pendidikan Dokter Spesialis di Fakultas Kedokteran. Kebijakan tersebut diambil salah satunya guna memenuhi kebutuhan tenaga dokter dan dokter spesialis di berbagai daerah, khususnya di Jawa Barat.';
			$modul_image = '1.jpg';
		}else if ($_GET['id'] == 2) {
			$modul_title = 'Para Pakar Kanker Berkumpul di Singapura Berbagi Ilmu';
			$modul_content = 'Liputan6.com, Singapura- Para dokter bedah onkologi, dokter radiasi onkologi serta para praktisi di bidang penanganan kanker dari Amerika Serikat, Kanada, dan negara-negara Asia berbagi ilmu dan pengalamannya dalam sebuah simposium bertajuk Minimally-Invasive Oncology: State-of-The-Art di Gleneagles Hospital, Singapura pada Sabtu, 21 November 2015.
				Salah satunya membicarakan tentang teknik robot surgery dalam tindakan operasi kasus kanker pankreas oleh asisstant professor of gastrointestinal surgical oncology division dari University of Pittsburgh, Amerika Serikat, Melissa E. Hogg.

				Lalu ada juga berbagi ilmu tentang penerapan perawatan pada pasien kanker liver menggunakan radiasi tepatnya Stereotactic Body Radiation Therapy (SBRT) di pada bagian hati dan pankreas oleh dokter Daniel Tan dari Asian American Radiation Oncology.

				Serta beberapa pakar lain berbagi teknik perawatan dalam melawan kanker.

				"Kami harap ide dan pandangan yang dibagikan dalam acara ini dari berbagai disiplin ilmu medis bemanfaat. Kami yakin simposium ini berdampak positif pada perawaran penyakit hati dan riset-riset lanjutan," tutur dokter bedah dari Asian American Liver Centre yang juga salah satu moderator acara ini, dokter Tan Kai Chah.

				Dalam acara ini hadir juga dokter-dokter dari dari beberapa daerah di Indonesia seperti Jakarta, Medan, Semarang.';
			$modul_image = 'pakar.jpg';
		}else{
			$modul_title = 'Tangani Korban Asap, Kemenkes Kirim 15 Dokter Spesialis';
			$modul_content = 'Jakarta - Kementerian Kesehatan mengirimkan 15 dokter spesialis dari Jakarta dan Medan ke Provinsi Riau guna mengantisipasi kabut asap akibat kebakaran lahan dan hutan. Sudah ada deretan nama korban jiwa, termasuk bayi, menjadi korban asap."Ada 3 rumah sakit yang mengirimkan tenaga dokter yaitu, RS Fatmawati Jakarta, RS M Jamil Padang, dan RS Adam Malik Medan," kata Koordinator Media Center Dinas Kesehatan Provinsi Riau, Rosita, di Pekanbaru, Minggu (11/10/2015).';
			$modul_image = 'korban_asap.jpg';
		}
	}

	
?>