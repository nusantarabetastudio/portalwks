<footer id="page-footer">
    <div class="footer-wrapper">
        <div class="block">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">
                    <div class="row">
       
        <!--end col-md-8-->
        <div class="col-md-12 col-sm-12">
            <div class="featured-contact">
                <i class="icon_comment_alt"></i>
                <h4>Ada pertanyaan seputar Program Wajib Kerja Dokter Spesialis?<br>Hubungi operator</h4>
                <h3> 021-7245517, 72797302</h3>
                <h4>Atau Melalui Form Email ini</h4>
            </div>
        </div>
    </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="first_name">Nama</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" value="Masukan Nama">
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="last_name">Email</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" value="Masukan Email">
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                            <!--end col-md-6-->
                            <div class="col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label for="last_name">Pertanyaan</label>
                                    <textarea class="form-control"> </textarea>
                                </div>
                                <!--end form-group-->
                            </div>
                            <div class="col-md-12 col-sm-12 text-right">
                               <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-rounded">Proses</button>
                                </div>
                                <!--end form-group-->
                            </div>
                            <!--end col-md-6-->
                          
                        </div>
                    </div>
                    <div class="element width-50 text-align-right">
                        <img src="assets/img/Screen Shot 2016-12-13 at 10.39.21 AM.png" width="520px" alt=""/>
                    </div>
                </div>
                <div class="background-wrapper">
                    <div class="bg-transfer opacity-50">
                        <img src="assets/img/footer-bg.png" alt="">
                    </div>
                </div>
                <!--end background-wrapper-->
            </div>
        </div>
        <div class="block">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">
                    </div>
                    <!-- <div class="element width-50 text-align-right">
                        <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                        <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                        <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                    </div> -->
                </div>
                <div class="background-wrapper">
                    <div class="bg-transfer opacity-50">
                        <img src="assets/img/footer-bg.png" alt="">
                    </div>
                </div>
                <!--end background-wrapper-->
            </div>
        </div>

        <div class="footer-navigation">
            <div class="container">
                <div class="vertical-aligned-elements">
                    <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                    <!-- <div class="element width-50 text-align-right">
                        <a href="index.html">Home</a>
                        <a href="listing-grid-right-sidebar.html">Listings</a>
                        <a href="submit.html">Submit Item</a>
                        <a href="contact.html">Contact</a>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
</footer>