<header id="page-header">
<style>
h1 { color: #38c8bb; font-family: 'Rouge Script', cursive; font-size: 30px; font-weight: bold; line-height: 48px;    }
</style>
    <nav>
        <div class="left">
            <a href="#" class="brand"> <h1> <img src="assets/img/logo.png" alt="" width="180"> Wajib Kerja Dokter Spesialis </h1></a>
        </div>
        <!--end left-->
        <div class="right">
            <div class="primary-nav has-mega-menu">
                <ul class="navigation">
                    <li class=""><a href="index.php">Beranda</a></li>
                    <li class=""><a href="information.php">Pengumuman</a></li>
                    <li class=""><a href="flow_register.php">Alur Pendaftaran</a></li>
                    
                </ul>
                <!--end navigation-->
            </div>
            <!--end primary-nav-->
            <div class="secondary-nav">
                <a href="login.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-lock"></i><span>Login</span></a>
                <a href="register.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-user"></i><span>Registrasi Ulang</span></a>
            </div>
            <!--end secondary-nav-->
            <div class="nav-btn">
                <i></i>
                <i></i>
                <i></i>
            </div>
            <!--end nav-btn-->
        </div>
        <!--end right-->
    </nav>
    <!--end nav-->
</header>