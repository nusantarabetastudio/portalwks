<!-- JS MAIN-->
<script type="text/javascript" src="assets/js/jquery-2.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-select.min.js"></script>
<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyBEDfNcQRmKQEyulDN8nGWjLYPm8s4YB58&amp;libraries=places"></script> -->
<script type="text/javascript" src="assets/js/richmarker-compiled.js"></script>
<script type="text/javascript" src="assets/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.nouislider.all.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

<script type="text/javascript" src="assets/js/jQuery.MultiFile.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>
<script type="text/javascript" src="assets/js/maps.js"></script>




<!-- datatables-->
<link rel="stylesheet" type="text/css" href="assets/datatable/media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="assets/datatable/examples/resources/syntax/shCore.css">

<script type="text/javascript" language="javascript" src="assets/datatable/media/js/jquery.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatable/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatable/examples/resources/syntax/shCore.js"></script>
<script type="text/javascript" language="javascript" src="assets/datatable/examples/resources/demo.js"></script>
<script type="text/javascript" language="javascript" class="init">


$(document).ready(function() {
	$('#example').DataTable({
		"bFilter" : false,               
		"bLengthChange": false,
		"bPaginate": false,
		"showNEntries" : false,
		"bInfo" : false,
		 "bSort": false
	});
} );


</script>

<!-- FULLCALENDAR-->
<link href='assets/fullcalendar-3.1.0/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/fullcalendar-3.1.0/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='assets/fullcalendar-3.1.0/lib/moment.min.js'></script>
<script src='assets/fullcalendar-3.1.0/lib/jquery.min.js'></script>
<script src='assets/fullcalendar-3.1.0/fullcalendar.min.js'></script>