<header id="page-header">
    <nav>
        <div class="left">
            <a href="#" class="brand"><h1><img src="assets/img/logo.png" alt="" width="180">  Portal WKS</h1></a>
        </div>
        <!--end left-->
        <div class="right">
            <div class="primary-nav has-mega-menu">
                <ul class="navigation">
                    <li class=""><a href="index.php">Website</a></li>
                    <li class="has-child"><a href="#">Daftar Form</a>
                        <div class="wrapper">
                            <div id="nav-listing" class="nav-wrapper">
                                <ul>
                                    <li class="has-child"><a href="#">Rumah Sakit</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="form_1.php">Form 1</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-child"><a href="#">Dinkes Kab/Kota</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="form_2.php">Form 2</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-child"><a href="#">Dinkes Provinsi</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="form_3.php">Form 3</a></li>
                                                <li><a href="form_instrumen.php">Instrumen Wajib Kerja Dokter Spesialis</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-child"><a href="#">Fak Kedokteran</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="form_4a.php">Form 4a</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-child"><a href="#">Kolegium</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="form_4b.php">Form 4b</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-child"><a href="#">Sekretariat KPDS</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="#">Form 7</a></li>
                                                <li><a href="#">Form 8</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="has-child"><a href="#">Peserta WKS</a>
                                        <div id="nav-grid-listing" class="nav-wrapper">
                                            <ul>
                                                <li><a href="#">Form 6</a></li>
                                                <li><a href="#">Surat Perjalanan Dinas (SPD)</a></li>
                                                <li><a href="#">Surat Pernyataan</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <li class="has-child"><a href="#">Pengaturan</a>
                            <div class="wrapper">
                                <div id="nav-locations" class="nav-wrapper">
                                    <ul>
                                        <li class="has-child"><a href="#">Master Data</a>
                                            <div class="nav-wrapper" id="nav-locations-new-york">
                                                <ul>
                                                    <li><a href="#">Prodi</a></li>
                                                    <li><a href="#">Jurusan</a></li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li><a href="#">Pengguna</a></li>
                                        <li><a href="#">Role</a></li>
                                        <li><a href="#">Hak Akses Menu</a></li>
                                    </ul>
                                </div>
                                <!--end nav-wrapper-->
                            </div>
                            <!--end wrapper-->
                        </li>
                    
                </ul>
                <!--end navigation-->
            </div>
            <!--end primary-nav-->
            <div class="secondary-nav">
                <a href="login.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-lock"></i><span>Logout</span></a>
                <a href="profile.php" class="btn btn-primary btn-small btn-rounded icon shadow add-listing"><i class="fa fa-user"></i><span>Peserta WKS</span></a>
            </div>
            <!--end secondary-nav-->
            <div class="nav-btn">
                <i></i>
                <i></i>
                <i></i>
            </div>
            <!--end nav-btn-->
        </div>
        <!--end right-->
    </nav>
    <!--end nav-->
</header>