<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header_admin.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Daftar Form</a></li>
                        <li class="active">Form 1</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">

                            <form class="form inputs-underline">
                                <section>
                                    <h3>Pencarian Data</h3>
                                    <div class="row">
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <label for="last_name">Nama Rumah Sakit</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Rumah Sakit--</option>
                                                    <option value="1">RSUD Depok</option>
                                                    <option value="1">RSUD Cipto Mangunkusumo</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <label for="last_name">Tahun</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Tahun--</option>
                                                    <option value="1">2015</option>
                                                    <option value="1">2016</option>
                                                    <option value="1">2017</option>
                                                    <option value="1">2018</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <label for="last_name">Semester</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Semester--</option>
                                                    <option value="1">Ganjil</option>
                                                    <option value="1">Genap</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-rounded btn-small">Pencarian</button>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                </section>
                            </form>

                            <h2 class="center">Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan Rumah Sakit</h2>

                            <table class="table table-striped table-bordered table-hover" style="font-size:12px">
                                        <thead>
                                            <tr>  
                                                <th class="center" rowspan="2" style="width: 50px">&nbsp;</th>
                                                <th class="center" rowspan="2">Jenis SDM Kesehatan<br>(Khusus dr. Spesialis dan Sub Spesialis)</th>
                                                <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                                                <th class="center" rowspan="2">SDMK Standar</th>
                                                <th class="center" rowspan="2" style="width: 120px">Kesenjangan<br>( 9 ) - ( 10 )</th>
                                                <th class="center" rowspan="2" style="width: 120px">Usulan Kebutuhan</th>
                                            </tr>

                                            <tr>
                                                <th class="center">PNS/<br>Pegawai Tetap</th>
                                                <th class="center">PPPK</th>
                                                <th class="center">PTT</th>
                                                <th class="center">Honorer/<br>Kontrak</th>
                                                <th class="center">BLU / BLUD</th>
                                                <th class="center">TKS</th>
                                                <th class="center">Total</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr>
                                                <td class="center">( 1 )</td>
                                                <td class="center">( 2 )</td>
                                                <td class="center">( 3 )</td>
                                                <td class="center">( 4 )</td>
                                                <td class="center">( 5 )</td>
                                                <td class="center">( 6 )</td>
                                                <td class="center">( 7 )</td>
                                                <td class="center">( 8 )</td>
                                                <td class="center">( 9 )</td>
                                                <td class="center">( 10 )</td>
                                                <td class="center">( 11 )</td>
                                                <td class="center">( 12 )</td>
                                            </tr>

                                            <tr>
                                                <td class="center"> 1. </td>
                                                <td> Dokter Umum </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 18 </td>
                                                <td class="center">
                                                    <div id="grade210100"> 
                                                        <span style="color:red"><strong>-18 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 2. </td>
                                                <td> Dokter Gigi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 4 </td>
                                                <td class="center">
                                                    <div id="grade210200"> 
                                                        <span style="color:red"><strong>-4 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 3. </td>
                                                <td> Spesialis Kesehatan Anak </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 6 </td>
                                                <td class="center">
                                                    <div id="grade210302"> 
                                                        <span style="color:red"><strong>-6 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 4. </td>
                                                <td> Spesialis Bedah </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 6 </td>
                                                <td class="center">
                                                    <div id="grade210303"> 
                                                        <span style="color:red"><strong>-6 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 5. </td>
                                                <td> Spesialis Penyakit Dalam </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 6 </td>
                                                <td class="center">
                                                    <div id="grade210301"> 
                                                        <span style="color:red"><strong>-6 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 6. </td>
                                                <td> Spesialis Obstetri dan Ginekolog </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 6 </td>
                                                <td class="center">
                                                    <div id="grade210304"> 
                                                        <span style="color:red"><strong>-6 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 7. </td>
                                                <td> Spesialis Anestesiologi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210401"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 8. </td>
                                                <td> Spesialis Radiologi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210402"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 9. </td>
                                                <td> Spesialis Patologi Klinik </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210403"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 10. </td>
                                                <td> Spesialis Rehabilitasi Medik </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210405"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 11. </td>
                                                <td> Spesialis Patologi Anatomi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210404"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 12. </td>
                                                <td> Spesialis THT </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210502"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 13. </td>
                                                <td> Spesialis Mata</td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210501"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 14. </td>
                                                <td> Spesialis Syaraf </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210503"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 15. </td>
                                                <td> Spesialis Jantung &amp; Pembuluh Darah </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210504"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 16. </td>
                                                <td> Spesialis Kulit &amp; Kelamin </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210505"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 17. </td>
                                                <td> Spesialis Kedokteran Jiwa </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210506"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 18. </td>
                                                <td> Spesialis Paru </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210507"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 19. </td>
                                                <td> Spesialis Orthopedi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210508"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 20. </td>
                                                <td> Spesialis Urologi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210509"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 21. </td>
                                                <td> Spesialis Bedah Syaraf </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210510"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 22. </td>
                                                <td> Spesialis Bedah Plastik </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210511"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 23. </td>
                                                <td> Spesialis Kedokteran Forensik </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 3 </td>
                                                <td class="center">
                                                    <div id="grade210512"> 
                                                        <span style="color:red"><strong>-3 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 24. </td>
                                                <td> Sub Spesialis Bedah </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210601"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 25. </td>
                                                <td> Sub Spesialis Penyakit Dalam </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210602"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 26. </td>
                                                <td> Sub Spesialis Kesehatan Anak </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210603"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 27. </td>
                                                <td> Sub Spesialis Obstetri &amp; Ginekolog </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210604"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 28. </td>
                                                <td> Sub Spesialis Mata </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210605"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 29. </td>
                                                <td> Sub Spesialis THT </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210606"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 30. </td>
                                                <td> Sub Spesialis Syaraf </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210607"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 31. </td>
                                                <td> Sub Spesialis Jantung &amp; Pembuluh Darah </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210608"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 32. </td>
                                                <td> Sub Spesialis Kulit &amp; Kelamin </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210609"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 33. </td>
                                                <td> Sub Spesialis Jiwa </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210610"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 34. </td>
                                                <td> Sub Spesialis Paru </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210611"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 35. </td>
                                                <td> Sub Spesialis Orthopedi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210612"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 36. </td>
                                                <td> Sub Spesialis Urologi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210613"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 37. </td>
                                                <td> Sub Spesialis Bedah Syaraf </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210614"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 38. </td>
                                                <td> Sub Spesialis Bedah Plastik </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210615"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 39. </td>
                                                <td> Sub Spesialis Gigi Mulut </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 2 </td>
                                                <td class="center">
                                                    <div id="grade210616"> 
                                                        <span style="color:red"><strong>-2 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 40. </td>
                                                <td> Spesialis Bedah Mulut </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210701"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 41. </td>
                                                <td> Spesialis Konservasi/ Endodonsi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210702"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 42. </td>
                                                <td> Spesialis Periodonti </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210703"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 43. </td>
                                                <td> Spesialis Orthodonti </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210704"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 44. </td>
                                                <td> Spesialis Prosthodonti </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210705"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 45. </td>
                                                <td> Spesialis Pedodonsi </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210706"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>
                                            <tr>
                                                <td class="center"> 46. </td>
                                                <td> Spesialis Penyakit Mulut </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 0 </td>
                                                <td class="center"> 1 </td>
                                                <td class="center">
                                                    <div id="grade210707"> 
                                                        <span style="color:red"><strong>-1 ( Kurang ) </strong></span>                          </div>
                                                </td>
                                                <td class="center"> <input type="text" style="width:70px"> </td>
                                            </tr>


                                        </tbody>
                                    </table>
                            <br>
                            <p>
                                <i>Mengetahui : Kepala Rumah Sakit</i>
                            </p>
                            <br>
                            <br>
                            <section class="center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Proses Rekapitulasi</button>
                                        <button type="submit" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                                    </div>
                                    <!--end form-group-->
                                </section>
                            
                        </div>
                        <!--end col-md-9-->

                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


