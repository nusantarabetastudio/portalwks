<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li class="active">Berita</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            
                            <?php include('section/view_article.php');?>
                            
                        </div>
                        <!--end col-md-9-->

                        <div class="col-md-4 col-sm-4">
                            <aside class="sidebar">
                                
                                <?php include('section/view_search.php');?>

                                <?php include('section/view_recent_news.php');?>

                                <?php include('section/view_information.php');?>

                            </aside>
                            <!--end sidebar-->
                        </div>
                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <?php include('include/footer.php');?>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


