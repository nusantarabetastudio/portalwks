<!DOCTYPE html>

<html lang="en-US">

    <?php include('include/head.php');?>

    <body>

        <div class="page-wrapper">
            <!--page-header-->
            
            <?php include('include/header_admin.php');?>
            
            <!--end page header-->

            <div id="page-content">

                <div class="container">

                    <ol class="breadcrumb">
                        <li><a href="#">Portal WKS</a></li>
                        <li><a href="#">Daftar Form</a></li>
                        <li class="active">Form 2</li>
                    </ol>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">

                            <form class="form inputs-underline">
                                <section>
                                    <h3>Pencarian Data</h3>
                                    <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Provinsi</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Provinsi--</option>
                                                    <option value="1">RSUD Depok</option>
                                                    <option value="1">RSUD Cipto Mangunkusumo</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <label for="last_name">Kab/Kota</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Kab/Kota--</option>
                                                    <option value="1">2015</option>
                                                    <option value="1">2016</option>
                                                    <option value="1">2017</option>
                                                    <option value="1">2018</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>

                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <label for="last_name">Nama Rumah Sakit</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Rumah Sakit--</option>
                                                    <option value="1">RSUD Depok</option>
                                                    <option value="1">RSUD Cipto Mangunkusumo</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <label for="last_name">Tahun</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Tahun--</option>
                                                    <option value="1">2015</option>
                                                    <option value="1">2016</option>
                                                    <option value="1">2017</option>
                                                    <option value="1">2018</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <label for="last_name">Semester</label>
                                                <select class="form-control" name="jurusan">
                                                    <option value="">--Pilih Semester--</option>
                                                    <option value="1">Ganjil</option>
                                                    <option value="1">Genap</option>
                                                </select>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-rounded btn-small">Pencarian</button>
                                            </div>
                                            <!--end form-group-->
                                        </div>
                                </section>
                            </form>

                            <h2 class="center">Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan Kab/Kota</h2>

                            <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%" style="font-size:12px">
                                <thead>
                                    <tr>
                                        <th width="30px">No</th>
                                        <th>Nama RS</th>
                                        <th>Jenis SDM Kesehatan <br> (Khusus dr. Spesialis dan Sub Spesialis)</th>
                                        <th width="60px">Jumlah<br>saat ini</th>
                                        <th width="60px">Jumlah<br>Standar</th>
                                        <th width="60px">Kelebihan/<br>Kekurangan</th>
                                        <th width="60px">Usulan<br>Institusi</th>
                                        <th width="60px">Verifikasi<br>Kab/Kota</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <tr>
                                        <td class="center" rowspan="3">1</td>
                                        <td rowspan="3">RSUD Depok</td>
                                    </tr>
                                    <?php for($i=1;$i<3;$i++):?>
                                    <tr>
                                        <td>Dokter Spesilais Kulit</td>
                                        <td class="center">12</td>
                                        <td class="center">20</td>
                                        <td class="center">8</td>
                                        <td class="center">8</td>
                                        <td class="center"><input type="checkbox" class="form-control"></td>
                                    </tr>
                                <?php endfor;?>
                                </tbody>
                            </table>
                            <br>
                            <p>
                                <i>Mengetahui : Kepala Dinkes Kab/Kota</i>
                            </p>
                            <br>
                            <br>
                            <section class="center">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Proses Rekapitulasi</button>
                                        <button type="submit" class="btn btn-default"><i class="fa fa-print"></i> Print</button>
                                    </div>
                                    <!--end form-group-->
                                </section>
                            
                        </div>
                        <!--end col-md-9-->

                        <!--end col-md-4-->
                    </div>
                    <!--end row-->
                </div>
                <!--end container-->
            </div>
            <!--end page-content-->

            <footer id="page-footer">
                <div class="footer-wrapper">
                    
                    <div class="block">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">
                                    <p data-toggle="modal" data-target="#myModal"><a href="blog.html#">Terms of Use</a> and <a href="blog.html#">Privacy Policy</a>.</p>
                                </div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="blog.html#" class="circle-icon"><i class="social_twitter"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_facebook"></i></a>
                                    <a href="blog.html#" class="circle-icon"><i class="social_youtube"></i></a>
                                </div> -->
                            </div>
                            <div class="background-wrapper">
                                <div class="bg-transfer opacity-50">
                                    <img src="assets/img/footer-bg.png" alt="">
                                </div>
                            </div>
                            <!--end background-wrapper-->
                        </div>
                    </div>

                    <div class="footer-navigation">
                        <div class="container">
                            <div class="vertical-aligned-elements">
                                <div class="element width-50">(C) 2016 BPPSDMK, Kementerian Kesehatan RI</div>
                                <!-- <div class="element width-50 text-align-right">
                                    <a href="index.html">Home</a>
                                    <a href="listing-grid-right-sidebar.html">Listings</a>
                                    <a href="submit.html">Submit Item</a>
                                    <a href="contact.html">Contact</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </footer>

            <!--end page-footer-->
        </div>
        <!--end page-wrapper-->

        <?php include('include/js.php');?>

    </body>
    
</html>


