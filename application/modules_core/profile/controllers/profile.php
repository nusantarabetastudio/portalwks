<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('master');
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->library('upload_file');
		$this->load->model('profile_model', 'profile');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Profil '.$this->session->userdata('data_user')->fullname.'', 'form_wks');
	}

	public function index()
	{
		//$this->breadcrumbs->push('Form 1', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKDS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['profile'] = $this->profile->get_profile_by_session($this->session->userdata('user_id'));
		$this->authuser->write_log();
		$this->load->view('index', $data);
	}

	public function detail()
	{
		$this->breadcrumbs->push('Detil profil peserta WKDS', ''.strtolower(get_class($this)).'/detail');
		$data['title'] = "Form WKDS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['profil'] = $this->rs->get_by_kode_rs($this->session->userdata('data_user')->kode_user);
		$this->authuser->write_log();
		$this->load->view('form', $data);
	}


}
