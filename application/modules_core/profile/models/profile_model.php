<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function get_profile_by_session($id_user)
	{
		$query = $this->db->get_where('v_reg_mahasiswa', array('id_user' => $id_user)); 
		//print_r($this->db->last_query());die;
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

}
