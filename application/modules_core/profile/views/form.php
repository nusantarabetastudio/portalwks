<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-editable.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />

<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>

<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">

              <!-- <div id="user-profile-1" class="user-profile row">
                    <div class="col-xs-12 col-sm-3 center">
                      <div>
                        <!-- #section:pages/profile.picture
                        <span class="profile-picture">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar"/>
                        </span>
                      </div>
                    </div>
              </div> -->

              <form class="form-horizontal" method="post" id="form_member" action="<?php echo site_url('kepegawaian/member/ajax_add')?>" enctype="multipart/form-data">
                <br>
                <div class="col-md-8">
                  <div class="form-group">
                    <label class="control-label col-md-3">Upload Foto</label>
                    <div class="col-md-4">
                      <input name="fupload" id="path_thumbnail" placeholder="Foto Profile" class="form-control" type="file">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Nama Lengkap</label>
                    <div class="col-md-6">
                      <input name="ktp_nama_lengkap" id="ktp_nama_lengkap" value="<?php echo isset($value)?$value->ktp_nama_lengkap:''?>" placeholder="" class="form-control" type="text">
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">No.KTP</label>
                    <div class="col-md-3">
                      <input name="ktp_nik" id="ktp_nik" value="<?php echo isset($value)?$value->ktp_nik:''?>" placeholder="" class="form-control" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Tempat Lahir</label>
                    <div class="col-md-3">
                      <input name="ktp_tempat_lahir" id="ktp_tempat_lahir" value="<?php echo isset($value)?$value->ktp_tempat_lahir:''?>" placeholder="" class="form-control" type="text">
                    </div>
                    <label for="id-date-picker-1" class="control-label col-md-2">Tanggal Lahir</label>
                    <div class="col-md-3">
                      <div class="input-group">
                        <input class="form-control date-picker" id="ktp_tanggal_lahir" name="ktp_tanggal_lahir" type="text" data-date-format="dd-mm-yyyy" value="<?php echo isset($value)?Tanggal::fieldDate($value->ktp_tanggal_lahir):''?>" />
                        <span class="input-group-addon">
                          <i class="fa fa-calendar bigger-110"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Jenis Kelamin</label>
                    <div class="col-md-9">
                      <div class="radio">
                            <label>
                              <input name="jk" type="radio" class="ace" value="L" <?php echo isset($value) ? ($value->ktp_jk == 'L') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                              <span class="lbl"> Laki-laki</span>
                            </label>
                            <label>
                              <input name="jk" type="radio" class="ace" value="P" <?php echo isset($value) ? ($value->ktp_jk == 'P') ? 'checked="checked"' : '' : ''; ?>/>
                              <span class="lbl">Perempuan</span>
                            </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div>
                      <!-- #section:pages/profile.picture -->
                      <span class="profile-picture">
                          <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="<?php echo base_url()?>assets/avatars/profile-pic.jpg" />
                      </span>
                  </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">&nbsp;</label>
                  <div class="col-md-6">
                    &nbsp;
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Alamat</label>
                  <div class="col-md-6">
                    <input name="ktp_alamat" id="ktp_alamat" value="<?php echo isset($value)?$value->ktp_alamat:''?>" placeholder="" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group" id="form-provinsi">
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5','6')) ) ? 'readonly' : '' ;  
                      echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
                  </div>

                  <label class="control-label col-md-1">Kabupaten</label>
                  <div class="col-md-3">
                    <?php 
                      $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5')) ) ? 'readonly' : '' ;  
                      echo Master::get_change_master_kabupaten(isset($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
                  </div>
                </div>


                <div class="form-group">
                  <label class="control-label col-md-2">RT</label>
                  <div class="col-md-1">
                    <input name="ktp_rt" id="ktp_rt" value="<?php echo isset($value)?$value->ktp_rt:''?>" placeholder="" class="form-control" type="text">
                  </div>
                  <label class="control-label col-md-2">RW</label>
                  <div class="col-md-1">
                    <input name="ktp_rw" id="ktp_rw" value="<?php echo isset($value)?$value->ktp_rw:''?>" placeholder="" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Marital</label>
                  <div class="col-md-2">
                    <?php echo Master::get_master_custom(array('table'=>'m_marital_status', 'where'=>array('active'=>'Y'), 'id'=>'ms_id', 'name'=>'ms_name'), isset($value->ms_id)?$value->ms_id:'','ms_id','ms_id', 'form-control','required','inline')?>
                  </div>
                  <label class="control-label col-md-2">Agama</label>
                  <div class="col-md-2">
                    <?php echo Master::get_master_custom(array('table'=>'m_religion', 'where'=>array('active'=>'Y'), 'id'=>'religion_id', 'name'=>'religion_name'), isset($value->religion_id)?$value->religion_id:'','religion_id','religion_id', 'form-control','required','inline')?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Pekerjaan</label>
                  <div class="col-md-2">
                    <?php echo Master::get_master_custom(array('table'=>'m_job', 'where'=>array('active'=>'Y'), 'id'=>'job_id', 'name'=>'job_name'), isset($value->job_id)?$value->job_id:'','job_id','job_id', 'form-control','required','inline')?>
                  </div>
                  <label class="control-label col-md-2">Gol.Darah</label>
                  <div class="col-md-2">
                    <?php echo Master::get_master_custom(array('table'=>'m_type_blood', 'where'=>array('active'=>'Y'), 'id'=>'tb_id', 'name'=>'tb_name'), isset($value->tb_id)?$value->tb_id:'','tb_id','tb_id', 'form-control','required','inline')?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kewarganegaraan</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="kewarganegaraan" type="radio" class="ace" value="WNI" <?php echo isset($value) ? ($value->ktp_kewarganegaraan == 'WNI') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> WNI</span>
                          </label>
                          <label>
                            <input name="kewarganegaraan" type="radio" class="ace" value="WNA" <?php echo isset($value) ? ($value->ktp_kewarganegaraan == 'WNA') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">WNA</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->gr_id:0?>"> -->

                  <a onclick="getMenu('profile')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Proses
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/profile.js'?>"></script>