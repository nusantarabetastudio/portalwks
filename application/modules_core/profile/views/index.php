<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->
<style type="text/css">
label.error { color:red; }
.input {text-align: center; margin-top: -10px; margin-bottom: -10px}
.warning_file {color:red; font-weight:bold; padding-left:15px}
.td-upload {width:230px !important}
</style>
<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
          <div id="user-profile-1" class="user-profile row">
            <div class="col-xs-12 col-sm-3 center">
                <div>
                    <!-- #section:pages/profile.picture -->
                    <span class="profile-picture">
                        <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="<?php echo base_url()?>assets/avatars/profile-pic.jpg" />
                    </span>

                    <!-- /section:pages/profile.picture -->
                    <div class="space-4"></div>

                    <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                        <div class="inline position-relative">
                            <a href="#" onclick="detail(1)" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                                <span class="white">dr. Muhammad Amin Lubis Sp.Og</span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="space-6"></div>

                <div class="profile-contact-info">
                    <div class="profile-contact-links align-left">
                        <a href="<?php echo base_url().'uploaded_files/file/SPPD.xlsx'?>" class="btn btn-link">
                            <i class="ace-icon fa fa-download bigger-120 green"></i>
                            Download SPPD
                        </a>
                        <br>
                        <a href="<?php echo base_url().'uploaded_files/file/PAKTA_INTEGRITAS.xlsx'?>" class="btn btn-link">
                            <i class="ace-icon fa fa-download bigger-120 green"></i>
                            Download Pakta Integritas
                        </a>
                    </div>

                    <div class="space-6"></div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-9">
                <div class="tabbable">
                    <ul class="nav nav-tabs" id="myTab">
                        <li class="active">
                            <a data-toggle="tab" href="#profile">
                                <i class="green ace-icon fa fa-user bigger-120"></i>
                                Profil Peserta
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#lampiran">
                                <i class="red ace-icon fa fa-file bigger-120"></i>
                                Lampiran Dokumen
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#akun">
                                <i class="pink ace-icon fa fa-key bigger-120"></i>
                                Ubah Akun
                            </a>
                        </li>

                        <li>
                            <a data-toggle="tab" href="#info">
                                <i class="blue ace-icon fa fa-info bigger-120"></i>
                                Informasi Umum
                            </a>
                        </li>

                    </ul>

                    <div class="tab-content">

                        <div id="profile" class="tab-pane fade in active">
                            <h3 class="header smaller lighter blue">
                                Profil Peserta WKDS
                            </h3>

                            <div class="widget-box transparent">
                                <div class="widget-body">
                                    <div class="widget-main padding-8">
                                        <!-- #section:pages/profile.feed -->
                                        <div class="profile-user-info profile-user-info-striped">

                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Nama Lengkap</div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"> <?php echo $profile->mp_nama_mhs?> </span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> NIM </div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"> <?php echo $profile->mp_nim_mhs?> </span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Asal Fakultas </div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"> <?php echo $profile->fk_name?> </span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Prodi </div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"> <?php echo $profile->prod_name?> </span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> No HP </div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"> <?php echo $profile->mp_no_telp?> </span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> Email </div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"> <?php echo $profile->mp_email?> </span>
                                                </div>
                                            </div>
                                            <div class="profile-info-row">
                                                <div class="profile-info-name"> &nbsp; </div>

                                                <div class="profile-info-value">
                                                    <span class="editable" id="username"><a href="#" class="btn btn-block btn-primary" onclick="detail(1)"> <i class="fa fa-edit"></i> Ubah profil </a> </span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div id="lampiran" class="tab-pane fade">
                            <h3 class="header smaller lighter blue">
                                File Lampiran
                            </h3>

                            <p class="warning_file"><i> * Lampiran dibawah ini harap dilengkapi sebagai syarat untuk mengikuti program Wajib Kerja Dokter Spesialis</i></p>
                            <form class="form-horizontal">
                                
                                <div class="widget-box transparent">
                                    <div class="widget-body">
                                        <div class="widget-main padding-8">
                                            <!-- #section:pages/profile.feed -->
                                            <div class="profile-user-info profile-user-info-striped">

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"><i class="fa fa-check green"></i> Pas Foto </div>
                                                    <div class="profile-info-value">
                                                        <input type="file" name="">
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"><i class="fa fa-check green"></i> KTP/SIM/PASSPOR </div>
                                                    <div class="profile-info-value">
                                                        <input type="file" name="">
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"><i class="fa fa-check green"></i> Hal Depan Buku Tabungan</div>
                                                    <div class="profile-info-value">
                                                        <input type="file" name="">
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"><i class="fa fa-check green"></i> Ijasah/Surat Keterangan Lulus</div>
                                                    <div class="profile-info-value">
                                                        <input type="file" name="">
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"><i class="fa fa-check green"></i> Pakta Intergitas</div>
                                                    <div class="profile-info-value">
                                                        <input type="file" name="">
                                                    </div>
                                                </div>
                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"><i class="fa fa-check green"></i> SPPD</div>
                                                    <div class="profile-info-value">
                                                        <input type="file" name="">
                                                    </div>
                                                </div>

                                                <div class="profile-info-row">
                                                    <div class="profile-info-name td-upload"> &nbsp; </div>

                                                    <div class="profile-info-value">
                                                        <span class="editable" id="username"><a href="#" class="btn btn-block btn-primary" onclick="detail(1)"> <i class="fa fa-upload"></i> Upload File </a> </span>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </form>
                        </div>

                        <div id="akun" class="tab-pane fade">
                            <h3 class="header smaller lighter blue">
                                Form Ubah Akun Pengguna
                            </h3>
                            <div class="widget-body">
                                <div class="widget-main no-padding">
                                  <form class="form-horizontal" method="post" id="form_user" action="<?php echo site_url('perencanaan/ajax_add')?>">
                                    <br>

                                    <div class="form-group">
                                      <label class="control-label col-md-2">Nama Pengguna</label>
                                      <div class="col-md-6">
                                        <input name="fullname" id="fullname" value="<?php echo isset($value)?$value->fullname:''?>" placeholder="Nama Pengguna" class="form-control" type="text">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-2">Username/Email</label>
                                      <div class="col-md-4">
                                        <input name="email" id="email" placeholder="Email" value="<?php echo isset($value)?$value->email:''?>" class="form-control" type="text">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label col-md-2">Password</label>
                                      <div class="col-md-3">
                                        <input name="password" id="password" placeholder="Password" value="<?php echo isset($value)?Encryption::decrypt_password_callback($value->password, SECURITY_KEY):''?>" class="form-control" type="password">
                                      </div>
                                    </div>

                                    <div class="form-group">
                                      <label class="control-label col-md-2">Konfirmasi password</label>
                                      <div class="col-md-3">
                                        <input name="confirm_password" id="confirm_password" placeholder="Konfirmasi password" class="form-control" type="password">
                                      </div>
                                    </div>

                                    <div class="form-actions center">
                                      <button type="submit" id="btnSave" name="submit" class="btn btn-block btn-info">
                                        <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                                        Proses ubah akun
                                      </button>
                                    </div>
                                  </form>
                                </div>
                              </div>

                        </div>
                        
                        <div id="info" class="tab-pane fade">
                            <h3 class="header smaller lighter blue">
                                Informasi Umum
                            </h3>
                            <p>Informasi Umum</p>
                        </div>

                    </div>
                </div>

        </div>

        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url().'assets/js/custom/profile.js'?>"></script>
<script type="text/javascript">

    function addBase(t) {
        var rs = '<div>' +
                    '<?php 
                        echo trim(preg_replace('/\r\n|\r|\n/', ' ', Master::get_master_custom(
                            array('table'=>'m_type_doc','where'=>array('active'=>'Y'), 'name'=>'td_name', 'id'=>'td_id'), '' ,'td_id[]','td_id','span12','style="height: 34px; width: auto; float:left; margin-right:5px"','inline')));?>'+
                            '<input type="file" style="width:300px; float:left; margin-right: 5px" class="form-control" id="form-field-icon-1" />' +
                    '<a href="javascript:void(0)" onclick="minBase(this)" style="line-height: 19px; margin-top: -2px;" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></a>'+
                '</div><br>';
        $('#dasar-x').append(rs);
    }

    function minBase(t) {
        $(t).parent().remove();
    }

</script>