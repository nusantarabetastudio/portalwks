<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_8 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	public function index()
	{
		$this->breadcrumbs->push('Form 8', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->authuser->write_log();
		$this->load->view('form_8/index', $data);
	}

	public function searchDataForm()
	{

		$standar_existing = $this->integration->getStandarExisting($_POST);
		echo json_encode($standar_existing);

	}

	public function show_data_search($idp)
	{
		$data = array(
			'id_perencanaan' => $idp,
		);
		//$standar_existing = $this->integration->getStandarExisting($_POST);
		$this->load->view('form_8/view_data_search', $data);

	}

	public function ajax_list_skm($id)
	{

		$params = array(
			'id_perencanaan' => $id,
			'search' => $_POST['search']['value'],
			'start' => $_POST['start'],
			'length' => $_POST['length'],
			'draw' => $_POST['draw'],
		);
		$list = $this->integration->getSKM($params);
		
		echo json_encode($list);
	}
	
}
