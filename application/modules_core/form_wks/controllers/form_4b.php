<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_4b extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('apps');
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->model('form_wks_model', 'form_wks');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	public function index()
	{
		$this->breadcrumbs->push('Form 4b (Prediksi Kelulusan Mahasiswa)', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->authuser->write_log();
		$this->load->view('form_4b/index', $data);
	}

	public function verifikasi($id)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'V');
		$this->breadcrumbs->push('Form 4b (Prediksi Kelulusan Mahasiswa)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Selengkapnya', 'form_wks/'.strtolower(get_class($this)).'/verifikasi/'.$id);
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getDataMahasiswaPesertaNoTbl2(array('plm_id'=>$id));
		$data['note'] = $this->form_wks->getNoteVerifikasi($id);
		/*echo '<pre>';print_r($data['value']);die;*/
		$this->authuser->write_log();
		$this->load->view('form_4b/form_verifikasi', $data);
	}

	public function printPreview($id, $type='')
	{
		$data['value'] = $this->form_wks->getDataMahasiswaPesertaNoTbl2(array('plm_id'=>$id));
		$data['type'] = $type;
		$data['id'] = $id;
		/*echo '<pre>'; print_r($data['value']);die;*/
		$this->authuser->write_log();
		$this->load->view('form_4b/print_preview', $data);
	}

	public function getPeriodeKelulusanMhs()
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '4b';
		$list = $this->form_wks->getPeriodeKelulusanMhs($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			$no++;
			$status_verifikasi = $this->form_wks->getStatusVerifikasi($uk->app_kolegium);
			$semester = ($uk->plm_semester==1)?'Ganjil':'Genap';
			$btn = '<a class="btn btn-xs btn-primary" href="javascript:void()" title="Selengkapnya" onclick="verifikasi('."'".$this->regex->_genRegex($uk->plm_id,'RGXINT')."'".')"><i class="fa fa-share-alt"></i> Selengkapnya </a>';

			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '<div class="center">[ '.$uk->plm_id.' ]</div>';
			$row[] = strtoupper($uk->fk_name);
			$row[] = '<div class="center">'.$uk->plm_tahun.'</div>';
			$row[] = '<div class="center">'.$this->tanggal->getBulan($uk->plm_bulan).'</div>';
			$row[] = '<div class="center">'.$semester.'</div>';
			$row[] = strtoupper($uk->nama_provinsi);
			/*$row[] = strtoupper($uk->nama_kabupaten);*/
			$row[] = '<div class="center">'.$uk->total_mhs.'</div>';
			$row[] = '<div class="center">'.$uk->total_tubel.'</div>';
			$row[] = '<div class="center">'.$uk->total_mandiri.'</div>';
			$row[] = '<div class="center">'.$uk->total_mhs_disetujui.'</div>';
			$row[] = '<div class="center">'.$status_verifikasi.'</div>';
			//add html for action
			$row[] = '<div class="center">'.$btn.'</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->plmcount_all($params),
						"recordsFiltered" => $this->form_wks->plmcount_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function prosesVerifikasiKolegium()
	{
		/*print_r($_POST);die;*/
		$this->db->trans_begin();
		$checked = $this->input->post('checked');
		if( count($checked) > 0 ) :
			foreach ($checked as $key => $value) {
				$this->db->update('t_detail_mhs_lulus', array('dmp_app_kolegium'=>$value), array('dmp_id'=>$key));
			}
			$this->db->update('t_periode_lulus_mhs', array('app_kolegium'=> 'Y'), array('plm_id'=>$this->input->post('plm_id')));
		endif;


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	
}
