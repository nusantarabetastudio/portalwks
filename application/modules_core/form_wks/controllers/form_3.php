<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_3 extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('apps');
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->model('form_wks_model', 'form_wks');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	// ================= LOAD ALL VIEW / FORM =================== \\
	public function index()
	{
		$this->breadcrumbs->push('Form 3 (Verifikasi Dinkes Provinsi)', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->authuser->write_log();
		$this->load->view('form_3/index', $data);
	}

	public function verifikasi($id)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'V');
		$this->breadcrumbs->push('Form 3 (Verifikasi Dinkes Provinsi)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Verifikasi', 'form_wks/'.strtolower(get_class($this)).'/verifikasi/'.$id);
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getUsulanById($id);
		$data['note'] = $this->form_wks->getNoteVerifikasi($id);
		$data['file'] = $this->upload_file->getUploadedFile(array('ref_id'=>$id, 'ref_table' => 't_usulan_kebutuhan', 'attc_owner' => 'Dinkes Prov'));
		/*echo '<pre>';print_r($data['value']);die;*/
		$this->authuser->write_log();
		$this->load->view('form_3/form_verifikasi', $data);
	}

	public function sendToVerifikator($id, $flag)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
		$this->breadcrumbs->push('Form 3 (Usulan Kebutuhan RS)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Verifikasi', 'form_wks/'.strtolower(get_class($this)).'/verifikasi/'.$id);
		$this->breadcrumbs->push('Persetujuan dan upload file lampiran', 'form_wks/'.strtolower(get_class($this)).'/sendToVerifikator/'.$id);
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getUsulanById($id);
		$data['note'] = $this->form_wks->getNoteVerifikasi($id);
		$data['flag'] = $flag;
		/*echo '<pre>';print_r($data['value']);die;*/
		$this->authuser->write_log();
		$this->load->view('form_3/form_upload_file', $data);
	}

	// ============================ LOAD DATA JSON =========================== \\
	public function getDataUsulan()
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = 3;
		$list = $this->form_wks->getDataUsulan($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			/*$status = $this->form_wks->getStatusVerifikasi($uk->status);*/

			if($uk->status == 3){

				if($uk->verifikasi_dinkes_prov == 'Y'){
					$btn = '<a class="btn btn-xs btn-warning" href="javascript:void()" title="Persetujuan Dinkes Provinsi" onclick="verifikasi('."'".$this->regex->_genRegex($uk->uk_id,'RGXINT')."'".')"> <i class="fa fa-edit"></i> </a>';
				}else{
					$btn = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Verifikasi" onclick="verifikasi('."'".$this->regex->_genRegex($uk->uk_id,'RGXINT')."'".')"> <i class="fa fa-caret-square-o-right"></i> </a> ';
				}
				
			}else if($uk->status == 4){
				$btn = '<a class="btn btn-xs btn-primary" href="javascript:void()" title="Selengkapnya" onclick="verifikasi('."'".$this->regex->_genRegex($uk->uk_id,'RGXINT')."'".')"> <i class="fa fa-eye"></i> </a>';
			}else{
				$btn = '-';
			}

			$status = ($uk->verifikasi_dinkes_prov == 'Y') ? '<i class="fa fa-check green"></i>' : '<i class="fa fa-times red"></i>';
			$app = ($uk->approval_prov == 'Y') ? '<i class="fa fa-check green"></i>' : '<i class="fa fa-times red"></i>';
			
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '<div class="center">[ '.$uk->uk_id.' ]</div>';
			$row[] = '<div class="center">'.$uk->tahun.'</div>';
			$row[] = $uk->nama_kabupaten;
			$row[] = strtoupper($uk->nama_rs);
			$row[] = '<div class="center">'.$uk->total_jenis_sdmk.'</div>';
			$row[] = '<div class="center">'.$uk->total_saat_ini.'</div>';
			$row[] = '<div class="center">'.$this->apps->get_format($uk->total_kesenjangan_all).'</div>';
			$row[] = '<div class="center">'.$uk->total_usulan_kebutuhan.'</div>';
			$row[] = '<div class="center">'.$uk->total_disetujui.'</div>';
			$row[] = '<div class="center">'.$uk->total_disetujui_prov.'</div>';
			$row[] = '<div class="center">'.$status.'</div>';
			$row[] = '<div class="center">'.$app.'</div>';
			
			//add html for action
			$row[] = '<div class="center">'.$btn.'</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->count_all($params),
						"recordsFiltered" => $this->form_wks->count_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}


	// ============================= ALL PROCESS =========================== \\
	public function prosesVerifikasiDinkesProv()
	{
		/*print_r($_POST);die;*/
		$this->db->trans_begin();

		$uk_id = $this->input->post('uk_id');
		$checked = $this->input->post('checked');
		$note = $this->input->post('note');

		if( $note ){
			foreach ($uk_id as $valid) {
				$data_note = array(
					'uk_id' => $valid,
					'nv_description' => $this->input->post('note'),
					'created_date' => date('Y-m-d H:i:s'),
					'created_by' => $this->session->userdata('data_user')->fullname,
				);
				$this->db->insert('t_note_verifikasi', $data_note);
			}
		}

		if( count($checked) > 0 ) :

			foreach ($uk_id as $valid2) {
				$this->db->update('t_detail_usulan_kebutuhan', array('app_dinkes_prov'=>'N'), array('uk_id'=>$valid2));
				$this->db->update('t_usulan_kebutuhan', array('verifikasi_dinkes_prov'=>'Y', 'ver_dp_date' => date('Y-m-d H:i:s'), 'ver_dp_by' => $this->session->userdata('data_user')->fullname), array('uk_id'=>$valid2));
			}


			foreach ($checked as $key => $value) {
				$total_value = ($this->input->post('disetujui_prov_'.$key.''))?$this->input->post('disetujui_prov_'.$key.''):0;
				$this->db->update('t_detail_usulan_kebutuhan', array('app_dinkes_prov'=>$value, 'jml_disetujui_prov'=>$total_value), array('duk_id'=>$key));
			}

		endif;
		
		/*
		if( count($checked) > 0 ) :

			foreach ($uk_id as $valid3) {
				$this->db->update('t_detail_usulan_kebutuhan', array('app_dinkes_prov'=>'N'), array('uk_id'=>$valid3));
				$this->db->update('t_usulan_kebutuhan', array('verifikasi_dinkes_prov'=>'Y'), array('uk_id'=>$valid3));
			}

			foreach ($checked as $key => $value) {
				$this->db->update('t_detail_usulan_kebutuhan', array('app_dinkes_prov'=>$value), array('duk_id'=>$key));
			}

		endif;*/


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function prosesSendToDinkesProv($id)
	{
		$this->db->trans_begin();

		//$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'S');

		$this->form_wks->sendToVerifikator(array('workflow'=>4, 'id'=>$id));
		$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>4));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function prosesUploadAndSendToVerifkator()
	{
		
		$this->load->library('upload_file');
		$this->db->trans_begin();
		
		$flag = $this->input->post('flag');
		$id = $this->input->post('uk_id');

		$path_file    = $_FILES['file']['tmp_name'];
        $type_file      = $_FILES['file']['type'];
        $name_file     = $_FILES['file']['name'];
        $random_number           = rand(1,999999);
        $unique_file_name = $random_number.'_'.$name_file;

		$params = array(
			'ref_id' => $id,
			'ref_table' => 't_usulan_kebutuhan',
			'attc_owner' => 'Dinkes Prov',
			'attc_name' => $name_file,
			'attc_path' => $unique_file_name,
			'attc_type' => $type_file,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('data_user')->fullname,
			);

		$exc_upload = $this->upload_file->doUpload($params, 'file', 'uploaded_files/file/');

		//$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'S');


		if($flag != 'R') {
			$this->db->update('t_usulan_kebutuhan', array('approval_prov'=>$flag, 'no_sk_dinkes_prov' => $this->input->post('no_sk')), array('uk_id'=>$id));
			if($flag != 'N'){
				$this->form_wks->sendToVerifikator(array('status'=>4), $id);
			$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>4));
			}else{
				$this->form_wks->sendToVerifikator(array('status'=>3),$id);
			$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>3));
			}
		}else{
			$this->db->update('t_usulan_kebutuhan', array('approval_prov'=>'N','verifikasi_dinkes_prov'=>'N'), array('uk_id'=>$id));
			$this->form_wks->sendToVerifikator(array('status'=>3), $id);
			$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>3));
		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function approval($id, $flag)
	{
		$this->db->trans_begin();

		//$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'S');
		if($flag != 'R') {
			$this->db->update('t_usulan_kebutuhan', array('approval_prov'=>$flag), array('uk_id'=>$id));
			if($flag != 'N'){
				$this->form_wks->sendToVerifikator(array('workflow'=>4, 'id'=>$id));
			$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>4));
			}else{
				$this->form_wks->sendToVerifikator(array('workflow'=>3, 'id'=>$id));
			$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>3));
			}
		}else{
			$this->db->update('t_usulan_kebutuhan', array('approval_prov'=>'N','verifikasi_dinkes_prov'=>'N'), array('uk_id'=>$id));
			$this->form_wks->sendToVerifikator(array('status'=>3), $id);
			$this->form_wks->setHistoryWorkfow(array('ref_id'=>$id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>3));
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}
	
}
