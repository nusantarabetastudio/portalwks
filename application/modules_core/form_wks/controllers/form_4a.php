<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_4a extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('master');
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->model('form_wks_model', 'form_wks');
		$this->load->model('master_data/m_provinsi_model', 'prov');
		$this->load->model('master_data/m_kabupaten_model', 'kab');
		$this->load->model('master_data/m_fk_model', 'fk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	public function index()
	{
		$this->breadcrumbs->push('Form 4a', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->authuser->write_log();
		$this->load->view('form_4a/index', $data);
	}

	public function getPeriodeKelulusanMhs()
	{
		
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '4a';

		$list = $this->form_wks->getPeriodeKelulusanMhs($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			$no++;
			$status_verifikasi = $this->form_wks->getStatusVerifikasi($uk->app_kolegium);
			$semester = ($uk->plm_semester==1)?'Ganjil':'Genap';
			$btn = '<a class="btn btn-xs btn-primary" href="javascript:void()" title="Tambahkan Mahasiswa Lulus" onclick="add_mhs('."'".$this->regex->_genRegex($uk->plm_id,'RGXINT')."'".')"><i class="fa fa-plus"></i> </a> <a class="btn btn-xs btn-success" href="javascript:void()" title="Selengkapnya" onclick="detailPeriodeKelulusan('."'".$this->regex->_genRegex($uk->plm_id,'RGXINT')."'".')"><i class="fa fa-eye"></i> </a> <a class="btn btn-xs btn-danger" href="javascript:void()" title="Selengkapnya" onclick="delete_periode('."'".$this->regex->_genRegex($uk->plm_id,'RGXINT')."'".')"><i class="fa fa-trash-o"></i> </a>';

			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '<div class="center">[ '.$uk->plm_id.' ]</div>';
			$row[] = strtoupper($uk->fk_name);
			$row[] = strtoupper($uk->nama_provinsi);
			$row[] = '<div class="center">'.$uk->plm_tahun.'</div>';
			$row[] = '<div class="center">'.$this->tanggal->getBulan($uk->plm_bulan).'</div>';
			$row[] = '<div class="center">'.$semester.'</div>';
			/*$row[] = strtoupper($uk->nama_kabupaten);*/
			$row[] = '<div class="center">'.$uk->total_mhs.'</div>';
			$row[] = '<div class="center">'.$uk->total_tubel.'</div>';
			$row[] = '<div class="center">'.$uk->total_mandiri.'</div>';
			$row[] = '<div class="center">'.$uk->total_mhs_disetujui.'</div>';
			$row[] = '<div class="center">'.$status_verifikasi.'</div>';
			//add html for action
			$row[] = '<div class="center">'.$btn.'</div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->plmcount_all($params),
						"recordsFiltered" => $this->form_wks->plmcount_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function add_periode_kelulusan($id='', $type='')
	{
		/*breadcrumb*/
		$this->breadcrumbs->push('Form 4a (Daftar prediksi kelulusan)', 'form_wks/'.strtolower(get_class($this)));
		
		/*value is id*/
		if($id != ''){
			if($type =='detail'){
				$this->breadcrumbs->push('Selengkapnya', 'form_wks/'.strtolower(get_class($this)).'/add_periode_kelulusan/'.$id.'/detail');
			}else{
				$this->breadcrumbs->push('Tambah mahasiswa lulus', 'form_wks/'.strtolower(get_class($this)).'/add_periode_kelulusan/'.$id.'');
			}
			$data['value'] = $this->db->get_where('vw_prediksi_periode_mhs', array('plm_id'=>$id))->row();
		}else{
			$this->breadcrumbs->push('Buat periode kelulusan', 'form_wks/'.strtolower(get_class($this)).'/add_periode_kelulusan');
		}

		/*title and breadcrumb*/
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		/*write log history*/
		$this->authuser->write_log();

		/*load view*/
		if($type=='detail'){
			$params['plm_id'] = $id;
			$data['mhs'] = $this->form_wks->getDmpByPlmId($params);
			//echo '<pre>'; print_r($data['result']);die;
			$this->load->view('form_4a/detail_periode_kelulusan_form', $data);
		}else{
			$this->load->view('form_4a/add_periode_kelulusan_form', $data);
		}
	}



	public function prosesTambahMhs()
	{
		/*print_r($_POST);die;*/
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
		$this->db->trans_begin();

		$post_plm_id = $this->input->post('plm_id')?$this->input->post('plm_id'):0;
		$params = array();
		$params['plm_tahun'] = $this->input->post('tahun');
		$params['plm_bulan'] = $this->input->post('bulan');
		$params['plm_semester'] = $this->input->post('semester');
		$params['id_provinsi'] = $this->input->post('id_provinsi');
		$params['id_kabupaten'] = $this->input->post('id_kabupaten');
		$params['fk_id'] = $this->input->post('fk_id');

		if($post_plm_id == 0){
			//========== cek periode lulus mhs ================//
			$existing = $this->db->get_where('t_periode_lulus_mhs', $params);
			if(empty($existing->num_rows())){
				$this->db->insert('t_periode_lulus_mhs', $params);
				$plm_id = $this->db->insert_id();
			}else{
				$plm_id = $existing->row()->plm_id;
			}
		}else{
			$this->db->update('t_periode_lulus_mhs', $params, array('plm_id'=>$post_plm_id));
			$plm_id = $post_plm_id;
		}
		

		$dataexc = array(
			'dmp_nim' => $this->regex->_genRegex($this->input->post('nim'), 'RGXQSL'),
			'dmp_nama' => $this->regex->_genRegex($this->input->post('nama_mahasiswa'), 'RGXQSL'),
			'dmp_tanggal_lulus' => $this->regex->_genRegex($this->input->post('tgl_lulus'), 'RGXQSL'),
			'dmp_status_peserta' => $this->regex->_genRegex($this->input->post('status_peserta'), 'RGXQSL'),
			'dmp_instansi' => $this->regex->_genRegex($this->input->post('instansi'), 'RGXQSL'),
			'dmp_pemberi_beasiswa' => $this->regex->_genRegex($this->input->post('dmp_pemberi_beasiswa'), 'RGXQSL'),
			'dmp_no_ijasah' => $this->regex->_genRegex($this->input->post('dmp_no_ijasah'), 'RGXQSL'),
			'prod_id' => $this->regex->_genRegex($this->input->post('prodi'), 'RGXINT'),
			'fk_id' => $this->regex->_genRegex($this->input->post('fk_id'), 'RGXINT'),
			'plm_id' => $this->regex->_genRegex($plm_id, 'RGXINT'),
			);
		//print_r($dataexc);die;

		$dmp_id = $this->input->post('dmp_id')?$this->regex->_genRegex($this->input->post('dmp_id'), 'RGXINT'):0;

		if($dmp_id == 0){
			$this->db->insert('t_detail_mhs_lulus', $dataexc);
		}else{
			$this->db->update('t_detail_mhs_lulus', $dataexc, array('dmp_id'=>$dmp_id)); 
			//print_r($this->db->last_query());die;
		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function ajax_list_data_mhs()
	{
		$plm_id = $this->input->post('ID');
		$params = isset($_GET)?$_GET:'';
		$params['plm_id'] = $plm_id;

		$list = $this->form_wks->getDataMahasiswaPeserta($params); //print_r($this->db->last_query());die;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $uk) {

			if($uk->dmp_status_peserta == 'TUBEL'){
				if($uk->dmp_instansi == NULL){
					$status_peserta = $uk->dmp_status_peserta;
				}else{
					$status_peserta = $uk->dmp_instansi;
				}
			}else{
				if($uk->dmp_pemberi_beasiswa == NULL){
					$status_peserta = $uk->dmp_status_peserta;
				}else{
					$status_peserta = $uk->dmp_pemberi_beasiswa;
				}
			}
			$no_ijasah = ($uk->dmp_no_ijasah == NULL)?'<i>(Ijasah belum keluar)</i>':$uk->dmp_no_ijasah;
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nama.'</div>';
			$row[] = '<div class="left">'.$uk->prod_name.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nim.'</div>';
			$row[] = '<div class="center">'.$status_peserta.'</div>';
			/*$row[] = '<div class="center">'.$uk->dmp_instansi.'</div>';
			$row[] = '<div class="center">'.$uk->dmp_pemberi_beasiswa.'</div>';*/
			$row[] = '<div class="left">'.$this->tanggal->formatDate($uk->dmp_tanggal_lulus).'</div>';
			$row[] = '<div class="center">'.$no_ijasah.'</div>';
			$row[] = '<div class="center"><a class="btn btn-xs btn-success" href="javascript:void()" title="Selengkapnya" onclick="detailMhs('."'".$this->regex->_genRegex($uk->dmp_id,'RGXINT')."'".')"> <i class="fa fa-edit"></i> </a> <a class="btn btn-xs btn-danger" href="javascript:void()" title="Selengkapnya" onclick="delete_mhs('."'".$this->regex->_genRegex($uk->dmp_id,'RGXINT')."'".')"> <i class="fa fa-trash-o"></i> </a></div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->dmpcount_all($params),
						"recordsFiltered" => $this->form_wks->dmpcount_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function prosesDeleteMhs($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->db->delete('t_detail_mhs_lulus', array('dmp_id'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function prosesDeletePeriode($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->db->delete('t_periode_lulus_mhs', array('plm_id'=>$id));
		echo json_encode(array("status" => TRUE));
	}

	public function prosesUpdatePeriodeKelulusan()
	{
		/*print_r($_POST);die;*/
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
		$this->db->trans_begin();

		$post_plm_id = $this->input->post('plm_id');
		$params = array();
		$params['plm_tahun'] = $this->input->post('tahun');
		$params['plm_bulan'] = $this->input->post('bulan');
		$params['plm_semester'] = $this->input->post('semester');
		$params['id_provinsi'] = $this->input->post('id_provinsi');
		$params['id_kabupaten'] = $this->input->post('id_kabupaten');
		$params['fk_id'] = $this->input->post('fk_id');

		$this->db->update('t_periode_lulus_mhs', $params, array('plm_id'=>$post_plm_id));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success', "result"=>$params));
		}
		
	}

	public function detailMhs($id)
	{
		$params['dmp_id'] = $id;
		$value = $this->form_wks->getDmpById($params);
		echo json_encode($value);
	}


}
