<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_1 extends CI_Controller {

	public function __construct()
	{

		parent::__construct();

		/*load library*/
		$this->load->library('master');
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->library('upload_file');

		/*load model*/
		$this->load->model('form_wks_model', 'form_wks');
		$this->load->model('master_data/m_provinsi_model', 'prov');
		$this->load->model('master_data/m_kabupaten_model', 'kab');
		$this->load->model('master_data/rs_model', 'rs');

		/*check session login*/
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

		/*breadcrumb default*/
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	public function index()
	{
		/*breadcrumb*/
		$this->breadcrumbs->push('Form 1 (Usulan Kebutuhan SDM Kesehatan)', 'form_wks/'.strtolower(get_class($this)));

		/*title and breadcrumb*/
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();

		/*profil rs*/
		$data['profil'] = $this->rs->get_by_kode_rs($this->session->userdata('data_user')->kode_user);

		/*write log history*/
		$this->authuser->write_log();

		/*load view*/
		$this->load->view('form_1/index', $data);
	}

	public function add_usulan()
	{
		/*breadcrumb*/
		$this->breadcrumbs->push('Form 1 (Usulan Kebutuhan SDM Kesehatan)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Buat usulan kebutuhan', 'form_wks/'.strtolower(get_class($this)).'/add_usulan');

		/*title and breadcrumb*/
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();

		/*profil rs*/
		$data['profil'] = $this->rs->get_by_kode_rs($this->session->userdata('data_user')->kode_user);

		/*write log history*/
		$this->authuser->write_log();

		/*load view*/
		$this->load->view('form_1/add_usulan_form', $data);
	}

	public function searchDataForm()
	{
		/*get data standar from skm apps*/
		$standar_existing = $this->integration->getStandarExisting($_POST);
		echo json_encode($standar_existing);
	}

	public function proses_usulan_kebutuhan()
	{
		/*auth user for create function*/
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');

		/*existing id*/
		$uk_id = $this->regex->_genRegex($this->input->post('id'), 'RGXINT');		

		/*transaction begin*/
		$this->db->trans_begin();

		/*post data*/
		$dataexc = array(
			'tahun' => $this->regex->_genRegex($this->input->post('tahun'), 'RGXQSL'),
			'bulan' => $this->regex->_genRegex($this->input->post('bulan'), 'RGXQSL'),
			'id_provinsi' => $this->regex->_genRegex($this->input->post('id_provinsi'), 'RGXQSL'),
			'id_kabupaten' => $this->regex->_genRegex($this->input->post('id_kabupaten'), 'RGXQSL'),
			'kode_rs' => $this->regex->_genRegex($this->input->post('kode_rsu'), 'RGXQSL')
		);

		$check = $this->form_wks->checkUsulanKebutuhanExist($dataexc);

		$dataexc['jumlah_tt'] = $this->regex->_genRegex($this->input->post('jumlah_tt'), 'RGXINT');
		
		if( $check == false ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->fullname;
			$action = $this->db->insert('t_usulan_kebutuhan', $dataexc);
			$last_id = $this->db->insert_id();
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->fullname;
			$action = $this->db->update('t_usulan_kebutuhan', $dataexc, array('uk_id'=>$uk_id));
			$last_id = $check->uk_id;
		}

		if( $last_id != 0 ) :
			$total_usulan = $this->input->post('usulan');
			$this->db->delete('t_detail_usulan_kebutuhan', array('uk_id'=>$last_id));
			foreach ($total_usulan as $key => $value) {
				if(!empty($value) || $value != 0) :
					$data_insert = array(
						'uk_id' => $last_id,
						'id_jenis_sdmk' => $_POST['id_jenis_sdmk'][$key],
						'nama_jenis_sdmk' => $_POST['nama_jenis_sdmk'][$key],
						'jml_pns' => $_POST['jml_pns'][$key],
						'jml_pppk' => $_POST['jml_pppk'][$key],
						'jml_ptt' => $_POST['jml_ptt'][$key],
						'jml_honorer' => $_POST['jml_honorer'][$key],
						'jml_blud' => $_POST['jml_blud'][$key],
						'jml_tks' => $_POST['jml_tks'][$key],
						'total_standar' => $_POST['total_standar'][$key],
						'total_jml' => $_POST['total_jml'][$key],
						'total_kesenjangan' => $_POST['total_kesenjangan'][$key],
						'usulan_kebutuhan' => $value,
						);
					$this->db->insert('t_detail_usulan_kebutuhan', $data_insert);
				endif;
			}
		endif;

		$this->form_wks->setHistoryWorkfow(array('ref_id'=>$last_id, 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>2));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function printPreview($id, $type='')
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
		$this->breadcrumbs->push('Form 1 (Usulan Kebutuhan RS)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Selengkapnya', 'form_wks/'.strtolower(get_class($this)).'/detail_usulan_kebutuhan/'.$id);
		$this->breadcrumbs->push('Print preview', 'form_wks/'.strtolower(get_class($this)).'/printPreview/'.$id);
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getUsulanById($id);
		$data['note'] = $this->form_wks->getNoteVerifikasi($id);
		$data['type'] = $type;
		/*echo '<pre>'; print_r($data['value']);die;*/
		$this->authuser->write_log();
		//$this->load->view('form_1/print_preview', $data);
		echo 'OK';
	}

	public function printPdf($uk_id)
	{
		$this->form_wks->printPdf($uk_id);
	}

	public function searchHistory()
	{	
		$params = array(
			'tahun' => $_POST['tahun'],
			'id_provinsi' => $_POST['id_provinsi'],
			'nama_provinsi' => ($_POST['id_provinsi'] != 0) ? $this->prov->get_by_id($_POST['id_provinsi'])->nama_provinsi : '',
			'id_kabupaten' => $_POST['id_kabupaten'],
			'nama_kabupaten' => ($_POST['id_kabupaten'] != 0) ? $this->kab->get_by_id($_POST['id_kabupaten'])->nama_kabupaten : '',
			'kode_rsu' => $_POST['kode_rsu'],
			'nama_rs' => ($_POST['kode_rsu'] != 0) ? $this->rs->get_by_kode_rs($_POST['kode_rsu'])->nama_rs : '', 
			);
		echo json_encode($params);
	}

	public function detail_usulan_kebutuhan($id)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
		$this->breadcrumbs->push('Form 1 (Usulan Kebutuhan RS)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Selengkapnya', 'form_wks/'.strtolower(get_class($this)).'/detail_usulan_kebutuhan/'.$id);
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getUsulanById($id);
		$data['note'] = $this->form_wks->getNoteVerifikasi($id);
		$data['file'] = $this->upload_file->getUploadedFile(array('ref_id'=>$id, 'ref_table' => 't_usulan_kebutuhan', 'attc_owner'=>'rs'));
		/*echo '<pre>';print_r($data['value']);die;*/
		$this->authuser->write_log();
		$this->load->view('form_1/detail_usulan_kebutuhan', $data);
	}

	public function sendToVerifikator($id)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
		$this->breadcrumbs->push('Form 1 (Usulan Kebutuhan RS)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Selengkapnya', 'form_wks/'.strtolower(get_class($this)).'/detail_usulan_kebutuhan/'.$id);
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getUsulanById($id);
		$data['note'] = $this->form_wks->getNoteVerifikasi($id);
		/*echo '<pre>';print_r($data['value']);die;*/
		$this->authuser->write_log();
		$this->load->view('form_1/form_upload_file', $data);
	}

	public function prosesUpdateUsulanKebutuhan()
	{
		/*print_r($_POST);die;*/
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
		$this->db->trans_begin();

		$usulan_kebutuhan = $this->input->post('usulan_kebutuhan');
		$this->db->update('t_usulan_kebutuhan', array('tahun'=>$this->input->post('tahun'), 'bulan'=>$this->input->post('bulan')), array('uk_id'=>$this->input->post('uk_id')));
		if( count($usulan_kebutuhan) > 0 ) :
			foreach ($usulan_kebutuhan as $key => $value) {
				$this->db->update('t_detail_usulan_kebutuhan', array('usulan_kebutuhan'=>$value), array('duk_id'=>$key));
			}
		endif;

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function ajax_list_history()
	{
		$params = isset($_GET)?$_GET:'';

		$list = $this->form_wks->getDataUsulan($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			$status_prov = $this->form_wks->getStatusVerifikasi($uk->verifikasi_dinkes_prov);
			$status_kab = $this->form_wks->getStatusVerifikasi($uk->verifikasi_dinkes_kab);

			$btn = '<a class="btn btn-xs btn-primary" href="javascript:void()" title="Selengkapnya" onclick="detail('."'".$this->regex->_genRegex($uk->uk_id,'RGXINT')."'".')"> <i class="fa fa-eye"></i> </a> ';
			// jika status masih dalam proses pembuatan usulan kebutuhan
			/*if( $uk->status == 1){
				$btn .= '<a class="btn btn-xs btn-inverse" href="javascript:void()" title="Kirim ke Dinkes Kab/Kota" onclick="sendToVerifikator('."'".$this->regex->_genRegex($uk->uk_id,'RGXINT')."'".')"><i class="fa fa-send"></i> </a>';
			}*/

			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '<div class="center">[ '.$uk->uk_id.' ]</div>';
			$row[] = strtoupper($uk->nama_rs);
			$row[] = '<div class="left">
						'.$this->tanggal->getBulan($uk->bulan).', '.$uk->tahun.'<br>
					  </div>';
			$row[] = $uk->nama_provinsi;
			$row[] = $uk->nama_kabupaten;
			$row[] = '<div class="center">'.$uk->total_jenis_sdmk.'</div>';
			$row[] = '<div class="center">'.$uk->total_saat_ini.'</div>';
			$row[] = '<div class="center">'.$this->apps->get_format($uk->total_kesenjangan_all).'</div>';
			$row[] = '<div class="center">'.$uk->total_usulan_kebutuhan.'</div>';
			$row[] = '<div class="center">'.$uk->total_disetujui.'</div>';
			$row[] = '<div class="center">'.$uk->total_disetujui_prov.'</div>';
			$row[] = '<div class="center" style="font-size:12px"><i>'.$uk->status_proses.'</i></div>';
			//add html for action
			$row[] = '<div class="center"> '.$btn.' </div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->count_all($params),
						"recordsFiltered" => $this->form_wks->count_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_skm($id='')
	{
		$params = array(
			'id_perencanaan' => $id,
			'search' => $_POST['search']['value'],
			'start' => $_POST['start'],
			'length' => $_POST['length'],
			'draw' => $_POST['draw'],
		);
		$list = $this->integration->getSKM($params);
		
		echo json_encode($list);
	}
	
	

	public function prosesDeleteUkSdmk($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
		$this->form_wks->delete_uk_sdmk_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function prosesUploadAndSendToVerifkator()
	{
		
		$this->load->library('upload_file');
		$this->db->trans_begin();

		$path_file    = $_FILES['file']['tmp_name'];
        $type_file      = $_FILES['file']['type'];
        $name_file     = $_FILES['file']['name'];
        $random_number           = rand(1,999999);
        $unique_file_name = $random_number.'_'.$name_file;

		$params = array(
			'ref_id' => $this->input->post('uk_id'),
			'ref_table' => 't_usulan_kebutuhan',
			'attc_owner' => 'rs',
			'attc_name' => $name_file,
			'attc_path' => $unique_file_name,
			'attc_type' => $type_file,
			'created_date' => date('Y-m-d H:i:s'),
			'created_by' => $this->session->userdata('data_user')->fullname,
			);

		$exc_upload = $this->upload_file->doUpload($params, 'file', 'uploaded_files/file/');

		//$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'S');

		$this->form_wks->sendToVerifikator(array('status'=>2), $this->input->post('uk_id'));
		$this->form_wks->setHistoryWorkfow(array('ref_id'=>$this->input->post('uk_id'), 'ref_table'=>'t_usulan_kebutuhan', 'workflow_id'=>2));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function processDeleteFile($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->db->delete('t_attachment', array('attc_id'=>$id));
		echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
	}

}
