<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_5a extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->model('form_wks_model', 'form_wks');
		$this->load->model('master_data/m_provinsi_model', 'prov');
		$this->load->model('master_data/m_kabupaten_model', 'kab');
		$this->load->model('master_data/m_fk_model', 'fk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	public function index()
	{
		/*breadcrumb*/
		$this->breadcrumbs->push('Form 5a (Periode Penempatan Wajib Kerja Dokter Spesialis)', 'form_wks/'.strtolower(get_class($this)));

		/*title and breadcrumb*/
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();

		/*profil rs*/
		//$data['profil'] = $this->rs->get_by_kode_rs($this->session->userdata('data_user')->kode_user);

		/*write log history*/
		$this->authuser->write_log();

		/*load view*/
		$this->load->view('form_5a/index', $data);
	}

	public function add_periode_penempatan()
	{
		/*breadcrumb*/
		$this->breadcrumbs->push('Form 5a (Periode Penempatan Wajib Kerja Dokter Spesialis)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Buat periode penempatan', 'form_wks/'.strtolower(get_class($this)).'/add_periode_penempatan');

		/*title and breadcrumb*/
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();

		/*write log history*/
		$this->authuser->write_log();

		/*load view*/
		$this->load->view('form_5a/add_periode_penempatan', $data);
	}

	public function penempatan_periode($id)
	{
		$this->breadcrumbs->push('Form 5a (Periode Penempatan Wajib Kerja Dokter Spesialis)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Penentuan Peserta WKDS', 'form_wks/'.strtolower(get_class($this)).'/penempatan_periode/'.$id.'');
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['periode'] = $this->db->get_where('t_periode_penempatan', array('perpp_id'=>$id))->row();
		$this->authuser->write_log();
		$this->load->view('form_5a/penempatan_periode', $data);
	}

	public function ajax_list_history()
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '5a';
		$list = $this->form_wks->getDataUsulan($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			$checked = ($uk->verifikasi_kemenkes == 'Y')?'checked':'';

			$html = '';
			$imp_sdmk = explode(':', $uk->sdmk);
			foreach($imp_sdmk as $rowsdmk){
				$html .= '- '.str_replace('-' , ', = ', $rowsdmk).'<br>';
			}
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = $uk->nama_provinsi;
			$row[] = '<b>'.strtoupper($uk->nama_rs).'</b><br><div style="font-size:11px"><b>Spesialis :</b> <br>'.$html.'</div>';
			$row[] = '<div class="center">'.$uk->total_jenis_sdmk.'</div>';
			$row[] = '<div class="center">'.$uk->total_saat_ini.'</div>';
			$row[] = '<div class="center">'.$this->apps->get_format($uk->total_kesenjangan_all).'</div>';
			$row[] = '<div class="center">'.$uk->total_disetujui_prov.'</div>';
			$row[] = '<div class="center">'.$uk->total_terpenuhi.'</div>';
			$row[] = '<div class="center">'.$this->tanggal->formatDate($uk->created_date).'</div>';
			$row[] = '<div class="center"><a class="btn btn-xs btn-warning" href="javascript:void()" title="Selengkapnya" onclick="penempatan_form_rs('."'".$this->regex->_genRegex($uk->uk_id,'RGXINT')."'".')"> <i class="fa fa-share-alt"></i> Penempatan </a></div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->count_all($params),
						"recordsFiltered" => $this->form_wks->count_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_data_mhs()
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '5a';
		$list = $this->form_wks->getDataMahasiswaPeserta($params); //print_r($this->db->last_query());die;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $uk) {

			$checked = ($uk->dmp_app_kemenkes == 'Y')?'checked':'';
			/*if($uk->pp_uk_id != NULL){
				$penempatan = '<b>'.$uk->uk_nama_rs.'</b> / '.$uk->uk_nama_provinsi.' / '.$uk->uk_nama_kabupaten;
			}else{
				$penempatan = '<b>'.$uk->pp_nama_rs.'</b> / '.$uk->pp_nama_provinsi.' / '.$uk->pp_nama_kabupaten;
			}*/
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nama.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nim.'</div>';
			$row[] = '<div class="left">'.$uk->nama_provinsi.'</div>';
			$row[] = '<div class="left">'.$uk->prod_name.'</div>';
			$row[] = '<div class="left">'.$uk->fk_name.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_status_peserta.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_email.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_no_telp.'</div>';
			$row[] = '<div class="left">'.$uk->pp_nama_rs.'</b> / '.$uk->pp_nama_provinsi.' / '.$uk->pp_nama_kabupaten.'</div>';
			$row[] = '<div class="center"><a class="btn btn-xs btn-primary" href="javascript:void()" title="Selengkapnya" onclick="penempatan_form('."'".$this->regex->_genRegex($uk->dmp_id,'RGXINT')."'".')"> <i class="fa fa-share-alt"></i> Penempatan </a></div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->dmpcount_all($params),
						"recordsFiltered" => $this->form_wks->dmpcount_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_penempatan_periode()
	{
		$params = isset($_GET)?$_GET:'';

		$list = $this->form_wks->getPenempatanPeriode($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			$btn = '<a class="btn btn-xs btn-primary" href="javascript:void()" title="Selengkapnya" onclick="penempatan_periode('."'".$this->regex->_genRegex($uk->perpp_id,'RGXINT')."'".')"> <i class="fa fa-eye"></i> </a> <a class="btn btn-xs btn-success" href="javascript:void()" title="Ubah Periode Penempatan" onclick="edit_penempatan_periode('."'".$this->regex->_genRegex($uk->perpp_id,'RGXINT')."'".')"> <i class="fa fa-edit"></i> </a>  ';

			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '<div class="center">[ '.$uk->perpp_id.' ]</div>';
			$row[] = '<div class="center">'.strtoupper($uk->perpp_tahun).'</div>';
			$row[] = '<div class="center">'.$this->tanggal->getBulan($uk->perpp_bulan).'</div>';
			$row[] = $this->tanggal->formatDate($uk->perpp_tgl_berangkat).' s/d '.$this->tanggal->formatDate($uk->perpp_tgl_kembali);
			$row[] = '<div class="center">20</div>';
			$row[] = '<div class="center">'.$uk->perpp_status.'</div>';
			//add html for action
			$row[] = '<div class="center"> '.$btn.' </div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->count_all_perpp($params),
						"recordsFiltered" => $this->form_wks->count_filtered_perpp($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_daftar_peserta($perpp_id)
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '5a';
		$params['perpp_id'] = $perpp_id;
		$list = $this->form_wks->getDataMahasiswaPeserta($params); //print_r($this->db->last_query());die;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $uk) {

			$checked = ($uk->dmp_app_kemenkes == 'Y')?'checked':'';
			/*if($uk->pp_uk_id != NULL){
				$penempatan = '<b>'.$uk->uk_nama_rs.'</b> / '.$uk->uk_nama_provinsi.' / '.$uk->uk_nama_kabupaten;
			}else{
				$penempatan = '<b>'.$uk->pp_nama_rs.'</b> / '.$uk->pp_nama_provinsi.' / '.$uk->pp_nama_kabupaten;
			}*/
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nama.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nim.'</div>';
			$row[] = '<div class="left">'.$uk->nama_provinsi.'</div>';
			$row[] = '<div class="left">'.$uk->prod_name.'</div>';
			$row[] = '<div class="left">'.$uk->fk_name.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_status_peserta.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_email.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_no_telp.'</div>';
			$row[] = '<div class="left">'.$uk->pp_nama_rs.'</b> / '.$uk->pp_nama_provinsi.' / '.$uk->pp_nama_kabupaten.'</div>';
			$row[] = '<div class="center"><a class="btn btn-xs btn-danger" href="javascript:void()" title="Hapus" onclick="hapus_peserta('."'".$this->regex->_genRegex($uk->pp_id,'RGXINT')."'".')"> <i class="fa fa-times-circle"></i>  </a></div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->dmpcount_all($params),
						"recordsFiltered" => $this->form_wks->dmpcount_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function updateRowStatusUk($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');	

		$this->db->trans_begin();
			$existing = $this->db->get_where('t_usulan_kebutuhan', array('uk_id'=>$id))->row();

			$this->db->update('t_usulan_kebutuhan', array('verifikasi_kemenkes'=>($existing->verifikasi_kemenkes=='N')?'Y':'N'), array('uk_id'=>$id));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function updateRowStatusDmp($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');	

		$this->db->trans_begin();
			$existing = $this->db->get_where('t_detail_mhs_lulus', array('dmp_id'=>$id))->row();

			$this->db->update('t_detail_mhs_lulus', array('dmp_app_kemenkes'=>($existing->dmp_app_kemenkes=='N')?'Y':'N'), array('dmp_id'=>$id));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function penempatan($dmp_id)
	{
		$this->breadcrumbs->push('Form 5a (Periode Penempatan Wajib Kerja Dokter Spesialis)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Penempatan Peserta WKDS', 'form_wks/'.strtolower(get_class($this)).'/penempatan/'.$dmp_id.'');
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->get_mhs_by_id($dmp_id);
		$this->authuser->write_log();
		$this->load->view('form_5a/penempatan_form', $data);
	}

	public function penempatan_by_rs($uk_id)
	{
		$this->breadcrumbs->push('Form 5a (Periode Penempatan Wajib Kerja Dokter Spesialis)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Penempatan Peserta WKDS berdasarkan RS', 'form_wks/'.strtolower(get_class($this)).'/penempatan/'.$uk_id.'');
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->getUsulanById($uk_id);
		$this->authuser->write_log();
		$this->load->view('form_5a/penempatan_form_by_rs', $data);
	}

	public function prosesPenempatan()
	{
		/*print_r($_POST);die;*/

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');	

		$this->db->trans_begin();

			$manual = $this->input->post('penempatan_manual')?$this->input->post('penempatan_manual'):0; 

			if( $manual == 'on'){
				$dataexc = array(
					'uk_id' => NULL,
					'id_provinsi' => $this->input->post('id_provinsi'),
					'id_kabupaten' => $this->input->post('id_kabupaten'),
					'kode_rs' => $this->input->post('kode_rsu'),
					);
			}else{
				$uk = $this->db->get_where('vw_usulan_kebutuhan', array('uk_id' => $this->input->post('checked_rs')))->row();
				$dataexc = array(
					'uk_id' => $this->input->post('checked_rs'),
					'id_provinsi' => $uk->id_provinsi,
					'id_kabupaten' => $uk->id_kabupaten,
					'kode_rs' => $uk->kode_rs,
					);
			}

			$dataexc['dmp_id'] = $this->input->post('dmp_id');
			$dataexc['tanggal_penempatan'] = date('Y-m-d');
			$dataexc['sampai_tanggal'] = date('Y-m-d', strtotime('+1 year', strtotime(date('Y-m-d'))));
			$dataexc['keterangan'] = $this->input->post('keterangan');
			$this->db->insert('t_penempatan_peserta', $dataexc);
			$this->db->update('t_detail_mhs_lulus', array('status_penempatan'=>'1'), array('dmp_id' => $this->input->post('dmp_id')));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function prosesPeriodePenempatan()
	{
		/*print_r($_POST);die;*/

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');	

		$this->db->trans_begin();
			$perpp_id = $this->input->post('perpp_id')?$this->input->post('perpp_id'):0;
			$dataexc = array(
					'perpp_tahun' => $this->regex->_genRegex($this->input->post('tahun'), 'RGXINT'),
					'perpp_bulan' => $this->regex->_genRegex($this->input->post('bulan'), 'RGXINT'),
					'perpp_tgl_berangkat' => $this->regex->_genRegex($this->input->post('tgl_berangkat'), 'RGXQSL'),
					'perpp_tgl_kembali' => $this->regex->_genRegex($this->input->post('tgl_kembali'), 'RGXQSL'),
					'perpp_status' => 'WAITING',
					);
			if($perpp_id == 0){
				$dataexc['created_date'] = date('Y-m-d H:i:s');
				$dataexc['created_by'] = $this->session->userdata('data_user')->fullname;
				$this->db->insert('t_periode_penempatan', $dataexc);
			}else{
				$dataexc['updated_date'] = date('Y-m-d H:i:s');
				$dataexc['updated_by'] = $this->session->userdata('data_user')->fullname;
				$this->db->update('t_periode_penempatan', $dataexc, array('perpp_id'=>$perpp_id));
			}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function delete_peserta($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->db->delete('t_penempatan_peserta', array('pp_id'=>$id));
		echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
	}

}
