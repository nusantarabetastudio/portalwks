<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_5b extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->model('form_wks_model', 'form_wks');
		$this->load->model('master_data/m_provinsi_model', 'prov');
		$this->load->model('master_data/m_kabupaten_model', 'kab');
		$this->load->model('master_data/m_fk_model', 'fk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Daftar form instrumen WKS', 'form_wks');
	}

	public function index()
	{
		$this->breadcrumbs->push('Form 5b (Analisa Kebutuhan SDMK dan Prediksi Kelulusan FK)', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->authuser->write_log();
		$this->load->view('form_5b/index', $data);
	}

	public function ajax_list_history()
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '5b';
		$list = $this->form_wks->getDataUsulan($params);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $uk) {

			$checked = ($uk->verifikasi_kemenkes == 'Y')?'checked':'';

			$html = '';
			$imp_sdmk = explode(':', $uk->sdmk);
			foreach($imp_sdmk as $rowsdmk){
				$html .= '- '.str_replace('-' , ', = ', $rowsdmk).'<br>';
			}
			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = $uk->nama_provinsi;
			$row[] = '<b>'.strtoupper($uk->nama_rs).'</b><br><div style="font-size:11px"><b>Spesialis :</b> <br>'.$html.'</div>';
			$row[] = '<div class="center">'.$uk->total_disetujui_prov.'</div>';
			$row[] = '<div class="center">'.$uk->total_terpenuhi.'</div>';
			$row[] = '<div class="center"><label>
							<input name="status_uk[]" onclick="update_status('.$uk->uk_id.')" class="ace ace-switch ace-switch-7" type="checkbox" '.$checked.'/>
							<span class="lbl"></span>
						</label></div>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->count_all($params),
						"recordsFiltered" => $this->form_wks->count_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_list_data_mhs()
	{
		$params = isset($_GET)?$_GET:'';
		$params['f'] = '4c';
		$list = $this->form_wks->getDataMahasiswaPeserta($params); //print_r($this->db->last_query());die;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $uk) {

			$checked = ($uk->dmp_app_kemenkes == 'Y')?'checked':'';

			if($uk->dmp_status_peserta == 'TUBEL'){
				if($uk->dmp_instansi == NULL){
					$status_peserta = $uk->dmp_status_peserta;
				}else{
					$status_peserta = $uk->dmp_instansi;
				}
			}else{
				if($uk->dmp_pemberi_beasiswa == NULL){
					$status_peserta = $uk->dmp_status_peserta;
				}else{
					$status_peserta = $uk->dmp_pemberi_beasiswa;
				}
			}

			$no++;
			$row = array();
			$row[] = '<div class="center">'.$no.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nama.'</div>';
			$row[] = '<div class="left">'.$uk->nama_provinsi.'</div>';
			$row[] = '<div class="left">'.$uk->dmp_nim.'</div>';
			$row[] = '<div class="left">'.$uk->prod_name.'</div>';
			$row[] = '<div class="left">'.$uk->fk_name.'</div>';
			$row[] = '<div class="left">'.$status_peserta.'</div>';
			$row[] = '<div class="left">'.$status_peserta.'</div>';
			/*$row[] = '<div class="center"><label>
							<input onclick="update_status_dmp('.$uk->dmp_id.')" name="switch-field-1" class="ace ace-switch ace-switch-7" type="checkbox" '.$checked.'/>
							<span class="lbl"></span>
						</label></div>';*/
			$row[] = '<div class="center"><i class="fa fa-check green"></i></div>';
			$row[] = '<div class="center"><i class="fa fa-check green"></i></div>';
			$row[] = '<div class="center"><i class="fa fa-check green"></i></div>';
			$row[] = '<div class="center"><i class="fa fa-check green"></i></div>';
			$row[] = '<div class="center"><i class="fa fa-check green"></i></div>';
			$row[] = '<div class="center"><i class="fa fa-check green"></i></div>';
			$row[] = '<div class="center">'.$uk->pp_nama_rs.'</b> / '.$uk->pp_nama_provinsi.' / '.$uk->pp_nama_kabupaten.'</div>';
			$row[] = '<div class="center">-</div>';
			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->form_wks->dmpcount_all($params),
						"recordsFiltered" => $this->form_wks->dmpcount_filtered($params),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function updateRowStatusUk($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');	

		$this->db->trans_begin();
			$existing = $this->db->get_where('t_usulan_kebutuhan', array('uk_id'=>$id))->row();

			$this->db->update('t_usulan_kebutuhan', array('verifikasi_kemenkes'=>($existing->verifikasi_kemenkes=='N')?'Y':'N'), array('uk_id'=>$id));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function updateRowStatusDmp($id)
	{

		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');	

		$this->db->trans_begin();
			$existing = $this->db->get_where('t_detail_mhs_lulus', array('dmp_id'=>$id))->row();

			$this->db->update('t_detail_mhs_lulus', array('dmp_app_kemenkes'=>($existing->dmp_app_kemenkes=='N')?'Y':'N'), array('dmp_id'=>$id));

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	/*public function penempatan($dmp_id)
	{
		$this->breadcrumbs->push('Form 5b (Analisa Kebutuhan SDMK dan Prediksi Kelulusan FK)', 'form_wks/'.strtolower(get_class($this)));
		$this->breadcrumbs->push('Penempatan Peserta WKDS', 'form_wks/'.strtolower(get_class($this)).'/penempatan/'.$dmp_id.'');
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['value'] = $this->form_wks->get_mhs_by_id($dmp_id);
		$this->authuser->write_log();
		$this->load->view('form_5b/penempatan_form', $data);
	}*/


}
