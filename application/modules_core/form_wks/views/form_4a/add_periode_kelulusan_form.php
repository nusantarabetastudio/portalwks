<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#fk-box option').remove()
                    $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.fk_id+'">'+o.fk_name+'</option>').appendTo($('#fk-box'));
                    });

                });

            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#kab-box'));
            }
        });

        /*$('select[name="fk_id"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                });
            } 

        });*/

    });
</script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="subtitle blue">
      <h3>Periode Kelulusan Mahasiswa</h3>
    </div>

    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value->plm_tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
                <input type="hidden" name="plm_id" value="<?php echo isset($value)?$value->plm_id:0?>">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->plm_bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Semester</label>
              <div class="col-md-2">
                <div class="radio">
                    <?php
                      $katergori_kelas = array('Ganjil','Genap');
                      foreach($katergori_kelas as $row_kategori_kelas){
                        $value_rkk = ($row_kategori_kelas=='Ganjil') ? 1 : 2;
                    ?>
                      <label>
                        <input name="semester" type="radio" class="ace" readonly value="<?php echo $value_rkk?>" <?php echo isset($value->plm_semester) ? ($value->plm_semester == $value_rkk) ? 'checked="checked"' : '' : 'checked'; ?>  />
                        <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                      </label>
                    <?php }?>
                </div>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6','9')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : (isset($value))?$value->id_provinsi:'' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Fakultas Kedokteran</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('9')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_fk(isset($value)?$value->fk_id:'' ,'fk_id','fk-box','form-control','required '.$readonly.'','inline');?>
              </div>
              <?php if(isset($value->plm_id)) : ?>
                <div class="col-md-4">
                  <a href="#" id="btn_update_periode" name="submit" value="submit" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-toggle-down icon-on-right bigger-110"></i>
                    Ubah periode kelulusan
                  </a>
                </div>
              <?php endif; ?>
            </div>
            

        </div>
      </div>

      <div class="subtitle blue">
        <h3>Daftar prediksi mahasiswa lulus</h3>
      </div>
      <br>
      <div class="form-group">
        <label class="control-label col-md-2">Nama Mahasiswa</label>
        <div class="col-md-4">
          <input type="hidden" name="dmp_id" id="dmp_id" class="form-control">
          <input type="text" name="nama_mahasiswa" id="nama_mahasiswa" class="form-control">
        </div>
        <label class="control-label col-md-1">NIM</label>
        <div class="col-md-2">
          <input type="text" name="nim" id="nim" class="form-control">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-2">Prodi</label>
        <div class="col-md-3">
          <?php echo $this->master->get_master_custom(array('table'=>'m_prodi', 'where'=>array('active'=>'Y'), 'id'=>'prod_id', 'name' => 'prod_name'), '','prodi','prodi','form-control','','inline');?>
        </div>

        <label class="control-label col-md-2">Tanggal Lulus</label>
        <div class="col-md-2">
          <div class="input-group">
            <input class="form-control date-picker" name="tgl_lulus" id="tgl_lulus" type="text" data-date-format="yyyy-mm-dd" />
            <span class="input-group-addon">
              <i class="fa fa-calendar bigger-110"></i>
            </span>
          </div>

          <!-- <input type="text" name="tgl_lulus" id="tgl_lulus" class="form-control"> -->
        </div>
      </div>
      
      <div class="form-group">
        <label class="control-label col-md-2">Status Kepesertaan ?</label>
          <div class="col-md-2">
            <div class="radio">
                  <label>
                    <input name="status_peserta" type="radio" class="ace" value="TUBEL"  />
                    <span class="lbl"> TUBEL </span>
                  </label>
                  <label>
                    <input name="status_peserta" type="radio" class="ace" value="MANDIRI" />
                    <span class="lbl"> MANDIRI </span>
                  </label>
            </div>
          </div>
          <label class="control-label col-md-3">Pemberi bantuan pendidikan/beasiswa</label>
          <div class="col-md-2">
            <input type="text" name="dmp_pemberi_beasiswa" id="dmp_pemberi_beasiswa" class="form-control">
          </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-2">Instansi Pengirim</label>
        <div class="col-md-2">
          <input type="text" name="instansi" id="instansi" class="form-control">
        </div>
        <label class="control-label col-md-1">No Ijasah</label>
        <div class="col-md-2">
          <input type="text" name="dmp_no_ijasah" id="dmp_no_ijasah" class="form-control">
        </div>
        
        <div class="col-md-2">
          <a href="#" id="addRow" name="submit" value="submit" class="btn btn-sm btn-primary">
            <div id="btn_name" style="float:left"> <i class="ace-icon fa fa-toggle-down icon-on-right bigger-110"></i>  Tambahkan </div>
          </a>
        </div>
      </div>

      <table id="data_mahasiswa" rel="<?php echo isset($value)?$value->plm_id:0?>" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th width="60px">No</th>
                  <th>Nama Mahasiswa</th>
                  <th>Nama Prodi</th>
                  <th>NIM</th>
                  <th width="200px">Status Kepesertaan<br>(TUBEL/MANDIRI)</th>
                  <!-- <th>Instansi Pengirim</th>
                  <th>Instansi Pemberi Bantuan<br>Pendidikan / Beasiswa</th> -->
                  <th>Tanggal Lulus</th>
                  <th>No Ijasah</th>
                  <th width="100px">Aksi</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table>        

      <div class="form-actions center">

        <a onclick="getMenu('form_wks/form_4a')" href="#" class="btn btn-sm btn-success">
          <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
          Kembali ke sebelumnya
        </a>
      </div>
      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url().'assets/js/custom/form_4a.js'?>"></script>
