<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="col-xs-12 col-sm-8 widget-container-col">
      <div class="widget-box widget-color-red2">
        <!-- #section:custom/widget-box.options -->
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-info"></i>
            Informasi Periode Kelulusan Mahasiswa
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td class="" style="width:20%">Tahun</td>
                  <td class="left">
                    <input type="hidden" name="uk_id" value="<?php echo $value->plm_id?>">
                    <?php echo $value->plm_tahun?>
                   </td>
                </tr>
                <tr>
                  <td class="" style="width:20%">Periode Bulan</td>
                  <td class="left"><?php echo $this->tanggal->getBulan($value->plm_bulan)?></td>
                </tr>
                <tr>
                  <td class="" style="width:20%">Semester</td>
                  <td class="left"><?php echo ($value->plm_semester == 1)?'Ganjil':'Genap'?></td>
                </tr>
                <tr>
                  <td class="" style="width:20%">Provinsi</td>
                  <td class="left"><?php echo $value->nama_provinsi?></td>
                </tr>
                <tr>
                  <td class="" style="width:20%">Fakultas</td>
                  <td class="left"><?php echo $value->fk_name?></td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-4 widget-container-col">
      <div class="widget-box widget-color-green2">
        <!-- #section:custom/widget-box.options -->
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-eye"></i>
            Preview dan Export Data
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td class="center">
                    <a class="btn btn-app btn-danger btn-sm" href="<?php echo base_url().'form_wks/form_1/printPdf/'.$value->plm_id.''?>" target="blank"><i class="ace-icon fa fa-file-pdf-o bigger-200"></i>PDF</a>

                    <a href="<?php echo base_url().'form_wks/form_1/printPreview/'.$value->plm_id.'/excel'?>" class="btn btn-app btn-success btn-sm"><i class="ace-icon fa fa-file-excel-o bigger-200"></i>Excel</a>

                    <a href="<?php echo base_url().'form_wks/form_1/printPreview/'.$value->plm_id.''?>" class="btn btn-app btn-inverse btn-sm" data-remote="false" data-toggle="modal" data-target="#myModal"><i class="ace-icon fa fa-search-plus bigger-200"></i>Preview</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
      <div class="page-header center">
        <h1>Daftar Prediksi Kelulusan Mahasiswa</h1>
      </div>

      <table class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th width="60px">No</th>
                  <th>Nama Prodi</th>
                  <th>Nama Mahasiswa</th>
                  <th>NIM</th>
                  <th>Status Kepesertaan<br>(TUBEL/MANDIRI)</th>
                  <th>Instansi Pengirim</th>
                  <th>Instansi Pemberi Bantuan<br>Pendidikan / Beasiswa</th>
                  <th>Tanggal Lulus</th>
              </tr>
          </thead>
          <tbody>
            <?php $no = 0; foreach($mhs as $rowmhs): $no++;?>
              <tr>
                  <td widtd="60px" class="center"><?php echo $no?></td>
                  <td><?php echo $rowmhs->prod_name?></td>
                  <td><?php echo $rowmhs->dmp_nama?></td>
                  <td><?php echo $rowmhs->dmp_nim?></td>
                  <td class="center"><?php echo $rowmhs->dmp_status_peserta?></td>
                  <td class="center"><?php echo $rowmhs->dmp_instansi?></td>
                  <td class="center"><?php echo $rowmhs->dmp_pemberi_beasiswa?></td>
                  <td class="center"><?php echo $this->tanggal->formatDate($rowmhs->dmp_tanggal_lulus)?></td>
              </tr>
            <?php endforeach;?>
          </tbody>
      </table>        

      <div class="form-actions center">

        <a onclick="getMenu('form_wks/form_4a')" href="#" class="btn btn-sm btn-success">
          <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
          Kembali ke sebelumnya
        </a>
      </div>
      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


<!-- ================== POPUP MODAL ======================= -->
<div class="modal fade" id="myModal" tabindex="-1" size="large" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title center" id="myModalLabel">Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan</h4>
      </div>
      <div class="modal-body">
        <center>
        <table id="" class="table" border="1" width="100%">
            <thead>
              <tr style="background-color:#428bca;color:white">  
                    <th class="center" style="width: 50px">&nbsp;</th>
                    <th class="left" style="width: 250px">Jenis SDM Kesehatan (Khusus dr. Spesialis dan Sub Spesialis)</th>
                    <th class="center" style="width: 120px">Jumlah SDMK<br>Saat Ini</th>
                    <th class="center" style="width: 120px">SDMK Standar</th>
                    <th class="center" style="width: 150px">Kesenjangan<br>( 3 ) - ( 4 )</th>
                    <th class="center" style="width: 120px">Usulan<br>Kebutuhan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">( 1 )</td>
                    <td align="center">( 2 )</td>
                    <td align="center">( 3 )</td>
                    <td align="center">( 4 )</td>
                    <td align="center">( 5 )</td>
                    <td align="center">( 6 )</td>
                </tr>
            </tbody>
            
        </table>


        </center>
      </div>
    </div>
  </div>
</div>



<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url().'assets/js/custom/form_4a.js'?>"></script>
