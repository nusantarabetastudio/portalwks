<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_detail_usulan_kebutuhan">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value['uk']->tahun:date('Y'),'tahun','tahun','form-control','disabled','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($value['uk']->id_provinsi)?$value['uk']->id_provinsi:$this->session->userdata('data_user')->id_provinsi, 'id_provinsi','id_provinsi','form-control','disabled','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten(isset($value['uk']->id_kabupaten)?$value['uk']->id_kabupaten:$this->session->userdata('data_user')->id_kabupaten,'id_kabupaten','kab-box','form-control','disabled','inline');?>
              </div>
            </div>
            
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Rumah Sakit</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_rsu(isset($value['uk']->kode_rs)?$value['uk']->kode_rs:$this->session->userdata('data_user')->kode_rs,'kode_rsu','rsu-box','form-control','disabled','inline');?>
              </div>
            </div>

            

            <div class="form-group" id="alamat_rs">
              <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-10">

                <div class="form-group" id="alamat_rs">
                  <label class="control-label col-md-2">Alamat</label>
                  <div class="col-md-6">
                    <input name="alamat" id="alamat_rs_form" value="<?php echo isset($value['uk']->alamat)?$value['uk']->alamat:''?>"  class="form-control" type="text" disabled>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-2">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_kategori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" readonly value="<?php echo $row_kategori_kelas?>" <?php echo isset($value['uk']->kelas_rs) ? ($value['uk']->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : ''; ?>  />
                            <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>

                  <label class="control-label col-md-1">Jumlah TT</label>
                  <div class="col-md-1">
                    <input name="jumlah_tt" id="jumlah_tt" value="<?php echo isset($value['uk']->jumlah_tt)?$value['uk']->jumlah_tt:''?>" class="form-control" type="text" disabled>
                    <input name="uk_id" id="uk_id" value="<?php echo isset($value['uk']->uk_id)?$value['uk']->uk_id:''?>" class="form-control" type="hidden" disabled>
                    <!-- <input type="text" id="id_perencanaan" name="id_perencanaan" value=""> -->
                  </div>

                </div>

                <div class="form-group">
                  
                  <!-- <div class="col-md-6">
                    <a href="#" id="btn_search" name="submit" value="submit" class="btn btn-sm btn-info">
                      <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                      Pencarian Data
                    </a>
                  </div> -->
                </div>

              </div>
            </div>

        </div>
      </div>

      <?php echo isset($value) ? ($value['uk']->status != 1) ? '<div class="alert alert-info"><b>Pemberitahuan !</b> '.$this->form_wks->getStatusVerifikasi($value['uk']->status).' </div>' : '' : '';?>
      <div id="div_table">
        <table id="data_skm" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>  
                    <th class="center" rowspan="2" style="width: 50px">&nbsp;</th>
                    <th class="center" rowspan="2" style="width:250px">Jenis SDM Kesehatan<br>(Khusus dr. Spesialis dan Sub Spesialis)</th>
                    <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                    <th class="center" rowspan="2" style="width: 70px">SDMK Standar</th>
                    <th class="center" rowspan="2" style="width: 70px">Kesenjangan<br>( 9 ) - ( 10 )</th>
                    <th class="center" rowspan="2" style="width: 80px">Usulan<br>Kebutuhan</th>
                    <th class="center" rowspan="2" style="width: 60px">Hapus</th>
                </tr>

                <tr>
                    <th class="center" style="width:70px">PNS/<br>Pegawai Tetap</th>
                    <th class="center" style="width:70px">PPPK</th>
                    <th class="center" style="width:70px">PTT</th>
                    <th class="center" style="width:70px">Honorer/<br>Kontrak</th>
                    <th class="center" style="width:70px">BLU / BLUD</th>
                    <th class="center" style="width:70px">TKS</th>
                    <th class="center" style="width:70px">Total</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td class="center">( 1 )</td>
                    <td class="center">( 2 )</td>
                    <td class="center">( 3 )</td>
                    <td class="center">( 4 )</td>
                    <td class="center">( 5 )</td>
                    <td class="center">( 6 )</td>
                    <td class="center">( 7 )</td>
                    <td class="center">( 8 )</td>
                    <td class="center">( 9 )</td>
                    <td class="center">( 10 )</td>
                    <td class="center">( 11 )</td>
                    <td class="center">( 12 )</td>
                    <td class="center">( 13 )</td>
                </tr>
                <?php 
                  if(isset($value)) :
                    if( $value['duk']['status'] == 1) :
                      if( count($value['duk']['result']) > 0 ) :
                        $no = 0; 
                        foreach($value['duk']['result'] as $rowduk) :
                          $no++; ?>
                        
                        <tr style="background-color:#84c3f4">
                          <td class="center"><?php echo $no;?></td>
                          <td class="left" colspan="12"><?php echo '<b>'.$rowduk->nama_rs.'</b>';?></td>
                        </tr>

                        <?php $noa=0; foreach($rowduk->detail as $rowdduk) : $noa++;?>
                        <tr id="<?php echo $rowdduk->duk_id?>">
                          <td class="center"></td>
                          <td class="left"><?php echo '&nbsp;&nbsp;&nbsp;'.$noa.'. '.$rowdduk->nama_jenis_sdmk.'';?></td>
                          <td class="center"><?php echo $rowdduk->jml_pns?></td>
                          <td class="center"><?php echo $rowdduk->jml_pppk?></td>
                          <td class="center"><?php echo $rowdduk->jml_ptt?></td>
                          <td class="center"><?php echo $rowdduk->jml_honorer?></td>
                          <td class="center"><?php echo $rowdduk->jml_blud?></td>
                          <td class="center"><?php echo $rowdduk->jml_tks?></td>
                          <td class="center"><?php echo $rowdduk->total_jml?></td>
                          <td class="center"><?php echo $rowdduk->total_standar?></td>
                          <td class="center"><?php echo $rowdduk->total_kesenjangan?></td>
                          <td class="center">
                            <div class="center">
                              <input type="text" class="form-control" value="<?php echo $rowdduk->usulan_kebutuhan?>" name="usulan_kebutuhan[<?php echo $rowdduk->duk_id?>]">
                            </div>
                          </td>
                          <td class="center">
                            <?php if($rowduk->status == 1) {?>
                              <div class="center">
                                <a href="#" onclick="delete_uk_sdmk(<?php echo $rowdduk->duk_id?>)" id="btnReset" class="btn btn-sm btn-danger">
                                  <i class="ace-icon fa fa-trash-o icon-on-right bigger-110"></i>
                                </a>
                              </div>
                            <?php }else{ echo '-'; } ?>
                          </td>
                      </tr>

                <?php 
                          endforeach;
                        endforeach;
                      endif; 
                    endif; 
                  endif; ?>

            </tbody>
        </table>

        <div id="accordion" class="accordion-style1 panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  <i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                  &nbsp; Note / Catatan 
                </a>
              </h4>
            </div>

            <div class="panel-collapse collapse" id="collapseOne">
              <div class="panel-body">
                <div class="comments">
                <?php if( !empty($note)) { foreach($note as $rownote) :?>
                  <div class="itemdiv commentdiv">
                    <div class="body">
                      <div class="name">
                        <a href="#"><?php echo $rownote->created_by?> | <i class="ace-icon fa fa-clock-o"></i>
                        <span class="green"><?php echo $this->tanggal->formatDateTime($rownote->created_date)?></span> </a>
                      </div>
                      <div class="text">
                        <i class="ace-icon fa fa-quote-left"></i>
                        <?php echo $rownote->nv_description?>
                      </div>
                    </div>
                  </div>
                <?php endforeach; } else{ echo '<p>Tidak ada catatan</p>'; }?>
                  
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="form-actions center">

          <a onclick="getMenu('form_wks/form_1')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>

          <a onclick="getMenu('form_wks/form_1')" href="#" class="btn btn-sm btn-grey">
            <i class="ace-icon fa fa-file-excel-o icon-on-right bigger-110"></i>
            Cetak Excel
          </a> 

          <?php if($value['uk']->status == 1) : ?>

            <button type="reset" onclick="getMenu('form_wks')" id="btnReset" class="btn btn-sm btn-danger">
              <i class="ace-icon fa fa-trash-o icon-on-right bigger-110"></i>
              Hapus semua
            </button>

            <a href="#" id="btn_update_usulan_kebutuhan" name="submit" value="submit" class="btn btn-sm btn-primary">
              <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
              Proses Ubah data
            </a>

            <a href="#" id="btn_send" onclick="sendToVerifikator(<?php echo $value['uk']->status?>)" name="submit" value="submit" class="btn btn-sm btn-inverse">
              <i class="ace-icon fa fa-send icon-on-right bigger-110"></i>
              Kirim ke Dinkes Kab/Kota
            </a>

          <?php endif; ?>

        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>
