<script type="text/javascript">
   
    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#fk-box option').remove()
                    $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.fk_id+'">'+o.fk_name+'</option>').appendTo($('#fk-box'));
                    });

                });

            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });
        
        $('select[name="fk_id"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    /*$('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);*/

                });
            } 

        });

    });
</script>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="page-header center">
      <h1>Daftar Prediksi Kelulusan Mahasiswa Program Pendidikan Dokter Spesialis </h1>
    </div>

    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Bulan</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->tahun:date('Y'),'bulan','bulan','form-control','required','inline');?>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2">Semester</label>
              <div class="col-md-2">
                <div class="radio">
                    <?php
                      $katergori_kelas = array('Ganjil','Genap');
                      foreach($katergori_kelas as $row_kategori_kelas){
                        $value_rkk = ($row_kategori_kelas=='Ganjil') ? 1 : 2;
                    ?>
                      <label>
                        <input name="semester" type="radio" class="ace" readonly value="<?php echo $value_rkk?>" <?php echo isset($profil->kelas_rs) ? ($profil->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : 'checked'; ?>  />
                        <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                      </label>
                    <?php }?>
                </div>
              </div>
            </div>
            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6','9')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
            <div class="form-group" id="form-fk">
              <label class="control-label col-md-2">Fakultas Kedokteran</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('9')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_fk(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','fk_id','fk-box','form-control','required '.$readonly.'','inline');?>
              </div>
              <div class="col-md-4">
                <a href="#" id="btn_search_uk" name="submit" value="submit" class="btn btn-sm btn-default">
                  <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                  Cari
                </a>
                <a href="#" id="btn_reset" name="submit" value="submit" class="btn btn-sm btn-default">
                  <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
                  Reset
                </a>
              </div>
            </div>
            
            <div class="form-actions center">
                <a href="#" class="btn btn-sm btn-primary" id="add_periode_kelulusan"><i class="glyphicon glyphicon-plus"></i> Buat periode kelulusan </a>
                <!-- <button class="btn btn-sm btn-success" onclick="add()"><i class="fa fa-file-excel-o"></i> Export Excel </button>
                <button class="btn btn-sm btn-danger" onclick="add()"><i class="fa fa-file-pdf-o"></i> Export PDF </button> -->
            </div>

        </div>
      </div>

      <table id="dynamic-table" class="table table-striped table-bordered table-hover">
             <thead>
              <tr>  
                <th class="center" style="width: 30px"></th>
                <th class="center" style="width: 60px">ID</th>
                <th style="width:150px">Fakultas Kedokteran</th>
                <th style="width:150px">Provinsi</th> 
                <th class="center" style="width:70px">Tahun</th>
                <th class="center" style="width:70px">Periode</th> 
                <th style="width:70px">Semester</th> 
                <!-- <th style="width:150px">Kab/Kota</th> --> 
                <th class="center" style="width:80px">Total<br>Mahasiswa<br>Lulus</th>
                <th class="center" style="width:80px">Total<br>TUBEL</th>
                <th class="center" style="width:80px">Total<br>MANDIRI</th>
                <th class="center" style="width:80px">Total<br>Disetujui<br>Kolegium</th>
                <th class="center" style="width:80px">Status<br>Verifikasi<br>Kolegium</th>
                <th style="width: 100px" class="center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>

    </form>


  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url().'assets/js/custom/form_4a.js'?>"></script>
