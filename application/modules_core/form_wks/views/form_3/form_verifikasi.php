<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
.checkbox{ float:left !important; margin-top:-8px !important; margin-right: -20px}
.input{ width: 55px; text-align: center; margin-right:-8px; margin-top: -5px; margin-bottom: -10px; border-radius: 20px !important; float:left !important  }
</style>

<div class="row">
  <div class="col-xs-12">

      <div class="page-header center" id="page_title"><h1>Verifikasi Usulan Kebutuhan SDM Kesehatan Rumah Sakit <br> oleh Dinkes Provinsi</h1></div>

      <div class="col-xs-12 col-sm-6 widget-container-col">
        <div class="widget-box widget-color-grey">
          <!-- #section:custom/widget-box.options -->
          <div class="widget-header">
            <h5 class="widget-title bigger lighter">
              <i class="ace-icon fa fa-eye"></i>
              Detil Pengajuan Usulan Kebutuhan
            </h5>
          </div>

          <!-- /section:custom/widget-box.options -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <table class="table table-striped table-bordered table-hover">
                <tbody>
                  <tr>
                    <td class="" style="width:20%">Tahun</td>
                    <td class="left"><?php echo $value['uk']->tahun?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Periode</td>
                    <td class="left"><?php echo $this->tanggal->getBulan($value['uk']->bulan)?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Provinsi</td>
                    <td class="left"><?php echo ucfirst($value['uk']->nama_provinsi)?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Kab/Kota</td>
                    <td class="left"><?php echo ucfirst($value['uk']->nama_kabupaten)?></td>
                  </tr>

                  <tr>
                    <td class="" style="width:20%">Rumah Sakit</td>
                    <td class="left"><?php echo ucfirst($value['uk']->nama_rs)?></td>
                  </tr>


                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-5 widget-container-col">
        <div class="widget-box widget-color-grey">
          <div class="widget-header">
            <h5 class="widget-title bigger lighter">
              <i class="ace-icon fa fa-exchange"></i>
              Status Verifikasi Dinkes
            </h5>
          </div>

          <!-- /section:custom/widget-box.options -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <table class="table table-striped table-bordered table-hover">
                <tbody>
                  <tr>
                    <td class="">Verifikasi Dinkes Kab/Kota</td>
                    <td class="center"><?php echo ($value['uk']->verifikasi_dinkes_kab == 'Y')?'<i class="fa fa-check green"></i>':'<i class="fa fa-times red"></i>'?></td>
                    <td class="center"><?php echo $this->tanggal->formatDateTime($value['uk']->ver_dk_date)?></td>
                  </tr>

                  <tr>
                    <td class="">Verifikasi Dinkes Provinsi</td>
                    <td class="center"><?php echo ($value['uk']->verifikasi_dinkes_prov == 'Y')?'<i class="fa fa-check green"></i>':'<i class="fa fa-times red"></i>'?></td>
                    <td class="center"><?php echo $this->tanggal->formatDateTime($value['uk']->ver_dp_date)?></td>
                  </tr>


                </tbody>
              </table>
            </div>
          </div>
        </div>
        <br>
        <a class="btn btn-app btn-danger btn-sm" href="<?php echo base_url().'form_wks/form_1/printPdf/'.$value['uk']->uk_id.''?>" target="blank"><i class="ace-icon fa fa-file-pdf-o bigger-200"></i>PDF</a>

        <a href="<?php echo base_url().'form_wks/form_1/printPreview/'.$value['uk']->uk_id.'/excel'?>" class="btn btn-app btn-success btn-sm"><i class="ace-icon fa fa-file-excel-o bigger-200"></i>Excel</a>

        <a href="<?php echo base_url().'form_wks/form_1/printPreview/'.$value['uk']->uk_id.''?>" class="btn btn-app btn-inverse btn-sm" data-remote="false" data-toggle="modal" data-target="#myModal"><i class="ace-icon fa fa-search-plus bigger-200"></i>Preview</a>
      
    </div>

    <div class="clearfix"></div>
    <hr class="separator">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_usulan_sdmk">

      <div class="tabbable">
        <ul class="nav nav-tabs" id="myTab">
          <li class="active">
            <a data-toggle="tab" href="#data">
              <i class="green ace-icon fa fa-list bigger-120"></i>
              Data Yang Diusulkan
            </a>
          </li>

          <li class="red">
            <a data-toggle="tab" href="#status">
              <i class="ace-icon fa fa-circle-o bigger-120"></i> Rekapitulasi dan Status Proses
            </a>
          </li>

          <li>
            <a data-toggle="tab" href="#note">
              <i class="red ace-icon fa fa-edit bigger-120"></i> Catatan Hasil Verifikasi
            </a>
          </li>

          <li>
            <a data-toggle="tab" href="#file">
              <i class="pink ace-icon fa fa-file-o bigger-120"></i> Lampiran File
            </a>
          </li>

        </ul>

        <div class="tab-content">
          <div id="data" class="tab-pane fade in active">
              <div class="page-header center"><h1>Data usulan kebutuhan SDM Kesehatan</h1></div>
              <table id="data_skm" class="table table-striped table-bordered table-hover">
                <thead>
                    <tr>  
                        <th class="center" rowspan="2" style="width: 50px">&nbsp;</th>
                        <th class="center" rowspan="2" style="width:250px">Jenis SDM Kesehatan<br>(Khusus dr. Spesialis dan Sub Spesialis)</th>
                        <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                        <th class="center" rowspan="2">SDMK Standar</th>
                        <th class="center" rowspan="2" style="width: 120px">Kesenjangan<br>( 9 ) - ( 10 )</th>
                        <th class="center" rowspan="2" style="width: 60px">Usulan<br>Kebutuhan</th>
                        <th class="center" rowspan="2" style="width: 100px">Verifikasi <br> Kab/Kota</th>
                        <th class="center" rowspan="2" style="width: 100px">Verifikasi <br> Provinsi</th>
                    </tr>

                    <tr>
                        <th class="center" style="width:70px">PNS/<br>Pegawai Tetap</th>
                        <th class="center" style="width:70px">PPPK</th>
                        <th class="center" style="width:70px">PTT</th>
                        <th class="center" style="width:70px">Honorer/<br>Kontrak</th>
                        <th class="center" style="width:70px">BLU / BLUD</th>
                        <th class="center" style="width:70px">TKS</th>
                        <th class="center" style="width:70px">Total</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="center">( 1 )</td>
                        <td class="center">( 2 )</td>
                        <td class="center">( 3 )</td>
                        <td class="center">( 4 )</td>
                        <td class="center">( 5 )</td>
                        <td class="center">( 6 )</td>
                        <td class="center">( 7 )</td>
                        <td class="center">( 8 )</td>
                        <td class="center">( 9 )</td>
                        <td class="center">( 10 )</td>
                        <td class="center">( 11 )</td>
                        <td class="center">( 12 )</td>
                        <td class="center">( 13 )</td>
                        <td class="center">( 14 )</td>
                    </tr>
                    <?php 
                      if(isset($value)) :
                        if( $value['duk']['status'] == 1) :
                          if( count($value['duk']['result']) > 0 ) :
                            $no = 0; 
                            foreach($value['duk']['result'] as $rowduk) :
                              $no++; ?>
                            
                            <tr>
                              <td class="center"><?php echo $no;?></td>
                              <td class="left" colspan="13">
                                <?php echo '<b>'.$rowduk->nama_rs.'</b>';?>
                                <input type="hidden" name="uk_id[]" value="<?php echo $rowduk->uk_id?>">  
                              </td>
                            </tr>

                            <?php $noa=0; foreach($rowduk->detail as $rowdduk) : $noa++;?>
                            <tr>
                              <td class="center"></td>
                              <td class="left"><?php echo '&nbsp;&nbsp;&nbsp;'.$noa.'. '.$rowdduk->nama_jenis_sdmk.'';?></td>
                              <td class="center"><?php echo $rowdduk->jml_pns?></td>
                              <td class="center"><?php echo $rowdduk->jml_pppk?></td>
                              <td class="center"><?php echo $rowdduk->jml_ptt?></td>
                              <td class="center"><?php echo $rowdduk->jml_honorer?></td>
                              <td class="center"><?php echo $rowdduk->jml_blud?></td>
                              <td class="center"><?php echo $rowdduk->jml_tks?></td>
                              <td class="center"><?php echo $rowdduk->total_jml?></td>
                              <td class="center"><?php echo $rowdduk->total_standar?></td>
                              <td class="center"><?php echo $this->apps->get_format($rowdduk->total_kesenjangan)?></td>
                              <td class="center"><?php echo $rowdduk->usulan_kebutuhan?></td>
                              <td class="center">
                                <?php echo ($rowdduk->app_dinkes_kab == 'Y')?'<b><span style="font-size:16px;color:green">'.$rowdduk->jml_disetujui_kab.'</span><b>':'<i class="fa fa-times red"></i>';?>

                              </td>

                              <td class="center">
                                <center>
                                <?php if($rowduk->status == 3) {?>
                                  <?php if($rowdduk->app_dinkes_kab == 'Y') {?>
                                    <?php if($rowdduk->app_dinkes_prov == 'Y') :?>
                                      <div id="get_<?php echo $rowdduk->duk_id?>">
                                        <input type="text" readonly class="input" name="disetujui_prov_<?php echo $rowdduk->duk_id?>" value="<?php echo ($rowdduk->jml_disetujui_kab != 0)?$rowdduk->jml_disetujui_kab:$rowdduk->usulan_kebutuhan?>">
                                      </div>
                                      <?php endif; ?>
                                    <div id="insertinputs_<?php echo $rowdduk->duk_id?>"></div>
                                    <div class="checkbox">
                                      <label class="block">
                                          <input onclick="dynInput(this, <?php echo $rowdduk->duk_id?>, <?php echo ($rowdduk->jml_disetujui_kab != 0)?$rowdduk->jml_disetujui_kab:$rowdduk->usulan_kebutuhan?>);" name="checked[<?php echo $rowdduk->duk_id?>]" id="<?php echo $rowdduk->duk_id?>" type="checkbox" class="ace input-lg" value="Y" <?php echo ($rowdduk->app_dinkes_prov == 'Y')?'checked':''?>/>
                                          <span class="lbl bigger-70">&nbsp;</span>
                                      </label>
                                    </div>

                                  <?php }else{ echo '<i class="fa fa-times red"></i>';}?>

                                <?php }else{
                                  echo ($rowdduk->app_dinkes_prov == 'Y')?'<b><span style="font-size:16px;color:green">'.$rowdduk->jml_disetujui_prov.'</span><b>':'<i class="fa fa-times red"></i>';
                                  }?>
                                  </center>
                              </td>

                              <!-- <td class="center">
                                <?php if($rowduk->status == 3) { ?>
                                  <?php if($rowdduk->app_dinkes_kab == 'Y'){?>
                                  <div class="checkbox" style="margin:-10px"><label class="block"><input name="checked[<?php echo $rowdduk->duk_id?>]" type="checkbox" class="ace input-lg" value="Y" <?php echo ($rowdduk->app_dinkes_prov == 'Y')?'checked':''?>/><span class="lbl bigger-70">&nbsp;</span></label></div>
                                  <?php }else{ echo '<i class="fa fa-times red"></i>'; }?>
                                <?php }else{
                                  echo ($rowdduk->app_dinkes_prov == 'Y')?'<i class="fa fa-check green"></i>':'<i class="fa fa-times red"></i>';
                                  }?>

                              </td> -->
                          </tr>
                          <?php 
                            $jml_pns[] = $rowdduk->jml_pns;
                            $jml_pppk[] = $rowdduk->jml_pppk;
                            $jml_ptt[] = $rowdduk->jml_ptt;
                            $jml_honorer[] = $rowdduk->jml_honorer;
                            $jml_blud[] = $rowdduk->jml_blud;
                            $jml_tks[] = $rowdduk->jml_tks;
                            $total_jml[] = $rowdduk->total_jml;
                            $total_standar[] = $rowdduk->total_standar;
                            $total_kesenjangan[] = $rowdduk->total_kesenjangan;
                            $total_diusulkan[] = $rowdduk->usulan_kebutuhan;
                            $jml_disetujui_kab[] = $rowdduk->jml_disetujui_kab;
                            $jml_disetujui_prov[] = $rowdduk->jml_disetujui_prov;

                            endforeach;
                          ?>

                          <xtr style="background-color:#b6b2b2">
                              <td class="center"></td>
                              <td class="center"><b>TOTAL SDMK</b></td>
                              <td class="center"><b><?php echo array_sum($jml_pns)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_pppk)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_ptt)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_honorer)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_blud)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_tks)?></b></td>
                              <td class="center"><b><?php echo array_sum($total_jml)?></b></td>
                              <td class="center"><b><?php echo array_sum($total_standar)?></b></td>
                              <td class="center"><b><?php echo array_sum($total_kesenjangan)?></b></td>
                              <td class="center"><b><?php echo array_sum($total_diusulkan)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_disetujui_kab)?></b></td>
                              <td class="center"><b><?php echo array_sum($jml_disetujui_prov)?></b></td>
                          </tr>

                    <?php 
                              
                            endforeach;
                          endif; 
                        endif; 
                      endif; ?>

                </tbody>
            </table>
          </div>

          <div id="status" class="tab-pane fade">

            <div class="row">
                  <div class="col-xs-12 col-sm-6 widget-container-col">
                    <div class="widget-box widget-color-green">
                      <!-- #section:custom/widget-box.options -->
                      <div class="widget-header">
                        <h5 class="widget-title bigger lighter">
                          <i class="ace-icon fa fa-calculator"></i>
                          Rekapitulasi Total
                        </h5>
                      </div>

                      <!-- /section:custom/widget-box.options -->
                      <div class="widget-body">
                        <div class="widget-main no-padding">
                          <table class="table table-striped table-bordered table-hover">

                            <tbody>
                              <tr>
                                <td class="">Total jenis SDMK yang diusulkan</td>
                                <td class="center"><?php echo $value['uk']->total_jenis_sdmk?></td>
                              </tr>
                              <tr>
                                <td class="">Total SDMK saat ini</td>
                                <td class="center"><?php echo $value['uk']->total_saat_ini?></td>
                              </tr>
                              <tr>
                                <td class="">Standar SDMK</td>
                                <td class="center"><?php echo $value['uk']->total_standar?></td>
                              </tr>
                              <tr>
                                <td class="">Kesenjangan</td>
                                <td class="center"><?php echo $this->apps->get_format($value['uk']->total_kesenjangan_all)?></td>
                              </tr>
                              <tr>
                                <td class="">Total yang diusulkan Rumah Sakit</td>
                                <td class="center"><?php echo $value['uk']->total_usulan_kebutuhan?></td>
                              </tr>
                              <tr>
                                <td class="">Total yang disetujui oleh Dinkes Kab/Kota</td>
                                <td class="center"><?php echo ($value['uk']->total_disetujui)?$value['uk']->total_disetujui:'<i class="fa fa-times red"></i>'?></td>
                              </tr>
                              <tr>
                                <td class="">Total yang disetujui oleh Dinkes Provinsi</td>
                                <td class="center"><?php echo ($value['uk']->total_disetujui_prov)?$value['uk']->total_disetujui_prov:'<i class="fa fa-times red"></i>'?></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-xs-12 col-sm-6 widget-container-col">

                    <div class="widget-box widget-color-red">
                      <!-- #section:custom/widget-box.options -->
                      <div class="widget-header">
                        <h5 class="widget-title bigger lighter">
                          <i class="ace-icon fa fa-gavel"></i>
                          Status Akhir Hasil Pengajuan Usulan Kebutuhan
                        </h5>
                      </div>

                      <!-- /section:custom/widget-box.options -->
                      <div class="widget-body">
                        <div class="widget-main no-padding">
                          <table class="table table-striped table-bordered table-hover">
                            <tbody>
                              <tr>
                                <td class="">Status pengajuan usulan kebutuhan</td>
                                <td class="center"><?php echo ucfirst($value['uk']->status_proses)?></td>
                              </tr>

                              <tr>
                                <td class="">Total SDMK yang akan ditempatkan</td>
                                <td class="center">-</td>
                              </tr>

                              <tr>
                                <td class="">Daftar dokter spesialis yang akan ditempatkan</td>
                                <td class="center"><a href="#"><i>Download Lampiran</i></a></td>
                              </tr>

                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div><!-- /.span -->
                </div>

          </div>

          <div id="note" class="tab-pane fade">
            <div class="panel-body">
                <div class="comments">
                <?php if( !empty($note)) { foreach($note as $rownote) :?>
                  <div class="itemdiv commentdiv">
                    <div class="body">
                      <div class="name">
                        <a href="#"><?php echo $rownote->created_by?> | <i class="ace-icon fa fa-clock-o"></i>
                        <span class="green"><?php echo $this->tanggal->formatDateTime($rownote->created_date)?></span> </a>
                      </div>
                      <div class="text">
                        <i class="ace-icon fa fa-quote-left"></i>
                        <?php echo $rownote->nv_description?>
                      </div>
                    </div>
                  </div>
                <?php endforeach; } else{ echo '<p>Tidak ada catatan</p>'; }?>
                  
                </div>
              </div>
          </div>

          <div id="file" class="tab-pane fade">
            <div class="panel-body">
                <?php echo ($file != '')?$file:'Tidak ada file'?>
              </div>
          </div>

        </div>
      </div>

      <div class="clearfix"></div>
      <hr class="separator">

      <div id="div_table">

        <div class="form-group">
          <label class="control-label col-md-2">Note</label>
          <div class="col-md-10">
            <textarea class="form-control" name="note" placeholder="Silahkan masukan note..."></textarea>  
          </div>
        </div>

        <div class="form-actions center">

          <a onclick="getMenu('form_wks/form_3')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>
          <?php if($value['uk']->approval_prov == 'Y' and in_array($value['uk']->status, array('3','4'))) :?>
              <a href="#" id="btn_review" onclick="approval(<?php echo $value['uk']->uk_id; ?>, 'R')" name="submit" value="submit" class="btn btn-sm btn-warning">
                <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Review kembali
              </a>
          <?php endif; ?>
          <?php if($value['uk']->status == 3) :?>
              <a href="#" id="btn_save_verifikasi" name="submit" value="submit" class="btn btn-sm btn-primary">
                <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                Proses verifikasi
              </a>
          <?php if($value['uk']->verifikasi_dinkes_prov == 'Y') :?>
          <a href="#" id="btn_approved" onclick="sendToVerifikator(<?php echo $value['uk']->uk_id; ?>, 'Y')" name="submit" value="submit" class="btn btn-sm btn-warning">
            <i class="ace-icon fa fa-check icon-on-right bigger-110"></i>
            Setujui
          </a>
          <a href="#" id="btn_not_approved" onclick="sendToVerifikator(<?php echo $value['uk']->uk_id; ?>, 'N')" name="submit" value="submit" class="btn btn-sm btn-danger">
            <i class="ace-icon fa fa-times-circle icon-on-right bigger-110"></i>
            Tidak disetujui
          </a>
        <?php endif; endif; ?>
        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


<!-- ================== POPUP MODAL ======================= -->
<div class="modal fade" id="myModal" tabindex="-1" size="large" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title center" id="myModalLabel">Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan<br><?php echo $value['duk']['result'][0]->nama_rs?> 
          Tahun <?php echo $value['uk']->tahun?> </h4>
      </div>
      <div class="modal-body">
      <div class="widget-box widget-color-blue">
        <!-- #section:custom/widget-box.options -->
            <div class="widget-main no-padding">
              <table class="table table-striped table-bordered table-hover">
                <tbody>
                  <tr>
                    <td class="" style="width:20%">Tahun</td>
                    <td class="left"><?php echo $value['uk']->tahun?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Periode</td>
                    <td class="left"><?php echo $this->tanggal->getBulan($value['uk']->bulan)?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Provinsi</td>
                    <td class="left"><?php echo ucfirst($value['uk']->nama_provinsi)?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Kab/Kota</td>
                    <td class="left"><?php echo ucfirst($value['uk']->nama_kabupaten)?></td>
                  </tr>

                  <tr>
                    <td class="" style="width:20%">Rumah Sakit</td>
                    <td class="left"><?php echo ucfirst($value['uk']->nama_rs)?></td>
                  </tr>


                </tbody>
              </table>
            </div>
        </div>
        <center>
        <table id="" class="table" border="1" width="100%">
            <thead>
              <tr style="background-color:#428bca;color:white">  
                    <th class="center" style="width: 50px">&nbsp;</th>
                    <th class="left" style="width: 250px">Jenis SDM Kesehatan (Khusus dr. Spesialis dan Sub Spesialis)</th>
                    <th class="center" style="width: 120px">Jumlah SDMK<br>Saat Ini</th>
                    <th class="center" style="width: 120px">SDMK Standar</th>
                    <th class="center" style="width: 150px">Kesenjangan<br>( 3 ) - ( 4 )</th>
                    <th class="center" style="width: 120px">Usulan<br>Kebutuhan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">( 1 )</td>
                    <td align="center">( 2 )</td>
                    <td align="center">( 3 )</td>
                    <td align="center">( 4 )</td>
                    <td align="center">( 5 )</td>
                    <td align="center">( 6 )</td>
                </tr>
                <?php 
                  if(isset($value)) :
                    if( $value['duk']['status'] == 1) :
                      if( count($value['duk']['result']) > 0 ) :
                        $no = 0; 
                        foreach($value['duk']['result'] as $rowduk) :
                          $no++; ?>
                        
                        <tr>
                          <td align="center"><?php echo $no;?></td>
                          <td class="left" colspan="5"><?php echo '<b>'.$rowduk->nama_rs.'</b>';?></td>
                        </tr>

                        <?php $noa=0; foreach($rowduk->detail as $rowdduk) : $noa++;?>
                        <tr id="<?php echo $rowdduk->duk_id?>">
                          <td align="center"></td>
                          <td class="left"><?php echo '&nbsp;&nbsp;&nbsp;'.$noa.'. '.$rowdduk->nama_jenis_sdmk.'';?></td>
                          <td align="center"><?php echo $rowdduk->total_jml?></td>
                          <td align="center"><?php echo $rowdduk->total_standar?></td>
                          <td align="center"><?php echo $this->apps->get_format($rowdduk->total_kesenjangan)?></td>
                          <td align="center">
                            <?php echo $rowdduk->usulan_kebutuhan?>
                          </td>
                          
                      </tr>

                <?php 
                          endforeach;
                        endforeach;
                      endif; 
                    endif; 
                  endif; ?>

                  </tbody>
            
        </table>

        <div>
          <p align="right">
                <u><i>(<?php echo $value['uk']->nama_kabupaten.', '.date('d/m/Y')?>)</i></u><br>
                Mengetahui,<br> 
                (Kepala RS.....................)
                <br>
                <br>
                <br>
                TTD Kepala RS dan Stempel RS<br>
                (nama dan gelar)
              </p>
        </div>

        </center>
      </div>
    </div>
  </div>
</div>

<!-- ================== END POPUP MODAL ======================= -->

<script src="<?php echo base_url().'assets/js/custom/form_3.js'?>"></script>

<script type="text/javascript">
 function dynInput(cbox, id, val) {

  $('#get_'+id+'').hide();

  if (cbox.checked) {
    var input = document.createElement("input");
    input.type = "text";
    input.name = "disetujui_prov_"+id+"";
    input.value = val;
    input.className = "input";
    input.readOnly = true;
    var div = document.createElement("div");
    div.id = cbox.name;
    //div.innerHTML = cbox.name;
    div.appendChild(input);
    document.getElementById("insertinputs_"+id+"").appendChild(div);
  } else {
    document.getElementById(cbox.name).remove();
  }
}
</script>
