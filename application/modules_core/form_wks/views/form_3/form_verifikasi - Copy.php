<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo $this->master->get_tahun(isset($value)?$value['uk']->tahun:date('Y'),'tahun','tahun','form-control','disabled','inline');?>
              </div>
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo $this->master->get_bulan(isset($value)?$value['uk']->bulan:date('m'),'bulan','bulan','form-control','disabled','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($value['uk']->id_provinsi)?$value['uk']->id_provinsi:$this->session->userdata('data_user')->id_provinsi, 'id_provinsi','id_provinsi','form-control','disabled','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten(isset($value['uk']->id_kabupaten)?$value['uk']->id_kabupaten:$this->session->userdata('data_user')->id_kabupaten,'id_kabupaten','kab-box','form-control','disabled','inline');?>
              </div>
            </div>
            
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Rumah Sakit</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_rsu(isset($value['uk']->kode_rs)?$value['uk']->kode_rs:$this->session->userdata('data_user')->kode_rs,'kode_rsu','rsu-box','form-control','disabled','inline');?>
              </div>

              <!-- <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-2">
                <a href="#" id="btn_search" name="submit" value="submit" class="btn btn-sm btn-info">
                  <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                  Pencarian Data
                </a>
              </div> -->

            </div>

            <div class="form-group">
              <label class="control-label col-md-2">File lampiran</label>
              <div class="col-md-4">
                <div style="padding-top:8px;text-align:none;">
                <?php echo ($file != '')?$file:'Tidak ada file'?>
                </div>
              </div>
            </div>

            

        </div>
      </div>

      <div id="search_status"></div>
      <div class="page-header center" id="page_title"><h1>Verifikasi Usulan Kebutuhan SDM Kesehatan Rumah Sakit <br> oleh Dinkes Provinsi</h1></div>
      <div id="div_table">
        <table id="data_skm" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>  
                    <th class="center" rowspan="2" style="width: 50px">&nbsp;</th>
                    <th class="center" rowspan="2" style="width:250px">Jenis SDM Kesehatan<br>(Khusus dr. Spesialis dan Sub Spesialis)</th>
                    <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                    <th class="center" rowspan="2">SDMK Standar</th>
                    <th class="center" rowspan="2" style="width: 120px">Kesenjangan<br>( 9 ) - ( 10 )</th>
                    <th class="center" rowspan="2" style="width: 60px">Usulan<br>Kebutuhan</th>
                    <th class="center" rowspan="2" style="width: 60px">Verifikasi <br> Kab/Kota</th>
                    <th class="center" rowspan="2" style="width: 60px">Verifikasi <br> Provinsi</th>
                </tr>

                <tr>
                    <th class="center" style="width:100px">PNS/<br>Pegawai Tetap</th>
                    <th class="center" style="width:100px">PPPK</th>
                    <th class="center" style="width:100px">PTT</th>
                    <th class="center" style="width:100px">Honorer/<br>Kontrak</th>
                    <th class="center" style="width:100px">BLU / BLUD</th>
                    <th class="center" style="width:100px">TKS</th>
                    <th class="center" style="width:100px">Total</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td class="center">( 1 )</td>
                    <td class="center">( 2 )</td>
                    <td class="center">( 3 )</td>
                    <td class="center">( 4 )</td>
                    <td class="center">( 5 )</td>
                    <td class="center">( 6 )</td>
                    <td class="center">( 7 )</td>
                    <td class="center">( 8 )</td>
                    <td class="center">( 9 )</td>
                    <td class="center">( 10 )</td>
                    <td class="center">( 11 )</td>
                    <td class="center">( 12 )</td>
                    <td class="center">( 13 )</td>
                    <td class="center">( 14 )</td>
                </tr>
                <?php 
                  if(isset($value)) :
                    if( $value['duk']['status'] == 1) :
                      if( count($value['duk']['result']) > 0 ) :
                        $no = 0; 
                        foreach($value['duk']['result'] as $rowduk) :
                          $no++; ?>
                        
                        <tr style="background-color:#84c3f4">
                          <td class="center"><?php echo $no;?></td>
                          <td class="left" colspan="13">
                            <?php echo '<b>'.$rowduk->nama_rs.'</b>';?>
                            <input type="hidden" name="uk_id[]" value="<?php echo $rowduk->uk_id?>">  
                          </td>
                        </tr>

                        <?php $noa=0; foreach($rowduk->detail as $rowdduk) : $noa++;?>
                        <tr>
                          <td class="center"></td>
                          <td class="left"><?php echo '&nbsp;&nbsp;&nbsp;'.$noa.'. '.$rowdduk->nama_jenis_sdmk.'';?></td>
                          <td class="center"><?php echo $rowdduk->jml_pns?></td>
                          <td class="center"><?php echo $rowdduk->jml_pppk?></td>
                          <td class="center"><?php echo $rowdduk->jml_ptt?></td>
                          <td class="center"><?php echo $rowdduk->jml_honorer?></td>
                          <td class="center"><?php echo $rowdduk->jml_blud?></td>
                          <td class="center"><?php echo $rowdduk->jml_tks?></td>
                          <td class="center"><?php echo $rowdduk->total_jml?></td>
                          <td class="center"><?php echo $rowdduk->total_standar?></td>
                          <td class="center"><?php echo $this->apps->get_format($rowdduk->total_kesenjangan)?></td>
                          <td class="center"><?php echo $rowdduk->usulan_kebutuhan?></td>
                          <td class="center">
                            <?php echo ($rowdduk->app_dinkes_kab == 'Y')?'<i class="fa fa-check green"></i>':'<i class="fa fa-times red"></i>';?>

                          </td>
                          <td class="center">
                            <?php if($rowduk->status == 3) { ?>
                              <?php if($rowdduk->app_dinkes_kab == 'Y'){?>
                              <div class="checkbox" style="margin:-10px"><label class="block"><input name="checked[<?php echo $rowdduk->duk_id?>]" type="checkbox" class="ace input-lg" value="Y" <?php echo ($rowdduk->app_dinkes_prov == 'Y')?'checked':''?>/><span class="lbl bigger-70">&nbsp;</span></label></div>
                              <?php }else{ echo '<i class="fa fa-times red"></i>'; }?>
                            <?php }else{
                              echo ($rowdduk->app_dinkes_prov == 'Y')?'<i class="fa fa-check green"></i>':'<i class="fa fa-times red"></i>';
                              }?>

                          </td>
                      </tr>
                      <?php 
                        $jml_pns[] = $rowdduk->jml_pns;
                        $jml_pppk[] = $rowdduk->jml_pppk;
                        $jml_ptt[] = $rowdduk->jml_ptt;
                        $jml_honorer[] = $rowdduk->jml_honorer;
                        $jml_blud[] = $rowdduk->jml_blud;
                        $jml_tks[] = $rowdduk->jml_tks;
                        $total_jml[] = $rowdduk->total_jml;
                        $total_standar[] = $rowdduk->total_standar;
                        $total_kesenjangan[] = $rowdduk->total_kesenjangan;
                        $total_diusulkan[] = $rowdduk->usulan_kebutuhan;

                        endforeach;
                      ?>

                      <tr style="background-color:#b6b2b2">
                          <td class="center"></td>
                          <td class="center"><b>TOTAL SDMK</b></td>
                          <td class="center"><b><?php echo array_sum($jml_pns)?></b></td>
                          <td class="center"><b><?php echo array_sum($jml_pppk)?></b></td>
                          <td class="center"><b><?php echo array_sum($jml_ptt)?></b></td>
                          <td class="center"><b><?php echo array_sum($jml_honorer)?></b></td>
                          <td class="center"><b><?php echo array_sum($jml_blud)?></b></td>
                          <td class="center"><b><?php echo array_sum($jml_tks)?></b></td>
                          <td class="center"><b><?php echo array_sum($total_jml)?></b></td>
                          <td class="center"><b><?php echo array_sum($total_standar)?></b></td>
                          <td class="center"><b><?php echo array_sum($total_kesenjangan)?></b></td>
                          <td class="center"><b><?php echo array_sum($total_diusulkan)?></b></td>
                          <td class="center"><b><?php echo $rowduk->total_disetujui?></b></td>
                          <td class="center"><b><?php echo $rowduk->total_disetujui_prov?></b></td>
                      </tr>

                <?php 
                          
                        endforeach;
                      endif; 
                    endif; 
                  endif; ?>

            </tbody>
        </table>

        <div id="accordion" class="accordion-style1 panel-group">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                  <i class="ace-icon fa fa-angle-down bigger-110" data-icon-hide="ace-icon fa fa-angle-down" data-icon-show="ace-icon fa fa-angle-right"></i>
                  &nbsp; Note / Catatan 
                </a>
              </h4>
            </div>

            <div class="panel-collapse collapse" id="collapseOne">
              <div class="panel-body">
                <div class="comments">
                <?php if( !empty($note)) { foreach($note as $rownote) :?>
                  <div class="itemdiv commentdiv">
                    <div class="body">
                      <div class="name">
                        <a href="#"><?php echo $rownote->created_by?> | <i class="ace-icon fa fa-clock-o"></i>
                        <span class="green"><?php echo $this->tanggal->formatDateTime($rownote->created_date)?></span> </a>
                      </div>
                      <div class="text">
                        <i class="ace-icon fa fa-quote-left"></i>
                        <?php echo $rownote->nv_description?>
                      </div>
                    </div>
                  </div>
                <?php endforeach; } else{ echo '<p>Tidak ada catatan</p>'; }?>
                  
                </div>
              </div>
            </div>
          </div>
          
        </div>
        

        <div class="form-group">
          <label class="control-label col-md-2">Note</label>
          <div class="col-md-8">
            <textarea class="form-control" name="note" placeholder="Silahkan masukan note..."></textarea>  
          </div>
        </div>

        <div class="form-actions center">

          <a onclick="getMenu('form_wks/form_3')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>
          <?php 
          $atts = array(
                  'class'       => 'btn btn-sm btn-grey',
                  'width'       => 1100,
                  'height'      => 700,
                  'screenx'   =>  200,
                  'screeny'   =>  200,
                  'directories'   =>  0,
                  'location'   =>  0,
                  'status'   =>  0,
                  'menubar'   =>  0,
                  'scrollbar'   =>  'yes',
          );
          echo anchor_popup('form_wks/form_3/printPreview/'.$value['uk']->uk_id.'', '<i class="fa fa-print"></i> Print Preview', $atts);?>
          <?php if($value['uk']->approval_prov == 'Y' and in_array($value['uk']->status, array('3','4'))) :?>
              <a href="#" id="btn_review" onclick="approval(<?php echo $value['uk']->uk_id; ?>, 'R')" name="submit" value="submit" class="btn btn-sm btn-warning">
                <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
              Review kembali
              </a>
          <?php endif; ?>
          <?php if($value['uk']->status == 3) :?>
              <a href="#" id="btn_save_verifikasi" name="submit" value="submit" class="btn btn-sm btn-primary">
                <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                Proses verifikasi
              </a>
          <?php if($value['uk']->verifikasi_dinkes_prov == 'Y') :?>
          <a href="#" id="btn_approved" onclick="sendToVerifikator(<?php echo $value['uk']->uk_id; ?>, 'Y')" name="submit" value="submit" class="btn btn-sm btn-warning">
            <i class="ace-icon fa fa-check icon-on-right bigger-110"></i>
            Setujui
          </a>
          <a href="#" id="btn_not_approved" onclick="sendToVerifikator(<?php echo $value['uk']->uk_id; ?>, 'N')" name="submit" value="submit" class="btn btn-sm btn-danger">
            <i class="ace-icon fa fa-times-circle icon-on-right bigger-110"></i>
            Tidak disetujui
          </a>
        <?php endif; endif; ?>
        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_3.js'?>"></script>
