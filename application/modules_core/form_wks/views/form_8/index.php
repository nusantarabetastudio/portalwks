<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
      <div class="widget-body">
        <div class="widget-main no-padding">
          <form class="form-horizontal" method="post" id="form_search_data">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>

              <label class="control-label col-md-1">Bulan</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->tahun:date('m'),'tahun','tahun','form-control','required','inline');?>
              </div>

            </div>

            <div class="form-group">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="col-md-6">
                <a href="#" id="btn_search" name="submit" value="submit" class="btn btn-sm btn-info">
                  <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                  Pencarian Data
                </a>
                <b><i>&nbsp;&nbsp;  * Silahkan lakukan pencarian data SKM terlebih dahulu</i></b>
              </div>
            </div>

          </form>
        </div>
      </div>

      <hr class="sparator">
      <div id="search_status"></div>
      <div class="page-header center" id="page_title"><h1>Surat Penugasan Penempatan Wajib Kerja Spesialis</h1></div>
      <div id="div_table">

        <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>NIM</th>
                    <th>Prodi/Jurusan</th>
                    <th>Asal FK</th>
                    <th>Tempat Penugasan</th>
                </tr>
            </thead>

            <tbody>
                <?php for($i=1;$i<3;$i++):?>
                <tr>
                    <td class="center"><?php echo $i?></td>
                    <td>dr.Muhammad Amin Lubis</td>
                    <td>108093000086</td>
                    <td>Penyakit Dalam</td>
                    <td>UNS</td>
                    <td>Papua</td>
                </tr>
            <?php endfor;?>
            </tbody>
        </table>

      </div>

      <div class="form-actions center">

        <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-success">
          <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
          Kembali ke sebelumnya
        </a>
        <!-- <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-warning">
          <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
          Lihat Riwayat Perencanaan
        </a> -->
        <!-- <button type="reset" onclick="getMenu('form_wks')" id="btnReset" class="btn btn-sm btn-danger">
          <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
          Hapus semua usulan kebutuhan
        </button> -->
        <button type="submit" id="btnSave" name="submit" value="submit" class="btn btn-sm btn-info">
          <i class="ace-icon fa fa-print icon-on-right bigger-110"></i>
          Cetak
        </button>
      </div>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>
