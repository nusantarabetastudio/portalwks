<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        <!-- <div class="clearfix">
          <button class="btn btn-sm btn-primary" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Fakultas Kedokteran </button><div class="pull-right tableTools-container"></div>
        </div> -->

        <div class="page-header center">
          <h1>Verifikasi Usulan Kebutuhan SDM Kesehatan Rumah Sakit<br>Oleh Dinkes Kab/Kota</h1>
        </div>

        <div>

        <form class="form-horizontal" method="post" id="form_usulan_sdmk">

          <div class="form-group">
            <label class="control-label col-md-2">Tahun</label>
            <div class="col-md-2">
              <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
            </div>
            <label class="control-label col-md-2 ">Periode Bulan</label>
            <div class="col-md-2">
              <?php echo Master::get_bulan(isset($value)?$value->bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
            </div>
          </div>

          <div class="form-group" id="form-provinsi">
            <label class="control-label col-md-2">Provinsi</label>
            <div class="col-md-3">
              <?php 
                $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5','6')) ) ? 'readonly' : '' ;  
                echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
            </div>

            <label class="control-label col-md-1">Kabupaten</label>
            <div class="col-md-3">
              <?php 
                $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5')) ) ? 'readonly' : '' ;  
                echo Master::get_change_master_kabupaten(isset($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
            </div>
          </div>
          
          <div class="form-group" id="form-rsu" >
            <label class="control-label col-md-2">Rumah Sakit</label>
            <div class="col-md-4">
              <?php 
                $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3')) ) ? 'readonly' : '' ; 
                echo Master::get_change_master_rsu(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','kode_rsu','rsu-box','form-control','required '.$readonly.'','inline');?><br><i>*Silahkan lakukan pencarian data berdasarkan parameter diatas</i>
            </div>
            <div class="col-md-4">
              <a href="#" id="btn_search_uk" name="submit" value="submit" class="btn btn-sm btn-default">
                <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                Cari
              </a>
              <a href="#" id="btn_reset" name="submit" value="submit" class="btn btn-sm btn-default">
                <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
                Reset
              </a>
            </div>

          </div>

          <!-- <div class="form-actions center">
              <button class="btn btn-sm btn-success" onclick="add()"><i class="fa fa-file-excel-o"></i> Export Excel </button>
              <button class="btn btn-sm btn-danger" onclick="add()"><i class="fa fa-file-pdf-o"></i> Export PDF </button>
          </div> -->

            <table id="dynamic-table" class="table table-striped table-bordered table-hover">
               <thead>
                <tr>  
                  <th class="center" style="width: 20px"></th>
                  <th class="center" style="width: 70px">ID</th>
                  <th style="width:250px">Nama Rumah Sakit</th>
                  <th style="width:100px">Tahun</th>
                  <th style="width:150px">Provinsi</th>
                  <th style="width:150px">Kabupaten</th> 
                  <th style="width:120px" class="center">Jumlah<br>Jenis SDMK</th>
                  <th style="width:100px" class="center">Total<br>saat ini</th>
                  <th style="width:100px" class="center">Kesenjangan</th>
                  <th style="width:100px" class="center">Total <br>diusulkan RS</th>
                  <th style="width:100px" class="center">Disetujui<br>Dinkes<br>Kab/Kota</th>
                  <th style="width:100px" class="center">Disetujui<br>Dinkes<br>Prov</th>
                  <th style="width: 100px" class="center">Verifikasi<br>Dinkes<br>Kab</th>
                  <th style="width: 100px" class="center">Verifikasi<br>Dinkes<br>Prov</th>
                  <th style="width: 120px" class="center">Aksi</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
        </div>
      </div>
    </div>
    </form>
  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_2.js'?>"></script>
