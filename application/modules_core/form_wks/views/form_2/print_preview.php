<?php 
  if(isset($type)){
    if($type != ''){
      $filename = 'PRINT'.Date("ymd").".xls";
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=$filename");
      header('Cache-Control: public');
      $width = '80%';
    }else{
      $width = '100%';
    }
  }
?>

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css" />

<style type="text/css">
table, h3{
  font-family: 'lucida grande', helvetica, verdana, arial, sans-serif;
}
.table {
    border-collapse: collapse;
    border-top: 1px;
}
th, td {
    padding: 5px;
}
th{
  height: 50px;
}

</style>

<center>
<h3>
  Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan <br> <?php echo $value['duk']['result'][0]->nama_rs?> 
  Tahun <?php echo $value['uk']->tahun?> </h3>
  <br>

<table id="" class="table" border="1" width="<?php echo $width?>">
    <thead>
      <tr style="background-color:#428bca;color:white">  
            <th align="center" style="width: 50px">&nbsp;</th>
            <th class="left">Jenis SDM Kesehatan (Khusus dr. Spesialis dan Sub Spesialis)</th>
            <th align="center" style="width: 170px">Jumlah SDMK Saat Ini</th>
            <th align="center" style="width: 150px">SDMK Standar</th>
            <th align="center" style="width: 150px">Kesenjangan<br>( 3 ) - ( 4 )</th>
            <th align="center" style="width: 100px">Usulan<br>Institusi</th>
            <th align="center" style="width: 100px">Verifikasi<br>Kab/Kota</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="center">( 1 )</td>
            <td align="center">( 2 )</td>
            <td align="center">( 3 )</td>
            <td align="center">( 4 )</td>
            <td align="center">( 5 )</td>
            <td align="center">( 6 )</td>
            <td align="center">( 7 )</td>
        </tr>
        <?php 
          if(isset($value)) :
            if( $value['duk']['status'] == 1) :
              if( count($value['duk']['result']) > 0 ) :
                $no = 0; 
                foreach($value['duk']['result'] as $rowduk) :
                  $no++; ?>
                
                <tr>
                  <td align="center"><?php echo $no;?></td>
                  <td class="left" colspan="6"><?php echo '<b>'.$rowduk->nama_rs.'</b>';?></td>
                </tr>

                <?php $noa=0; foreach($rowduk->detail as $rowdduk) : $noa++;?>
                <tr id="<?php echo $rowdduk->duk_id?>">
                  <td align="center"></td>
                  <td class="left"><?php echo '&nbsp;&nbsp;&nbsp;'.$noa.'. '.$rowdduk->nama_jenis_sdmk.'';?></td>
                  <td align="center"><?php echo $rowdduk->total_jml?></td>
                  <td align="center"><?php echo $rowdduk->total_standar?></td>
                  <td align="center"><?php echo $this->apps->get_format($rowdduk->total_kesenjangan)?></td>
                  <td align="center">
                    <?php echo $rowdduk->usulan_kebutuhan?>
                  </td>
                  <td align="center">
                  <?php echo ($rowdduk->app_dinkes_kab == 'Y') ? '<label style="color:green">Y</label>' : '<label style="color:red">N</label>' ;?>
                  </td>
                  
              </tr>

        <?php 
                  endforeach;
                endforeach;
              endif; 
            endif; 
          endif; ?>

          </tbody>
    
</table>
<br><br>
<?php if($type == '') :?>
<a href="<?php echo base_url().'form_wks/form_2/printPreview/'.$value['uk']->uk_id.'/excel'?>" class="btn btn-sm btn-success">
  <i class="ace-icon fa fa-file-excel-o icon-on-right bigger-110"></i>
  Cetak Excel
</a>  
<a id="printPreview" href="#" class="btn btn-sm btn-danger">
  <i class="ace-icon fa fa-file-pdf-o icon-on-right bigger-110"></i>
  Cetak PDF
</a> 
<?php endif; ?>

</center>




