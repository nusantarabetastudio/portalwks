<script type="text/javascript">
   
    $(function() {
        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#fk-box option').remove()
                    $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.fk_id+'">'+o.fk_name+'</option>').appendTo($('#fk-box'));
                    });

                });

            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        


    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="page-header center">
      <h1>Analisa Kebutuhan SDMK dan Prediksi Kelulusan Program Pendidikan Dokter Spesialis</h1>
    </div>

    <form class="form-horizontal" method="post" id="form_usulan_sdmk" action="action.php">

      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Semester</label>
              <div class="col-md-2">
                <div class="radio">
                    <?php
                      $katergori_kelas = array('Ganjil','Genap');
                      foreach($katergori_kelas as $row_kategori_kelas){
                        $value_rkk = ($row_kategori_kelas=='Ganjil') ? 1 : 2;
                    ?>
                      <label>
                        <input name="semester" type="radio" class="ace" readonly value="<?php echo $value_rkk?>" <?php echo isset($profil->kelas_rs) ? ($profil->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : 'checked'; ?>  />
                        <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                      </label>
                    <?php }?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->tahun:date('Y'),'bulan','bulan','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Sampai</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->tahun:date('Y'),'to','to','form-control','required','inline');?>
              </div>
              
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten('','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
            
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Fakultas Kedokteran</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_fk('','fk_id','fk-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-fk">
              <label class="control-label col-md-2">&nbsp;</label>
              <div class="col-md-5">
                <a href="#" id="btn_search_and_anlyze" name="submit" value="submit" class="btn btn-sm btn-primary">
                  <i class="ace-icon fa fa-toggle-down icon-on-right bigger-110"></i>
                  Tampilkan
                </a>
                <a href="#" id="btn_reset" name="submit" value="submit" class="btn btn-sm btn-default">
                  <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
                  Reset
                </a>
                <!-- <button class="btn btn-sm btn-success" onclick="add()"><i class="fa fa-file-excel-o"></i> Export Excel </button>
                <button class="btn btn-sm btn-danger" onclick="add()"><i class="fa fa-file-pdf-o"></i> Export PDF </button> -->
              </div>
            </div>




            <!-- <div class="form-group" id="alamat_rs">
              <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-10">


                <div class="form-actions center">

                  <a href="#" id="btn_search_and_anlyze" name="submit" value="history" class="btn btn-block btn-danger">
                      <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                      Lakukan proses pencarian data dan mulai menganalisa
                    </a>

                </div>

              </div>
            </div> -->

        </div>
      </div>

      <table id="data_prediksi" class="table table-striped table-bordered" cellspacing="0" width="100%">
          <thead>
              <tr>
                  <th rowspan="2" align="center">No</th>
                  <th class="center" colspan="7">Biodata Mahasiswa</th>
                  <th class="center" colspan="6">Kelengkapan Administrasi</th>
                  <th class="center" rowspan="2">Penempatan</th>
                  <th class="center" rowspan="2">No Registrasi</th>
              </tr>
              <tr>
                  <th>Nama</th>
                  <th>Provinsi</th>
                  <th>NIM</th>
                  <th class="center">Prodi/<br>Jurusan<br>&nbsp;</th>
                  <th class="center">Asal<br>Fakultas</th>
                  <th class="center">Status<br>Peserta</th>
                  <th class="center">Status</th>
                  <th class="center" style="width:60px">Foto</th>
                  <th class="center" style="width:60px">Ijasak/ SKL</th>
                  <th class="center" style="width:60px">Buku Tab</th>
                  <th class="center" style="width:60px">KTP</th>
                  <th class="center" style="width:60px">Pakta Integritas</th>
                  <th class="center" style="width:60px">SPPD</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
      </table> 
      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5b.js'?>"></script>

<!-- <script src="<?php echo base_url()?>assets/js/jquery.raty.js"></script>
<script type="text/javascript">
  jQuery(function($){

    $('.rating').raty({
      'cancel' : true,
      'half': true,
      'starType' : 'i'
    })

  });
</script> -->