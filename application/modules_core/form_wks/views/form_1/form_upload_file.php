<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_upload_and_send" enctype="multipart/form-data">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo $this->master->get_tahun(isset($value)?$value['uk']->tahun:date('Y'),'tahun','tahun','form-control','disabled','inline');?>
              </div>
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo $this->master->get_bulan(isset($value)?$value['uk']->bulan:date('m'),'bulan','bulan','form-control','disabled','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php  
                  echo $this->master->get_master_provinsi(isset($value['uk']->id_provinsi)?$value['uk']->id_provinsi:$this->session->userdata('data_user')->id_provinsi, 'id_provinsi','id_provinsi','form-control','disabled','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php  
                  echo $this->master->get_change_master_kabupaten(isset($value['uk']->id_kabupaten)?$value['uk']->id_kabupaten:$this->session->userdata('data_user')->id_kabupaten,'id_kabupaten','kab-box','form-control','disabled','inline');?>
              </div>
            </div>
            
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Rumah Sakit</label>
              <div class="col-md-3">
                <?php 
                  echo $this->master->get_change_master_rsu(isset($value)?$value['uk']->kode_rs:$this->session->userdata('data_user')->kode_user,'kode_rsu','rsu-box','form-control','disabled','inline');?>
              </div>
            </div>

            

            <div class="form-group" id="alamat_rs">
              <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-10">

                <div class="form-group" id="alamat_rs">
                  <label class="control-label col-md-2">Alamat</label>
                  <div class="col-md-6">
                    <input name="alamat" id="alamat_rs_form" value="<?php echo isset($value['uk']->alamat)?$value['uk']->alamat:''?>"  class="form-control" type="text" disabled>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-3">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_kategori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" readonly value="<?php echo $row_kategori_kelas?>" <?php echo isset($value['uk']->kelas_rs) ? ($value['uk']->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : ''; ?>  />
                            <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>

                  <label class="control-label col-md-1">Jumlah TT</label>
                  <div class="col-md-1">
                    <input name="jumlah_tt" id="jumlah_tt" value="<?php echo isset($value['uk']->jumlah_tt)?$value['uk']->jumlah_tt:''?>" class="form-control" type="text" disabled>
                    
                    <!-- <input type="text" id="id_perencanaan" name="id_perencanaan" value=""> -->
                  </div>

                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Upload lampiran</label>
                  <div class="col-md-4">
                    <input name="uk_id" id="uk_id" value="<?php echo $value['uk']->uk_id ?>" type="hidden">
                    <input type="file" name="file" id="file" class="form-control" />
                  </div>
                </div>

              </div>
            </div>

        </div>
      </div>
      
      <div id="div_table">
        <div class="form-actions center">

          <a onclick="getMenu('form_wks/form_1')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>

          <?php if($value['uk']->status == 1) : ?>

            <a href="#" id="btn_proses_upload_and_send_to_verifikator" name="submit" value="submit" class="btn btn-sm btn-inverse">
              <i class="ace-icon fa fa-send icon-on-right bigger-110"></i>
              Proses upload dan lanjutkan kirim ke Dinkes Kab/Kota
            </a>

          <?php endif; ?>

        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>
