<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="alert alert-warning"><b>[ <i class="fa fa-info"></i> ] Pemberitahuan !</b> Silahkan lakukan pencarian data SKM terlebih dahulu untuk dapat membuat usulan kebutuhan.</div>
    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo $this->master->get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo $this->master->get_bulan(isset($value)?$value->bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5','6')) ) ? 'readonly' : '' ;  
                  echo $this->master->get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5')) ) ? 'readonly' : '' ;  
                  echo $this->master->get_change_master_kabupaten(isset($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
            
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Rumah Sakit</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3')) ) ? 'readonly' : '' ; 
                  echo $this->master->get_change_master_rsu(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','kode_rsu','rsu-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>

            <div class="form-group" id="alamat_rs">
              <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-10">

                <div class="form-group">
                  <label class="control-label col-md-2">Kategori Kelas</label>
                  <div class="col-md-9">
                    <div class="radio">
                        <?php
                          $katergori_kelas = array('A','B','C','D');
                          foreach($katergori_kelas as $row_kategori_kelas){
                        ?>
                          <label>
                            <input name="kategori_kelas" type="radio" class="ace" readonly value="<?php echo $row_kategori_kelas?>" <?php echo isset($profil->kelas_rs) ? ($profil->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : ''; ?>  />
                            <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                          </label>
                        <?php }?>
                    </div>
                  </div>
                </div>

                

                <div class="form-actions center">
                    <a onclick="getMenu('form_wks/form_1')" href="#" class="btn btn-sm btn-success">
                      <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                      Kembali ke sebelumnya
                    </a>
                    <a href="#" id="btn_search" name="submit" value="submit" class="btn btn-sm btn-info">
                      <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                      Pencarian Data SKM
                    </a>
                </div>

              </div>
            </div>

        </div>
      </div>

      <div id="search_status"></div>
      <div class="page-header center" id="page_title"></div>
      
      <div id="div_table" style="display:none">
        <table id="dynamic-table" class="table table-striped table-bordered table-hover" rel="">
            <thead>
                <tr>  
                    <th class="center" rowspan="2" style="width: 50px">&nbsp;</th>
                    <th class="center" rowspan="2" style="width:250px">Jenis SDM Kesehatan<br>(Khusus dr. Spesialis dan Sub Spesialis)</th>
                    <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                    <th class="center" rowspan="2">SDMK Standar</th>
                    <th class="center" rowspan="2" style="width: 120px">Kesenjangan<br>( 9 ) - ( 10 )</th>
                    <th class="center" rowspan="2" style="width: 60px">Usulan<br>Kebutuhan</th>
                </tr>

                <tr>
                    <th class="center" style="width:100px">PNS/<br>Pegawai Tetap</th>
                    <th class="center" style="width:100px">PPPK</th>
                    <th class="center" style="width:100px">PTT</th>
                    <th class="center" style="width:100px">Honorer/<br>Kontrak</th>
                    <th class="center" style="width:100px">BLU / BLUD</th>
                    <th class="center" style="width:100px">TKS</th>
                    <th class="center" style="width:100px">Total</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <td class="center">( 1 )</td>
                    <td class="center">( 2 )</td>
                    <td class="center">( 3 )</td>
                    <td class="center">( 4 )</td>
                    <td class="center">( 5 )</td>
                    <td class="center">( 6 )</td>
                    <td class="center">( 7 )</td>
                    <td class="center">( 8 )</td>
                    <td class="center">( 9 )</td>
                    <td class="center">( 10 )</td>
                    <td class="center">( 11 )</td>
                    <td class="center">( 12 )</td>
                </tr>
            </tbody>
        </table>

        <div class="form-actions center">

          <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>
          <!-- <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-warning">
            <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
            Lihat Riwayat Perencanaan
          </a> -->
          <!-- <button type="reset" onclick="getMenu('form_wks')" id="btnReset" class="btn btn-sm btn-danger">
            <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
            Hapus semua usulan kebutuhan
          </button> -->
          <a href="#" id="btn_save_usulan" name="submit" value="submit" class="btn btn-sm btn-info">
            <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
            Proses usulan kebutuhan
          </a>
        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>
