<div>
  <table id="data_history" class="table table-striped table-bordered table-hover">
     <thead>
      <tr>  
        <th class="center" style="width: 50px"></th>
        <th class="center" style="width: 80px">ID</th>
        <th>Nama Rumah Sakit</th>
        <th style="width:150px">Provinsi</th>
        <th style="width:150px">Kabupaten</th>
        <th style="width:100px">Tahun</th>
        <th style="width: 200px">Status</th>
        <th style="width: 100px">Aksi</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
</div>

<script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>
