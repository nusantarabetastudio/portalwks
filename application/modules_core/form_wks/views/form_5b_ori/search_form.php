<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->

    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Semester</label>
              <div class="col-md-2">
                <div class="radio">
                    <?php
                      $katergori_kelas = array('Ganjil','Genap');
                      foreach($katergori_kelas as $row_kategori_kelas){
                        $value_rkk = ($row_kategori_kelas=='Ganjil') ? 1 : 2;
                    ?>
                      <label>
                        <input name="semester" type="radio" class="ace" readonly value="<?php echo $value_rkk?>" <?php echo isset($profil->kelas_rs) ? ($profil->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : 'checked'; ?>  />
                        <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                      </label>
                    <?php }?>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1 "> Sampai </label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->bulan:date('m'),'sampai_bulan','sampai_bulan','form-control','required','inline');?>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3','5')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten(isset($this->session->userdata('data_user')->id_kabupaten) ? $this->session->userdata('data_user')->id_kabupaten : '','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
            
            <div class="form-actions center">

              <a href="#" id="btn_view_history" name="submit" value="history" class="btn btn-sm btn-warning">
                <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                Pencarian Data Peserta WKS
              </a>
              <!-- <a href="#" id="btn_search" name="submit" value="submit" class="btn btn-sm btn-info">
                <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                Pencarian Data SKM
              </a> -->

            </div>

          </div>
        </div>

        </div>
      </div>

      <div id="search_status"></div>
      <div class="page-header center" id="page_title"></div>

      <div id="div_history" style="display:none">
        <table id="data_history" class="table table-striped table-bordered table-hover">
           <thead>
             <tr>
                <th width="60px">No</th>
                <th>Nama Mahasiswa</th>
                <th>NIM</th>
                <th>Nama Prodi</th>
                <th>Asal FK</th>
                <th>Tanggal Lulus</th>
                <th>Penempatan</th>
                <th>Nomor Registrasi</th>
                <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
      
      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5b.js'?>"></script>
