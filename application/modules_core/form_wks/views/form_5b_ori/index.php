<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        

        <div class="page-header center">
          <h1>Daftar Kelulusan Mahasiswa Fakultas Kedokteran</h1>
        </div>
        <div class="clearfix" style="padding-left:10px">
          <button class="btn btn-sm btn-primary" onclick="search_form()"><i class="glyphicon glyphicon-plus"></i> Pencarian Daftar Peserta WKS </button><div class="pull-right tableTools-container"></div>
        </div>
        <div>

          <table id="dynamic-table" class="table table-striped table-bordered table-hover">
             <thead>
              <tr>  
                <th class="center" style="width: 30px"></th>
                <th class="center" style="width: 60px">ID</th>
                <th class="center" style="width:100px">Tahun</th>
                <th class="center" style="width:100px">Periode<br>Bulan</th> 
                <th style="width:100px">Semester</th> 
                <th style="width:150px">Provinsi</th> 
                <th style="width:150px">Kab/Kota</th> 
                <th style="width:150px">Fakultas Kedokteran</th>
                <th class="center" style="width:100px">Total<br>Mahasiswa</th>
                <th class="center" style="width:100px">Total<br>Disetujui</th>
                <th class="center" style="width:100px">Status<br>Verifikasi</th>
                <th style="width: 100px" class="center">Aksi</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5b.js'?>"></script>
