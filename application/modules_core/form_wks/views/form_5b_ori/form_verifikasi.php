<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_312204').hide();
                $('#form_312205').hide();
                $('#form_312206 ').hide();
            }else{
                $('#form_312204').show();
                $('#form_312205').show();
                $('#form_312206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">

        </div>
      </div>

      <div id="search_status"></div>
      <div class="page-header center" id="page_title">
        <h1>Daftar Peserta Wajib Kerja Dokter Spesialis (WKDS)<br>
          <?php $semester = ($value['periode_lulus']->plm_semester==1)?'Ganjil':'Genap'; echo 'Provinsi '.$value['periode_lulus']->nama_provinsi.', Kab/Kota '.$value['periode_lulus']->nama_kabupaten.' <br> '.$value['periode_lulus']->fk_name.'<br> Semester '.$semester.' Periode '.$this->tanggal->getBulan($value['periode_lulus']->plm_bulan).' Tahun '.$value['periode_lulus']->plm_tahun?>
        </h1></div>
      <div id="div_table">
        <table id="data-mhs" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="50px">No</th>
                    <th>Prodi</th>
                    <th>Universitas</th>
                    <th>Provinsi</th>
                    <th>Nama dr</th>
                    <th>NIM</th>
                    <th>Tanggal Lulus</th>
                    <th class="center" width="120px">Verifikasi<br>Kolegium</th>
                    <th class="center" width="120px">Status<br>Penempatan</th>
                    <th class="center" width="120px">Penempatan</th>
                    <th class="center" width="120px">Keterangan</th>
                    <!-- <th width="100px">Keterangan</th> -->
                </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($value['detail_mhs'] as $rowmhs) :?>
              <tr>
                <td width="50px" class="center"><?php echo $no;?></td>
                <td><?php echo $rowmhs->prod_name?></td>
                <td><?php echo $value['periode_lulus']->fk_name?></td>
                <td><?php echo $rowmhs->nama_provinsi?></td>
                <td><?php echo $rowmhs->dmp_nama?></td>
                <td><?php echo $rowmhs->dmp_nim?></td>
                <td><?php echo $this->tanggal->formatDate($rowmhs->dmp_tanggal_lulus)?></td>
                <td class="center">
                  <input type="hidden" name="plm_id" value="<?php echo $rowmhs->plm_id?>">
                  <?php echo $this->form_wks->getStatusVerifikasi($rowmhs->dmp_app_kolegium);?>
                </td>
                <td align="center"><?php echo ($rowmhs->uk_id != NULL)?'<span style="color:green">Sesuai</span>':'<span style="color:red">Tidak Sesuai</span>'?></td>
                <td align="center"><?php echo isset($rowmhs->nama_rs)?$rowmhs->nama_rs:''?></td>
                <td align="center"><?php echo $rowmhs->keterangan?></td>
                <!-- 
                <td class="center"><input type="text" class="form-control" name=""></td> -->
              </tr>
            <?php $no++; endforeach;?>
            </tbody>
        </table>

        <p>
          <b>Keterangan : <br><b>
          <table>
            <tr>
              <td style="width:100px"><b><span style="color:green">(Sesuai)</span></b></td>
              <td> : Sesuai dengan usulan kebutuhan</td>
            </tr>
            <tr>
              <td style="width:100px"><b><span style="color:red">(Tidak Sesuai)</span></b></td>
              <td> : Tidak sesuai dengan usulan kebutuhan</td>
            </tr>
          </table>
          <!-- <span style="color:green">Sesuai</span> : Sesuai dengan usulan kebutuhan <br>
          <span style="color:red">Tidak Sesuai</span> : Tidak sesuai dengan usulan kebutuhan -->
        </p>
        <div class="form-actions center">

          <a onclick="getMenu('form_wks/form_5b')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>

          <!-- <?php 
          $atts = array(
                  'class'       => 'btn btn-sm btn-grey',
                  'width'       => 1100,
                  'height'      => 600,
                  'screenx'   =>  200,
                  'screeny'   =>  200,
                  'directories'   =>  0,
                  'location'   =>  0,
                  'status'   =>  0,
                  'menubar'   =>  0,
                  'scrollbar'   =>  'yes',
          );
          echo anchor_popup('form_wks/form_5b/printPreview/'.$value['periode_lulus']->plm_id.'', '<i class="fa fa-print"></i> Print Preview', $atts);?> -->



          <!-- <a href="#" id="btn_save_verifikasi" name="submit" value="submit" class="btn btn-sm btn-primary">
            <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
            Proses verifikasi
          </a> -->
          
        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5b.js'?>"></script>
