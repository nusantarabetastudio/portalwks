<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="col-xs-12 col-sm-8 widget-container-col">
        <div class="widget-box widget-color-pink">
          <!-- #section:custom/widget-box.options -->
          <div class="widget-header">
            <h5 class="widget-title bigger lighter">
              <i class="ace-icon fa fa-info"></i>
              Informasi Periode Kelulusan Mahasiswa
            </h5>
          </div>

          <!-- /section:custom/widget-box.options -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <table class="table table-striped table-bordered table-hover">
                <tbody>
                  <tr>
                    <td class="" style="width:20%">Tahun</td>
                    <td class="left">
                      <input type="hidden" name="uk_id" value="<?php echo $value['periode_lulus']->plm_id?>">
                      <?php echo $value['periode_lulus']->plm_tahun?>
                     </td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Periode Bulan</td>
                    <td class="left"><?php echo $this->tanggal->getBulan($value['periode_lulus']->plm_bulan)?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Semester</td>
                    <td class="left"><?php echo ($value['periode_lulus']->plm_semester == 1)?'Ganjil':'Genap'?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Provinsi</td>
                    <td class="left"><?php echo $value['periode_lulus']->nama_provinsi?></td>
                  </tr>
                  <tr>
                    <td class="" style="width:20%">Fakultas</td>
                    <td class="left"><?php echo $value['periode_lulus']->fk_name?></td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      <div class="col-xs-12 col-sm-4 widget-container-col">
      <div class="widget-box widget-color-orange">
        <!-- #section:custom/widget-box.options -->
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-eye"></i>
            Preview dan Export Data
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td class="center">
                    <a class="btn btn-app btn-danger btn-sm" href="<?php echo base_url().'form_wks/form_1/printPdf/'.$value['periode_lulus']->plm_id.''?>" target="blank"><i class="ace-icon fa fa-file-pdf-o bigger-200"></i>PDF</a>

                    <a href="<?php echo base_url().'form_wks/form_1/printPreview/'.$value['periode_lulus']->plm_id.'/excel'?>" class="btn btn-app btn-success btn-sm"><i class="ace-icon fa fa-file-excel-o bigger-200"></i>Excel</a>

                    <a href="<?php echo base_url().'form_wks/form_1/printPreview/'.$value['periode_lulus']->plm_id.''?>" class="btn btn-app btn-inverse btn-sm" data-remote="false" data-toggle="modal" data-target="#myModal"><i class="ace-icon fa fa-search-plus bigger-200"></i>Preview</a>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

      <div class="clearfix"></div>

      <div id="search_status"></div>
      <div id="div_table">
        <table id="data-mhs" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th width="50px">No</th>
                    <th>Nama dr</th>
                    <th>NIM</th>
                    <th>Prodi</th>
                    <th>Universitas</th>
                    <th>Provinsi</th>
                    <th>Tanggal Lulus</th>
                    <th>Status Peserta</th>
                    <th>No Ijasah</th>
                    <th class="center" width="120px">Verifikasi<br>Kolegium</th>
                    <!-- <th width="100px">Keterangan</th> -->
                </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($value['detail_mhs'] as $rowmhs) :?>
              <tr>
                <td width="50px" class="center"><?php echo $no;?></td>
                <td><?php echo $rowmhs->dmp_nama?></td>
                <td><?php echo $rowmhs->dmp_nim?></td>
                <td><?php echo $rowmhs->prod_name?></td>
                <td><?php echo $rowmhs->fk_name?></td>
                <td><?php echo $rowmhs->nama_provinsi?></td>
                <td><?php echo $this->tanggal->formatDate($rowmhs->dmp_tanggal_lulus)?></td>
                <td><?php echo $rowmhs->dmp_status_peserta?></td>
                <td><?php echo $rowmhs->dmp_no_ijasah?></td>
                <td class="center">
                  <input type="hidden" name="plm_id" value="<?php echo $rowmhs->plm_id?>">
                  <div class="checkbox" style="margin-top:-10px"><label class="block"><input name="checked[<?php echo $rowmhs->dmp_id?>]" type="checkbox" class="ace input-lg" value="Y" <?php echo ($rowmhs->dmp_app_kolegium=='Y')?'checked':''?> /><span class="lbl bigger-70">&nbsp;</span></label></div>
                </td><!-- 
                <td class="center"><input type="text" class="form-control" name=""></td> -->
              </tr>
            <?php $no++; endforeach;?>
            </tbody>
        </table>

        <div class="form-actions center">

          <a onclick="getMenu('form_wks/form_4b')" href="#" class="btn btn-sm btn-success">
            <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
            Kembali ke sebelumnya
          </a>

          <a href="#" id="btn_save_verifikasi" name="submit" value="submit" class="btn btn-sm btn-primary">
            <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
            Proses verifikasi
          </a>
        </div>

        </div>
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


<!-- ================== POPUP MODAL ======================= -->
<div class="modal fade" id="myModal" tabindex="-1" size="large" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title center" id="myModalLabel">Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan</h4>
      </div>
      <div class="modal-body">
        <center>
        <table id="" class="table" border="1" width="100%">
            <thead>
              <tr style="background-color:#428bca;color:white">  
                    <th class="center" style="width: 50px">&nbsp;</th>
                    <th class="left" style="width: 250px">Jenis SDM Kesehatan (Khusus dr. Spesialis dan Sub Spesialis)</th>
                    <th class="center" style="width: 120px">Jumlah SDMK<br>Saat Ini</th>
                    <th class="center" style="width: 120px">SDMK Standar</th>
                    <th class="center" style="width: 150px">Kesenjangan<br>( 3 ) - ( 4 )</th>
                    <th class="center" style="width: 120px">Usulan<br>Kebutuhan</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">( 1 )</td>
                    <td align="center">( 2 )</td>
                    <td align="center">( 3 )</td>
                    <td align="center">( 4 )</td>
                    <td align="center">( 5 )</td>
                    <td align="center">( 6 )</td>
                </tr>
            </tbody>
            
        </table>


        </center>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url().'assets/js/custom/form_4b.js'?>"></script>
