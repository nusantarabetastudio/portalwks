<?php 
  if(isset($type)){
    if($type != ''){
      $filename = 'PRINT'.Date("ymd").".xls";
      header("Content-type: application/vnd.ms-excel");
      header("Content-Disposition: attachment; filename=$filename");
      header('Cache-Control: public');
      $width = '80%';
    }else{
      $width = '100%';
    }
  }
?>

<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css" />
<style type="text/css">
table, h3{
  font-family: 'lucida grande', helvetica, verdana, arial, sans-serif;
}
.table {
    border-collapse: collapse;
    border-top: 1px;
}
th, td {
    padding: 5px;
}
th{
  height: 50px;
}

</style>
<?php if($type == '') :?>
<a href="<?php echo base_url().'form_wks/form_4b/printPreview/'.$id.'/excel'?>" class="btn btn-sm btn-success">
  <i class="ace-icon fa fa-file-excel-o icon-on-right bigger-110"></i>
  Cetak Excel
</a>  

<a id="printPreview" href="#" class="btn btn-sm btn-danger">
  <i class="ace-icon fa fa-file-pdf-o icon-on-right bigger-110"></i>
  Cetak PDF
</a> 
<?php endif; ?>

<center>

<h3>
  Rekapitulasi Usulan Prediksi Kelulusan Mahasiswa Spesialis<br>Semester <?php echo $value['periode_lulus']->plm_semester?> 
  Tahun <?php echo $value['periode_lulus']->plm_tahun?> </h3>
  <br>

<table id="" class="table" border="1" width="<?php echo $width?>">
    <thead>
      <tr style="background-color:#428bca;color:white">  
            <th align="center" style="width: 50px">&nbsp;</th>
            <th align="center" style="width: 170px">Prodi</th>
            <th align="center" style="width: 150px">Nama Dokter</th>
            <th align="center" style="width: 150px">NIM</th>
            <th align="center" style="width: 150px">Status Kepesertaan</th>
            <th align="center" style="width: 150px">Instansi Pengirim</th>
            <th align="center" style="width: 150px">Tanggal Lulus</th>
            <th align="center" style="width: 150px">Verifikasi Kolegium</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="center">( 1 )</td>
            <td align="center">( 2 )</td>
            <td align="center">( 3 )</td>
            <td align="center">( 4 )</td>
            <td align="center">( 5 )</td>
            <td align="center">( 6 )</td>
            <td align="center">( 7 )</td>
            <td align="center">( 8 )</td>
        </tr>

        <?php $no=1; foreach($value['detail_mhs'] as $row) : ?>
        
        <tr>
            <td align="center"><?php echo $no;?></td>
            <td align="left"><?php echo $row->prod_name?></td>
            <td align="left"><?php echo $row->dmp_nama?></td>
            <td align="left"><?php echo $row->dmp_nim?></td>
            <td align="center"><?php echo $row->dmp_status_peserta?></td>
            <td align="left"><?php echo $row->dmp_instansi?></td>
            <td align="left"><?php echo $row->dmp_tanggal_lulus?></td>
            <td align="center">
              <?php echo ($row->dmp_app_kolegium == 'Y') ? '<label style="color:green">Y</label>' : '<label style="color:red">N</label>' ;?>
            </td>
            
        </tr>

      <?php $no++;  endforeach;?>
    </tbody>
    
</table>

<br><br>

<div style="width:80%; float:left">
  &nbsp;
</div>

<!-- <div style="width:20%;float:right;align:center;">
  <p align="right">
        <u><i>(<?php echo $value['uk']->nama_kabupaten.', '.date('d/m/Y')?>)</i></u><br>
        Mengetahui,<br> 
        (Kepala RS.....................)
        <br>
        <br>
        <br>
        TTD Kepala RS dan Stempel RS<br>
        (nama dan gelar)
      </p>
</div>
 -->
</center>




