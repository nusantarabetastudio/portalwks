<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_fk_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#fk-box option').remove()
                    $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.fk_id+'">'+o.fk_name+'</option>').appendTo($('#fk-box'));
                    });

                });
            } else {
                $('#fk-box option').remove()
                $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
            }
        });

        $('select[name="fk_id"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    /*$('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);*/

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="page-header center">
          <h1>Penempatan Peserta Wajib Kerja Dokter Spesialis (WKDS)</h1>
        </div>

    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
            <label class="control-label col-md-2">Nama Mahasiswa</label>
            <div class="col-md-4">
              <input type="hidden" name="dmp_id" id="dmp_id" class="form-control" value="<?php echo isset($value)?$value->dmp_id:''?>">
              <input type="text" name="nama_mahasiswa" id="nama_mahasiswa" value="<?php echo isset($value)?$value->dmp_nama:''?>" class="form-control" readonly>
            </div>
            <label class="control-label col-md-1">NIM</label>
            <div class="col-md-2">
              <input type="text" name="nim" id="nim" class="form-control" value="<?php echo isset($value)?$value->dmp_nim:''?>" class="form-control" readonly>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Prodi</label>
            <div class="col-md-3">
              <?php echo $this->master->get_master_custom(array('table'=>'m_prodi', 'where'=>array('active'=>'Y'), 'id'=>'prod_id', 'name' => 'prod_name'),isset($value)?$value->prod_id:'','prodi','prodi','form-control','readonly','inline');?>
            </div>

            <label class="control-label col-md-2">Tanggal Lulus</label>
            <div class="col-md-2">
              <div class="input-group">
                <input class="form-control date-picker" name="tgl_lulus" id="tgl_lulus" type="text" data-date-format="yyyy-mm-dd" value="<?php echo isset($value)?$value->dmp_tanggal_lulus:''?>" class="form-control" readonly/>
                <span class="input-group-addon">
                  <i class="fa fa-calendar bigger-110"></i>
                </span>
              </div>

              <!-- <input type="text" name="tgl_lulus" id="tgl_lulus" class="form-control"> -->
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-2">Status Kepesertaan ?</label>
              <div class="col-md-2">
                <div class="radio">
                      <label>
                        <input name="status_peserta" type="radio" class="ace" value="1" <?php echo isset($value) ? ($value->dmp_status_peserta == 'TUBEL') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                        <span class="lbl"> TUBEL </span>
                      </label>
                      <label>
                        <input name="status_peserta" type="radio" class="ace" value="2" <?php echo isset($value) ? ($value->dmp_status_peserta == 'MANDIRI') ? 'checked="checked"' : '' : ''; ?>/>
                        <span class="lbl"> MANDIRI </span>
                      </label>
                </div>
              </div>
              <label class="control-label col-md-3">Pemberi bantuan pendidikan/beasiswa</label>
              <div class="col-md-2">
                <input type="text" name="dmp_pemberi_beasiswa" id="dmp_pemberi_beasiswa" class="form-control" value="<?php echo isset($value)?$value->dmp_pemberi_beasiswa:''?>" class="form-control" readonly>
              </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Instansi Pengirim</label>
            <div class="col-md-2">
              <input type="text" name="instansi" id="instansi" class="form-control" value="<?php echo isset($value)?$value->dmp_instansi:''?>" class="form-control" readonly>
            </div>
            
          </div>

          <div class="page-header">
            <h1>
              Lokasi Penempatan Dokter Spesialis
            </h1>
          </div>

          <div class="form-group" id="form-provinsi">
            <label class="control-label col-md-2">Provinsi</label>
            <div class="col-md-3">
              <?php 
                $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
            </div>

            <label class="control-label col-md-1">Kabupaten</label>
            <div class="col-md-3">
              <?php 
                $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                echo Master::get_change_master_kabupaten('','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
            </div>
          </div>
            
          <div class="form-group" id="form-rsu" >
            <label class="control-label col-md-2">Rumah Sakit</label>
            <div class="col-md-3">
              <?php 
                $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3')) ) ? 'readonly' : '' ; 
                echo Master::get_change_master_rsu(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','kode_rsu','rsu-box','form-control','required '.$readonly.'','inline');?>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-md-2">Keterangan</label>
            <div class="col-md-6">
              <textarea class="form-control"></textarea>
            </div>
            
          </div>


            <div class="form-group" id="alamat_rs">
              <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-10">


                <div class="form-actions center">

                <a onclick="getMenu('form_wks/form_5a')" href="#" class="btn btn-sm btn-success">
                  <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                  Kembali ke sebelumnya
                </a>

                <a href="#" id="btn_update_usulan_kebutuhan" name="submit" value="submit" class="btn btn-sm btn-primary">
                  <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                  Proses
                </a>

                </div>

              </div>
            </div>

        </div>
      </div>

      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5a.js'?>"></script>
