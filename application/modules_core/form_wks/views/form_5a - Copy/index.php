<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_fk_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#fk-box option').remove()
                    $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.fk_id+'">'+o.fk_name+'</option>').appendTo($('#fk-box'));
                    });

                });
            } else {
                $('#fk-box option').remove()
                $('<option value="">(Pilih Fakultas Kedokteran)</option>').appendTo($('#fk-box'));
            }
        });

        $('select[name="fk_id"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    /*$('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);*/

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->

    <div class="page-header center">
          <h1>Analisa Kebutuhan SDMK dan Prediksi Kelulusan FK</h1>
        </div>

    <form class="form-horizontal" method="post" id="form_usulan_sdmk">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>
              
            </div>

            <div class="form-group">
              <label class="control-label col-md-2 ">Periode Bulan</label>
              <div class="col-md-2">
                <?php echo Master::get_bulan(isset($value)?$value->tahun:date('Y'),'bulan','bulan','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Semester</label>
              <div class="col-md-2">
                <div class="radio">
                    <?php
                      $katergori_kelas = array('Ganjil','Genap');
                      foreach($katergori_kelas as $row_kategori_kelas){
                        $value_rkk = ($row_kategori_kelas=='Ganjil') ? 1 : 2;
                    ?>
                      <label>
                        <input name="semester" type="radio" class="ace" readonly value="<?php echo $value_rkk?>" <?php echo isset($profil->kelas_rs) ? ($profil->kelas_rs == $row_kategori_kelas) ? 'checked="checked"' : '' : 'checked'; ?>  />
                        <span class="lbl"> <?php echo $row_kategori_kelas?></span>
                      </label>
                    <?php }?>
                </div>
              </div>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten('','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
            
            <!-- <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Fakultas Kedokteran</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_fk('','fk_id','fk-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div> -->

            <div class="form-group" id="alamat_rs">
              <label class="control-label col-md-1">&nbsp;</label>
              <div class="col-md-10">


                <div class="form-actions center">

                  <a href="#" id="btn_view_history" name="submit" value="history" class="btn btn-sm btn-primary">
                      <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                      Proses Analisa
                    </a>
                    <!-- <a href="#" id="btn_search" name="submit" value="submit" class="btn btn-sm btn-info">
                      <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
                      Pencarian Data SKM
                    </a> -->

                </div>

              </div>
            </div>

        </div>
      </div>

      <div id="search_status"></div>
      <!-- <div class="page-header center" id="page_title"></div> -->

      <div id="div_history" style="display:none">

        <div class="page-header center" id="page_title"></div>
          <div class="col-md-6">
            <table id="data_usulan_prediksi" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" align="center">No</th>
                        <th colspan="4" class="center">Usulan Kebutuhan SDM Kesehatan Rumah Sakit</th>
                    </tr>
                    <tr>
                        <th>Provinsi</th>
                        <th>Faskes</th>
                        <th>Spesialis</th>
                        <th align="center">Total<br>dibutuhkan</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table> 
          </div>

          <div class="col-md-6">
            <table id="data_prediksi" class="table table-striped table-bordered" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th rowspan="2" align="center">No</th>
                        <th class="center" colspan="6">Prediksi Kelulusan Mahasiswa Fakultas Kedokteran</th>
                    </tr>
                    <tr>
                        <th>Provinsi</th>
                        <th>Nama</th>
                        <th>Prodi/Jurusan<br>&nbsp;</th>
                        <th>Asal FK</th>
                        <th>Penempatan</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table> 
          </div>

        <!-- <div class="col-md-6">
          <table id="data_prediksi" class="table table-striped table-bordered" cellspacing="0" width="100%">
              <thead>
                  <tr>
                      <th rowspan="2">No</th>
                      <th class="center" colspan="6">Prediksi Kelulusan Mahasiswa Fakultas Kedokteran</th>
                  </tr>
                  <tr>
                    <th>Provinsi</th>
                    <th>Nama</th>
                    <th>NIM</th>
                    <th>Prodi/Jurusan</th>
                    <th>Asal FK</th>
                    <th>Tanggal<br>Lulus</th>
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>   
        </div> -->
        

         

      </div>
      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5a.js'?>"></script>
