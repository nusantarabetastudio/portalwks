<div>
  <form>
    <input type="hidden" id="id_perencanaan" name="id_perencanaan" value="<?php echo isset($id_perencanaan)?$id_perencanaan:''; ?>">
    <table id="dynamic-table" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>  
                <th class="center" rowspan="2" style="width: 60px">&nbsp;</th>
                <th class="center" rowspan="2" style="width:200px">Jenis SDM Kesehatan<br>(Khusus dr. Spesialis dan Sub Spesialis)</th>
                <th class="center" colspan="7">Jumlah SDMK Saat Ini</th>
                <th class="center" rowspan="2">SDMK Standar</th>
                <th class="center" rowspan="2" style="width: 120px">Kesenjangan<br>( 9 ) - ( 10 )</th>
                <th class="center" rowspan="2" style="width: 120px">Usulan Kebutuhan</th>
            </tr>

            <tr>
                <th class="center">PNS/<br>Pegawai Tetap</th>
                <th class="center">PPPK</th>
                <th class="center">PTT</th>
                <th class="center">Honorer/<br>Kontrak</th>
                <th class="center">BLU / BLUD</th>
                <th class="center">TKS</th>
                <th class="center">Total</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td class="center">( 1 )</td>
                <td class="center">( 2 )</td>
                <td class="center">( 3 )</td>
                <td class="center">( 4 )</td>
                <td class="center">( 5 )</td>
                <td class="center">( 6 )</td>
                <td class="center">( 7 )</td>
                <td class="center">( 8 )</td>
                <td class="center">( 9 )</td>
                <td class="center">( 10 )</td>
                <td class="center">( 11 )</td>
                <td class="center">( 12 )</td>
            </tr>
        </tbody>
    </table>

    <div class="form-actions center">

      <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-success">
        <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
        Kembali ke sebelumnya
      </a>
      <!-- <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-warning">
        <i class="ace-icon fa fa-folder-o icon-on-right bigger-110"></i>
        Lihat Riwayat Perencanaan
      </a> -->
      <button type="reset" onclick="getMenu('form_wks')" id="btnReset" class="btn btn-sm btn-danger">
        <i class="ace-icon fa fa-circle-o icon-on-right bigger-110"></i>
        Hapus semua usulan kebutuhan
      </button>
      <button type="submit" id="btnSave" name="submit" value="submit" class="btn btn-sm btn-info">
        <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
        Proses usulan kebutuhan
      </button>
    </div>

  </form>
</div>
  <script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>