<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->

      <div class="page-header center"><h1>Formulir Peserta Wajib Kerja Dokter Spesialis</h1></div>

      <div class="widget-body">
        <div class="widget-main no-padding">
          <form class="form-horizontal" method="post" id="form_search_data">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Nama Peserta</label>
              <div class="col-md-3">
                <input type="text" name="nama_peserta" class="form-control">
              </div>
              <label class="control-label col-md-1">NIM</label>
              <div class="col-md-2">
                <input type="text" name="nim" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Asal FK</label>
              <div class="col-md-3">
                <?php echo $this->master->get_master_custom(array('table'=>'m_fk', 'where'=>array('active'=>'Y'), 'id'=>'fk_id', 'name' => 'fk_name'), '','fk','fk','form-control','','inline');?>
              </div>
              <label class="control-label col-md-1">Prodi</label>
              <div class="col-md-3">
                <?php echo $this->master->get_master_custom(array('table'=>'m_prodi', 'where'=>array('active'=>'Y'), 'id'=>'prod_id', 'name' => 'prod_name'), '','prodi','prodi','form-control','','inline');?>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">No HP yang bisa dihubungi</label>
              <div class="col-md-2">
                <input type="text" name="nim" class="form-control">
              </div>
              <label class="control-label col-md-2">Email (Untuk korespondensi)</label>
              <div class="col-md-2">
                <input type="text" name="nim" class="form-control">
              </div>
            </div>

            <div class="page-header"><h1>Mohon lampirkan file berikut:</h1></div>

            <div class="form-group">
              <label class="control-label col-md-2">Foto</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * max size 1MB
              </div>
              <label class="control-label col-md-2">Surat Ket Lulus/Ijasah</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * max size 1MB
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Hal depan buku tabungan</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * max size 1MB
              </div>
              <label class="control-label col-md-2">KTP</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * max size 1MB
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">NPWP</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * max size 1MB
              </div>
              <label class="control-label col-md-2">Surat keterangan sehat</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * max size 1MB
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-2">Pakta Integritas</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * Silahkan <a href="">dowload</a>, isi, tempel materai, ttd, dan upload
              </div>
              <label class="control-label col-md-2">SPPD</label>
              <div class="col-md-3">
                <input type="file" name="foto" class="form-control"> * Silahkan download dibawah ini dan isi bagian yang diwarnai kuning.
              </div>
            </div>

            <div class="form-actions center">

              <a onclick="getMenu('form_wks')" href="#" class="btn btn-sm btn-success">
                <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                Kembali ke sebelumnya
              </a>
              <a href="#" id="btn_update_usulan_kebutuhan" name="submit" value="submit" class="btn btn-sm btn-primary">
                <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                Proses
              </a>
            </div>

          </form>
        </div>
      </div>

      

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_1.js'?>"></script>
