<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
table {font-size: 12px}
</style>

<div class="row">
  <div class="col-xs-12">

  <div class="page-header center">
      <h1>Periode Penempatan Wajib Kerja Dokter Spesialis</h1>
    </div>

    <form class="form-horizontal" method="post" id="form_usulan_sdmk">

      <div class="form-group">
        <label class="control-label col-md-2">Tahun</label>
        <div class="col-md-2">
          <?php echo Master::get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-2 ">Periode Bulan</label>
        <div class="col-md-2">
          <?php echo Master::get_bulan(isset($value)?$value->bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
        </div>
        <label class="control-label col-md-1">s/d Bulan</label>
        <div class="col-md-2">
          <?php echo Master::get_bulan(isset($value)?$value->bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
        </div>
        <div class="col-md-4">
          <a href="#" id="btn_search_uk" name="submit" value="submit" class="btn btn-sm btn-default">
            <i class="ace-icon fa fa-search icon-on-right bigger-110"></i>
            Cari
          </a>
          <a href="#" id="btn_reset" name="submit" value="submit" class="btn btn-sm btn-default">
            <i class="ace-icon fa fa-refresh icon-on-right bigger-110"></i>
            Reset
          </a>
        </div>
      </div>

      <div class="form-actions center">
          <a href="#" class="btn btn-sm btn-primary" id="add_periode_penempatan"><i class="glyphicon glyphicon-plus"></i> Buat periode penempatan </a>
          <!-- <button class="btn btn-sm btn-success" onclick="add()"><i class="fa fa-file-excel-o"></i> Export Excel </button>
          <button class="btn btn-sm btn-danger" onclick="add()"><i class="fa fa-file-pdf-o"></i> Export PDF </button> -->
      </div>

      <div class="col-md-12">
        <table id="data_periode_penempatan" class="table table-striped table-bordered table-hover">
           <thead>
            <tr>  
                <th class="center" style="width: 10px"></th>
                <th class="center" style="width: 50px">ID</th>
                <th style="width:70px">Tahun</th>
                <th style="width:80px">Periode</th>
                <th style="width:100px">Masa Kerja</th>
                <th style="width:100px">Total Peserta</th>
                <th style="width: 100px" class="center">Status</th>
                <th style="width: 60px" class="center">Aksi</th>
              </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>

      

    </form>
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5a.js'?>"></script>
