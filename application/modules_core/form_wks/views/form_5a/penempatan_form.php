<script type="text/javascript">
   
    $(function() {

        $('input[type=radio][name=kategori_kelas]').change(function () {
            if (this.value == 'C' || this.value == 'D') {
                $('#form_212204').hide();
                $('#form_212205').hide();
                $('#form_212206 ').hide();
            }else{
                $('#form_212204').show();
                $('#form_212205').show();
                $('#form_212206 ').show();
            }
            
        });

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Komponen)</option>').appendTo($('#kab-box'));
            }
        });

        $('select[name="id_kabupaten"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
            }
        });

        $('select[name="kode_rsu"]').click(function() {
          

            if ($(this).val()) {

                $.getJSON("<?php echo site_url('master_data/m_rs/get_rs_by_kode_json') ?>/" + $(this).val(), '', function(data) {

                    $('#alamat_rs').show();
                    $('#alamat_rs_form').val(data.alamat);
                    $('#jumlah_tt').val(data.jumlah_tt);
                    $("input[name=kategori_kelas][value=" + data.kelas_rs + "]").prop('checked', true);

                });
            } 

        });

    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="page-header center">
          <h1>Penempatan Peserta Wajib Kerja Dokter Spesialis (WKDS)</h1>
        </div>

    <form class="form-horizontal" method="post" id="form_penempatan_lokus">

      <div class="col-xs-12 col-sm-6 widget-container-col">
      <div class="widget-box widget-color-blue">
        <!-- #section:custom/widget-box.options -->
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-user"></i>
            Profil Mahasiswa
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td class="" style="width:30%">Nama Mahasiswa</td>
                  <td class="left"><input type="hidden" name="dmp_id" value="<?php echo $value['result']->dmp_id?>"><?php echo isset($value)?$value['result']->dmp_nama:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">NIM</td>
                  <td class="left"><?php echo isset($value)?$value['result']->dmp_nim:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Prodi</td>
                  <td class="left"><?php echo isset($value)?$value['result']->prod_name:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Asal Fakultas</td>
                  <td class="left"><?php echo isset($value)?$value['result']->fk_name:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Tanggal Lulus</td>
                  <td class="left"><?php echo isset($value)?$this->tanggal->formatDate($value['result']->dmp_tanggal_lulus):''?></td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-6 widget-container-col">
      <div class="widget-box widget-color-orange">
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-exchange"></i>
            Status Kepesertaan
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td class="" style="width:40%">Status Kepesertaan</td>
                  <td class="left"><?php echo isset($value)?$value['result']->dmp_status_peserta:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:40%">Pemberi bantuan pendidikan/beasiswa</td>
                  <td class="left"><?php echo isset($value)?$value['result']->dmp_pemberi_beasiswa:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:40%">Instansti Pengirim</td>
                  <td class="left"><?php echo isset($value)?$value['result']->dmp_instansi:''?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
    <br>
      <div class="widget-body">
        <div class="widget-main no-padding">

        <div class="col-xs-12 col-sm-12 widget-container-col">
          <div class="widget-box widget-color-red2">
            <div class="widget-header">
              <h5 class="widget-title bigger lighter">
                <i class="ace-icon fa fa-building"></i>
                Rumah Sakit yang direkomendasikan berdasarkan regional
              </h5>
            </div>

            <!-- /section:custom/widget-box.options -->
            <div class="widget-body">
              <div class="widget-main no-padding">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr style="color:black">
                      <th class="">&nbsp;</th>
                      <th class="">Kode RS</th>
                      <th class="">Rumah Sakit</th>
                      <th class="">Alamat</th>
                      <th class="">Kelas RS</th>
                      <th class="">Total Dibutuhkan</th>
                      <th class="">Total Terpenuhi</th>
                      <th class="">Tanggal Pengajuan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if(!empty($value['rek_rs'])){
                        $no = 1;
                        foreach($value['rek_rs'] as $rowrs){
                    ?>
                    <tr>
                      <td class="center">
                            <input name="checked_rs" type="radio" class="ace" value="<?php echo $rowrs->uk_id?>">
                            <span class="lbl">&nbsp;</span>
                      </td>
                      <td class=""><?php echo $rowrs->kode_rs?></td>
                      <td class=""><?php echo $rowrs->nama_rs?></td>
                      <td class=""><?php echo $rowrs->alamat.' / '.$rowrs->nama_provinsi.' / '.$rowrs->nama_kabupaten?></td>
                      <td class="center"><?php echo $rowrs->kelas_rs?></td>
                      <td class="center"><?php echo $rowrs->total_disetujui_prov?></td>
                      <td class="center"><?php echo $rowrs->total_terpenuhi?></td>
                      <td class=""><?php echo $this->tanggal->formatDate($rowrs->created_date)?></td>
                    </tr>
                    <?php $no++;} } else{ echo '<tr><td colspan="8"><i><b>Tidak ada Rumah Sakit yang direkomendasikan</b></i></td></tr>'; }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <div style="padding-left:10px;padding-top:20px" >
          <div class="checkbox">
            <label>
              <input name="penempatan_manual" class="ace ace-checkbox-2" onchange="dynInput('penempatan_manual_form',this)" type="checkbox">
              <span class="lbl"> <i>Pilih lokus penempatan secara manual</i></span>
            </label>
          </div>
        </div>

        <div class="clearfix"></div>

          <div id="penempatan_manual_form" style="display:none">

            <div class="page-header blue">
              <h3>
                Lokasi Penempatan Dokter Spesialis
              </h3>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten('','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
              
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Rumah Sakit</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_rsu(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','kode_rsu','rsu-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Keterangan</label>
              <div class="col-md-6">
                <textarea class="form-control" name="keterangan"></textarea>
              </div>
              
            </div>

          </div>

          <div class="form-actions center">

            <a onclick="getMenu('form_wks/form_5a')" href="#" class="btn btn-sm btn-success">
              <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
              Kembali ke sebelumnya
            </a>

            <a href="#" id="btn_proses_penempatan" name="submit" value="submit" class="btn btn-sm btn-primary">
              <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
              Proses Penempatan
            </a>

          </div>

        </div>
      </div>

      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5a.js'?>"></script>
