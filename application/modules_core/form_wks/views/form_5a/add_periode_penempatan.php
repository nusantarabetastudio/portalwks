<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <form class="form-horizontal" method="post" id="form_periode_penempatan">
      <div class="widget-body">
        <div class="widget-main no-padding">
            <br>

            <div class="form-group">
              <label class="control-label col-md-2">Tahun</label>
              <div class="col-md-2">
                <?php echo $this->master->get_tahun(isset($value)?$value->tahun:date('Y'),'tahun','tahun','form-control','required','inline');?>
              </div>
              <label class="control-label col-md-1">Periode</label>
              <div class="col-md-2">
                <?php echo $this->master->get_bulan(isset($value)?$value->bulan:date('m'),'bulan','bulan','form-control','required','inline');?>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Masa Kerja</label>
              <div class="col-md-2">
                <div class="input-group">
                  <input class="form-control date-picker" name="tgl_berangkat" id="tgl_berangkat" type="text" data-date-format="yyyy-mm-dd" />
                  <span class="input-group-addon">
                    <i class="fa fa-calendar bigger-110"></i>
                  </span>
                </div>
              </div>
              <label class="control-label col-md-1">s/d</label>
              <div class="col-md-2">
                <div class="input-group">
                  <input class="form-control date-picker" name="tgl_kembali" id="tgl_kembali" type="text" data-date-format="yyyy-mm-dd" />
                  <span class="input-group-addon">
                    <i class="fa fa-calendar bigger-110"></i>
                  </span>
                </div>
              </div>
            </div>
                

            <div class="form-actions center">
                <a onclick="getMenu('form_wks/form_5a')" href="#" class="btn btn-sm btn-success">
                  <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                  Kembali ke sebelumnya
                </a>
                <a href="#" id="btn_submit_periode_penempatan" name="submit" value="submit" class="btn btn-sm btn-info">
                  <i class="ace-icon fa fa-toggle-down icon-on-right bigger-110"></i>
                  Proses
                </a>
            </div>

          </div>
        </div>

    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url().'assets/js/custom/form_5a.js'?>"></script>
