<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.selected { background-color: #a59f9d }
.odd .selected { background-color: #a59f9d }
</style>

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
    <div class="page-header center">
          <h1>Penempatan Peserta Wajib Kerja Dokter Spesialis (WKDS)</h1>
        </div>

    <form class="form-horizontal" method="post" id="form_penempatan_lokus">

      <div class="col-xs-12 col-sm-6 widget-container-col">
      <div class="widget-box widget-color-blue">
        <!-- #section:custom/widget-box.options -->
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-building"></i>
            Profil Rumah Sakit
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <td class="" style="width:30%">Kode RS</td>
                  <td class="left"><input type="hidden" name="uk_id" value="<?php echo $value['uk']->uk_id?>"><?php echo isset($value)?$value['uk']->kode_rs:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Nama Rumah Sakit</td>
                  <td class="left"><?php echo isset($value)?$value['uk']->nama_rs:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Alamat</td>
                  <td class="left"><?php echo isset($value)?$value['uk']->alamat:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Kelas</td>
                  <td class="left"><?php echo isset($value)?$value['uk']->kelas_rs:''?></td>
                </tr>
                <tr>
                  <td class="" style="width:30%">Provinsi / Kab / Kota</td>
                  <td class="left"><?php echo isset($value)?$value['uk']->nama_provinsi.' / '.$value['uk']->nama_kabupaten:''?></td>
                </tr>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-xs-12 col-sm-6 widget-container-col">
      <div class="widget-box widget-color-orange">
        <div class="widget-header">
          <h5 class="widget-title bigger lighter">
            <i class="ace-icon fa fa-exchange"></i>
            Kebutuhan Dokter Spesialis
          </h5>
        </div>

        <!-- /section:custom/widget-box.options -->
        <div class="widget-body">
          <div class="widget-main no-padding">
            <table class="table table-striped table-bordered table-hover">
              <tbody>
                <?php 
                  $exp_sdmk = explode(':',$value['uk']->sdmk);
                  foreach($exp_sdmk as $rowduk):?>
                <tr>
                  <td class="" style="width:40%"><?php echo ' <i class="fa fa-circle-o"></i> '.str_replace('-', ', <i class="fa fa-angle-double-right"></i> ',  $rowduk)?></td>
                </tr>
              <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
    <br>
      <div class="widget-body">
        <div class="widget-main no-padding">

        <div class="col-xs-12 col-sm-12 widget-container-col">
          <div class="widget-box widget-color-red2">
            <div class="widget-header">
              <h5 class="widget-title bigger lighter">
                <i class="ace-icon fa fa-users"></i>
                Daftar mahasiswa berdasarkan prediksi kelulusan
              </h5>
            </div>

            <!-- /section:custom/widget-box.options -->
            <div class="widget-body">
              <div class="widget-main no-padding">
                <table class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr style="color:black">
                      <th class="">&nbsp;</th>
                      <th class="">Nama Mahasiswa</th>
                      <th class="">NIM</th>
                      <th class="">Prodi / Jurusan</th>
                      <th class="">Asal Fakultas</th>
                      <th class="">Tanggal Kelulusan</th>
                      <th class="">No Ijasah</th>
                      <th class="">MANDIRI</th>
                      <th class="">TUBEL</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      if(!empty($value['prediksi_kelulusan'])){
                        $no = 1;
                        foreach($value['prediksi_kelulusan'] as $rowrs){
                    ?>
                    <tr>
                      <td class="center">
                            <input name="checked_rs" type="checkbox" class="ace" value="<?php echo $rowrs->dmp_id?>">
                            <span class="lbl">&nbsp;</span>
                      </td>
                      <td class=""><?php echo $rowrs->dmp_nama?></td>
                      <td class=""><?php echo $rowrs->dmp_nim?></td>
                      <td class=""><?php echo $rowrs->prod_name?></td>
                      <td class=""><?php echo $rowrs->fk_name?></td>
                      <td class="center"><?php echo $this->tanggal->formatDate($rowrs->dmp_tanggal_lulus)?></td>
                      <td class="center">-</td>
                      <td class="center"><?php echo ($rowrs->dmp_status_peserta == 'MANDIRI')?$rowrs->dmp_pemberi_beasiswa: ($rowrs->dmp_pemberi_beasiswa)?'MANDIRI':' - ' ?></td>
                      <td class="center"><?php echo ($rowrs->dmp_status_peserta == 'TUBEL')?$rowrs->dmp_instansi:'-'?></td>
                    </tr>
                    <?php $no++;} } else{ echo '<tr><td colspan="8"><i><b>Tidak ada mahasiswa yang direkomendasikan</b></i></td></tr>'; }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

        <!-- <div style="padding-left:10px;padding-top:20px" >
          <div class="checkbox">
            <label>
              <input name="penempatan_manual" class="ace ace-checkbox-2" onchange="dynInput('penempatan_manual_form',this)" type="checkbox">
              <span class="lbl"> <i>Pilih lokus penempatan secara manual</i></span>
            </label>
          </div>
        </div> -->

        <div class="clearfix"></div>

          <!-- <div id="penempatan_manual_form" style="display:none">

            <div class="page-header blue">
              <h3>
                Lokasi Penempatan Dokter Spesialis
              </h3>
            </div>

            <div class="form-group" id="form-provinsi">
              <label class="control-label col-md-2">Provinsi</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('4','6')) ) ? 'readonly' : '' ;  
                  echo Master::get_master_provinsi(isset($this->session->userdata('data_user')->id_provinsi) ? $this->session->userdata('data_user')->id_provinsi : '' ,'id_provinsi','id_provinsi','form-control','required '.$readonly.'','inline');?>
              </div>

              <label class="control-label col-md-1">Kabupaten</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('6')) ) ? 'readonly' : '' ;  
                  echo Master::get_change_master_kabupaten('','id_kabupaten','kab-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>
              
            <div class="form-group" id="form-rsu" >
              <label class="control-label col-md-2">Rumah Sakit</label>
              <div class="col-md-3">
                <?php 
                  $readonly = ( in_array($this->session->userdata('data_user')->id_role, array('3')) ) ? 'readonly' : '' ; 
                  echo Master::get_change_master_rsu(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','kode_rsu','rsu-box','form-control','required '.$readonly.'','inline');?>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-2">Keterangan</label>
              <div class="col-md-6">
                <textarea class="form-control" name="keterangan"></textarea>
              </div>
              
            </div>

          </div> -->

          <div class="form-actions center">

            <a onclick="getMenu('form_wks/form_5a')" href="#" class="btn btn-sm btn-success">
              <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
              Kembali ke sebelumnya
            </a>

            <a href="#" id="btn_proses_penempatan" name="submit" value="submit" class="btn btn-sm btn-primary">
              <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
              Proses Penempatan
            </a>

          </div>

        </div>
      </div>

      
    </form>

    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/form_5a.js'?>"></script>
