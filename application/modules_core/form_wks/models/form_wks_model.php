<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_wks_model extends CI_Model {

	var $table = 'vw_usulan_kebutuhan';
	var $column = array('uk_id','tahun','id_provinsi','id_kabupaten','kode_rs','jumlah_tt','created_date','updated_date','nama_rs','nama_provinsi','nama_kabupaten');
	var $order = array('uk_id' => 'desc');

	//=== mahasiswa peserta
	var $table_mp = 'vw_detail_mhs_lulus';
	var $column_mp = array('vw_detail_mhs_lulus.dmp_id','dmp_nim','dmp_nama','dmp_email','dmp_npwp','dmp_tanggal_lulus','fk_name','prod_name');
	var $select_mp = '*';
	var $order_mp = array('vw_detail_mhs_lulus.dmp_id' => 'desc');

	//=== Periode Lulus Mahasiswa
	var $table_plm = 'vw_prediksi_periode_mhs';
	var $column_plm = array('plm_id','plm_tahun','plm_bulan','plm_semester','id_provinsi','id_kabupaten','fk_id','fk_name','nama_provinsi','nama_kabupaten');
	var $select_plm = '*';
	var $order_plm = array('plm_id' => 'desc');


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _main_query($params=''){

		$this->db->from('vw_usulan_kebutuhan');

		if(isset($params)) {
			if(isset($params['f'])) { 
				if($params['f'] == '4c'){
					$this->db->where('verifikasi_dinkes_prov', 'Y');
					$this->db->where('status >= 3'); 
				}else if($params['f'] == '5a'){
					$this->db->where('verifikasi_kemenkes', 'Y');
				}else if($params['f'] == '5b'){
					$this->db->where('verifikasi_kemenkes', 'Y');
				}else{
					$this->db->where('status >= '.$params['f'].''); 
				}
			}
			if(isset($params['y'])) { ($params['y'] != 0) ? $this->db->where('tahun', $params['y']): '' ; }
			if(isset($params['m'])) { ($params['m'] != 0) ? $this->db->where('bulan', $params['m']): '' ; }
			if(isset($params['prov'])) { ($params['prov'] != 0) ? $this->db->where('id_provinsi', $params['prov']): '' ; }
			if(isset($params['kab'])) { ($params['kab'] != 0) ? $this->db->where('id_kabupaten', $params['kab']): '' ; }
			if(isset($params['kode'])) { ($params['kode'] != 0) ? $this->db->where('kode_rs', $params['kode']): '' ; }
		}

		if($this->session->userdata('data_user')->id_role == 6){ // prov
			$this->db->where('id_provinsi', $this->session->userdata('data_user')->id_provinsi);
		}

		if($this->session->userdata('data_user')->id_role == 5){ // kab
			$this->db->where('id_kabupaten', $this->session->userdata('data_user')->id_kabupaten);
		}

		if($this->session->userdata('data_user')->id_role == 3){ // kab
			$this->db->where('kode_rs', $this->session->userdata('data_user')->kode_user);
		}

	}

	function checkUsulanKebutuhanExist($params)
	{
		$query = $this->db->get_where('t_usulan_kebutuhan', $params);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	function searchDataUsulanRs($params)
	{
		
		$this->_main_query($params);
		$this->db->where($params);
		$query = $this->db->get()->result();

		if( !empty($query) ) {
			$data = array(
				'status' => 1,
				'message' => 'Proses berhasil dilakukan',
			);
			foreach ($query as $key => $value) {
				$detail_usulan = $this->db->get_where('t_detail_usulan_kebutuhan', array('uk_id'=>$value->uk_id))->result();
				$value->detail = $detail_usulan;
				$getData[] = $value;
			}
			$data['result'] = $getData;
		}else{
			$data = array(
				'status' => 0,
				'message' => 'Data tidak ditemukan',
			);
		}

		return $data;
	}

	

	private function _get_datatables_query($params='')
	{
		
		$this->_main_query($params);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getDataUsulan($params='')
	{
		$this->_get_datatables_query($params);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get(); //print_r($this->db->last_query());
		return $query->result();
	}

	function getDataUsulanNoTbl($params='')
	{
		$getData = array();
		$this->_main_query($params);
		$query = $this->db->get()->result(); 
		//print_r($this->db->last_query());die;
		foreach ($query as $value) {
			if($params['app']){
				$this->db->where('app_dinkes_prov', 'Y');
			}
			$this->db->where(array('uk_id'=>$value->uk_id));
			$detail_sdmk = $this->db->get('t_detail_usulan_kebutuhan')->result();
			$value->rowspan = count($detail_sdmk);
			$value->detail_sdmk = $detail_sdmk;
			$getData[] = $value;
		}
		
		return $getData;
	}

	function getAnalisaKebutuhan($params='')
	{
		$query = 'select a.dmp_id, k.nama_provinsi, usulan_kebutuhan.nama_rs,usulan_kebutuhan.nama_jenis_sdmk, 
					a.dmp_nama,a.dmp_nim,j.prod_name, j.id_jenis_sdmk, l.fk_name, a.dmp_tanggal_lulus
					from t_detail_mhs_lulus a
					left join t_periode_lulus_mhs b on b.plm_id=a.plm_id
					left join m_prodi j on j.prod_id=a.prod_id
					left join m_provinsi k on k.id_provinsi=b.id_provinsi
					left join m_fk l on l.fk_id=a.fk_id
					/* join query get data usulan kebutuhan */
					left join (
							select f.tahun, f.id_provinsi,f.id_kabupaten, f.kode_rs,f.nama_rs, e.duk_id, e.id_jenis_sdmk, e.nama_jenis_sdmk 
							from t_detail_usulan_kebutuhan e 
							join (SELECT g.*, m_rumah_sakit.nama_rs FROM t_usulan_kebutuhan g 
							left join m_rumah_sakit on m_rumah_sakit.kode_rs=g.kode_rs 
							WHERE g.tahun=2016 AND g.id_provinsi='.$params['prov'].') as f on f.uk_id=e.uk_id
							where e.uk_id in (SELECT c.uk_id FROM t_usulan_kebutuhan c WHERE c.tahun=2016 AND c.id_provinsi='.$params['prov'].')
							and e.app_dinkes_prov="Y"
						) as usulan_kebutuhan on usulan_kebutuhan.id_jenis_sdmk=j.id_jenis_sdmk
					/* end query get data usulan kebutuhan */

					where b.plm_tahun = 2016 and b.plm_bulan=3 
					and b.plm_semester=1 and b.id_provinsi='.$params['prov'].' 

					group by a.dmp_id';

		/*$getData = array();
		$this->_main_query($params);
		$query = $this->db->get()->result();

		foreach ($query as $value) {
			$detail_sdmk = $this->db->get_where('t_detail_usulan_kebutuhan', array('uk_id'=>$value->uk_id))->result();
			$value->rowspan = count($detail_sdmk);
			$value->detail_sdmk = $detail_sdmk;
			$getData[] = $value;
		}*/
		$exc_qry = $this->db->query($query)->result();
		return $exc_qry;
	}

	function count_filtered($params='')
	{
		$this->_get_datatables_query($params);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($params='')
	{
		$this->_main_query($params);
		return $this->db->count_all_results();
	}

	public function getNoteVerifikasi($id)
	{
		$this->db->from('t_note_verifikasi');
		$this->db->where(array('uk_id'=>$id));
		$this->db->order_by('created_date', 'DESC');
		return $this->db->get()->result();
	}

	public function getUsulanById($id)
	{
		$this->_main_query('');
		$this->db->where(array('uk_id'=>$id));
		
		$data['uk'] = $this->db->get()->row();
		$data['duk'] = $this->searchDataUsulanRs(array('uk_id'=>$id));
		$data['prediksi_kelulusan'] = $this->getMhsByRegion($data['uk']->id_provinsi); 
		//print_r($this->db->last_query());die;

		return $data;
	}

	public function getMhsByRegion($id_provinsi)
	{
		$this->db->from('vw_detail_mhs_lulus');
		$this->db->where('fk_id IN (SELECT fk_id FROM t_fk_region WHERE id_provinsi='.$id_provinsi.')');
		return $this->db->get()->result();
	}

	public function getStatusVerifikasi($status)
	{

		$status = ($status == 'Y') ? '<i class="fa fa-check green"></i>' : '<i class="fa fa-times red"></i>';

		return $status;
	}

	public function searchHistoryUsulan($params)
	{
		$usulan_kebutuhan = $this->db->where($params)->get('t_usulan_kebutuhan')->result();
		
		if( !empty($usulan_kebutuhan) ){

			echo json_encode(array('status'=>1, 'message'=>'Sukses', 'data' => $usulan_kebutuhan, 'params' => $params));
		}else{
			echo json_encode(array('status'=>0, 'message'=>'Anda belum membuat rencana kebutuhan SDM Kesehatan pada aplikasi Standar Ketenagaan Minimal (SKM)'));
		}

	}

	public function delete_uk_sdmk_by_id($id)
	{
		$this->db->where('duk_id', $id);
		$this->db->delete('t_detail_usulan_kebutuhan');
	}

	public function sendToVerifikator($data, $id)
	{
		$this->db->update('t_usulan_kebutuhan', $data, array('uk_id'=>$id));
	}

	public function setHistoryWorkfow($params)
	{
		$params['created_date'] = date('Y-m-d H:i:s');
		$params['created_by'] = $this->session->userdata('data_user')->fullname;
		$this->db->insert('t_history_workflow', $params);
	}

	//=========== FORM 4a ================//

	private function dmp_main_query($params=''){

		$this->db->from($this->table_mp);
		
		if(isset($params['perpp_id'])){
			$this->db->where('perpp_id', $params['perpp_id']);
		}

		if(isset($params['plm_id'])){

			if($params['plm_id'] != 0) { 
				$this->db->where('plm_id', $params['plm_id']); 
			}else{
				$this->db->where('plm_tahun', isset($params['y'])?$params['y']:0);
				$this->db->where('plm_bulan', isset($params['m'])?$params['m']:0);
				$this->db->where('plm_semester', isset($params['smt'])?$params['smt']:0);
				$this->db->where('id_provinsi', isset($params['prov'])?$params['prov']:0);
				$this->db->where('fk_id', isset($params['kode'])?$params['kode']:0);
			}

		}else{

			if(isset($params['f'])){
				if($params['f'] == '4c'){
					$this->db->where('app_kolegium', 'Y');
				}else if($params['f'] == '5a'){
					$this->db->where('dmp_app_kemenkes', 'Y');
				}
			}

			if(isset($params['y'])) { ($params['y'] != 0) ? $this->db->where('plm_tahun', $params['y']): '' ; }
			if(isset($params['m'])) { ($params['m'] != 0) ? $this->db->where('plm_bulan', $params['m']): '' ; }
			if(isset($params['smt'])) { ($params['smt'] != 0) ? $this->db->where('plm_semester', $params['smt']): '' ; }

			if(isset($params['to'])) { 
				($params['to'] != 0) ? $this->db->where('plm_bulan BETWEEN '.$params['m'].' AND '.$params['to'].'') : ''; 
			}
			
			if(isset($params['prov'])) { ($params['prov'] != 0) ? $this->db->where('id_provinsi', $params['prov']): '' ; }
			if(isset($params['kab'])) { ($params['kab'] != 0) ? $this->db->where('id_kabupaten', $params['kab']): '' ; }
			if(isset($params['kode'])) { ($params['kode'] != 0) ? $this->db->where('fk_id', $params['kode']): '' ; }

		}
		
	}

	private function dmp_get_datatables_query($params='')
	{
		
		$this->dmp_main_query($params);

		$i = 0;
	
		foreach ($this->column_mp as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_mp))
		{
			$order = $this->order_mp;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getDataMahasiswaPeserta($params='')
	{
		$this->dmp_get_datatables_query($params);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get(); //print_r($this->db->last_query());die;
		return $query->result();
	}

	function getDmpByPlmId($params='')
	{
		$this->dmp_main_query($params);
		$query = $this->db->get();
		return $query->result();
	}

	function dmpcount_filtered($params='')
	{
		$this->dmp_get_datatables_query($params);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function dmpcount_all($params='')
	{
		$this->dmp_main_query($params);
		return $this->db->count_all_results();
	}

	function getDataMahasiswaPesertaNoTbl($params='')
	{
		//print_r($params);die;
		$this->dmp_main_query($params);
		$query = $this->db->get(); //print_r($this->db->last_query());die;
		return $query->result();
	}

	function getDataMahasiswaPesertaNoTbl2($params='')
	{	
		//print_r($params);die;
		$this->plm_main_query($params);

		$data['periode_lulus'] = $this->db->where($params)->get()->row();

		$this->dmp_main_query($params);
		if($this->session->userdata('data_user')->id_role == 4){
			$this->db->where('kolegium_id', $this->session->userdata('data_user')->kode_user);
		}
		$data['detail_mhs'] = $this->db->get()->result(); //print_r($this->db->last_query());die;
		return $data;
	}

	function getDmpById($params)
	{
		$this->dmp_main_query();
		$this->db->where('dmp_id', $params['dmp_id']);
		$query = $this->db->get(); //print_r($this->db->last_query());die;
		return $query->row();
	}


	//=========== FORM plm ================//

	private function plm_main_query($params=''){

		$this->db->from('vw_prediksi_periode_mhs');

		if(isset($params)) {

			if(isset($params['y'])) { ($params['y'] != 0) ? $this->db->where('plm_tahun', $params['y']): '' ; }

			if(isset($params['m'])) { ($params['m'] != 0) ? $this->db->where('plm_bulan', $params['m']): '' ; }

			if(isset($params['smt'])) { $this->db->where('plm_semester', $params['smt']); }

			if(isset($params['prov'])) { ($params['prov'] != 0) ? $this->db->where('id_provinsi', $params['prov']): '' ; }

			if(isset($params['kab'])) { ($params['kab'] != 0) ? $this->db->where('id_kabupaten', $params['kab']): '' ; }

			if(isset($params['kode'])) { ($params['kode'] != 0) ? $this->db->where('fk_id', $params['kode']): '' ; }

			
		}

		if($this->session->userdata('data_user')->id_role == 6){ // prov
			$this->db->where('id_provinsi', $this->session->userdata('data_user')->id_provinsi);
		}

		if($this->session->userdata('data_user')->id_role == 9){ // fk
			$this->db->where('fk_id', $this->session->userdata('data_user')->kode_user);
		}

		if($this->session->userdata('data_user')->id_role == 4){ // fk
			if(isset($params['f'])) {
				if($params['f'] == '4b'){
					$this->db->where('plm_id in ((SELECT plm_id FROM vw_detail_mhs_lulus WHERE kolegium_id='.$this->session->userdata('data_user')->kode_user.' GROUP BY plm_id))');
				} 
			}
		}

	}

	private function plm_get_datatables_query($params='')
	{
		
		$this->plm_main_query($params);

		$i = 0;
	
		foreach ($this->column_plm as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_plm))
		{
			$order = $this->order_plm;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getPeriodeKelulusanMhs($params='')
	{
		$this->plm_get_datatables_query($params);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get(); //print_r($this->db->last_query());die;
		return $query->result();
	}

	function plmcount_filtered($params='')
	{
		$this->plm_get_datatables_query($params);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function plmcount_all($params='')
	{
		$this->plm_main_query($params);
		return $this->db->count_all_results();
	}

	public function get_mhs_by_id($dmp_id)
	{
		$data = array();
		$result = $this->db->get_where('vw_detail_mhs_lulus', array('dmp_id'=>$dmp_id))->row();
		$rekomendasi_rs = $this->db->get_where('vw_usulan_kebutuhan', array('id_provinsi'=>$result->id_provinsi, 'verifikasi_kemenkes' => 'Y'))->result();
		$data['result'] = $result;
		$data['rek_rs'] = $rekomendasi_rs;

		return $data;
	}

	function get_jenis_sdmk_prodi(){
		$jenis_sdmk = $this->db->select('id_jenis_sdmk')->get('m_prodi')->result();
		foreach ($jenis_sdmk as $key => $value) {
			# code...
			$getData[] = $value->id_jenis_sdmk;
		}
		return $getData;
	}

	public function printExcel($pkt_id){
		
	}

	public function printPdf($pkt_id) { 
        
		
		$this->load->model('form_wks_model');
		$value = $this->form_wks_model->getUsulanById($pkt_id);
		
		$nama_rsud = $value['duk']['result'][0]->nama_rs;
		$tahun = $value['uk']->tahun;
		$nama_kabupaten = $value['uk']->nama_kabupaten;
		$nama_provinsi = $value['uk']->nama_provinsi;
		$bulan = $value['uk']->bulan;
		$nama_bulan = $this->tanggal->getBulan($bulan);
		$date = date('d/m/Y');
			

        $this->load->library('pdf');
        $tanggal = new Tanggal();
        $pdf = new TCPDF('P', PDF_UNIT, array(297,230), true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        
        $pdf->SetAuthor('BPPSDMK Kementerian Kesehatan RI');
        $pdf->SetTitle('Rekapitulasi Usulan Kebutuhan SDM Kesehatan');

    // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

    // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT,PDF_MARGIN_BOTTOM);

    // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
    
    // auto page break //
        $pdf->SetAutoPageBreak(TRUE, 30);

        //set page orientation
        
    // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }
        
        $pdf->SetFont('helvetica', '', 10);
        $pdf->ln();

        //kotak form
        $pdf->AddPage('L', 'A4');
        //$pdf->setY(10);
        //$pdf->setXY(20,20);
        $pdf->SetMargins(30, 20, 30, 20); 
        /* $pdf->Cell(150,42,'',1);*/
        //field 1	
		$html = <<<EOD
		<link rel="stylesheet" href="'.file_get_contents(_BASE_PATH_.'/assets/css/bootstrap.css)'" />
EOD;
		$html .= <<<EOD
		<head>
		<style>
			h3{
				text-align: center;
				}
		</style>
		</head>
		<body>
			<h3>
				Rekapitulasi Keadaan dan Kebutuhan SDM Kesehatan <br>
				$nama_rsud Tahun $tahun 				
			</h3>
			<table>
				<tr style="padding-top:10px; padding-bottom:10px;">
					<td style="width: 100px"><b>Tahun</b></td>
					<td style="width: 20px">:</td>
					<td style="width: 150px"><b> $tahun</b></td>
				</tr>
				<tr>
					<td style="width: 100px"><b>Periode</b></td>
					<td style="width: 20px">:</td>
					<td style="width: 150px"><b> $nama_bulan</b></td>
				</tr>
				<tr>
					<td style="width: 100px"><b>Provinsi</b></td>
					<td style="width: 20px">:</td>
					<td style="width: 150px"><b> $nama_provinsi</b></td>
				</tr>
				<tr>
					<td style="width: 100px"><b>Kab/Kota</b></td>
					<td style="width: 20px">:</td>
					<td style="width: 150px"><b> $nama_kabupaten</b></td>
				</tr>
				<tr>
					<td style="width: 100px"><b>Rumah Sakit</b></td>
					<td style="width: 20px">:</td>
					<td style="width: 150px"><b>$nama_rsud</b></td>
				</tr>
			</table>
			<center>
				<table  border="1">
				    <thead class="table-usulan">
				      <tr style="background-color:#428bca;color:white">  
            			<th align="center" style="width: 50px">&nbsp;</th>
            			<th align="center" style="width: 400px">Jenis SDM Kesehatan (Khusus dr. Spesialis dan Sub Spesialis)</th>
            			<th align="center" style="width: 100px">Jumlah SDMK Saat Ini</th>
            			<th align="center" style="width: 100px">SDMK Standar</th>
            			<th align="center" style="width: 120px">Kesenjangan<br>( 3 ) - ( 4 )</th>
            			<th align="center" style="width: 100px">Usulan<br>Kebutuhan</th>
        			</tr>
				    </thead>
				    	<tbody>
				        	<tr>
				            	<td align="center" style="width: 50px">( 1 )</td>
				            	<td align="center" style="width: 400px">( 2 )</td>
				            	<td align="center" style="width: 100px">( 3 )</td>
				        	    <td align="center" style="width: 100px">( 4 )</td>
					            <td align="center" style="width: 120px">( 5 )</td>
					            <td align="center" style="width: 100px">( 6 )</td>
				    	    </tr>
EOD;

		if(isset($value)){
			if($value['duk']['status'] == 1){
				if(count($value['duk']['result']) > 0){
					$no = 0;
					foreach($value['duk']['result'] as $rowduk){
						$no++;
						$nama_rs = $rowduk->nama_rs;
						$html .= <<<EOD
							<tr>
								<td align="center">$no</td>
								<td align="left" colspan="5">$nama_rs</td>
							</tr>
EOD;
						$noa = 0;
						foreach($rowduk->detail as $rowdduk){
							$noa++;
							$nama_jenis_sdmk = $rowdduk->nama_jenis_sdmk;
							$total_jml = $rowdduk->total_jml;
							$total_standar = $rowdduk->total_standar;
							$total_kesenjangan = $this->apps->get_format($rowdduk->total_kesenjangan);
							$usul_kebutuhan = $rowdduk->usulan_kebutuhan;
							$html .= <<<EOD
								<tr>
									<td align="center" style="width: 50px"></td>
									<td align="left" style="width: 400px">&nbsp;&nbsp;&nbsp; $noa . $nama_jenis_sdmk</td>
									<td align="center" style="width: 100px">$total_jml</td>
									<td align="center" style="width: 100px">$total_standar</td>
									<td align="center" style="width: 120px">$total_kesenjangan</td>
									<td align="center" style="width: 100px">$usul_kebutuhan</td>
								</tr>
EOD;
						}
					}
				}
			}
		}
		$html .= <<<EOD
				</tbody>
			</table>
		</center>
		<br><br>
		<div style="width:80%; float:left">
			&nbsp;
		</div>
		<div style="width:20%;float:right;align:center;">
			<p align="right">
				<u><i>( $nama_kabupaten, $date )</i></u>
				<br>Mengetahui,<br>
				(Kepala RS.....................)
				<br>
				<br>
				<br>
				TTD Kepala RS dan Stempel RS<br>
        		(nama dan gelar)	
			</p>
		</div>
		</body>
EOD;
        $result = $html;

        // output the HTML content
        $pdf->writeHTML($result, true, false, true, false, '');
            
        ob_end_clean();
        $pdf->Output('PKT.pdf', 'I'); 
        

    }


    // ======================================PERIODE PENEMPATAN ========================== \\

    private function _main_query_penempatan_periode($params=''){

		$this->db->from('t_periode_penempatan');

		if(isset($params)) {
			if(isset($params['f'])) { 
				if($params['f'] == '4c'){
					$this->db->where('verifikasi_dinkes_prov', 'Y');
					$this->db->where('status >= 3'); 
				}else if($params['f'] == '5a'){
					$this->db->where('verifikasi_kemenkes', 'Y');
				}else if($params['f'] == '5b'){
					$this->db->where('verifikasi_kemenkes', 'Y');
				}else{
					$this->db->where('status >= '.$params['f'].''); 
				}
			}
			if(isset($params['y'])) { ($params['y'] != 0) ? $this->db->where('tahun', $params['y']): '' ; }
			if(isset($params['m'])) { ($params['m'] != 0) ? $this->db->where('bulan', $params['m']): '' ; }
			if(isset($params['prov'])) { ($params['prov'] != 0) ? $this->db->where('id_provinsi', $params['prov']): '' ; }
			if(isset($params['kab'])) { ($params['kab'] != 0) ? $this->db->where('id_kabupaten', $params['kab']): '' ; }
			if(isset($params['kode'])) { ($params['kode'] != 0) ? $this->db->where('kode_rs', $params['kode']): '' ; }
		}

		if($this->session->userdata('data_user')->id_role == 6){ // prov
			$this->db->where('id_provinsi', $this->session->userdata('data_user')->id_provinsi);
		}

		if($this->session->userdata('data_user')->id_role == 5){ // kab
			$this->db->where('id_kabupaten', $this->session->userdata('data_user')->id_kabupaten);
		}

		if($this->session->userdata('data_user')->id_role == 3){ // kab
			$this->db->where('kode_rs', $this->session->userdata('data_user')->kode_user);
		}

	}

    private function _get_datatables_query_penempatan_periode($params='')
	{
		
		$this->_main_query_penempatan_periode($params);

		$table = 't_periode_penempatan';
		$column = array('perpp_id','perpp_tahun','perpp_bulan','perpp_tgl_berangkat','perpp_tgl_kembali','perpp_keterangan','perpp_status');
		$order = array('perpp_id' => 'desc');

		$i = 0;
	
		foreach ($column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($order))
		{
			$order = $order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getPenempatanPeriode($params='')
	{
		$this->_get_datatables_query_penempatan_periode($params);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get(); //print_r($this->db->last_query());
		return $query->result();
	}

	function count_filtered_perpp($params='')
	{
		$this->_get_datatables_query_penempatan_periode($params);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all_perpp($params='')
	{
		$this->_main_query_penempatan_periode($params);
		return $this->db->count_all_results();
	}

}
