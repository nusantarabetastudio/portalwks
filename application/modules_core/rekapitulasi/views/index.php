<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $breadcrumbs?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">
        <!-- PAGE CONTENT BEGINS -->
          <!-- <div class="alert alert-danger center">[ <i class="fa fa-info"></i> ] <b>Dalam proses pengembangan</b></div> -->
          <div class="col-md-6">
            <div id="container"></div><br>
          </div>
          <div class="col-md-6">
            <div id="piechart"></div>
          </div>
          <hr class="separator">


            
            
        <!-- PAGE CONTENT ENDS -->
      </div><!-- /.col -->
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


<script src="<?php echo base_url()?>assets/chart/highcharts.js"></script>
<script src="<?php echo base_url()?>assets/chart/modules/exporting.js"></script>

<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        exporting: { enabled: false },
        chart: {
            type: 'column'
        },
        title: {
            text: 'Rekapitulasi Penempatan Dokter Spesialis <br> per Periode Tahun 2016'
        },
        subtitle: {
            text: 'Sumber data : (dummy)'
        },

        xAxis: {
            categories: ['Januari -Maret','April-Juni','Juli-September','Oktober-Desember']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Obstetri dan Ginekologi',
            data: [49.9, 71.5, 106.4, 129.2]

        }, {
            name: 'Ilmu Penyakit Dalam',
            data: [83.6, 78.8, 98.5, 93.4]

        }, {
            name: 'Ilmu Kesehatan Anak',
            data: [48.9, 38.8, 39.3, 41.4]

        },{
            name: 'Ilmu Bedah',
            data: [48.9, 38.8, 39.3, 41.4]

        }, {
            name: 'Anestesiologi dan Terapi Intensif',
            data: [42.4, 33.2, 34.5, 39.7]

        }]
    });
});

$(function () {
    $('#piechart').highcharts({
        exporting: { enabled: false },
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: 'Persentase Kebutuhan Dokter Spesialis<br>Tahun 2016'
        },
        subtitle: {
            text: 'Sumber data : (dummy)'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Persentase',
            data: [
                ['Anestesiologi dan Terapi Intensif',   35.4],
                ['Ilmu Penyakit Dalam',       30.5],
                ['Ilmu Kesehatan Anak',     26.2],
                ['Ilmu Bedah',   10.7],
                ['Anestesiologi dan Terapi Intensif',   10.7]
            ]
        }]
    });
});
    </script>