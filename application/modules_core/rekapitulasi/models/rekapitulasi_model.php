<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi_model extends CI_Model {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function checkUsulanKebutuhanExist($params)
	{
		$query = $this->db->get_where('t_usulan_kebutuhan', $params);
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

}
