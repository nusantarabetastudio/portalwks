<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekapitulasi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('master');
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->library('integration');
		$this->load->library('upload_file');
		$this->load->model('master_data/m_provinsi_model', 'prov');
		$this->load->model('master_data/m_kabupaten_model', 'kab');
		$this->load->model('master_data/rs_model', 'rs');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->breadcrumbs->push('Rekapitulasi', 'form_wks');
	}

	public function index()
	{
		//$this->breadcrumbs->push('Form 1', 'form_wks/'.strtolower(get_class($this)));
		$data['title'] = "Form WKS";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$data['profil'] = $this->rs->get_by_kode_rs($this->session->userdata('data_user')->kode_user);
		$this->authuser->write_log();
		$this->load->view('index', $data);
	}


}
