<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function modul(){
		$modul = new stdClass;
		$modul->title = 'Beranda';
		$modul->subtitle = 'Subberanda';
		return $modul;
	}

	public function index()
	{
		$data['modul'] = $this->modul();
		$this->kerangka->site('home_view',$data);
	}
	
}
