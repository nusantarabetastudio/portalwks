<script type="text/javascript">
    
    /*$('#form-provinsi').hide();
    $('#form-kabupaten').hide();
    $('#form-puskesmas').hide();*/

    $('select[name=role]').change(function () {
        if ($(this).val() == 1) { // ADMINISTRATOR
              $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-rsu').hide();
              $('#form-fk').hide();
              $('#form-kolegium').hide();
        } else if ($(this).val() == 2) { // PUSAT
              $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-rsu').hide();
              $('#form-fk').hide();
              $('#form-kolegium').hide();
        } else if ($(this).val() == 3) { // RS
              $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-rsu').show();
              $('#form-fk').hide();
              $('#form-kolegium').hide();
        } else if ($(this).val() == 4) { // KOLEGIUM
              $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-rsu').hide();
              $('#form-fk').hide();
              $('#form-kolegium').show();

        }else if ($(this).val() == 5) { // DINKES KAB
              $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-rsu').hide();
              $('#form-fk').hide();
              $('#form-kolegium').hide();
        }else if ($(this).val() == 6) { // PROVINSI
              $('#form-provinsi').show();
              $('#form-kabupaten').hide();
              $('#form-rsu').hide();
              $('#form-fk').hide();
              $('#form-kolegium').hide();
        }else if ($(this).val() == 7) { // KPDS
              $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-puskesmas').hide();
              $('#form-rsu').hide();
              $('#form-btkl').hide();
              $('#form-kkp').hide();
              $('#form-klinik').hide();
        }else if ($(this).val() == 8) { // PESERTA
              $('#form-provinsi').show();
              $('#form-kabupaten').show();
              $('#form-rsu').hide();
              $('#form-fk').show();
              $('#form-kolegium').hide();
        }else if ($(this).val() == 9) { // FK
              $('#form-provinsi').show();
              $('#form-kabupaten').hide();
              $('#form-rsu').hide();
              $('#form-fk').show();
              $('#form-kolegium').hide();
        } else {
              $('#form-provinsi').hide();
              $('#form-kabupaten').hide();
              $('#form-rsu').hide();
              $('#form-fk').hide();
              $('#form-kolegium').hide();
        }
    });

    $(function() {

        //new
        $('select[name="id_provinsi"]').change(function() {
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_provinsi/get_kab_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#kab-box option').remove()
                    $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.id_kabupaten+'">'+o.nama_kabupaten+'</option>').appendTo($('#kab-box'));
                    });

                });
            } else {
                $('#kab-box option').remove()
                $('<option value="">(Pilih Kabupaten)</option>').appendTo($('#kab-box'));
            }

            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_fk/get_fk_by_prov') ?>/" + $(this).val(), '', function(data) {
                    $('#fk option').remove()
                    $('<option value="">(Pilih Fakultas)</option>').appendTo($('#fk'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.fk_id+'">'+o.fk_name+'</option>').appendTo($('#fk'));
                    });

                });
            } else {
                $('#fk option').remove()
                $('<option value="">(Pilih Fakultas)</option>').appendTo($('#fk'));
            }

        });
        $('select[name="id_kabupaten"]').change(function() {

            // rumah sakit umum //
            if ($(this).val()) {
                $.getJSON("<?php echo site_url('master_data/m_kabupaten/get_rsu_by_kab') ?>/" + $(this).val(), '', function(data) {
                    $('#rsu-box option').remove()
                    $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#rsu-box'));
                    $.each(data, function(i, o) {
                        $('<option value="'+o.kode_rs+'">'+o.nama_rs+'</option>').appendTo($('#rsu-box'));
                    });

                });
            } else {
                $('#rsu-box option').remove()
                $('<option value="">(Pilih Rumah Sakit)</option>').appendTo($('#puskes-box'));
            }

        });
    });
</script>
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_user" action="<?php echo site_url('perencanaan/ajax_add')?>">
                <br>
                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_user:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Pengguna</label>
                  <div class="col-md-6">
                    <input name="fullname" id="fullname" value="<?php echo isset($value)?$value->fullname:''?>" placeholder="Nama Pengguna" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Username/Email</label>
                  <div class="col-md-4">
                    <input name="email" id="email" placeholder="Email" value="<?php echo isset($value)?$value->email:''?>" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-2">Password</label>
                  <div class="col-md-3">
                    <input name="password" id="password" placeholder="Password" value="<?php echo isset($value)?Encryption::decrypt_password_callback($value->password, SECURITY_KEY):''?>" class="form-control" type="password">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Konfirmasi password</label>
                  <div class="col-md-3">
                    <input name="confirm_password" id="confirm_password" placeholder="Konfirmasi password" class="form-control" type="password">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Role</label>
                  <div class="col-md-3">
                    <?php echo $this->master->get_master_role(isset($value)?$value->id_role:'','role','role','chosen-select form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group" id="form-provinsi" <?php echo in_array($value->id_role, array(6,8,9) ) ? '' : 'style="display: none;"' ;?> >
                  <label class="control-label col-md-2">Provinsi</label>
                  <div class="col-md-3">
                    <?php echo $this->master->get_master_provinsi(isset($value)?$value->id_provinsi:'','id_provinsi','id_provinsi','form-control','required','inline');?>
                  </div>
                </div>

                <div class="form-group" id="form-kabupaten" 
                  <?php echo in_array($value->id_role, array(3,5,6,8) ) ? '' : 'style="display: none;"' ;?> >
                  <label class="control-label col-md-2">Kabupaten</label>
                  <div class="col-md-3">
                    <?php echo $this->master->get_change_master_kabupaten(isset($value)?$value->id_kabupaten:'','id_kabupaten','kab-box','form-control','','inline');?>
                  </div>
                </div>
                
                <div class="form-group" id="form-rsu" <?php echo in_array($value->id_role, array(3,5,6) ) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Rumah Sakit</label>
                  <div class="col-md-3">
                    <?php 
                      echo Master::get_change_master_rsu(isset($this->session->userdata('data_user')->kode_user) ? $this->session->userdata('data_user')->kode_user : '','kode_rsu','rsu-box','form-control','required ','inline');?>
                  </div>
                </div>
                <div class="form-group" id="form-fk" <?php echo in_array($value->id_role, array(6,8,9) ) ? isset($value)? in_array($value->id_role, array(6,8,9) )? '' : 'style="display:none"' : '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Fakultas</label>
                  <div class="col-md-3">
                    <?php 
                      echo $this->master->get_master_custom(array('table'=>'m_fk', 'where'=>array('active'=>'Y'), 'id'=>'fk_id', 'name' => 'fk_name'),isset($this->session->userdata('data_user')->kode_user)?$this->session->userdata('data_user')->kode_user:isset($value)?$value->kode_user:'','fk','fk','form-control','','inline');?>
                  </div>
                </div>
                <div class="form-group" id="form-kolegium" <?php echo in_array($value->id_role, array('4')) ? '' : 'style="display: none;"' ;?>>
                  <label class="control-label col-md-2">Kolegium</label>
                  <div class="col-md-3">
                    <?php 
                      echo $this->master->get_master_custom(array('table'=>'m_kolegium', 'where'=>array('active'=>'Y'), 'id'=>'kolegium_id', 'name' => 'kolegium_name'),isset($value->kode_user)?$value->kode_user:'','kolegium','kolegium','form-control','','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_user:0?>"> -->

                  <a onclick="getMenu('pengaturan/user')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/user.js'?>"></script>
