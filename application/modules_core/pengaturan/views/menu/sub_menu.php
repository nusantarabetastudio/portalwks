<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-xs-12">

        
        <br>
        <div class="row">

            <div class="col-xs-12 col-sm-12 pricing-box">

                <div class="col-sm-12">

                  <?php 
                    foreach ($value_sub_menu as $key => $value) {
                      
                  ?>
                  <div class="col-sm-4">
                    <center>
                      <a href="#" title="<?php echo ucwords($value->name)?>" onclick="getMenu('<?php echo $value->link?>')" style="text-decoration: none">
                        <div class="well well-lg">
                          <h2><i class="<?php echo $value->icon?> bigger-250"></i></h2>
                          <h4 class="blue"><?php echo ucwords($value->name)?></h4>
                      </div>
                      </a>
                    </center>
                  </div>

                  <?php }?>
                
                </div><!-- /.col -->

            </div>

            <!-- /section:pages/pricing.large -->
          </div>

      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/menu.js'?>"></script>
