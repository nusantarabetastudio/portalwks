<!-- page specific plugin styles -->
<!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-duallistbox.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap-multiselect.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/select2.css" />
 -->
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_menu_role" action="<?php echo site_url('pengaturan/menu_role/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_role:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Role</label>
                  <div class="col-md-6">
                    <input name="role_name" id="role_name" value="<?php echo isset($value)?strtoupper($value->role_name):''?>" placeholder="Nama Role" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Hak Akses Menu Role</label>
                  <div class="col-md-9">
                    <table class="table table-striped table-bordered">
                          <thead>
                            <tr style="color:black">
                              <th class="center">Nama Menu</th>
                              <?php 
                                $no = 0;
                                $func = Master::get_func_action_data();
                                foreach ($func as $key => $values) {
                              ?>
                              <th class="center"><?php echo strtoupper($values['name']). '<br>[ '.$values['code'].' ]'?></th>
                              <?php $no++; }?>
                            </tr>
                          </thead>

                          <tbody>
                            <?php
                              $menu = Master::get_menu_data(); //echo '<pre>';print_r($menu);die;
                              foreach ($menu as $key2 => $row) {
                            ?>
                            <tr>
                              <td><?php echo ucfirst($row['name'])?></td>

                               <?php 
                                $no = 0;
                                $func_row = Master::get_func_action_data();
                                foreach ($func_row as $key3 => $func_row) {
                              ?>

                              <td class="center">
                                <label class="pos-rel">
                                  <?php if($row['link'] != '#'){?>
                                    <input type="checkbox" name="chk[]" value="<?php echo $row['id_menu']?>-<?php echo $func_row['code']?>" class="ace" <?php echo $this->menu_role->get_checked_form($row['id_menu'], $value->id_role, $func_row['code'])?>/>
                                    <span class="lbl"></span>
                                  <?php }?>
                                </label>
                              </td>

                              <?php $no++; }?>

                            </tr>
                            <?php foreach ($row['submenu'] as $rowsubmenu) {?>
                                <tr>
                                <td>&nbsp;&nbsp;&nbsp;<i class="fa fa-check"></i> <?php echo ucfirst($rowsubmenu['name'])?></td>

                                 <?php 
                                  $no = 0;
                                  $func_row = Master::get_func_action_data();
                                  foreach ($func_row as $key3 => $func_row) {
                                ?>

                                <td class="center">
                                  <label class="pos-rel">
                                    <input type="checkbox" name="chk[]" value="<?php echo $rowsubmenu['id_menu']?>-<?php echo $func_row['code']?>" class="ace" <?php echo $this->menu_role->get_checked_form($rowsubmenu['id_menu'], $value->id_role, $func_row['code'])?>/>
                                    <span class="lbl"></span>
                                  </label>
                                </td>

                                <?php $no++; }?>

                              </tr>

                              <?php }?>
                            
                            <?php }?>
                        </tbody>
                    </table>
                  </div>
                </div>


                <!-- <div class="form-group">
                    <label class="col-sm-2 control-label no-padding-top" for="duallist"> Hak Akses </label>

                    <div class="col-sm-8">
                      <select multiple="multiple" size="10" name="duallistbox_demo1[]" id="duallist">
                        <option value="option1">Option 1</option>
                        <option value="option2">Option 2</option>
                        <option value="option3" selected="selected">Option 3</option>
                        <option value="option4">Option 4</option>
                        <option value="option5">Option 5</option>
                        <option value="option6" selected="selected">Option 6</option>
                        <option value="option7">Option 7</option>
                        <option value="option8">Option 8</option>
                        <option value="option9">Option 9</option>
                        <option value="option0">Option 10</option>
                      </select>

                      <div class="hr hr-16 hr-dotted"></div>
                    </div>
                  </div> -->

                


                

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_role:0?>"> -->

                  <a onclick="getMenu('pengaturan/menu_role')" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <!-- <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a> -->
                  <!-- <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button> -->
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<!-- page specific plugin scripts -->
<!-- 
<script src="<?php echo base_url()?>assets/js/jquery.bootstrap-duallistbox.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.raty.js"></script>
<script src="<?php echo base_url()?>assets/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url()?>assets/js/select2.js"></script>
<script src="<?php echo base_url()?>assets/js/typeahead.jquery.js"></script>
 -->
 <!-- 
<script type="text/javascript">
      jQuery(function($){
          var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
        var container1 = demo1.bootstrapDualListbox('getContainer');
        container1.find('.btn').addClass('btn-white btn-info btn-bold');
      
        /**var setRatingColors = function() {
          $(this).find('.star-on-png,.star-half-png').addClass('orange2').removeClass('grey');
          $(this).find('.star-off-png').removeClass('orange2').addClass('grey');
        }*/
        $('.rating').raty({
          'cancel' : true,
          'half': true,
          'starType' : 'i'
          /**,
          
          'click': function() {
            setRatingColors.call(this);
          },
          'mouseover': function() {
            setRatingColors.call(this);
          },
          'mouseout': function() {
            setRatingColors.call(this);
          }*/
        })//.find('i:not(.star-raty)').addClass('grey');
        
        
        
        //////////////////
        //select2
        $('.select2').css('width','200px').select2({allowClear:true})
        $('#select2-multiple-style .btn').on('click', function(e){
          var target = $(this).find('input[type=radio]');
          var which = parseInt(target.val());
          if(which == 2) $('.select2').addClass('tag-input-style');
           else $('.select2').removeClass('tag-input-style');
        });
        
        //////////////////
        $('.multiselect').multiselect({
         enableFiltering: true,
         buttonClass: 'btn btn-white btn-primary',
         templates: {
          button: '<button type="button" class="multiselect dropdown-toggle" data-toggle="dropdown"></button>',
          ul: '<ul class="multiselect-container dropdown-menu"></ul>',
          filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="fa fa-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
          filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default btn-white btn-grey multiselect-clear-filter" type="button"><i class="fa fa-times-circle red2"></i></button></span>',
          li: '<li><a href="javascript:void(0);"><label></label></a></li>',
          divider: '<li class="multiselect-item divider"></li>',
          liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
         }
        });
        
        
        ///////////////////
          
        //typeahead.js
        //example taken from plugin's page at: https://twitter.github.io/typeahead.js/examples/
        var substringMatcher = function(strs) {
          return function findMatches(q, cb) {
            var matches, substringRegex;
           
            // an array that will be populated with substring matches
            matches = [];
           
            // regex used to determine if a string contains the substring `q`
            substrRegex = new RegExp(q, 'i');
           
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                // the typeahead jQuery plugin expects suggestions to a
                // JavaScript object, refer to typeahead docs for more info
                matches.push({ value: str });
              }
            });
      
            cb(matches);
          }
         }
      
         $('input.typeahead').typeahead({
          hint: true,
          highlight: true,
          minLength: 1
         }, {
          name: 'states',
          displayKey: 'value',
          source: substringMatcher(ace.vars['US_STATES'])
         });
          
          
        ///////////////
        
        
        //in ajax mode, remove remaining elements before leaving page
        $(document).one('ajaxloadstart.page', function(e) {
          $('[class*=select2]').remove();
          $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox('destroy');
          $('.rating').raty('destroy');
          $('.multiselect').multiselect('destroy');
        });
      
      });
    </script> -->

<script src="<?php echo base_url().'assets/js/custom/menu_role.js'?>"></script>
