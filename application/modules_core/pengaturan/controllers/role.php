<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}
		$this->load->library('authuser');
		$this->load->library('tanggal');
		$this->load->library('breadcrumbs');
		$this->load->library('regex');
		$this->load->model('pengaturan/role_model','role');
		$this->breadcrumbs->push('Daftar role', 'pengaturan/'.strtolower(get_class($this)));

	}

	public function index()
	{	
		$data['title'] = "Pengaturan Role";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->authuser->write_log();
		$this->load->view('role/index', $data);
	}

	public function form($id='')
	{
		if( $id != '' ){
			$this->breadcrumbs->push('Ubah role', 'pengaturan/'.strtolower(get_class($this)).'/'.__FUNCTION__.'/'.$id);
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
			$this->authuser->write_log(array('action'=>'R','message'=>'Read data on module role'));
			$data['value'] = $this->role->get_by_id($id);
		}else{
			$this->authuser->write_log(array('action'=>'C','message'=>'Form create role'));
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->breadcrumbs->push('Tambah role', 'pengaturan/'.strtolower(get_class($this)).'/form');
		}

		$data['title'] = "Pengaturan Role";
		$data['breadcrumbs'] = $this->breadcrumbs->show();
		$this->load->view('role/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->role->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $role) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = $role->id_role;
			$row[] = strtoupper($role->role_name);
			$row[] = ($role->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $role->updated_date?$this->tanggal->formatDateTime($role->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".$this->regex->_genRegex($role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  	  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_role('."'".$this->regex->_genRegex($role->id_role,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->role->count_all(),
						"recordsFiltered" => $this->role->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_role = $this->regex->_genRegex($this->input->post('id'), 'RGXINT');
		$this->db->trans_begin();
		$dataexc = array(
			'role_name' => $this->regex->_genRegex($this->input->post('role_name'), 'RGXQSL'),
			'active' => $this->regex->_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_role == 0 ){
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->role->save($dataexc);
			$this->authuser->write_log(array('last_query' => $this->db->last_query()));
		}else{
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->role->update(array('id_role'=>$id_role), $dataexc);
			$this->authuser->write_log(array('last_query' => $this->db->last_query()));
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->role->delete_by_id($id);
		$this->authuser->write_log(array('last_query' => $this->db->last_query()));	
		echo json_encode(array("status" => TRUE));
	}

	
}
