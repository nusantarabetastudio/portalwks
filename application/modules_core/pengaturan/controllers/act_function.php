<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Act_function extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('pengaturan/act_function_model','act_function');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Fungsi Aksi";
		$data['subtitle'] = "Daftar Fungsi Aksi";
		$this->load->view('act_function/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Fungsi Aksi";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->act_function->get_by_id($id);
		}
		$this->load->view('act_function/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->act_function->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $act_function) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($act_function->code);
			$row[] = strtoupper($act_function->name);
			$row[] = $act_function->description;
			$row[] = ($act_function->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $act_function->updated_date?Tanggal::formatDateTime($act_function->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($act_function->id_func_action,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_act_function('."'".Regex::_genRegex($act_function->id_func_action,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->act_function->count_all(),
						"recordsFiltered" => $this->act_function->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_func_action = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'code' => Regex::_genRegex($this->input->post('code'), 'RGXALNUM'),
			'name' => Regex::_genRegex($this->input->post('name'), 'RGXALNUM'),
			'description' => Regex::_genRegex($this->input->post('description'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => '',
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( $id_func_action == 0 ){
			$this->act_function->save($dataexc);
		}else{
			$this->act_function->update(array('id_func_action'=>$id_func_action), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->act_function->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
