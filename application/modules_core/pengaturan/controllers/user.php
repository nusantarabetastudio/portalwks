<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('master');
		$this->load->model('pengaturan/user_model','user');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Pengguna";
		$data['subtitle'] = "Daftar Pengguna";
		$this->load->view('user/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Pengguna";
		$data['subtitle'] = "";
		$provinsi = $this->db->get('m_provinsi')->result_array();
		$arr_prov = array();
            foreach ($provinsi as $prov) {
                $arr_prov[$prov['id_provinsi']] = $prov['nama_provinsi'];
            }

		$data['provinsi'] = $arr_prov;

		if( $id != '' ){
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->user->get_by_id($id);

			$kabupaten = $this->db->get('m_kabupaten')->result_array();
			$arr_kab = array('' => '(Pilih Kabupaten)');
	            foreach ($kabupaten as $kab) {
	                $arr_kab[$kab['id_kabupaten']] = $kab['nama_kabupaten'];
	            }

			$data['kabupaten'] = $arr_kab;

		}else{
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
			$data['kabupaten'] = array('' => '(Pilih Kabupaten)');
			$data['puskesmas'] = array('' => '(Pilih Puskesmas)');

		}
		$this->load->view('user/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->user->get_datatables(); //print_r($this->db->last_query());
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $user) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = $user->fullname;
			$row[] = $user->email;
			$row[] = '<a href="javascript:void()" title="Reset password" onclick="reset_password('."'".Regex::_genRegex($user->id_user,'RGXINT')."'".')">RESET PASSWORD </a>';
			$row[] = strtoupper($user->role_name);
			$row[] = ($user->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $user->updated_date?Tanggal::formatDateTime($user->updated_date):Tanggal::formatDateTime($user->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($user->id_user,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_user('."'".Regex::_genRegex($user->id_user,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->user->count_all(),
						"recordsFiltered" => $this->user->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_user = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'fullname' => Regex::_genRegex($this->input->post('fullname'), 'RGXQSL'),
			'email' => Regex::_genRegex($this->input->post('email'), 'RGXQSL'),
			'password' => Encryption::encrypt_password_callback(Regex::_genRegex($this->input->post('password'), 'RGXALNUM'), SECURITY_KEY),
			'id_role' => Regex::_genRegex($this->input->post('role'), 'RGXINT'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ')
		);
		
		if(Regex::_genRegex($this->input->post('role'), 'RGXINT') == 6){ // Provinsi
			$dataexc['id_provinsi'] = Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT');
			$dataexc['id_kabupaten'] = NULL;
			$dataexc['kode_user'] = Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT');
		}elseif (Regex::_genRegex($this->input->post('role'), 'RGXINT') == 5) { // kabupaten
			$dataexc['id_provinsi'] = Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT');
			$dataexc['id_kabupaten'] = Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT');
			$dataexc['kode_user'] = Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT');
			# code...
		}elseif (Regex::_genRegex($this->input->post('role'), 'RGXINT') == 3) { // rsu
			$dataexc['id_provinsi'] = Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT');
			$dataexc['id_kabupaten'] = Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT');
			$dataexc['kode_user'] = Regex::_genRegex($this->input->post('kode_rsu'), 'RGXALNUM');
			# code...
		}elseif (Regex::_genRegex($this->input->post('role'), 'RGXINT') == 4) { // kolegium
			$dataexc['id_provinsi'] = NULL;
			$dataexc['id_kabupaten'] = NULL;
			$dataexc['kode_user'] = Regex::_genRegex($this->input->post('kolegium'), 'RGXALNUM');
			# code...
		}elseif (Regex::_genRegex($this->input->post('role'), 'RGXINT') == 8) { // peserta
			$dataexc['id_provinsi'] = Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT');
			$dataexc['id_kabupaten'] = Regex::_genRegex($this->input->post('id_kabupaten'), 'RGXINT');
			$dataexc['kode_user'] = rand(9, 999).$this->input->post('id_provinsi').$this->input->post('fk'); 
			# code...
		}elseif (Regex::_genRegex($this->input->post('role'), 'RGXINT') == 9) { // FK
			$dataexc['id_provinsi'] = Regex::_genRegex($this->input->post('id_provinsi'), 'RGXINT');
			$dataexc['id_kabupaten'] = NULL;
			$dataexc['kode_user'] = $this->input->post('fk');
			# code...
		}else{ // puskesmas
			$dataexc['id_provinsi'] = NULL;
			$dataexc['id_kabupaten'] = NULL;
			$dataexc['kode_user'] = NULL;
			# code...
		}

		if( $id_user == 0 ){
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
			$dataexc['created_by'] = $this->session->userdata('data_user')->id_user;
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$this->user->save($dataexc);
		}else{
			$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->id_user;
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$this->user->update(array('id_user'=>$id_user), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function get_kode_user(){
		$role = $this->input->post('role');
		switch ($role) {

			case '5':
				$result = $this->input->post('id_kabupaten'); 
				break;
			case '6':
				$result = $this->input->post('id_provinsi'); 
				break;
			case '3':
				$result = $this->input->post('kode_rsu'); 
				break;
			case '4':
				$result = $this->input->post('kolegium'); 
				break;
			case '9':
				$result = $this->input->post('fk'); 
				break;
			case '8':
				$result = $this->input->post('id_provinsi').$this->input->post('fk'); 
				break;
			
			default:
				$result = rand(999999);
				break;
		}

		return $result;

	}

	public function ajax_delete($id)
	{
		$this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->user->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_reset($id)
	{
		$this->user->reset_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
	
}
