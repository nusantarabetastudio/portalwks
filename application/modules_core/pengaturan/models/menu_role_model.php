
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_role_model extends CI_Model {

	var $table = 'm_role';
	var $column = array('m_role.role_name','m_role.active','m_role.updated_date');
	var $select = 'm_role.id_role, m_role.role_name, m_role.active, m_role.updated_date';

	var $order = array('m_role.id_role' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select($this->select);
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->where(''.$this->table.'.id_role',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_role', $id);
		$this->db->delete($this->table);
	}

	public function get_checked_form($id_menu, $id_role, $code)
	{
		$data = $this->db->get_where('t_menu_role', array('id_menu'=>$id_menu, 'id_role'=>$id_role, 'code'=>$code));
		if($data->num_rows() > 0 ){
			return 'checked';
		}else{
			return false;
		}
	}

	public function get_menu_role_action($id_role)
	{
		$this->db->from('t_menu_role');
		$this->db->join('m_menu', 'm_menu.id_menu=t_menu_role.id_menu', 'left');
		$this->db->group_by('t_menu_role.id_menu');
		$this->db->where(array('id_role'=>$id_role));
		$data = $this->db->get();

		if($data->num_rows() > 0 ){
			$result = $data->result_array();
			$string = '<ul>';
			foreach ($result as $key => $value) {
				$string .= '<li>';
				$string .= ' '.$value['name'].' ';
				$code = $this->get_code(array('id_role'=>$value['id_role'], 'id_menu'=>$value['id_menu']));

				$string .= '[ '.$code.' ]</br>';
				$string .= '</li>';
			}
			
			return $string;
		}else{
			return false;
		}
	}

	public function get_code($arr_where){

		$code = $this->db->get_where('t_menu_role', $arr_where)->result_array();
		foreach ($code as $row_code) {
			$arr_code[] = $row_code['code'];
		}
		$implode = implode(' , ', $arr_code);

		return $implode;

	}


}
