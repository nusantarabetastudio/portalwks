<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	var $table = 'm_user';
	var $column = array('m_user.email','m_user.password','m_user.fullname','m_role.role_name','m_user.active','m_user.updated_date');
	var $select = 'm_user.*,m_role.role_name';

	var $order = array('m_user.id_user' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_role', 'm_role.id_role='.$this->table.'.id_role', 'left');

		if( in_array($this->session->userdata('data_user')->id_role, array('1','2')) ){
			$this->db->where(array('m_user.active'=>'Y'));
		}else{
			$this->db->where(''.$this->table.'.created_by', $this->session->userdata('data_user')->id_user);
			$this->db->or_where(''.$this->table.'.updated_by', $this->session->userdata('data_user')->id_user);
		}

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_role', 'm_role.id_role='.$this->table.'.id_role', 'left');
		$this->db->where(''.$this->table.'.id_user',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_user', $id);
		$this->db->delete($this->table);
	}

	public function reset_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_user', $id);
		$this->db->update($this->table, array('password' => Encryption::encrypt_password_callback('123456', SECURITY_KEY)));
	}


}
