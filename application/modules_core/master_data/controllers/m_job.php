<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_job extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_job_model','job');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Pekerjaan";
		$data['subtitle'] = "Daftar Pekerjaan";
		$this->load->view('m_job/index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Pekerjaan";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->job->get_by_id($id);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('m_job/form', $data);

	}

	public function ajax_list()
	{
		$list = $this->job->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m_job) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$m_job->job_id.' ]';
			$row[] = $m_job->job_name;
			$row[] = $m_job->created_date?Tanggal::formatDateTime($m_job->created_date):'-';
			$row[] = $m_job->updated_date?Tanggal::formatDateTime($m_job->updated_date):'-';
			$row[] = ($m_job->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($m_job->job_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_job('."'".Regex::_genRegex($m_job->job_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->job->count_all(),
						"recordsFiltered" => $this->job->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$job_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'job_name' => Regex::_genRegex($this->input->post('job_name'), 'RGQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
		);
		
		if( $job_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->job->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->job->update(array('job_id'=>$job_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->job->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
