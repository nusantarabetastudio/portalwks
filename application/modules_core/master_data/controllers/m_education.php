<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_education extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_education_model','education');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Pendidikan";
		$data['subtitle'] = "Daftar Pendidikan";
		$this->load->view('m_education/index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Pendidikan";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->education->get_by_id($id);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('m_education/form', $data);

	}

	public function ajax_list()
	{
		$list = $this->education->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m_education) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$m_education->education_id.' ]';
			$row[] = $m_education->education_name;
			$row[] = $m_education->created_date?Tanggal::formatDateTime($m_education->created_date):'-';
			$row[] = $m_education->updated_date?Tanggal::formatDateTime($m_education->updated_date):'-';
			$row[] = ($m_education->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($m_education->education_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_education('."'".Regex::_genRegex($m_education->education_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->education->count_all(),
						"recordsFiltered" => $this->education->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$education_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'education_name' 	=> Regex::_genRegex($this->input->post('education_name'), 'RGQSL'),
			'active' 			=> Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
		);
		
		if( $education_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->education->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->education->update(array('education_id'=>$education_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->education->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
