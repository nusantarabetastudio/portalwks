<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_religion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_religion_model','religion');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Agama";
		$data['subtitle'] = "Daftar Agama";
		$this->load->view('m_religion/index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Agama";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->religion->get_by_id($id);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('m_religion/form', $data);

	}

	public function ajax_list()
	{
		$list = $this->religion->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m_religion) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$m_religion->religion_id.' ]';
			$row[] = $m_religion->religion_name;
			$row[] = $m_religion->created_date?Tanggal::formatDateTime($m_religion->created_date):'-';
			$row[] = $m_religion->updated_date?Tanggal::formatDateTime($m_religion->updated_date):'-';
			$row[] = ($m_religion->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($m_religion->religion_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_religion('."'".Regex::_genRegex($m_religion->religion_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->religion->count_all(),
						"recordsFiltered" => $this->religion->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$religion_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'religion_name' => Regex::_genRegex($this->input->post('religion_name'), 'RGQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
		);
		
		if( $religion_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->religion->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->religion->update(array('religion_id'=>$religion_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->religion->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
