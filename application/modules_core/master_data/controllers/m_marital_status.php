<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_marital_status extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_marital_model','marital_status');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Status Pernikahan";
		$data['subtitle'] = "Daftar Status Pernikahan";
		$this->load->view('m_marital_status/index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Status Pernikahan";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->marital_status->get_by_id($id);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('m_marital_status/form', $data);

	}

	public function ajax_list()
	{
		$list = $this->marital_status->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m_marital_status) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$m_marital_status->ms_id.' ]';
			$row[] = $m_marital_status->ms_name;
			$row[] = $m_marital_status->created_date?Tanggal::formatDateTime($m_marital_status->created_date):'-';
			$row[] = $m_marital_status->updated_date?Tanggal::formatDateTime($m_marital_status->updated_date):'-';
			$row[] = ($m_marital_status->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($m_marital_status->ms_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_ms('."'".Regex::_genRegex($m_marital_status->ms_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->marital_status->count_all(),
						"recordsFiltered" => $this->marital_status->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$ms_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'ms_name' => Regex::_genRegex($this->input->post('ms_name'), 'RGQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
		);
		
		if( $ms_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->marital_status->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->marital_status->update(array('ms_id'=>$ms_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->marital_status->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
