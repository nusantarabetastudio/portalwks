<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kabupaten extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_kabupaten_model','kabupaten');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Kabupaten";
		$data['subtitle'] = "Daftar Kabupaten";
		$this->load->view('kabupaten/index', $data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Kabupaten";
		$data['subtitle'] = "";
		if( $id != '' ){
			$data['value'] = $this->kabupaten->get_by_id($id);
		}
		$this->load->view('kabupaten/form', $data);
	}

	public function ajax_list()
	{
		$list = $this->kabupaten->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $kabupaten) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($kabupaten->id_kabupaten);
			$row[] = strtoupper($kabupaten->nama_kabupaten);
			$row[] = strtoupper($kabupaten->nama_provinsi);
			$row[] = ($kabupaten->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $kabupaten->updated_date?Tanggal::formatDateTime($kabupaten->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($kabupaten->id_kabupaten,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_kabupaten('."'".Regex::_genRegex($kabupaten->id_kabupaten,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->kabupaten->count_all(),
						"recordsFiltered" => $this->kabupaten->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$id_kabupaten = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'id_kabupaten' => $id_kabupaten,
			'id_provinsi' => Regex::_genRegex($this->input->post('id_provinsi'), 'RGXQSL'),
			'nama_kabupaten' => Regex::_genRegex($this->input->post('nama_kabupaten'), 'RGXQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			'updated_by' => $this->session->userdata('data_user')->fullname,
			'updated_date' => date('Y-m-d H:i:s')
		);
		
		if( !$this->kabupaten->get_by_id($id_kabupaten) ){
			$this->kabupaten->save($dataexc);
		}else{
			$this->kabupaten->update(array('id_kabupaten'=>$id_kabupaten), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{
		$this->kabupaten->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

    function get_rsu_by_kab($id) {
		
        $this->db->select('kode_rs, nama_rs')->where('id_kabupaten', $id)->order_by('nama_rs', 'ASC');
        $result = $this->db->get('m_rumah_sakit')->result_array();
        echo json_encode($result);
        exit;
    }

    function get_fk_by_kab($id) {
		
        $this->db->select('fk_id, fk_name')->where('id_kabupaten', $id)->order_by('fk_name', 'ASC');
        $result = $this->db->get('m_fk')->result_array();
        echo json_encode($result);
        exit;
    }

	
}
