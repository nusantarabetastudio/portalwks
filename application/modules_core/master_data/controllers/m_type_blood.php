<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_type_blood extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_type_blood_model','type_blood');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Golongan Darah";
		$data['subtitle'] = "Daftar Golongan Darah";
		$this->load->view('m_type_blood/index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Golongan Darah";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->type_blood->get_by_id($id);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('m_type_blood/form', $data);

	}

	public function ajax_list()
	{
		$list = $this->type_blood->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m_type_blood) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$m_type_blood->tb_id.' ]';
			$row[] = $m_type_blood->tb_name;
			$row[] = ($m_type_blood->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $m_type_blood->created_date?Tanggal::formatDateTime($m_type_blood->created_date):'-';
			$row[] = $m_type_blood->updated_date?Tanggal::formatDateTime($m_type_blood->updated_date):'-';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($m_type_blood->tb_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_type_blood('."'".Regex::_genRegex($m_type_blood->tb_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->type_blood->count_all(),
						"recordsFiltered" => $this->type_blood->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$type_blood_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'tb_name' => Regex::_genRegex($this->input->post('tb_name'), 'RGQSL'),
			'active'			=> Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
			
		);
		
		if( $type_blood_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			
			$this->type_blood->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			
			$this->type_blood->update(array('tb_id'=>$type_blood_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->type_blood->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	
}
