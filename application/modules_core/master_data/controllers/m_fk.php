<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_fk extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('m_fk_model','fk');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Fakultas Kedokteran";
		$data['subtitle'] = "Daftar Fakultas Kedokteran";
		$this->load->view('m_fk/index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Fakultas Kedokteran";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->fk->get_by_id($id);
		}else{
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('m_fk/form', $data);

	}

	public function ajax_list()
	{
		$list = $this->fk->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m_fk) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = '[ '.$m_fk->fk_id.' ]';
			$row[] = $m_fk->fk_name;
			$row[] = $m_fk->created_date?Tanggal::formatDateTime($m_fk->created_date):'-';
			$row[] = $m_fk->updated_date?Tanggal::formatDateTime($m_fk->updated_date):'-';
			$row[] = ($m_fk->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($m_fk->fk_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_fk('."'".Regex::_genRegex($m_fk->fk_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->fk->count_all(),
						"recordsFiltered" => $this->fk->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		$fk_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'fk_name' => Regex::_genRegex($this->input->post('fk_name'), 'RGQSL'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ'),
		);
		
		if( $fk_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'C');
			$this->fk->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'U');
			$this->fk->update(array('fk_id'=>$fk_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = Authuser::get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->fk->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	function get_fk_by_kode_json($fk_id) {
		
        $this->db->select('m_fk.*');
        $this->db->from('m_fk');
       
        $this->db->where('fk_id', $fk_id);
        $this->db->order_by('fk_name', 'ASC');
        
        $result = $this->db->get()->row_array();
        echo json_encode($result);
    }

    function get_fk_by_prov($id) {
		
        $this->db->select('m_fk.fk_id, m_fk.fk_name')->join('m_fk','t_fk_region.fk_id=m_fk.fk_id','left')->where('t_fk_region.id_provinsi', $id)->order_by('fk_name', 'ASC');
        $result = $this->db->get('t_fk_region')->result_array();
        echo json_encode($result);
        exit;
    }

	
}
