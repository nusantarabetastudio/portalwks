<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rs_model extends CI_Model {

	var $table = 'm_rumah_sakit';
	var $column = array('m_rumah_sakit.nama_rs','m_rumah_sakit.alamat','m_rumah_sakit.jenis_rs','m_rumah_sakit.kelas_rs','m_rumah_sakit.jumlah_tt','m_rumah_sakit.active','m_rumah_sakit.updated_date','m_rumah_sakit.created_date');
	var $select = 'm_rumah_sakit.*';

	var $order = array('id_rs' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		
		$this->db->select($this->select);
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->where(''.$this->table.'.id_rs',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function get_by_kode_rs($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->where(''.$this->table.'.kode_rs',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_rs', $id);
		$this->db->delete($this->table);
	}


}
