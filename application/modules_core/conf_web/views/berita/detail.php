<div class="profile-user-info profile-user-info-striped">
  
  <div class="profile-info-row">
    <div class="profile-info-name"> Judul Berita </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->judul_berita ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Sub Judul Berita </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->sub_judul ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Penulis </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->penulis ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Tanggal Berita </div>
    <div class="profile-info-value">
      <span class="editable"><?= $this->tanggal->tgl_indo($value->tanggal_berita) ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Sumber </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->sumber ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Isi Berita </div>
    <div class="profile-info-value">
      <span class="editable"><p align="justify"><?= $value->isi_berita ?></p></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Gambar </div>
    <div class="profile-info-value">
      <span class="editable"> 
        <?php if(isset($value->path_thumbnail)){?><img src="<?php echo base_url().'uploaded_files/berita/'.$value->path_thumbnail.'' ?>" width="50%"><?php }else{echo 'Tidak ada gambar';};?>
      </span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Slider </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo ($value->main == 'Y') ? '<span class="label label-xs label-success">Active</span>' : '<span class="label label-xslabel-danger">Not active</span>' ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Publish </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo ($value->publish == 'Y') ? '<span class="label label-xs label-success">Active</span>' : '<span class="label label-xslabel-danger">Not active</span>' ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Dibaca </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo $value->dibaca ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Status Data </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo ($value->active == 'Y') ? '<span class="label label-xs label-success">Active</span>' : '<span class="label label-xslabel-danger">Not active</span>' ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Kategori berita </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->category_name ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Created Date </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $this->tanggal->formatDateTime($value->created_date) ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Created by </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->created_by ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Updated Date </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo $value->updated_date?$this->tanggal->formatDateTime($value->updated_date):'-' ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Updated by </div>
    <div class="profile-info-value">
      <span class="editable"><?php echo $value->updated_by?$value->updated_by:'-' ?></span>
    </div>
  </div>


  </div>