<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/ui.jqgrid.css" />
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<!-- modal -->
<div id="contentDetail" class="hide"></div>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
    <div class="clearfix" style="float:left">
      <button class="btn btn-sm btn-primary" id="btn_add_menu" ><i class="glyphicon glyphicon-plus"></i> Tambah Berita </button><div class="pull-right tableTools-container"></div>
    </div>
    <br>
    <hr class="separator">

   <form class="form-inline" id="form_search_index_berita">
    <label class="inline">
      Pencarian berdasarkan : 
      <select id="search_by_category_berita" class="input-small" style="width:300px">
        <option value="">-Silahkan pilih-</option>
        <option value="judul_berita">Judul Berita</option>
        <option value="sub_judul">Sub Judul</option>
        <option value="penulis">Penulis</option>
        <option value="category_name">Kategori</option>
        <option value="sumber">Sumber</option>
        <option value="tanggal_berita">Tanggal Berita</option>
      </select>
    </label>

    <label class="inline">
      &nbsp;&nbsp; Kata Kunci : 
      <input type="text" id="keyword_berita" style="width: 200px" class="input-small" placeholder="Kata Kunci" />
    </label>

    <button type="button" id="btn_search_berita" class="btn btn-default btn-sm">
      <i class="ace-icon fa fa-search bigger-110"></i>Cari
    </button> 
    <button type="button" id="btn_reset_berita" class="btn btn-danger btn-sm">
      <i class="ace-icon fa fa-refresh bigger-110"></i>Reset
    </button> 

    <br><br>

    <table id="grid_table_berita"></table>
    <div id="grid_pager_berita"></div>

    </form>
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url()?>assets/js/custom/berita.js"></script>