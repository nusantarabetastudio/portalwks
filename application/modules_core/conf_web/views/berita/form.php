<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.none { display:none; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_berita" action="<?php echo site_url('berita/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->id_berita:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Judul Berita</label>
                  <div class="col-md-6">
                    <input name="judul_berita" id="judul_berita" value="<?php echo isset($value)?$value->judul_berita:''?>"  class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Sub Judul Berita</label>
                  <div class="col-md-6">
                    <input name="sub_judul" id="sub_judul" value="<?php echo isset($value)?$value->sub_judul:''?>"  class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Penulis</label>
                  <div class="col-md-3">
                    <input name="penulis" id="penulis" value="<?php echo isset($value)?$value->penulis:''?>"  class="form-control" type="text">
                  </div>
                  <label class="control-label col-md-2">Sumber</label>
                  <div class="col-md-4">
                    <input name="sumber" id="sumber" value="<?php echo isset($value)?$value->sumber:''?>"  class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tanggal Berita</label>
                  <div class="col-md-3">
                    <input name="tanggal_berita" id="tanggal_berita" value="<?php echo isset($value)?$value->tanggal_berita:''?>"  class="form-control date-picker" data-date-format="yyyy-mm-dd" type="text">
                  </div>
                  <label class="control-label col-md-2">Kategori berita</label>
                  <div class="col-md-3">
                    <?php 
                      echo $this->master->get_master_custom(array('table'=>'m_category','where'=>array(),'name'=>'category_name','id'=>'category_id'),isset($value)?$value->category_id:'','category_id','category_id','select2','','inline');
                    ?>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-md-2">Jenis Berita</label>
                  <div class="col-md-4">
                    <div class="radio">
                          <label>
                            <input name="jenis_berita" type="radio" class="ace" value="foto" <?php echo isset($value) ? ($value->jenis_berita == 'foto') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Foto </span>
                          </label>
                          <label>
                            <input name="jenis_berita" type="radio" class="ace" value="video" <?php echo isset($value) ? ($value->jenis_berita == 'video') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl"> Video </span>
                          </label>
                    </div>
                  </div>
                </div>

                <div id="fotoDiv" 
                <?php echo isset($value)?($value->jenis_berita=='foto')?'':'style="display:none"':''?>>
                <div class="form-group">
                  <label class="control-label col-md-2">Upload Foto</label>
                  <div class="col-md-4">
                    <input name="path_thumbnail" id="path_thumbnail"  type="file">
                  </div>
                </div>
                <?php if(isset($value->path_thumbnail)){?>
                <div class="form-group">
                  <label class="control-label col-md-2">Images</label>
                  <div class="col-md-4">
                    <img src="<?php echo base_url().'uploaded_files/berita/'.$value->path_thumbnail.''?>" width="250px">
                  </div>
                </div>
                <?php }?>

                <div class="form-group">
                  <label class="control-label col-md-2">Jadikan Slider ?</label>
                  <div class="col-md-4">
                    <div class="radio">
                          <label>
                            <input name="main" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->main == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="main" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->main == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>
                </div>

                <div id="videoDiv" <?php echo isset($value)?($value->jenis_berita=='video')?'':'style="display:none"':'style="display:none"'?> >
                  <div class="form-group">
                    <label class="control-label col-md-2">Link video</label>
                    <div class="col-md-6">
                      <input name="link_video" id="link_video" value="<?php echo isset($value)?$value->link_video:''?>"  class="form-control" type="text"><small>Masukan link youtube, example : https://www.youtube.com/watch?v=uBbYGt5Qldc</small>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Publish Berita ?</label>
                  <div class="col-md-4">
                    <div class="radio">
                          <label>
                            <input name="publish" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->publish == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="publish" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->publish == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Set running text</label>
                  <div class="col-md-4">
                    <div class="radio">
                          <label>
                            <input name="running_text" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->running_text == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="running_text" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->running_text == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-4">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Isi Berita</label>
                  <div class="col-md-2">
                    <textarea cols="80" id="isi_berita" class="ckeditor" name="isi_berita" rows="5"><?php echo isset($value)?$value->isi_berita:''?></textarea>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->id_berita:0?>"> -->

                  <a onclick="backlist()" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a id="btn_add_menu" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/berita.js'?>"></script>
<script src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">

$(function() {
    $('input[name="jenis_berita"]').change(function() {
        var rad = $(this).attr('value'); 
          if(rad == 'foto'){
            $('#fotoDiv').show();
            $('#videoDiv').hide();
          }else if (rad == 'video') {
            $('#fotoDiv').hide();
            $('#videoDiv').show();
          }
            
    });
});

$( document ).ready( function() {
  $( 'textarea#isi_berita' ).ckeditor();
} );

$('.date-picker').datepicker({
        dateFormat:'yy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        changeMonth: true,
        changeYear: true
      })
      //show datepicker when clicking on the icon
      .next().on(ace.click_event, function(){
        $(this).prev().focus();
      });
</script>
