<link rel="stylesheet" href="<?php echo base_url()?>assets/css/daterangepicker.css" />
<link rel="stylesheet" href="<?php echo base_url()?>/assets/css/bootstrap-datetimepicker.css" />
<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_t_agenda" action="<?php echo site_url('t_agenda/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->agenda_id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Kegiatan</label>
                  <div class="col-md-4">
                    <input name="agenda_name" id="agenda_name" value="<?php echo isset($value)?$value->agenda_name:''?>"  class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tempat</label>
                  <div class="col-md-3">
                    <input name="agenda_place" id="agenda_place" value="<?php echo isset($value)?$value->agenda_place:''?>" class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Waktu</label>
                  <div class="col-md-3">
                    <div class="row">
                      <div class="col-xs-8 col-sm-11">
                        <!-- #section:plugins/date-time.daterangepicker -->
                        <div class="input-group">
                          <input type="text" class="datetimepicker form-control" name="agenda_start_date" value="<?php echo isset($value)? $this->tanggal->formatDateTimeForm($value->agenda_start_date) : '' ?>" />
                          <span class="input-group-addon">
                            <i class="fa fa-clock-o bigger-110"></i>
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <label class="control-label col-md-1">s/d</label>
                  <div class="col-md-3">
                    <div class="row">
                      <!-- #section:plugins/date-time.daterangepicker -->
                      <div class="input-group">
                        <input type="text" class="datetimepicker form-control" name="agenda_end_date" value="<?php echo isset($value)? $this->tanggal->formatDateTimeForm($value->agenda_end_date) : '' ?>"/>
                        <span class="input-group-addon">
                          <i class="fa fa-clock-o bigger-110"></i>
                        </span>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Deskripsi Singkat</label>
                  <div class="col-md-8">
                    <textarea name="agenda_description" class="form-control"><?php echo isset($value)?$value->agenda_description:''?></textarea>
                  </div>
                </div>


                <div class="form-group">
                  <label class="control-label col-md-2">Author</label>
                  <div class="col-md-5">
                    <?php 
                      echo $this->master->get_master_custom(
                          array('table'=>'m_work_unit','where'=>array('active'=>'Y'), 'name'=>'wu_name', 'id'=>'wu_id'), isset($value)?$value->agenda_author:'','agenda_author','agenda_author','select2','','inline');?>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-9">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>
                  
                <h3 class="header smaller lighter blue">Peserta Kegiatan</h3>
                  <div class="form-group">
                    <label class="control-label col-md-2"> Unit Kerja </label>
                    <div class="col-md-10">
                      <div class="span10" id="dasar-x">

                          <?php

                            if(isset($value)){
                              $exp_wu = explode('#', $value->agenda_participant);

                              if(count($exp_wu) > 0){
                                $no = 0;
                                foreach($exp_wu as $value_exp_wu){
                                  if($value_exp_wu != ''){
                                    if($no == 1){ ?>

                                    <div class="row-fluid">
                                        <?php 
                                          echo $this->master->get_master_custom(
                                              array('table'=>'m_work_unit','where'=>array('active'=>'Y'), 'name'=>'wu_name', 'id'=>'wu_id'),$value_exp_wu,'wu_id[]','wu_id','select2','style="height: 34px; width: auto;"','inline');?>
                                        <a href="javascript:void(0)" onclick="addBase(this)" style="line-height: 19px; margin-top: -2px;" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-plus"></i></a>
                                    </div>

                                  <?php }else{ ?>

                                    <div class="row-fluid">
                                        <?php 
                                          echo $this->master->get_master_custom(
                                              array('table'=>'m_work_unit','where'=>array('active'=>'Y'), 'name'=>'wu_name', 'id'=>'wu_id'),$value_exp_wu,'wu_id[]','wu_id','select2','style="height: 34px; width: auto;"','inline');?>
                                        <a href="javascript:void(0)" onclick="minBase(this)" style="line-height: 19px; margin-top: -2px;" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                                    </div>

                                    <?php } ?>

                              <?php
                                  }
                                $no++; }
                              }

                            }else{?>

                            <div class="row-fluid">
                                <?php 
                                  echo $this->master->get_master_custom(
                                      array('table'=>'m_work_unit','where'=>array('active'=>'Y'), 'name'=>'wu_name', 'id'=>'wu_id'),'','wu_id[]','wu_id','select2','style="height: 34px; width: auto;"','inline');?>
                                <a href="javascript:void(0)" onclick="addBase(this)" style="line-height: 19px; margin-top: -2px;" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-plus"></i></a>
                            </div>

                            <?php }?>

                          
                      </div>
                    </div>
                  </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->agenda_id:0?>"> -->

                  <a onclick="backlist()" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a onclick="add()" id="btnAdd" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <?php if(isset($value)):?>
                  <a onclick="view_table(<?php echo $value->agenda_id?>)" id="btnView" href="#" class="btn btn-sm btn-warning">
                    <i class="ace-icon fa fa-list icon-on-right bigger-110"></i>
                    Lihat dalam bentuk tabel
                  </a>
                  <?php endif;?>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url()?>/assets/js/date-time/bootstrap-datetimepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/daterangepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.maskedinput.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
  jQuery(function($) {

    $('input[name=agenda_date]').daterangepicker({
      'applyClass' : 'btn-sm btn-success',
      'cancelClass' : 'btn-sm btn-default',
      locale: {
        applyLabel: 'Apply',
        cancelLabel: 'Cancel',
      }
    })
    .prev().on(ace.click_event, function(){
      $(this).next().focus();
    });

    $.mask.definitions['~']='[+-]';
    $('.input-mask-date').mask('99/99/9999');

    $('.datetimepicker').datetimepicker().next().on(ace.click_event, function(){
      $(this).prev().focus();
    });

  });
</script>

<script type="text/javascript">

    function addBase(t) {
        var rs = '<div class="row-fluid">' +
                    '<?php 
                        echo trim(preg_replace('/\r\n|\r|\n/', ' ', $this->master->get_master_custom(
                            array('table'=>'m_work_unit','where'=>array('active'=>'Y'), 'name'=>'wu_name', 'id'=>'wu_id'), '' ,'wu_id[]','wu_id','select2','style="height: 34px; margin-right:3px; width: auto;"','inline')));?>'+
                    '<a href="javascript:void(0)" onclick="minBase(this)" style="line-height: 19px; margin-top: -2px;" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i></a>'+
                '</div>';
        $('#dasar-x').append(rs);
    }

    function minBase(t) {
        $(t).parent().remove();
    }

</script>

<script src="<?php echo base_url().'assets/js/custom/t_agenda.js'?>"></script>
