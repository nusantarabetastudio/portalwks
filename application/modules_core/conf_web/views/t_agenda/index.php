<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/jquery-ui.custom.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/fullcalendar.css" />
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">

    <!-- PAGE CONTENT BEGINS -->
   
    <div class="row">
      <div class="col-sm-9">

        <div class="clearfix">
          <button class="btn btn-sm btn-primary" onclick="add()"><i class="glyphicon glyphicon-plus"></i> Tambah Kegiatan </button>
          <button class="btn btn-sm btn-warning" onclick="view_table()"><i class="fa fa-table"></i> Lihat Kegiatan Dalam Bentuk Tabel </button>
          <div class="pull-right tableTools-container"></div>
        </div>

        <div class="space"></div>

        <!-- #section:plugins/data-time.calendar -->
        <div id="calendar"></div>

        <!-- /section:plugins/data-time.calendar -->
      </div>

      <div class="col-sm-3">
        <div class="widget-box transparent">
          <div class="widget-header">
            <h4>Indikator Warna Eselon I</h4>
          </div>

          <div class="widget-body">
            <div class="widget-main no-padding">
              <div id="external-events">

              <?php 
                $eselon = $this->master->get_custom_data(array('table'=>'m_work_unit', 'where'=>array('wu_level'=>1, 'active'=>'Y')));
                foreach($eselon as $row_eselon){
              ?>

              <div class="external-event <?php echo $row_eselon['wu_color_indicator']?>" data-class="<?php echo $row_eselon['wu_color_indicator']?>">
                <i class="ace-icon fa fa-circle-o"></i>
                <?php 
                    $count_str = strlen($row_eselon['wu_name']);
                    $points = ($count_str >= 35) ? '...' : ''; 
                    echo substr($row_eselon['wu_name'], 0, 30). $points; ?>
                    
              </div>

              <?php }?>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->


<!-- page specific plugin scripts -->
<script src="<?php echo base_url().'assets/js/custom/t_agenda.js'?>"></script>
<script src="<?php echo base_url()?>assets/js/jquery-ui.custom.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.ui.touch-punch.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/moment.js"></script>
<script src="<?php echo base_url()?>assets/js/fullcalendar.js"></script>
<script src="<?php echo base_url()?>assets/js/bootbox.js"></script> 
<!-- inline scripts related to this page -->
<script type="text/javascript">
  jQuery(function($) {

/* initialize the external events
-----------------------------------------------------------------*/

$('#external-events div.external-event').each(function() {

// create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
// it doesn't need to have a start or end
var eventObject = {
  title: $.trim($(this).text()) // use the element's text as the event title
};

// store the Event Object in the DOM element so we can get to it later
$(this).data('eventObject', eventObject);

// make the event draggable using jQuery UI
/*$(this).draggable({
  zIndex: 999,
  revert: true,      // will cause the event to go back to its
  revertDuration: 0  //  original position after the drag
});*/

});




/* initialize the calendar
-----------------------------------------------------------------*/

var date = new Date();
var d = date.getDate();
var m = date.getMonth();
var y = date.getFullYear();


var calendar = $('#calendar').fullCalendar({
//isRTL: true,
 buttonHtml: {
  prev: '<i class="ace-icon fa fa-chevron-left"></i>',
  next: '<i class="ace-icon fa fa-chevron-right"></i>'
},

header: {
  left: 'prev,next today',
  center: 'title',
  right: 'month,agendaWeek,agendaDay'
},
events: {
        url: '<?php echo base_url()?>t_agenda/get_agenda_json',
        type: 'POST',
        data: {
            custom_param1: 'something',
            custom_param2: 'somethingelse'
        },
        error: function() {
            alert('there was an error while fetching events!');
        }
    }
,
editable: false,
droppable: false, // this allows things to be dropped onto the calendar !!!
drop: function(date, allDay) { // this function is called when something is dropped

  // retrieve the dropped element's stored Event Object
  var originalEventObject = $(this).data('eventObject');
  var $extraEventClass = $(this).attr('data-class');
  
  
  // we need to copy it, so that multiple events don't have a reference to the same object
  var copiedEventObject = $.extend({}, originalEventObject);
  
  // assign it the date that was reported
  copiedEventObject.start = date;
  copiedEventObject.allDay = allDay;
  if($extraEventClass) copiedEventObject['className'] = [$extraEventClass];
  
  // render the event on the calendar
  // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
  $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);
  
  // is the "remove after drop" checkbox checked?
  if ($('#drop-remove').is(':checked')) {
    // if so, remove the element from the "Draggable Events" list
    $(this).remove();
  }
  
}
,
selectable: true,
selectHelper: true,
/*select: function(start, end, allDay) {
  
  bootbox.prompt("New Event Title:", function(title) {
    if (title !== null) {
      calendar.fullCalendar('renderEvent',
        {
          title: title,
          start: start,
          end: end,
          allDay: allDay,
          className: 'label-info'
        },
        true // make the event "stick"
      );
    }
  });
  

  calendar.fullCalendar('unselect');
}
,*/
eventClick: function(calEvent, jsEvent, view) {

  //display a modal
  var modal = 
  '<div class="modal fade">\
    <div class="modal-dialog">\
     <div class="modal-content">\
     <div class="modal-body">\
       <button type="button" class="close" data-dismiss="modal" style="margin-top:0px;">&times;</button>\
       <form class="form-horizontal" action="t_agenda/edit_agenda">\
        <div class="form-group">\
          <label class="control-label col-md-2">Nama Kegiatan</label>\
          <div class="col-md-8">\
            <input name="id" id="id" value="' + calEvent.title + '" placeholder="Auto" class="form-control" type="text" readonly>\
          </div>\
        </div>\
        <div class="form-group">\
          <label class="control-label col-md-2">Tempat</label>\
          <div class="col-md-6">\
            <input name="id" id="id" value="' + calEvent.place + '" placeholder="Auto" class="form-control" type="text" readonly>\
          </div>\
        </div>\
        <div class="form-group">\
          <label class="control-label col-md-2">Tanggal</label>\
          <div class="col-md-6">\
            <input name="id" id="id" value="' + new Date(calEvent.start).toUTCString().replace("GMT", "WIB") + '" placeholder="Auto" class="form-control" type="text" readonly>\
          </div>\
        </div>\
        <div class="form-group">\
          <label class="control-label col-md-2">s/d</label>\
          <div class="col-md-6">\
            <input name="id" id="id" value="' + new Date(calEvent.end).toUTCString().replace("GMT","WIB") + '" placeholder="Auto" class="form-control" type="text" readonly>\
          </div>\
        </div>\
        <div class="form-group">\
          <label class="control-label col-md-2">Author</label>\
          <div class="col-md-8">\
            <input name="id" id="id" value="' + calEvent.author + '" placeholder="Auto" class="form-control" type="text" readonly>\
          </div>\
        </div>\
        <div class="form-group">\
          <label class="control-label col-md-2">Tempat</label>\
          <div class="col-md-6">\
            <textarea class="form-control" readonly>' + calEvent.description + '</textarea>\
          </div>\
        </div>\
       </form>\
     </div>\
     <div class="modal-footer">\
      <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="ace-icon fa fa-times"></i> Close</button>\
     </div>\
    </div>\
   </div>\
  </div>';


  var modal = $(modal).appendTo('body');
  modal.find('form').on('submit', function(ev){
    ev.preventDefault();

    calEvent.title = $(this).find("input[type=text]").val();
    calendar.fullCalendar('updateEvent', calEvent);
    modal.modal("hide");
  });
  modal.find('button[data-action=delete]').on('click', function() {
    calendar.fullCalendar('removeEvents' , function(ev){
      return (ev._id == calEvent._id);
    })
    modal.modal("hide");
  });
  
  modal.modal('show').on('hidden', function(){
    modal.remove();
  });


  //console.log(calEvent.id);
  //console.log(jsEvent);
  //console.log(view);

  // change the border color just for fun
  //$(this).css('border-color', 'red');

}

});


})
</script>
