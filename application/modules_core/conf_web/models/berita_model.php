<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_model extends CI_Model {

	var $table = 't_berita';
	var $column = array('m_category.category_name','t_berita.jenis_berita','t_berita.judul_berita','t_berita.sub_judul','t_berita.isi_berita','t_berita.penulis','t_berita.tanggal_berita','t_berita.sumber','t_berita.main','t_berita.publish','t_berita.active');
	var $select = 't_berita.*,m_category.category_name';

	var $order = array('t_berita.id_berita' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	public function get_data($params)
	{
		//print_r($params);die;
		$this->db->from('t_berita');
		$this->db->join('m_category', 'm_category.category_id=t_berita.category_id','left');
		if (!empty($params['search_by'] and $params['keyword'])){
			$this->db->where(''.$params['search_by'].' like', '%'.$params['keyword'].'%');	
		} 
		$this->db->limit($params['limit'],$params['start']);
		$this->db->order_by('id_berita '.$params['sort']);

		$data = $this->db->get()->result();
		//print_r($this->db->last_query());die;
		foreach($data as $row){
			$getdata = array(
				'id' => $row->id_berita,
				'judul_berita' => $row->judul_berita,
				'penulis' => $row->penulis,
				'sumber' => $row->sumber,
				'tanggal_berita' => $this->tanggal->formatDate($row->tanggal_berita),
				'category_name' => $row->category_name,
				'jenis_berita' => $row->jenis_berita,
				'main' => $row->main,
				'publish' => $row->publish,
				'active' => $row->active,
				'myid' => $row->id_berita,

				);
			$arr_data[] = $getdata;
		}

		return $arr_data;

	}

	public function total_data($params)
	{
		$this->db->select("count(*) as total");
		$this->db->from('t_berita');
		$this->db->join('m_category', 'm_category.category_id=t_berita.category_id','left');
		if (!empty($params['search_by'] and $params['keyword'])){
			$this->db->where(''.$params['search_by'].' like', '%'.$params['keyword'].'%');	
		} 
		$total = $this->db->get()->row();
		return $total->total;
	} 

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_category', 'm_category.category_id=t_berita.category_id','left');
		$this->db->where(''.$this->table.'.id_berita',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.id_berita', $id);
		$this->db->delete($this->table);
	}
	

}
