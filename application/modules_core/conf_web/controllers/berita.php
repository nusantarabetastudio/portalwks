<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('tanggal');
		$this->load->library('regex');
		$this->load->library('authuser');
		$this->load->library('encryption');
		$this->load->library('master');
		$this->load->library('authuser');
		$this->load->library('youtube');
		$this->load->library('apps');

		$this->load->model('Berita_model','berita');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Daftar Berita";
		$data['subtitle'] = "Pengaturan Berita";
		$this->load->view('berita/index', $data);
	}

	public function get_data(){

		$limit = $this->input->post('rows')?$this->input->post('rows'):10;

		$params = array(
			'search_by' => $this->input->post('search_by'),
			'keyword' => $this->input->post('keyword')
		);
		$total = $this->berita->total_data($params);
		$total_pages = ($total >0)?ceil($total/$limit):1;
		$page = $this->input->post('page')?$this->input->post('page'):1;
		
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit;

		$params['start'] = $start;
		$params['limit'] = $limit;
		$params['sort'] = $this->input->post('sord');

		$rowData = $this->berita->get_data($params);

		$data = array();
		$data['totalPages'] = $total_pages;
		$data['page'] = $page;
		$data['records'] = $total;
		$data['rows'] = $rowData;

		echo json_encode($data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Berita";
		$data['subtitle'] = "Pengaturan konten berita";
		if( $id != '' ){
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
			$data['value'] = $this->berita->get_by_id($id);
		}else{
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
		}
		$this->load->view('berita/form', $data);
	}

	public function detail($id)
	{

		$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
		$data['value'] = $this->berita->get_by_id($id);
		$this->load->view('berita/detail', $data);

	}

	public function ajax_add()
	{
		//print_r($_POST);die;
		$id_berita = $this->regex->_genRegex($this->input->post('id'), 'RGXINT');
		

		$this->db->trans_begin();
		$dataexc = array(
			'judul_berita' => $this->regex->_genRegex($this->input->post('judul_berita'), 'RGXQSL'),
			'sub_judul' => $this->regex->_genRegex($this->input->post('sub_judul'), 'RGXQSL'),
			'isi_berita' => $this->regex->_genRegex($this->input->post('isi_berita'), 'RGXQSL'),
			'penulis' => $this->regex->_genRegex($this->input->post('penulis'), 'RGXQSL'),
			'tanggal_berita' => $this->regex->_genRegex($this->input->post('tanggal_berita'), 'RGXQSL'),
			'jenis_berita' => $this->regex->_genRegex($this->input->post('jenis_berita'), 'RGXQSL'),
			'link_video' => $this->regex->_genRegex($this->input->post('link_video'), 'RGXQSL'),
			'category_id' => $this->regex->_genRegex($this->input->post('category_id'), 'RGXINT'),
			'sumber' => $this->regex->_genRegex($this->input->post('sumber'), 'RGXQSL'),
			'main' => $this->regex->_genRegex($this->input->post('main'), 'RGXQSL'),
			'publish' => $this->regex->_genRegex($this->input->post('publish'), 'RGXQSL'),
			'running_text' => $this->regex->_genRegex($this->input->post('running_text'), 'RGXQSL'),
			'active' => $this->regex->_genRegex($this->input->post('active'), 'RGXAZ')
		);

		//upload lampiran
		$lokasi_file    = $_FILES['path_thumbnail']['tmp_name'];
	    $tipe_file      = $_FILES['path_thumbnail']['type'];
	    $nama_file      = $_FILES['path_thumbnail']['name'];
	    $acak           = rand(1,99);
	    $nama_file_unik = $acak.$nama_file;

		if (!empty($lokasi_file)) {

			if($id_berita != 0){
				$data_exist = $this->berita->get_by_id($id_berita);
				$url = 'uploaded_files/berita/'.$data_exist->path_thumbnail.'';
				if(isset($data_exist->path_thumbnail)) :
					if(file_exists($url)){
						unlink($url);
					}
				endif;
			}

			$config['upload_path'] = 'uploaded_files/berita';
			$config['file_name'] = $nama_file_unik;
			$config['allowed_types'] = '*';
			$config['max_size']     = '50000';
			$config['overwrite'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('path_thumbnail'))
			{
				$error = array('error' => $this->upload->display_errors(),'data' => $this->upload->data());
				//print_r($error);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				//print_r($data);
			}
					
			$dataexc['path_thumbnail'] = $data['upload_data']['file_name']; 	
		}
		//end upload lampiran
		
		if( $id_berita == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->fullname;
			$this->berita->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->fullname;
			$this->berita->update(array('id_berita'=>$id_berita), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function ajax_delete()
	{
		$id = $this->input->post('ID');
		$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		//print_r($auth);die;

		if(is_array($auth)){
			echo json_encode($auth);
		}else{
			$data = $this->berita->get_by_id($id);
			$path = 'uploaded_files/berita/'.$data->path_thumbnail.''; 
			if(isset($data->path_thumbnail)) :
				if(file_exists($path)){
					unlink($path);
				}
			endif;

			$this->berita->delete_by_id($id);
			echo json_encode(array("message" => 'ID ['.$id.'] berhasil dihapus!', "gritter" => 'gritter-success'));
		}
		
	}

	
}
