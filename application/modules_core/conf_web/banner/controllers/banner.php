<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('tanggal');
		$this->load->library('regex');
		$this->load->library('authuser');

		$this->load->model('Banner_model','banner');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Daftar Banner";
		$data['subtitle'] = "Pengaturan Banner";
		$this->load->view('index', $data);
	}

	public function get_data(){

		$limit = $this->input->post('rows')?$this->input->post('rows'):10;

		$params = array(
			'search_by' => $this->input->post('search_by'),
			'keyword' => $this->input->post('keyword')
		);
		$total = $this->banner->total_data($params);
		$total_pages = ($total >0)?ceil($total/$limit):1;
		$page = $this->input->post('page')?$this->input->post('page'):1;
		
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit;

		$params['start'] = $start;
		$params['limit'] = $limit;
		$params['sort'] = $this->input->post('sord');

		$rowData = $this->banner->get_data($params);

		$data = array();
		$data['totalPages'] = $total_pages;
		$data['page'] = $page;
		$data['records'] = $total;
		$data['rows'] = $rowData;

		echo json_encode($data);
	}

	public function form($id='')
	{
		
		$data['title'] = "Form Banner";
		$data['subtitle'] = "Pengaturan konten banner";
		if( $id != '' ){
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
			$data['value'] = $this->banner->get_by_id($id);
		}else{
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
		}
		$this->load->view('form', $data);
	}

	public function detail($id)
	{

		$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
		$data['value'] = $this->banner->get_by_id($id);
		$this->load->view('detail', $data);

	}

	public function ajax_add()
	{
		//print_r($_POST);die;
		$banner_id = $this->regex->_genRegex($this->input->post('id'), 'RGXINT');
		

		$this->db->trans_begin();
		$dataexc = array(
			'banner_name' => $this->regex->_genRegex($this->input->post('banner_name'), 'RGXQSL'),
			'author' => $this->regex->_genRegex($this->input->post('author'), 'RGXQSL'),
			'banner_link' => $this->regex->_genRegex($this->input->post('banner_link'), 'RGXQSL'),
			'banner_description' => $this->regex->_genRegex($this->input->post('banner_description'), 'RGXQSL'),
			'banner_date' => $this->regex->_genRegex($this->input->post('banner_date'), 'RGXQSL'),
			'tb_id' => $this->regex->_genRegex($this->input->post('tb_id'), 'RGXINT'),
			'active' => $this->regex->_genRegex($this->input->post('active'), 'RGXAZ')
		);

		//upload lampiran
		$lokasi_file    = $_FILES['path_thumbnail']['tmp_name'];
	    $tipe_file      = $_FILES['path_thumbnail']['type'];
	    $nama_file      = $_FILES['path_thumbnail']['name'];
	    $acak           = rand(1,99);
	    $nama_file_unik = $acak.$nama_file;

		if (!empty($lokasi_file)) {

			if($banner_id != 0){
				$data_exist = $this->banner->get_by_id($banner_id);
				$url = 'uploaded_files/banner/'.$data_exist->banner_image.'';
				if(file_exists($url)){
					unlink($url);
				}
			}

			$config['upload_path'] = 'uploaded_files/banner';
			$config['file_name'] = $nama_file_unik;
			$config['allowed_types'] = '*';
			$config['max_size']     = '50000';
			$config['overwrite'] = TRUE;
			$config['remove_spaces'] = TRUE;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload('path_thumbnail'))
			{
				$error = array('error' => $this->upload->display_errors(),'data' => $this->upload->data());
				//print_r($error);
			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				//print_r($data);
			}
					
			$dataexc['banner_image'] = $data['upload_data']['file_name']; 	
		}
		//end upload lampiran
		
		if( $banner_id == 0 ){
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->fullname;
			$this->banner->save($dataexc);
		}else{
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->fullname;
			$this->banner->update(array('banner_id'=>$banner_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			die('Error');
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("message" => 'Proses berhasil dilakukan!', "gritter" => 'gritter-success'));
		}
		
	}

	public function ajax_delete()
	{
		$id = $this->input->post('ID');
		$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		//print_r($auth);die;

		if(is_array($auth)){
			echo json_encode($auth);
		}else{
			$data = $this->banner->get_by_id($id);
			$path = 'uploaded_files/banner/'.$data->path_thumbnail.''; 
			unlink($path);
			$this->banner->delete_by_id($id);
			echo json_encode(array("message" => 'ID ['.$id.'] berhasil dihapus!', "gritter" => 'gritter-success'));
		}
		
	}

	
}
