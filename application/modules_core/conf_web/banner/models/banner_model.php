<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {

	var $table = 't_banner';
	var $column = array('banner_id','banner_type','banner_name','banner_link', 'author', 'banner_date','banner_description','m_tipe_banner.tb_name');
	var $select = 't_banner.*,m_tipe_banner.tb_name';
	var $order = array('t_banner.banner_id' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	public function get_data($params)
	{
		//print_r($params);die;
		$this->db->from('t_banner');
		$this->db->join('m_tipe_banner','m_tipe_banner.tb_id=t_banner.tb_id', 'left');
		if (!empty($params['search_by'] and $params['keyword'])){
			$this->db->where(''.$params['search_by'].' like', '%'.$params['keyword'].'%');	
		} 
		$this->db->limit($params['limit'],$params['start']);
		$this->db->order_by('banner_id '.$params['sort']);

		$data = $this->db->get()->result();
		foreach($data as $row){
			$getdata = array(
				'id' => $row->banner_id,
				'banner_type' => $row->tb_name,
				'banner_name' => $row->banner_name,
				'banner_link' => $row->banner_link,
				'banner_date' => $this->tanggal->formatDate($row->banner_date),
				'author' => $row->author,
				'banner_description' => $row->banner_description,
				'active' => $row->active,
				'myid' => $row->banner_id,

				);
			$arr_data[] = $getdata;
		}

		return $arr_data;

	}

	public function total_data($params)
	{
		$this->db->select("count(*) as total");
		$this->db->from('t_banner');
		if (!empty($params['search_by'] and $params['keyword'])){
			$this->db->where(''.$params['search_by'].' like', '%'.$params['keyword'].'%');	
		} 
		$total = $this->db->get()->row();
		return $total->total;
	} 

	public function get_by_id($id)
	{
		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_tipe_banner','m_tipe_banner.tb_id=t_banner.tb_id', 'left');
		$this->db->where(''.$this->table.'.banner_id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.banner_id', $id);
		$this->db->delete($this->table);
	}
	

}
