<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/ui.jqgrid.css" />
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<!-- modal -->
<div id="contentDetail" class="hide"></div>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
    <div class="clearfix" style="float:left">
      <button class="btn btn-sm btn-primary" id="btn_add_menu" ><i class="glyphicon glyphicon-plus"></i> Tambah Banner </button><div class="pull-right tableTools-container"></div>
    </div>
    <br>
    <hr class="separator">

   <form class="form-inline" id="form_search_index_banner">
    <label class="inline">
      Pencarian berdasarkan : 
      <select id="search_by_category_banner" class="input-small" style="width:300px">
        <option value="">-Silahkan pilih-</option>
        <option value="banner_name">Judul Banner</option>
        <option value="banner_type">Tipe</option>
        <option value="banner_link">Link</option>
        <option value="banner_description">Deskripsi</option>
      </select>
    </label>

    <label class="inline">
      &nbsp;&nbsp; Kata Kunci : 
      <input type="text" id="keyword_banner" style="width: 200px" class="input-small" placeholder="Kata Kunci" />
    </label>

    <button type="button" id="btn_search_banner" class="btn btn-default btn-sm">
      <i class="ace-icon fa fa-search bigger-110"></i>Cari
    </button> 
    <button type="button" id="btn_reset_banner" class="btn btn-danger btn-sm">
      <i class="ace-icon fa fa-refresh bigger-110"></i>Reset
    </button> 

    <br><br>

    <table id="grid_table_banner"></table>
    <div id="grid_pager_banner"></div>

    </form>
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url()?>assets/js/custom/banner.js"></script>