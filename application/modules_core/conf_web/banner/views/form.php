<title><?php echo $title?></title>
<!-- ajax layout which only needs content area -->
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<style type="text/css">
label.error { color:red; }
.none { display:none; }
</style>

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
          <div class="widget-body">
            <div class="widget-main no-padding">
              <form class="form-horizontal" method="post" id="form_banner" action="<?php echo site_url('banner/ajax_add')?>">
                <br>

                <div class="form-group">
                  <label class="control-label col-md-2">ID</label>
                  <div class="col-md-1">
                    <input name="id" id="id" value="<?php echo isset($value)?$value->banner_id:0?>" placeholder="Auto" class="form-control" type="text" readonly>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Nama Banner</label>
                  <div class="col-md-6">
                    <input name="banner_name" id="banner_name" value="<?php echo isset($value)?$value->banner_name:''?>"  class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Author</label>
                  <div class="col-md-3">
                    <input name="author" id="author" value="<?php echo isset($value)?$value->author:''?>"  class="form-control" type="text">
                  </div>
                  <label class="control-label col-md-2">Link Banner</label>
                  <div class="col-md-4">
                    <input name="banner_link" id="banner_link" value="<?php echo isset($value)?$value->banner_link:''?>"  class="form-control" type="text">
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Tanggal Banner</label>
                  <div class="col-md-3">
                    <input name="banner_date" id="banner_date" value="<?php echo isset($value)?$value->banner_date:''?>"  class="form-control date-picker" data-date-format="yyyy-mm-dd" type="text">
                  </div>
                  <label class="control-label col-md-2">Tipe banner</label>
                  <div class="col-md-3">
                    <?php 
                      echo $this->master->get_master_custom(array('table'=>'m_tipe_banner','where'=>array(),'name'=>'tb_name','id'=>'tb_id'),isset($value)?$value->tb_id:'','tb_id','tb_id','select2','','inline');
                    ?>
                  </div>
                </div>
                
                

                <div id="fotoDiv">
                <div class="form-group">
                  <label class="control-label col-md-2">Upload Gambar</label>
                  <div class="col-md-4">
                    <input name="path_thumbnail" id="path_thumbnail"  type="file">
                  </div>
                </div>
                <?php if(isset($value->banner_image)){?>
                <div class="form-group">
                  <label class="control-label col-md-2">Images</label>
                  <div class="col-md-4">
                    <img src="<?php echo base_url().'uploaded_files/banner/'.$value->banner_image.''?>" width="250px">
                  </div>
                </div>
                <?php }?>

                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Status Aktif ?</label>
                  <div class="col-md-4">
                    <div class="radio">
                          <label>
                            <input name="active" type="radio" class="ace" value="Y" <?php echo isset($value) ? ($value->active == 'Y') ? 'checked="checked"' : '' : 'checked="checked"'; ?>  />
                            <span class="lbl"> Ya</span>
                          </label>
                          <label>
                            <input name="active" type="radio" class="ace" value="N" <?php echo isset($value) ? ($value->active == 'N') ? 'checked="checked"' : '' : ''; ?>/>
                            <span class="lbl">Tidak</span>
                          </label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-2">Deskripsi singkat</label>
                  <div class="col-md-2">
                    <textarea cols="80" id="banner_description" class="ckeditor" name="banner_description" rows="5"><?php echo isset($value)?$value->banner_description:''?></textarea>
                  </div>
                </div>

                <div class="form-actions center">

                  <!--hidden field-->
                  <!-- <input type="text" name="id" value="<?php echo isset($value)?$value->banner_id:0?>"> -->

                  <a onclick="backlist()" href="#" class="btn btn-sm btn-success">
                    <i class="ace-icon fa fa-arrow-left icon-on-right bigger-110"></i>
                    Kembali ke daftar
                  </a>
                  <a id="btn_add_menu" <?php echo isset( $value ) ? '' : 'style="display:none"' ;?> href="#" class="btn btn-sm btn-primary">
                    <i class="ace-icon fa fa-plus icon-on-right bigger-110"></i>
                    Tambah baru
                  </a>
                  <button type="reset" id="btnReset" class="btn btn-sm btn-danger">
                    <i class="ace-icon fa fa-close icon-on-right bigger-110"></i>
                    Reset
                  </button>
                  <button type="submit" id="btnSave" name="submit" class="btn btn-sm btn-info">
                    <i class="ace-icon fa fa-check-square-o icon-on-right bigger-110"></i>
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
    
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url().'assets/js/custom/banner.js'?>"></script>
<script src="<?php echo base_url()?>assets/ckeditor/ckeditor.js"></script>
<script src="<?php echo base_url()?>assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">

$( document ).ready( function() {
  $( 'textarea#banner_description' ).ckeditor();
} );

$('.date-picker').datepicker({
        dateFormat:'yy-mm-dd',
        autoclose: true,
        todayHighlight: true,
        changeMonth: true,
        changeYear: true
      })
      //show datepicker when clicking on the icon
      .next().on(ace.click_event, function(){
        $(this).prev().focus();
      });
</script>
