<div class="profile-user-info profile-user-info-striped">
  
  <div class="profile-info-row">
    <div class="profile-info-name"> Judul Banner </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->banner_name ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Tipe Banner </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->tb_name ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Author </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->author ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Tanggal Banner </div>
    <div class="profile-info-value">
      <span class="editable"><?= $this->tanggal->tgl_indo($value->banner_date) ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Link Banner </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->banner_link ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Deskripsi singkat </div>
    <div class="profile-info-value">
      <span class="editable"><p align="justify"><?= $value->banner_description ?></p></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Gambar </div>
    <div class="profile-info-value">
      <span class="editable"> 
        <?php if(isset($value->banner_image)){?><img src="<?php echo base_url().'uploaded_files/banner/'.$value->banner_image.'' ?>" width="50%"><?php }else{echo 'Tidak ada gambar';};?>
      </span>
    </div>
  </div>

  <div class="profile-info-row">
    <div class="profile-info-name"> Status Data </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo ($value->active == 'Y') ? '<span class="label label-xs label-success">Active</span>' : '<span class="label label-xslabel-danger">Not active</span>' ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Created Date </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $this->tanggal->formatDateTime($value->created_date) ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Created by </div>
    <div class="profile-info-value">
      <span class="editable"> <?= $value->created_by ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Updated Date </div>
    <div class="profile-info-value">
      <span class="editable"> <?php echo $value->updated_date?$this->tanggal->formatDateTime($value->updated_date):'-' ?></span>
    </div>
  </div>
  <div class="profile-info-row">
    <div class="profile-info-name"> Updated by </div>
    <div class="profile-info-value">
      <span class="editable"><?php echo $value->updated_by?$value->updated_by:'-' ?></span>
    </div>
  </div>


  </div>