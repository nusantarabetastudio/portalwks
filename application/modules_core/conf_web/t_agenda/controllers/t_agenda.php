<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class T_agenda extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('t_agenda_model','t_agenda');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Pengaturan Kegiatan";
		$data['subtitle'] = "Daftar Kegiatan";
		$data['agenda'] = $this->t_agenda->get_all_agenda('next_date'); // last_date, all_date

		$this->load->view('index', $data);
	}

	public function form($id='')
	{
		// Authentication //
		$data['title'] = "Form Kegiatan";
		$data['subtitle'] = "";
		if( $id != '' ){

			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'R');
			$data['value'] = $this->t_agenda->get_by_id($id);
		}else{
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
		}

		$this->load->view('form', $data);

	}

	public function view_table($wu_id='')
	{
		// Authentication //
		$data['title'] = "Kegiatan";
		$data['subtitle'] = 'Daftar Kegiatan';
		$data['wu_id'] = ($wu_id!='')?$wu_id:0;
		
		$this->load->view('index_table', $data);

	}

	public function ajax_list($wu_id)
	{
		$list = $this->t_agenda->get_datatables($wu_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $t_agenda) {
			$no++;
			$row = array();
			$row[] = '<label class="pos-rel">
						<input type="checkbox" class="ace" />
						<span class="lbl"></span>
					</label>';
			$row[] = strtoupper($t_agenda->agenda_name);
			$row[] = $t_agenda->agenda_place;
			$row[] = ''.$this->tanggal->formatDate($t_agenda->agenda_start_date).' s/d '.$this->tanggal->formatDate($t_agenda->agenda_end_date).'';
			$row[] = $t_agenda->wu_name;
			$row[] = ($t_agenda->active == 'Y') ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Not active</span>';
			$row[] = $t_agenda->updated_date?$this->tanggal->formatDateTime($t_agenda->updated_date):$this->tanggal->formatDateTime($t_agenda->created_date);

			//add html for action
			$row[] = '<a class="btn btn-xs btn-success" href="javascript:void()" title="Edit" onclick="edit('."'".Regex::_genRegex($t_agenda->agenda_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void()" title="Delete" onclick="delete_t_agenda('."'".Regex::_genRegex($t_agenda->agenda_id,'RGXINT')."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->t_agenda->count_all($wu_id),
						"recordsFiltered" => $this->t_agenda->count_filtered($wu_id),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_add()
	{
		//print_r($_POST);die;
		$agenda_id = Regex::_genRegex($this->input->post('id'), 'RGXINT');

		$this->db->trans_begin();
		$dataexc = array(
			'agenda_name' => Regex::_genRegex($this->input->post('agenda_name'), 'RGXQSL'),
			'agenda_place' => Regex::_genRegex($this->input->post('agenda_place'), 'RGXQSL'),
			'agenda_start_date' => Regex::_genRegex($this->tanggal->sqlDateTime($this->input->post('agenda_start_date')), 'RGXQSL'),
			'agenda_end_date' => Regex::_genRegex($this->tanggal->sqlDateTime($this->input->post('agenda_end_date')), 'RGXQSL'),
			'agenda_description' => Regex::_genRegex($this->input->post('agenda_description'), 'RGXQSL'),
			'agenda_author' => Regex::_genRegex($this->input->post('agenda_author'), 'RGXINT'),
			'active' => Regex::_genRegex($this->input->post('active'), 'RGXAZ')
		);
		
		//print_r($dataexc);die;

		// participant //
		$wu_id = $this->input->post('wu_id');
		if(is_array($wu_id)){
			$imp_wu_id = implode('#', $wu_id);
			$dataexc['agenda_participant'] = '#'.$imp_wu_id;
		}

		if( $agenda_id == 0 ){
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'C');
			$dataexc['created_date'] = date('Y-m-d H:i:s');
			$dataexc['created_by'] = $this->session->userdata('data_user')->id_user;
			$this->t_agenda->save($dataexc);
		}else{
			$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'U');
			$dataexc['updated_date'] = date('Y-m-d H:i:s');
			$dataexc['updated_by'] = $this->session->userdata('data_user')->id_user;
			$this->t_agenda->update(array('agenda_id'=>$agenda_id), $dataexc);
		}


		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			echo json_encode(array("status" => FALSE));
		}
		else
		{
			$this->db->trans_commit();
			echo json_encode(array("status" => TRUE));
		}
		
	}

	public function ajax_delete($id)
	{

		$auth = $this->authuser->get_auth_action_user(strtolower(get_class($this)), 'D');
		$this->t_agenda->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

	public function get_agenda_json()
		{
			$agenda = $this->t_agenda->get_agenda_json();

			echo json_encode($agenda);
		}

	
}
