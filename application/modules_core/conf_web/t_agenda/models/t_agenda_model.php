<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class T_agenda_model extends CI_Model {

	var $table = 't_agenda';
	var $column = array('t_agenda.agenda_name','t_agenda.agenda_place','t_agenda.agenda_author','t_agenda.agenda_description','t_agenda.agenda_start_date','t_agenda.agenda_end_date','m_work_unit.wu_name','t_agenda.active','t_agenda.created_date','t_agenda.updated_date');
	var $select = 't_agenda.*, m_work_unit.*';

	var $order = array('t_agenda.agenda_id' => 'desc');

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _query(){

		$this->db->select($this->select);
		$this->db->from($this->table);
		$this->db->join('m_work_unit', 'm_work_unit.wu_id='.$this->table.'.agenda_author','left');

	}

	private function _get_datatables_query($wu_id)
	{
		
		$this->_query();

		if($wu_id != 0){
			$this->db->where($this->table.'.agenda_author', $wu_id);
		}

		$i = 0;
	
		foreach ($this->column as $item) 
		{
			if($_POST['search']['value'])
				($i===0) ? $this->db->like($item, $_POST['search']['value']) : $this->db->or_like($item, $_POST['search']['value']);
			$column[$i] = $item;
			$i++;
		}
		
		if(isset($_POST['order']))
		{
			$this->db->order_by($column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function get_datatables($wu_id)
	{
		$this->_get_datatables_query($wu_id);
		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered($wu_id)
	{
		$this->_get_datatables_query($wu_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all($wu_id)
	{
		$this->db->from($this->table);
		if($wu_id != 0){
			$this->db->where($this->table.'.agenda_author', $wu_id);
		}
		return $this->db->count_all_results();
	}

	public function get_by_id($id)
	{
		$this->_query();
		$this->db->where(''.$this->table.'.agenda_id',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete_by_id($id)
	{
		$this->db->where(''.$this->table.'.agenda_id', $id);
		$this->db->delete($this->table);
	}

	public function get_all_agenda($type)
	{
		$params = new stdClass();

		switch ($type) {
			case 'next_date':
				# code...
				$params->where = $this->db->where('agenda_start_date >=', date('Y-m-d H:i:s'));
				break;

			case 'last_date':
				# code...
				$params->where = $this->db->where('agenda_start_date <=', date('Y-m-d H:i:s'));
				break;
			
			default:
				# code...
				$params->where = $this->db->where('active', 'Y');
				break;
		}

		$this->_query();
		$params->where;
		return $this->db->get()->result();
	}

	public function get_agenda_json()
	{
		$getData = array();
		$this->_query();
		$agenda = $this->db->get()->result_array();

		foreach ($agenda as $key => $value) {
			$tag_color = ($value['wu_tag_color'] != NULL) ? $value['wu_tag_color'] : '#FFFFF' ;
			$color_indicator = ($value['wu_color_indicator'] != NULL) ? $value['wu_color_indicator'] : '' ;
			$txt_color = ($value['wu_tag_color'] != NULL) ? 'white' : 'black' ;
			$data = array(
				'title' => $value['agenda_name'],
				'place' => $value['agenda_place'],
				'author' => $value['wu_name'],
				'description' => $value['agenda_description'],
				'start' => $value['agenda_start_date'],
				'end' => $value['agenda_end_date'],
				'className' => $color_indicator,
				'color' => $tag_color,   // a non-ajax option
  				'textColor' => $txt_color
				);
			$getData[] = $data;
		}

		return $getData;
	}


}
