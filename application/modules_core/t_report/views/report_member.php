<?php
  //print_r($posted);die;
    $filename = "EXPORT_EXCEL_".$member->member_no.'_'.Date("ymd").".xls";
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$filename");
    header('Cache-Control: public');
?>

<center>
<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
    <?php if($excel != TRUE) :?>
      <a href="<?php echo base_url().'t_report/preview_member/'.$params['member_id'].'/'.$params['format'].'/'.$params['periode'].'/'.$params['report'].'/'.$params['type'].'/excel'?>" target="blank" id="btn_excel" class="btn btn-inverse btn-sm">
          <i class="fa fa-file-excel-o"></i> Excel
        </a>
    <?php endif; ?>
    <p>
      PRIM KOPTI JAKARTA SELATAN<br>
      UNIT <?php echo strtoupper($member->unit_name)?> <br>
      <?php echo '[ '.strtoupper($member->member_no).' ] ' .strtoupper($member->ktp_nama_lengkap) ?>
    </p>

      <center>
          <br>
          <h2>Laporan Simpanan Anggota</h2>
          <b>Periode <?php echo $periode?></b>
          <br>
          <br>
          <table class="table table-bordered table-hover" border="1" style="width:80%">
            <thead>
              <tr>
                <th class="center">No</th>
                <th class="center">Keterangan</th>
                <th class="center">Tanggal</th>
                <th class="center">Saldo Awal</th>
                <th class="center">Debet</th>
                <th class="center">Kredit</th>
                <th class="center">Saldo Akhir</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; if($result){foreach($result as $rows){?>
                <tr>
                  <td align="center" width="30px"> <?php echo $no;?> </td>
                  <td width="200px"> <?php echo $rows->st_name;?> </td>
                  <td align="center" width="110px"> <?php echo Tanggal::formatDateForm($rows->tr_date);?> </td>
                  <td align="right" width="170px"> <?php echo number_format($rows->tr_first_saldo,2);?> </td>
                  <td align="right" width="170px"> <?php echo number_format($rows->tr_debit,2);?> </td>
                  <td align="right" width="170px"> <?php echo number_format($rows->tr_credit,2);?> </td>
                  <td align="right" width="170px"> <?php echo number_format($rows->tr_last_saldo,2);?> </td>
                </tr>
              <?php 
                $last_saldo = number_format($rows->tr_last_saldo,2);
              $no++; } }else{ echo '<b>-Tidak ada data ditemukan-</b>';}?>
              <tr>
                <td align="center" colspan="6"> <b>SALDO AKHIR SAMPAI DENGAN PERIODE INI</b></td>
                <td align="right"> <b><?php echo isset($last_saldo)?$last_saldo:0;?></b> </td>
              </tr>

            </tbody>
          </table>
          <br>
      </center>
   
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
</center>
