<!-- page specific plugin styles -->
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/colorbox.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/datepicker.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/daterangepicker.css" />
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/ui.jqgrid.css" />
<div class="page-header">
  <h1>
    <?php echo $title?>
    <small>
      <i class="ace-icon fa fa-angle-double-right"></i>
      <?php echo $subtitle?>
    </small>
  </h1>
</div><!-- /.page-header -->

<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->

    <form class="form-horizontal" method="post" id="form_report_search" action="#">

      <div class="form-group">
        <label class="control-label col-md-2">Jenis Laporan</label>
        <div class="col-md-2">
          <select class="form-control" name="report_type" id="report_type">
            <option value="0">-silahkan pilih-</option>
            <option value="Simpanan">Simpanan</option>
            <option value="Distribusi">Distribusi</option>
          </select>
        </div>
        <label class="control-label col-md-1" style="margin-left:-100px">&nbsp;</label>
        <div class="col-md-4">
          <div class="radio">
                <label>
                  <input name="report_for" type="radio" class="ace" value="Anggota" />
                  <span class="lbl"> Anggota</span>
                </label>
                <label>
                  <input name="report_for" type="radio" class="ace" value="Unit" />
                  <span class="lbl"> Unit</span>
                </label>
                <label>
                  <input name="report_for" type="radio" class="ace" value="Koperasi" />
                  <span class="lbl"> Koperasi</span>
                </label>
          </div>
        </div>
      </div>

      <!--- DIV ANGGOTA -->
      <div id="div_anggota" style="display: none;">
        
        <h3 class="header smaller lighter blue"><div id="member_name">Pencarian Data Anggota</div></h3>
        <div class="form-group">
          <label class="col-sm-2 control-label">Kata Kunci<br><small>Masukan Nama/No.Anggota</small></label>
          <div class="col-sm-6 input-group" style="margin-top: 10px">
             <input type="text" class="form-control" name="keyword" id="keyword_form_member"/>                            
            <span class="input-group-btn" style="margin-top:-10px">
              <button class="btn btn-xs btn-primary" type="button" id="search_member" style="height:33px"> <i class="fa fa-search bigger-100"></i> Pencarian </button>
            </span>
          </div>
        </div>

        <div class="form-group" id="form_result_hidden">
          <label class="col-sm-2 control-label">&nbsp;</label>
          <div class="col-sm-8">
             <div id="result"></div>
          </div>
        </div>

        <div id="newDiv" style="display: none;">
          <div class="form-group">
            <label class="col-sm-2 control-label">NIA *</label>
            <div class="col-sm-1">
               <input type="text" id="member_no" class="form-control" name="member_no" value="" readonly>    
               <input type="hidden" id="member_id" class="form-control" name="member_id" value="" readonly>    
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Nama *</label>
            <div class="col-sm-2">
               <input type="text" id="ktp_nama_lengkap" class="form-control" name="ktp_nama_lengkap" value="" readonly>    
            </div>
            <label class="col-sm-1 control-label">Unit</label>
            <div class="col-sm-3">
              <?php 
                echo Master::get_master_custom(
                    array('table'=>'m_unit','where'=>array('active'=>'Y'), 'name'=>'unit_name', 'id'=>'unit_id'),'','unit_id','unit_id','form-control','','inline');?> 

            </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-2">Format Laporan</label>
            <div class="col-md-2">
              <select id="report_format" name="report_format" class="form-control" required>
                <option value="0">-Silahkan Pilih-</option>
                <option value="day">Harian</option>
                <option value="month">Bulanan</option>
                <option value="custom">Periode</option>
              </select>
            </div>

            <div id="div_day" style="display:none">
              <label class="control-label col-md-1">Tanggal</label>
                <div class="col-md-2">
                  <div class="input-group">
                    <input class="form-control date-picker" id="report_date" name="report_date" type="text" data-date-format="dd-mm-yyyy" value="" />
                    <span class="input-group-addon">
                      <i class="fa fa-calendar bigger-110"></i>
                    </span>
                  </div>
                </div>
            </div>

            <div id="div_month" style="display:none">
              <label class="control-label col-md-1">Bulan</label>
              <div class="col-md-2">
                <select id="report_month" name="month" class="form-control" required>
                  <option value="0">-Silahkan Pilih-</option>
                  <?php for($i=1; $i<13; $i++){?>
                    <option value="<?php echo $i?>"><?php echo Tanggal::getBulan($i)?></option>
                  <?php }?>
                </select>
              </div>
            </div>

            <div id="div_custom" style="display:none">
              <label class="control-label col-md-1">Periode</label>
                <div class="col-md-3">
                  <div class="row">
                    <div class="col-xs-8 col-sm-11">
                      <!-- #section:plugins/date-time.daterangepicker -->
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="fa fa-calendar bigger-110"></i>
                        </span>

                        <input class="daterangepickerform form-control" type="text" name="periode_member" value="" id="id-date-range-picker-1" />
                      </div>

                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!--- END DIV ANGGOTA -->

      <!--- DIV UNIT -->
      <div id="div_unit" style="display: none;">
        
        <h3 class="header smaller lighter blue">Laporan Berdasarkan Unit</h3>
        <div class="form-group">
          <label class="col-sm-2 control-label">Unit</label>
          <div class="col-sm-3">
             <?php 
                echo Master::get_master_custom(
                    array('table'=>'m_unit','where'=>array('active'=>'Y'), 'name'=>'unit_name', 'id'=>'unit_id'),'','unit_id_unit','unit_id_unit','form-control','','inline');?>   
          </div>
          <label class="control-label col-md-1">Periode</label>
            <div class="col-md-3">
              <div class="row">
                <div class="col-xs-8 col-sm-11">
                  <!-- #section:plugins/date-time.daterangepicker -->
                  <div class="input-group">
                    <span class="input-group-addon">
                      <i class="fa fa-calendar bigger-110"></i>
                    </span>

                    <input class="daterangepickerform form-control" type="text" name="periode_unit" value="" id="id-date-range-picker-1" />
                  </div>

                </div>
              </div>
            </div>
        </div>

      </div>
      <!--- END DIV UNIT -->

      <!--- DIV UNIT -->
      <div id="div_koperasi" style="display: none;">
        
        <h3 class="header smaller lighter blue">Laporan Berdasarkan Koperasi</h3>
        <div class="form-group">
          <label class="control-label col-md-2">Periode</label>
                <div class="col-md-3">
                  <div class="row">
                    <div class="col-xs-8 col-sm-11">
                      <!-- #section:plugins/date-time.daterangepicker -->
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="fa fa-calendar bigger-110"></i>
                        </span>

                        <input class="daterangepickerform form-control" type="text" name="periode_koperasi" value="" id="id-date-range-picker-1" />
                      </div>

                    </div>
                  </div>
                </div>
                
        </div>

      </div>
      <!--- END DIV UNIT -->

      


      <div class="form-actions center">
        
        <button type="button" id="btn_search_report" class="btn btn-default btn-sm">
          <i class="ace-icon fa fa-search bigger-110"></i>Cari
        </button> 
        <button type="button" id="btn_reset_report" class="btn btn-danger btn-sm">
          <i class="ace-icon fa fa-refresh bigger-110"></i>Reset
        </button>
      </div>
    </form>

   
    <hr class="sparator">

    <div id="report_content"></div>

    </form>
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->

<script src="<?php echo base_url()?>assets/js/custom/t_report.js"></script>


<script src="<?php echo base_url()?>assets/js/jquery.colorbox.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/moment.js"></script>
<script src="<?php echo base_url()?>assets/js/date-time/daterangepicker.js"></script>
<script src="<?php echo base_url()?>assets/js/jquery.maskedinput.js"></script>
<script type="text/javascript">

jQuery(function($) {

  var $overflow = '';
  var colorbox_params = {
    rel: 'colorbox',
    reposition:true,
    scalePhotos:true,
    scrolling:false,
    previous:'<i class="ace-icon fa fa-arrow-left"></i>',
    next:'<i class="ace-icon fa fa-arrow-right"></i>',
    close:'&times;',
    current:'{current} of {total}',
    maxWidth:'100%',
    maxHeight:'100%',
    onOpen:function(){
      $overflow = document.body.style.overflow;
      document.body.style.overflow = 'hidden';
    },
    onClosed:function(){
      document.body.style.overflow = $overflow;
    },
    onComplete:function(){
      $.colorbox.resize();
    }
  };

  $('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
  $("#cboxLoadingGraphic").html("<i class='ace-icon fa fa-spinner orange fa-spin'></i>");//let's add a custom loading icon
  
  
  $(document).one('ajaxloadstart.page', function(e) {
    $('#colorbox, #cboxOverlay').remove();
   });

  //datepicker plugin
      //link
      $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
      })
      //show datepicker when clicking on the icon
      .next().on(ace.click_event, function(){
        $(this).prev().focus();
      });

  $.mask.definitions['~']='[+-]';
  $('.input-mask-date').mask('99/99/9999');

  //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
  $('.daterangepickerform').daterangepicker({
    'applyClass' : 'btn-sm btn-success',
    'cancelClass' : 'btn-sm btn-default',
    locale: {
      applyLabel: 'Apply',
      cancelLabel: 'Cancel',
    }
  })
  .prev().on(ace.click_event, function(){
    $(this).next().focus();
  });


});
</script>