<?php
  //print_r($posted);die;
    $filename = "EXPORT_EXCEL_PER_KOPERASI".Date("ymd").".xls";
    header("Content-type: application/vnd.ms-excel");
    header("Content-Disposition: attachment; filename=$filename");
    header('Cache-Control: public');
?>
<center>
<div class="row">
  <div class="col-xs-12">
    <!-- PAGE CONTENT BEGINS -->
      <?php if($excel != TRUE) :?>
    <a href="<?php echo base_url().'t_report/preview_koperasi/'.$params['periode'].'/'.$params['report'].'/'.$params['type'].'/excel'?>" target="blank" id="btn_excel" class="btn btn-inverse btn-sm">
          <i class="fa fa-file-excel-o"></i> Excel
        </a>
    <?php endif; ?>
    <p>
      PRIM KOPTI JAKARTA SELATAN<br>
    </p>

      <center>
          <br>
          <br>
          <h2>Rekap Simpanan Koperasi</h2>
          <b>Periode <?php echo $periode?></b>
          <br>
          <br>
          <table class="table table-bordered table-hover" border="1" style="width:100%">
            <thead>
              <tr>
                <th rowspan="2" class="center" width="30px">No</th>
                <th rowspan="2" class="center">NIA</th>
                <th rowspan="2" class="center" width="200px">NAMA</th>
                <th colspan="9" class="center">SIMPANAN</th>
                <th rowspan="2" class="center">TOTAL</th>
                <th rowspan="2" class="center">OMSET</th>
              </tr>
              <tr>
                <td width="100px" align="center">Pokok</td>
                <td width="100px" align="center">Wajib</td>
                <td width="100px" align="center">Tgl.12</td>
                <td width="100px" align="center">DMP</td>
                <td width="100px" align="center">Perumahan</td>
                <td width="100px" align="center">D.Penebusan</td>
                <td width="100px" align="center">Sukarela</td>
                <td width="100px" align="center">D.Pembangunan</td>
                <td width="100px" align="center">T.Anggota</td>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; if($result){
                foreach($result as $rows){
              ?>
                <tr>
                  <td align="center" width="30px"> <?php echo $no;?> </td>
                  <td> <?php echo $rows->member_no;?> </td>
                  <td> <?php echo $rows->ktp_nama_lengkap;?> </td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['pokok'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['wajib'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['tgl_12'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['dmp'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['perumahan'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['penebusan'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['sukarela'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['pembangunan'],2)?></td>
                    <td width="100px" align="right"><?php echo number_format($rows->hasil_rekap['titipan'],2)?></td>
                  <td align="right"><?php echo number_format(array_sum($rows->hasil_rekap), 2)?></td>
                  <td></td>
                </tr>
              <?php 
              $no++; } }else{ echo '<b>-Tidak ada data ditemukan-</b>';}?>
              <tr>
                <td align="center" colspan="3"> <b>SUB TOTAL</b></td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
                <td align="right"> <b>0</b> </td>
              </tr>

            </tbody>
          </table>
          <br>
      </center>
   
    <!-- PAGE CONTENT ENDS -->
  </div><!-- /.col -->
</div><!-- /.row -->
</center>
