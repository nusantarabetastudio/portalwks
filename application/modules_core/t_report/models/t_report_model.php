<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class T_report_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$this->load->database('default', TRUE);
	}

	public function get_data_report($params)
	{
		$data = array();
		// get pkt
		$pkt = $this->db->join('m_work_unit','m_work_unit.wu_id=t_pkt.wu_id','left')->get_where('t_pkt', array('year'=>$params['year'],'t_pkt.wu_id'=>$params['work_unit']));

		if($pkt->num_rows() > 0){

			$data_pkt = $pkt->row();
			//pkt plan action
			$ppa = $this->db->get_where('t_pkt_plan_action', array('pkt_id'=>$data_pkt->pkt_id, 'ppa_month'=>$params['month']));

			$data['pkt'] = $data_pkt;
			$data['ppa'] = $ppa;

			return $data;

		}else{
			return false;
		}


	}

	function findDetilFromId($nia)
  {
    $query = 'SELECT * FROM v_member WHERE member_id='.$nia.'';

    $data = $this->db->query($query)->row(); //print_r($nik);die;
    return $data;
  }

	function findData($key)
	  {
	    $data = array();
	    $query = "SELECT * FROM v_member WHERE ktp_nama_lengkap LIKE '%".$key."%' OR member_no LIKE '%".$key."%'";
	    $nik = $this->db->query($query);
	    //if($nik->num_rows() > 0){
	    $data['count'] = $nik->num_rows();
	    $data['result'] = $nik->result();
	    return $data;
	    //}
	    
	      
	  }

	  function getDataFromNik($count, $data){

    	$tpl = '<i><b>'.$count.' data ditemukan dari hasil pencarian</b></i>
            <table id="" class="table table-condensed dataTable no-footer">
              <thead  style="background-color:grey;color:white">
                <tr>
                  <th width="20px" data-hide="phone,tablet"><b>NO<b></th>
                  <th width="200px" data-hide="phone,tablet"><b>NIA<b></th>
                  <th width="200px" data-hide="phone,tablet"><b>NAMA LENGKAP</b></th>
                  <th width="200px" data-hide="phone,tablet"><b>UNIT</b></th>
                  <th width="70px">DETIL</th>
                </tr>
              </thead>
              <tbody>';
              $no = 1;
              foreach($data as $row){
                $tpl .= '
                  <tr>
                    <td><b>'.$no.'</b></td>
                    <td>'.$row->member_no.'</td>
                    <td>'.$row->ktp_nama_lengkap.'</td>
                    <td>'.$row->unit_name.'</td>
                    <th width="70px"><a onclick="javascript:showDetil('.$row->member_id.')" class="label label-success"><i class="fa fa-arrow-circle-down"></i></a></th>
                  </tr>
                ';
                $no++;
              }
    $tpl .= '</tbody>
          </table>
          <hr>';

    return $tpl;

  }

	
	
}
