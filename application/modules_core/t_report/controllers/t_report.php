<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class T_report extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('T_report_model','report');
		if($this->session->userdata('login')==false){
			redirect(base_url().'login');
		}

	}

	public function index()
	{
		
		$data['title'] = "Laporan";
		$data['subtitle'] = "Laporan rencana aksi bulanan dan triwulan";
		$this->load->view('index', $data);
	}


	public function preview_member($member_id, $format, $periode, $report_for, $report_type, $excel='')
	{
		// Report Anggota //
		$data = array();
		$this->db->from('t_transaction');
		$this->db->join('m_saving_type', 'm_saving_type.st_id=t_transaction.st_id', 'left');
		$this->db->where('member_id', $member_id);
		if($format == 'day'){
			$this->db->where('tr_date', Tanggal::sqlDate($periode));
			$periode_cs = Tanggal::formatDateForm($periode);
		}elseif ($format == 'month') {
			$this->db->where('MONTH(tr_date)', $periode);
			$periode_cs = Tanggal::getBulan($periode);
		}elseif ($format == 'custom') {
			$exp = explode('_', $periode);
			$this->db->where("tr_date BETWEEN '".Tanggal::sqlDateMdy($exp[0])."' AND '".Tanggal::sqlDateMdy($exp[1])."'");
			$periode_cs = Tanggal::formatDateMdy($exp[0]).' s/d '.Tanggal::formatDateMdy($exp[1]);
		}
		$data['result'] = $this->db->get()->result();
		//print_r($this->db->last_query());die;
		$data['periode'] = $periode_cs;
		$data['member'] = $this->db->get_where('v_member', array('member_id'=>$member_id))->row();
		$data['params'] = array('member_id'=> $member_id, 'format' => $format, 'periode' => $periode, 'report' => $report_for, 'type' => $report_type);
		$data['excel'] = ($excel != '')?TRUE:FALSE;


		$this->load->view('report_member', $data);
		
	}

	public function preview_unit($unit_id, $periode, $report_for, $report_type, $excel='')
	{
		// Report Anggota //
		$data = array();
		$exp = explode('_', $periode);
		$periodeunit = Tanggal::formatDateMdy($exp[0]).' s/d '.Tanggal::formatDateMdy($exp[1]);
		// get member by unit //
		$member_unit = $this->db->get_where('v_member', array('unit_id'=>$unit_id))->result();
		$getData = array();
		foreach($member_unit as $row_mu){
			$hasil_rekap = array(
				'pokok' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>1,'periode'=>$exp)),
				'wajib' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>2,'periode'=>$exp)),
				'tgl_12' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>3,'periode'=>$exp)),
				'dmp' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>4,'periode'=>$exp)),
				'perumahan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>5,'periode'=>$exp)),
				'penebusan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>6,'periode'=>$exp)),
				'sukarela' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>7,'periode'=>$exp)),
				'pembangunan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>8,'periode'=>$exp)),
				'titipan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>9,'periode'=>$exp)),
			);
			$row_mu->hasil_rekap = $hasil_rekap;
			$getData[] = $row_mu;
		}
		/*echo '<pre>';
		print_r($getData);die;
*/
		$data['result'] = $getData;
		$data['periode'] = $periodeunit;
		$data['unit'] = $this->db->get_where('m_unit', array('unit_id'=>$unit_id))->row();
		$data['params'] = array('unit' => $unit_id, 'periode' => $periode, 'report' => $report_for, 'type' => $report_type);
		$data['excel'] = ($excel != '')?TRUE:FALSE;

		$this->load->view('report_unit', $data);
		
	}

	public function preview_koperasi($periode, $report_for, $report_type, $excel='')
	{
		// Report Anggota //
		$data = array();
		$exp = explode('_', $periode);
		$periodekoperasi = Tanggal::formatDateMdy($exp[0]).' s/d '.Tanggal::formatDateMdy($exp[1]);
		// get member by koperasi //
		$member_koperasi = $this->db->get_where('v_member')->result();
		$getData = array();
		foreach($member_koperasi as $row_mu){
			$hasil_rekap = array(
				'pokok' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>1,'periode'=>$exp)),
				'wajib' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>2,'periode'=>$exp)),
				'tgl_12' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>3,'periode'=>$exp)),
				'dmp' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>4,'periode'=>$exp)),
				'perumahan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>5,'periode'=>$exp)),
				'penebusan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>6,'periode'=>$exp)),
				'sukarela' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>7,'periode'=>$exp)),
				'pembangunan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>8,'periode'=>$exp)),
				'titipan' => $this->transaction->get_total_saving_rekap(array('member_id'=>$row_mu->member_id, 'st_id'=>9,'periode'=>$exp)),
			);
			$row_mu->hasil_rekap = $hasil_rekap;
			$getData[] = $row_mu;
		}
		/*echo '<pre>';
		print_r($getData);die;
*/
		$data['result'] = $getData;
		$data['periode'] = $periodekoperasi;
		$data['params'] = array('periode' => $periode, 'report' => $report_for, 'type' => $report_type);
		$data['excel'] = ($excel != '')?TRUE:FALSE;

		$this->load->view('report_koperasi', $data);
		
	}

	public function findData(){

		$keyword = $this->input->post('keyword');

		$nik = $this->report->findData($keyword); //print_r($nik);die;

		if($nik['count'] > 0){
			echo $this->report->getDataFromNik(count($nik['result']), $nik['result']);
		}else{
			echo '<span style="color:red">-Tidak ada data ditemukan-</span>';
		}

	}

	public function findDetilFromId(){

		$nia = $this->input->post('nia');
		$result = $this->report->findDetilFromId($nia); //print_r($this->db->last_query());die;
		
		echo json_encode($result);
	}



}
