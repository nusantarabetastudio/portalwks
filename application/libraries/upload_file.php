<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

final Class upload_file {

    function doUpload($params, $inputname, $path)
    {
        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);

        $vdir_upload = ''.$path.'';
        $vfile_upload = $vdir_upload . $params['attc_path'];
        $tipe_file   = $_FILES['file']['type'];

        if(move_uploaded_file($_FILES[$inputname]["tmp_name"], $vfile_upload)){
            $db->insert('t_attachment', $params);
        }

        return true;
    } 

    function getUploadedFile($params){

        $CI =&get_instance();
        $db = $CI->load->database('default', TRUE);
        $html = '';
        $files = $db->order_by('attc_id', 'ASC')->get_where('t_attachment', $params)->result();
        foreach ($files as $key => $value) {
            $html .= '<a href="#" title="Delete file" onclick="delete_file_lampiran('.$value->attc_id.')"><i class="fa fa-times red"></i></a> &nbsp; <a href="'.base_url().'uploaded_files/file/'.$value->attc_path.'">'.$value->attc_name.'</a> (<i>'.$value->attc_owner.'</i>)<br>';
        }
        return $html;

    }
	
}

?>
